<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';
session_start();
ReportLogInSessionCheck();

$conn = Connect();
$pmiconnect = pmimain_connect();

$ProdID = $_GET['ProdID'];
$CustID = $_GET['CustID'];
$ReciptID = $_GET['ReciptID'];

$Order = "SELECT * FROM orderplaced WHERE ProdID = '".$ProdID."' AND CustomerID = '".$CustID."' AND ReceiptID = '".$ReciptID."'";
$Cust = "SELECT * FROM customers WHERE ID = '".$CustID."'";
$TransNote = "SELECT * FROM orderplacednotes WHERE ProdID = '".$TransID."' AND CustID = '".$CustID."' AND ReceiptID = '".$ReciptID."'";
$Prod = "SELECT * FROM AF_SP_Info WHERE AF = '".$ProdID."'";

// Gather Customer Info
$result = $conn->query($Cust);

$CustomerTable = $result->fetch_assoc();

foreach ($CustomerTable as $key => $value){
    }

//Gather Class Info
$result = $pmiconnect->query($Prod);

$ProdTable = $result->fetch_assoc();

foreach ($ProdTable as $key => $value){
    }
function CardType($Cardtype){                
    switch ($Cardtype){
        case '001':
        echo "Visa";
        break;
        
        case '002':
        echo "Mastercard";
        break;
        
        case '003':
        echo "American Express";
        break;
    }
}
    
// Gather Order Info
function OrderTable($Order){
    $conn = Connect();
    $result = $conn->query($Order);
    if($result->num_rows>0){
        ?>
        <table>
        	<tr>
        		<td>Catalog ID</td>
        		<td>Prod Name</td>
        		<td>Amount</td>
        		<td>Post Date</td>
        		<td>Card Type</td>
        		<td>Card Number</td>
        	</tr>
        <?

        while($OrderTable = $result->fetch_assoc()){ 
        ?>
        <?php if($_SESSION['Role'] == "CS" or $_SESSION['Role'] == "Manager"){ ?>
            <tr>
            	<td><?php echo $OrderTable['CataID']?></td>
                <td><?php echo $OrderTable['PName']?></td>
                <td><?php echo $OrderTable['Price']?></td>
                <td><?php echo $OrderTable['RecDate']?></td>
                <td><?php CardType($OrderTable['CardType'])?></td>
                <td><?php echo $OrderTable['CardNumber']?></td>
            </tr>
            <?php }
            else if($_SESSION['Role'] == "Accounting" or $_SESSION['Role'] == "Master"){?>
            <tr>
            	<td><?php echo $OrderTable['CataID']?></td>
                <td><?php echo $OrderTable['PName']?></td>
                <td><form action=""><input class="form-control" id="Amount<?php echo $OrderTable['ID']?>"name="Amount" type="text" value="<?php echo $OrderTable['Price']?>"></form></td>
                <td><form action=""><input class="form-control" id="PostDate<?php echo $OrderTable['ID']?>" name="PostDate" type="text" value="<?php echo $OrderTable['RecDate']?>">
                <script>$("#PostDate<?php echo $OrderTable['ID']?>" ).datepicker({ dateFormat: 'yy-mm-dd' });</script></form></td>
                <td><?php CardType($OrderTable['CardType'])?></td>
                <td><?php echo $OrderTable['CardNumber']?></td>
                <td><button id="<?php echo $OrderTable['ID']?>" class="Edit btn btn-success">Update</button></td>
                <td><button id="<?php echo $OrderTable['ID']?>" class="Remove btn btn-danger">Remove</button></td> 
            </tr>
            <?php }?>
        <?php
        }
        
    }
    echo "</table>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Online Training Report</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script> 
       <style>
       body{
           color:black;}
           </style>
</head>

<body>
<!-- Nav bar -->
<?php include "includes/nav.php";?><!-- Main Content -->
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-4">
			<h3>Customer Info</h3>
			<hr>
			<p><?php echo $CustomerTable['Fname'] . " " .$CustomerTable['Lname']?></p>
			<p><?php echo $CustomerTable['Address'];?></p>
			<p><?php echo $CustomerTable['City'] . ' ' . $CustomerTable['State'] . ",". $CustomerTable['Zip']?></p>
			<p><?php echo $CustomerTable['Phone']?></p>
			<p><?php echo $CustomerTable['Email'];?></p>
		</div>
		<div class="col-sm-4">
			<h3>Class Info</h3>
			<hr>
			<table>
				<tr>
					<td>Catalog ID:</td>
					<td><?php echo $ProdTable['CataID']?></td>
				</tr>
				<tr>
					<td>Product ID:</td>
					<td><?php echo $ProdTable['AF']?></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><?php echo $ProdTable['Name']?></td>
				</tr>
				<tr>
					<td>Price:</td>
					<td>$<?php echo number_format($ProdTable['Price'], 2)?></td>
				</tr>
			</table>
		</div>
		<div class="col-sm-4">
			<h3>Add Transaction</h3>
			<hr>
			<form action="../script/addproduct.php" class="AddProdForm">
				<div class="form-group">
					<input class="form-control price" name="Price" placeholder="Amount" type="text">
				</div>
				<div class="form-group">
					<button id="AddTransaction" class="btn btn-success" type="submit">Add Transaction</button>
				</div>
			</form>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-8">
            <div id="Transactions">
                <h3>Transactions</h3>
                <hr>
                <?php OrderTable($Order)?>
            </div>
        </div>
        <!-- Transactions Notes Notes -->
		<div class="col-md-4">
			<div class="row">
				<h3>Transaction Notes</h3>
				<hr>
				<div style="min-height: 200px; max-height:200px; overflow-y:scroll; padding:10px;">
                <?php 
                    $select = "SELECT * FROM orderplacednotes WHERE ReciptID = '".$ReciptID."' ORDER BY Date DESC";
                    $result = $conn->query($select);
                        while($row = $result->fetch_assoc()){
                            ?>
                            <p><?php echo $row["Note"]?></p>
                            <p>-<?php echo $row['User']?><br>
                            <?php echo date('m-d-y g:i a', strtotime($row['Date']))?></p>
                            <hr>
                            <?
                        }
                ?>
				</div>
			</div>
			<div class="row">
				<form action="script/transactionnotes.php" id="NoteForm" name="NoteForm">
					<div class="form-group">
						<br>
						<label for="">Enter Notes Below</label> 
						<textarea class="form-control" id="" name="Note" rows="5"></textarea>
					</div>
					<div class="form-group">
						<input class="form-control User" name="User" value="<?php echo $_SESSION['LoginName']?>" type="hidden">
					</div>
				</form>
                <button class="btn btn-primary" id="TransactionNoteButton">Add Note</button>
			</div>
		</div>
	</div>
    
    
    <!-- Modal -->
    <div id="AreYouSureModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Remove Transaction</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to remove this transaction?</p>
            <p>Transaction will be deleted forever upon clicking yes.</p>
            <button id="yes" class="btn btn-danger">Yes</button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
<!-- SCRIPTS -->
    <script>
    $("button[class^=Edit]").click(function(e) {
        OrderID = this.id;
        Amount = $('#Amount'+OrderID).val();
        PostDate = $('#PostDate'+OrderID).val();
        
        // Below need to ajax to an edit function and reload page when a sucess
        // prevent page from leaving to desination
        destination = "script/updatetransactions.php";
        //jason format (Delievered to server)
        var JsonObj = {
                "OrderID":""+OrderID+"",
                "Ammount":""+Amount+"",
                "Postdate":""+PostDate+""
            }
        // querystring json & set desination for data   
        var values = $.param(JsonObj);
		e.preventDefault();	
		$.ajax({
			  url: destination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                  location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
    });
    
    $("button[class^=Remove]").click(function(e) {
        OrderID = this.id;
        $('#AreYouSureModal').modal('show');
        
        $('#yes').click(function(){  
            

            // Below need to ajax to an edit function and reload page when a sucess
            // prevent page from leaving to desination
            var destination = "script/removetransactions.php";
            //jason format (Delievered to server)
            var JsonObj = {
                    "OrderID":""+OrderID+"",
                    "ProdID":"<?php echo $ProdID?>",
                    "CustID":"<?php echo $CustID?>"
                }
            // querystring json & set desination for data   
            var values = $.param(JsonObj);
            console.log(values);
            e.preventDefault();	
            $.ajax({
                  url: destination,
                  type: 'post',
                  data: values, 
                  success: function(data){
                      window.location ='<? echo baseurl()?>/report/portal/index.php?ID=<?php echo $CustID?>';
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
            });
        })
   });
    
    $("#AddTransaction").click(function(e) {
        CustID = '<?php echo $CustID?>';
        ProdID = '<?php echo  $ProdID ?>';
        Amount = $('input[name=Price]').val();
        ReceiptID = '<?php echo $ReciptID?>';
        
        // Below need to ajax to an edit function and reload page when a sucess
        // prevent page from leaving to desination
        destination = "Script/AddTransaction.php";
        //jason format (Delievered to server)
        var JsonObj = {
                "CustID":""+CustID+"",
                "ProdID":""+ProdID+"",
                "Amount":""+Amount+"",
                "ReceiptID":""+ReceiptID+""
            }
        // querystring json & set desination for data   
        var values = $.param(JsonObj);
        console.log(values);
		e.preventDefault();	
		$.ajax({
			  url: destination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                  location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
    });  
    
    $('#TransactionNoteButton').click(function(e){
      CustID = '<?php echo $CustID?>';
      ProdID = '<?php echo $ProdID?>';
      ReceiptID = '<?php echo $ReciptID?>';
      Note = $('textarea[name=Note]').val();
      User = $('input[name=User]').val();
      var JsonObj = {
            "CustID":""+CustID+"",
            "ProdID":""+ProdID+"",
            "ReceiptID":""+ReceiptID+"", 
            "Note":""+Note+"",
            "User":""+User+""
        }
    // querystring json & set desination for data   
    var values = $.param(JsonObj);      
        
	// get value of action attribute   
	var desination = $('#NoteForm').prop('action');   	
	// get current location url			
	
	// prevent page from leaving to desination
	e.preventDefault();	
    $.ajax({
          url: desination,
          type: 'post',
          data: values, 
          success: function(data){
              console.log(data);
              location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
    });

});
    </script>
</div>
</body>