<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';
session_start();
ReportLogInSessionCheck();
$find = trim($_GET['find']);
?>
<?php 
function ReportShipping($custID, $find){
	$conn = Connect();
	$selctShippingRepot = "SELECT * FROM shippinginfo WHERE OrderID = '".$find."' AND CustomerID = '".$custID."'";
	$result = $conn->query($selctShippingRepot);
	if($result->num_rows > 0){
		while($row = $result->fetch_assoc()){
			foreach($row as $ShipName => $Shipvalue) {
						$ShippingArray[$ShipName] = $Shipvalue;
			}
			if($ShippingArray['Fname'] = ""){
				echo "NULL";
			}
			else{
			echo $ShippingArray['Fname'] . " " . $ShippingArray['Lname'] . "<br>" ;
			echo $ShippingArray['Address']."<br>" ;
			echo $ShippingArray['City'].", ". $ShippingArray['State']. " ". $ShippingArray['Zip']. "<br>" ;
			echo $ShippingArray['Phone']."<br>" ;
			}
		}
	}
	else{
		echo "NULL";
	}
	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<!-- Nav bar -->
<?php include "includes/nav.php";?>

<!-- Preimitive search function for customer -->
<div class="container-fluid">
	<div class="row">
      <?php 
        if($find !=""){
            ?>
            <div class="col-lg-12" style="margin: 15px 0px">
               <a href="Script/roster_export.php?CataID=<?php echo $find?>"><button class="btn btn-primary">Export to CSV</button></a>
            </div>
            <?php
        }
        else{}
        ?>
       
    	<div class="col-lg-12">
           	<table width="100%">
        	<tr style="border-bottom:1px solid #e6e6e6">
        		<td valign="top">First Name</td>
        		<td>Last Name</td>
        		<td>Address</td>
        		<td>City</td>
        		<td>State</td>
        		<td>Zip</td>
        		<td width="250px">Email</td>
        		<td>Phone</td>
        		<td>Practice</td>
                <td>Manager</td>
                <td>Shipping Info</td>
                <td>Date Registered</td>
                <td>Receipt</td>
                <td></td>
        	</tr>
        <?php
		$conn = Connect();
		$selectOrderID = "SELECT * FROM orderplaced WHERE CataID LIKE '%".$find."%' OR ReceiptID = '".$find."' ORDER BY DateOrdered DESC";
		$resultOrderID = $conn->query($selectOrderID);
		// gather info from receipt number (OrderID)
		while($rowOrderID = $resultOrderID->fetch_assoc()){
			// assign to var
			$CustomerID = $rowOrderID['CustomerID'];
			
			// use CustomerID to get customer from Customers table. 
			$selectCustomer = "SELECT * FROM customers WHERE ID = '".$CustomerID."'";
			$resultCustomer = $conn->query($selectCustomer);
			
			// Fetch Info from DB where customerID = ID
			// and return results.
			while($rowCustomer = $resultCustomer->fetch_assoc()){
				foreach($rowCustomer as $Customer => $value) {
					$CustomerArray[$Customer] = $value;
				}
			?>
            <tr>
        		<td><?php echo $CustomerArray['Fname'];?></td>
        		<td><?php echo $CustomerArray['Lname'];?></td>
        		<td><?php echo $CustomerArray['Address'];?></td>
        		<td><?php echo $CustomerArray['City'];?></td>
                <td><?php echo $CustomerArray['State'];?></td>
                <td><?php echo $CustomerArray['Zip'];?></td>
                <td><?php echo $CustomerArray['Email'];?></td>
                <td><?php echo $CustomerArray['Phone'];?></td>
                <td><?php echo $CustomerArray['Practice'];?></td>
                <td><?php echo $CustomerArray['Manager'];?></td>
                <td><?php ReportShipping($CustomerID, $find)?></td>
                <td><?php echo date('m/d/y', strtotime($rowOrderID['DateOrdered']));?></td>
                <td><?php 
				if(!empty($find)){
				foreach (glob(baseurl()."receipt/sp/*".$CustomerArray['ReceiptID']."*.html") as $filename) {
				echo "<a target='_blank' href='".$filename."'>".$find."</a><br>";
				}}?></td>
                <td></td>
                <td><a href="<?php echo baseurl()?>report/portal/index.php?ID=<?php echo $CustomerID?>"><button class="btn btn-primary">Customer Info</button></a></td>
        	</tr>
            <tr>
            <?php
			}
		}
		?>
        </tr>
        </table>

        </div>
    </div>
</div>

