<?php 
//connect to server
function base_url(){
    echo baseurl();
}
if($_SERVER["HTTPS"] != "on"){
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

function ReportLogInSessionCheck(){
    
    if($_SESSION['Login'] ==1 ){
        // Do nothing
    }
    else{
        //Redirect to login page. 
          header('Location: https://www.pmimd.com/onlinetraining/report/login.php'); 
    }
}

// Returns random number and letter for passwordrest key. 
function PasswordResetKey(){
	$count = 0;
	while($count<8){
    $int = rand(0,36);
    $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $rand_letter .= $a_z[$int];
	$count++;
	}
	return $rand_letter;
}

function class_listing(){
    $conn = pmimain_connect();
    $select = "SELECT * FROM AF_SP_Info ORDER BY AF";
    $result = $conn->query($select);
    while($row = $result->fetch_assoc()){
        ?>
        <option value="<?php echo $row['AF']?>"><?php echo $row['AF'] . ": ". $row['Name']?>
        <?php
    }
}

function OrderID(){
    $orderid = GetOrderID();
    $conn = Connect();
    $newid = 0;
    while($newid == 0){
        $select = "SELECT ReceiptID FROM orderplaced WHERE ReceiptID = '".$orderid."'";
        $result = $conn->query($select);
    
        if($result->num_rows >0){
            $orderid = GetOrderID();
        }
        else{
            $newid = 1;
            return $orderid;
        }
        
    }
}

function GetOrderID(){
    //update Need to check to make sure order id isnt used before. 
    
	$count = 0;
	while($count<8){
    $int = rand(0,36);
    $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $rand_letter .= $a_z[$int];
	$count++;
	}
	return $rand_letter;
}

function DateandTime(){
    $DateTimeArray = array(date("Y-m-d"), date(G)-5);
    return $DateTimeArray;
}
//function that figures out he predetermined rec/post date for accounting. 

function ReconcileDate($Array){
                $day = $Array[0];
                $DayOfWeek = date('w', strtotime($Array[0]));
                date(G)-5;
                
                if($DayOfWeek == "5" && $Array[1] < 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+3 days"));
                }
                elseif($DayOfWeek == "5" && $Array[1] > 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+4 days"));
                }
                elseif($DayOfWeek == "6"){
                    $RecDate = date('Y-m-d',strtotime($day ."+3 days"));
                }
                elseif($DayOfWeek == "0"){
                    $RecDate = date('Y-m-d',strtotime($day ."+2 days"));
                }
                elseif($Array[1] > 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+2 days"));
                }
                else{
                    $RecDate = date('Y-m-d',strtotime($day ."+1 days"));
                }
                return $RecDate;
            }

//Cert Discount, checks upon log-in and saved as session cookie. 
function GradCheck($CustInfo){
    $pmiconnect = pmimain_connect();
    $GradSelect = "SELECT GradID FROM Current_Grad_Check 
            WHERE GradFirstName = '".$CustInfo['Fname']."' 
            AND GradLastName = '".$CustInfo['Lname']."' 
            AND GradID = '".$CustInfo['Cert']."' GROUP BY GradID";
    $result = $pmiconnect->query($GradSelect);
    if($result->num_rows >0){
    return 1;
    }
    else{
    return 0;
    }
}

function VideoCompletePrecent($CustID){
    
    function CompletionCheck($CompletePrecent){
        if($CompletePrecent == '100%'){
            echo "<span style='color:green;'<i class='glyphicon glyphicon-ok'></i></span>";
        }
        else{
        }
        
    }
    $conn = Connect();
    // Below get course ID from Videotracking and also gets the count of completed videos to create % on completion. 
    $orderplaced = "SELECT ProdID, CataID FROM `orderplaced` WHERE CustomerID = '".$CustID."' GROUP BY ProdID ORDER BY DateOrdered DESC";
    
    $result = $conn->query($orderplaced);
    
    if($result->num_rows>0){
    ?>
    <tr>
        <td>
            <a target="_blank" href="../script/progress_report.php?custID=<?php echo $CustID?>">Progress Report</a>
        </td>
    </tr>
    <?php
        while($Row = $result->fetch_assoc()){
            
            $ProdID = $Row['ProdID'];
            $CataID = $Row['CataID'];
            
            $TotalVidCount = "SELECT COUNT(*) AS Count FROM videos WHERE ClassID = '".$ProdID."'";
            $TotalVidCount = $conn->query($TotalVidCount);
            $TotalVidCount = $TotalVidCount->fetch_assoc();
            // Get total Count of videos
            $TotalVidCount = $TotalVidCount['Count'];
            
            $VideoTrackingCount = "SELECT COUNT(*) as Count FROM videotracking WHERE CourseID = '".$ProdID."' AND CustomerID = '".$CustID."' AND Completed = 1";
            $VideoTrackingCount = $conn->query($VideoTrackingCount);
            $VideoTrackingCount = $VideoTrackingCount->fetch_assoc();
            $VideoTrackingCount = $VideoTrackingCount['Count'];
            
            // Set Value if none is set to zero.
            if($VideoTrackingCount == ""){
                $VideoTrackingCount = 0;
            }
            if($VideoTrackingCount == 0){
                $CompletedPrecent = 0;
            }
            else{
                $CompletedPrecent = $VideoTrackingCount/$TotalVidCount*100;
            }
            
            
            
            ?>
            	<tr>
            		<td><?php echo $CataID?></td>
            		<td><?php echo number_format($CompletedPrecent, 2, '.', '')?>%</td>
                    <td><?php CompletionCheck($CompletedPrecent) ?></td>
            	</tr>
            <?php
        }
    }
    else{
        echo "No Video Recoreds.";
    }
        
}

function displayCartPortal($email){
    $conn = Connect();
    $pmicon = pmimain_connect();
    $select = "SELECT * FROM cart WHERE Email = '".$email."'";
    $result = $conn->query($select);
    
    while($row = $result->fetch_assoc()){
        $prodID = $row['ProdID'];
        
        $selectProdInfo = "SELECT * FROM AF_SP_Info WHERE AF = '".$prodID."'";
        $prodInfo = $pmicon->query($selectProdInfo);
        
        while($rowProd = $prodInfo->fetch_assoc()){
            ?>
            <tr id="<?php echo $rowProd['AF']?>">
                <td><?php echo $rowProd['Name']?></td>
                <td><?php echo number_format($rowProd['Price'], 2)?></td>
                <td><button id="<?php echo $rowProd['AF']?>" class="RemoveClass btn btn-danger">Remove</button></td>
            </tr>
            <?
        }
    }
    
}

class CartSummary{
    
    // get cart info 
    public function cartinfo($email){
        $conn = Connect();
        $select = "SELECT ProdID FROM cart WHERE Email = '".$email."'";
        $result = $conn->query($select);
        return $result->fetch_all();
    }
    
    //gather customer info 
    public function custinfo($id){
        $conn = Connect();
        $select = "SELECT * FROM customers WHERE ID = '".$id."'";
        $result = $conn->query($select);
        return $result->fetch_assoc(); 
    }
    
    // get price of said product
    private function priceTotal($ProdID){
        $conn = pmimain_connect();
        $priceArray = array();
        $select = "SELECT Price FROM AF_SP_Info WHERE AF = '".$ProdID."'";
        $result = $conn->query($select);
        return $result->fetch_assoc();
    }
    
    //find item count. 
    private function itemcount($email){
        $conn = Connect();
        $select = "SELECT COUNT(*) AS Count FROM cart WHERE Email = '".$email."'";
        $result = $conn->query($select);
        $count = $result->fetch_assoc();
        return $count['Count'];
    }
    
    //check for discount (if they are currently certified)
    private function discount($fname, $lname, $certID){
            $pmiconnect = pmimain_connect();
            $GradSelect = "SELECT GradID FROM Current_Grad_Check 
                    WHERE GradFirstName = '".$fname."' 
                    AND GradLastName = '".$lname."' 
                    AND GradID = '".$certID."' GROUP BY GradID";
            $result = $pmiconnect->query($GradSelect);
            if($result->num_rows >0){
                return 1;
            }
            else{
                return 0;
            }
    }
    
    public function CartSummaryDisplay($cart_data, $cart_summary){
        $conn = pmimain_connect();
        $EmailPackage .= "<table border='0' cellpadding='5' cellspacing='0' width='600' id='emailContainer'>";
        for($x = 0; $x < sizeof($cart_data); $x++){
            $select = "SELECT * FROM AF_SP_Info WHERE AF = '".$cart_data[$x][0]."'";
            $result = $conn->query($select);

            while($data = $result->fetch_assoc()){
                $EmailPackage .= "<tr><td class='mcnTextContent' align='left' valign='top'>".$data['Name']."</td><td class='mcnTextContent' align='center' valign='top'>$".number_format($data["Price"],2)."</td></tr>";
            }
        }
        $EmailPackage .= "<tr><td style='padding:15px;'></td><td></td></tr>";
        $EmailPackage .= "<tr><td class='mcnTextContent' align='left' valign='top'><h3>Total</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".number_format($cart_summary['Price'], 2)."</h3></td></tr>";
        $EmailPackage .="</table>";
        
        return $EmailPackage;
    }
    
    //main function that produces data needed for display. 
    public function main($id){
        $customerArray = $this->custinfo($id);
        $itemCount = $this->itemcount($customerArray['Email']);
        $cartData = $this->cartinfo($customerArray['Email']);
        $discount = $this->discount($customerArray['Fname'], $customerArray['Lname'], $customerArray['Cert']);
        $AssocPrice = array();

        foreach($cartData as $cartData){
            array_push($AssocPrice, $this->priceTotal($cartData[0]));
        }
        for($x=0; $x<sizeof($AssocPrice); $x++){
                $price = $price + $AssocPrice[$x]['Price'];
        }
        if($discount == 1){
            $discountprice = number_format($price * .1,2);
            $price = number_format($price *.9, 2);
        }
        if(isset($_SESSION['CartPrice'])){
            $price = $_SESSION['CartPrice'];
        }
        
        return array("Discount" =>$discount, "Price"=>$price, "ItemCount"=>$itemCount);
    }
}

//  Google Analytics Ecommerce Script
function Google_Ecommerce_Back_Office($orderData){
    ?>
    <script>
        ga('require', 'ecommerce');
    </script>
    <?php
    $conn = Connect();
    $CartSelect = "SELECT * FROM cart WHERE Email = '".$orderData['req_bill_to_email']."'";
    $Result = $conn->query($CartSelect);
    
    while($Cart = $Result->fetch_assoc()){
        $SPSelect = "SELECT * FROM selfpaced WHERE ProdID = '".$Cart['ProdID']."'";
        $SPResult = $conn->query($SPSelect);
        
        while($SPItem = $SPResult->fetch_assoc()){
            ?>
            <script>
                ga('ecommerce:addItem', {
                  'id': '<?php echo $orderData['req_reference_number']?>',                                                // Transaction ID. Required.
                  'name': '<?php echo $SPItem['Name']?>',                                       // Product name. Required.
                  'price': '<?php echo $orderData['req_amount']?>',                 // Unit price.
                });
            </script>
            <?           
        }
    }    
    ?>
        <script>
        // Below is used for final ammount (your total revenue)
        ga('ecommerce:addTransaction', {
          'id': '<?php echo $OrderID?>',                     // Transaction ID. Required.
          'affiliation': 'PMI Online Order',   // Affiliation or store name.
          'revenue': '<?php echo $TotalAmount?>',               // Grand Total.
        });
        ga('ecommerce:send');
        </script>
    <?php 
}

function OrderPlaced($orderData, $id){
    // what if customer orders one product at a discount? Backend orders?
    
    //Get customer info first. User Obj and find with session var with ID
    $recDate = ReconcileDate(DateAndTime);
    $newobject = new CartSummary;
    $custData = $newobject->custinfo($id);
    //get all items in the cart with said customer. 
    $conn = Connect();
    $pmiconnect = pmimain_connect();
    $cart_select = "SELECT ProdID FROM cart WHERE Email = '".$custData['Email']."'";
    $resut = $conn->query($cart_select);
        $cart_data = $resut->fetch_all();
        //get the data from cart and apply to class info database. 
        foreach($cart_data as $key => $value){
            echo $selectProd = "SELECT * FROM AF_SP_Info WHERE AF = '".$value[0]."'";
            $result = $pmiconnect->query($selectProd);

            //take class info from database and unsert into orderplaced table. 
            while($row = $result->fetch_assoc()){
                $insert = "INSERT orderplaced (ProdID, CustomerID, Email, Price, CataID, PName, ReceiptID, RecDate, CardType, CardNumber) 
                    VALUES ('".$row['AF']."','".$id."','".$custData['Email']."','".$row['Price']."','".$row['CataID']."','".$row['Name']."','".$orderData['req_reference_number']."','".$recDate."', '".$orderData['req_card_type']."', '".$orderData['req_card_number']."')";
                
                if ($conn->query($insert) === TRUE) {
                    echo "New records created successfully";
                } else {
                    echo "Error: " . $insert . "<br>" . $conn->error;
                }
            }
        }
}

//Last function to EXE... clean the cart of users data. 
function AddProdToProtal($custID){
	$conn = Connect();
	$select = "SELECT * FROM cart WHERE CookieID = '".$custID."'";
	
	$result = $conn->query($select);
	
	if($result->num_rows > 0){
		while($rows = $result->fetch_assoc()){
			$prodID = $rows['ProdID'];
			$email = $_SESSION['email'];	
			
			$insert = "INSERT custportal (custID, prodID) 
				VALUES ('".$_SESSION['Profile']['ID']."','".$prodID."')";
			
            if ($conn->query($insert) === TRUE) {
            } else {
                echo "There was an error entering product into your portal, please contact support@pmimd.com";
            }		
		}
	}
    
	// Expires Cookie
	ExpireCookie();
}

function ExpireSession(){
    unset($_SESSION['CartPrice']);
}

function ProdEmailDisplay($id){
    $cust_obj = new CartSummary;
    $cust_data = $cust_obj->custinfo($id);
    $cart_data = $cust_obj->main($id);
}
?>
