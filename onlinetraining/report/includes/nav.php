<head>
<style>
.dropdown:hover .dropdown-menu {
	display: block;
}
    .navbar{
        background-color: #ffffff;
    }
</style>
</head>
<nav class="navbar" style="border-bottom:1px solid #E6E6E6;">
            <div class="container-fluid">
                <div class="row">
                    <div class="navbar-header">
                        <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button"><span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span></button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <?php 
                               if($_SESSION['Role'] == "Manager" || $_SESSION['Role'] == "Master" || $_SESSION['Role'] == "Accounting"){ ?>
                            <li>
                                <a href="<?php echo baseurl()?>report/index.php">Dashboard</a>
                            </li>
                            <?php }?>
                            <li>
                                <a href="<?php echo baseurl()?>report/ordersearch.php">Order Search</a>
                            </li>
                            <li>
                                <a href="<?php echo baseurl()?>report/customersearch.php">Customer Search</a>
                            </li>
                            <?php 
                               if($_SESSION['Role'] == "Master" || $_SESSION['Role'] == "Accounting"){ ?>
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="<?php echo baseurl()?>report/accounting.php">Accounting<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo baseurl()?>report/third_party/invoice.php"">Third-Part Reports</a>
                                    </li>
                                </ul>
                            </li>
                            <?php }?>
                            <li>
                                <a href="<?php echo baseurl()?>report/accountcreation.php">Account Creation</a>
                            </li>
                        </ul>
                <div class="nav navbar-nav navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="hidden-md">
                            <form action="<?php echo baseurl()?>report/ordersearch.php" class="navbar-form" method="get" role="search" class="col-lg-12 col-md-12">
                                <div class="input-group">
                                    <input class="form-control" list="type" name="find" placeholder="Search" type="text"> 
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>