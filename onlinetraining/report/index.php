<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';
session_start();
ReportLogInSessionCheck();
if($_SESSION['Role'] != "Master" xor $_SESSION['Role'] != "Accounting" xor $_SESSION['Role'] != "Manager"){
    header('Location: '.baseurl().'report/customersearch.php');
}

$conn = Connect();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
    
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
.dashboard_link{
    font-size: 15px!important;
}
</style>
</head>
<body>
<!-- Nav bar -->
<?php include "includes/nav.php";?>

<div class="container-fluid">
	<div class="row">
    	<!-- Cart Detials -->
 
       <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Currently In cart</h2>
            <hr>
            <table width="100%">
            <!-- code to find how many is in cart -->
			<?php 
            
            $pminet = pmimain_connect();
            
            $select = "SELECT AF, Name,COUNT(*) AS Count FROM cart_report GROUP BY Name ORDER BY COUNT(*) DESC";
            
            $result = $pminet->query($select);
                while($data = $result->fetch_assoc()){
                    ?>
                    <tr>
                        <td><?php echo $data["Name"]?></td>
                        <td><?php echo $data["Count"]?></td>
                    </tr>
                    <?php 
                    
                }
                
                ?>
            </table>
        </div>
        <!-- Online Training Overview -->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
        <h2> Overview of Online Training</h2>
        <hr>
        <table>
           	<tr>
            	<td>Total Sales</td>
            	<td>$
					<?
					$totalMoney = "SELECT SUM(Price) AS Total FROM orderplaced WHERE YEAR(DateOrdered) = YEAR(CURRENT_DATE)";
					$result = $conn->query($totalMoney);
					$totalMoney1 = $result->fetch_assoc();
					print_r($totalMoney1['Total']);
					?>
				</td>
            </tr>
            <td><a class="dashboard_link" href="<?php echo baseurl()?>report/detailed_reports/email_tracking.php">Email Conversions<br>(1 Month Span)</a></td>
            <td><?php
            // Query Set Up
			$DailyActivity = "SELECT SUM(Amount) AS Amount FROM mcecommerce WHERE Date > DATE_ADD(now(), INTERVAL -31 DAY)";
            $DailyActivity = $conn->query($DailyActivity);
            $DailyActivity = $DailyActivity->fetch_assoc();
            echo '$'.$DailyActivity["Amount"];
            
			?></td></tr>
        <tr>
            <td>Accounts:</td> 
			<td><?php 
            $select = "SELECT COUNT(Fname) AS Count FROM customers";
            $result = $conn->query($select);
			while($AccountsCount = $result->fetch_assoc()){
			echo $AccCount = $AccountsCount["Count"];
			}?></td></tr>
            
            <tr>
            <td>Orders Placed:</td>
            <td><?php 
			// Excludes all pmimd.com email address for they are not actual sales. 
            $select = "SELECT COUNT(ID) AS Count FROM orderplaced WHERE Email NOT LIKE'%pmimd.com%' AND Price > 0";
            $result = $conn->query($select);
			while($OrdersPlaced = $result->fetch_assoc()){
			echo $OrPL = $OrdersPlaced["Count"];
			}?></td></tr>
            
            <tr><td>Active Classes: </td>
			<td><?php 
            $select = "SELECT COUNT(ID) AS Count FROM custportal";
            $result = $conn->query($select);
			while($ActiveClasses = $result->fetch_assoc()){
			echo $ActClasses = $ActiveClasses["Count"];
			}?></td></tr>
            
            <tr>
            <td>Archived Classes:</td>
            <td><?php 
            $select = "SELECT COUNT(ID) AS Count FROM custarchive";
            $result = $conn->query($select);
			while($ArchivedClasses = $result->fetch_assoc()){
			echo $ArchClasses = $ArchivedClasses["Count"];
			}?></td></tr>
            
            <tr>
            <td>Cart Count:</td>
            <td><?php 
            $select = "SELECT COUNT(ID) AS Count FROM cart";
            $result = $conn->query($select);
			while($CartCount = $result->fetch_assoc()){
			echo $CCount = $CartCount["Count"];
			}?></td></tr>
            
            <tr>
            <td>Orders/Customer Ratio</td>
            <td><?php 
           	$ratio = $OrPL / $AccCount * 100;
			echo $number = number_format($ratio, 2) . '%';
			?></td></tr>
            
            <tr>
            <td>Cart Conversion <br>(7 Day Range)</td>
            <td><?php
			// get count from last 7 days
			$CartSelect = "SELECT COUNT(*) AS CountCart FROM cart WHERE Date > DATE_ADD(now(), INTERVAL -7 DAY)";
			$OrderSelect = "SELECT COUNT(*) AS CountOrder FROM orderplaced WHERE DateOrdered > DATE_ADD(now(), INTERVAL -7 DAY) AND Price > 0 ";
			
			$resultCartSelect = $conn->query($CartSelect);
			$resultOrderSelect = $conn->query($OrderSelect);
			
			while($CartRow = $resultCartSelect->fetch_assoc()){
				$MathCartRow = $CartRow["CountCart"];
			}
			
			while($OrderRow = $resultOrderSelect->fetch_assoc()){
				$MathOrderRow = $OrderRow["CountOrder"];
			}
			$TotalRatio = $MathOrderRow / $MathCartRow * 100;
			
			echo $EchoTotalRatio = number_format($TotalRatio, 2). '%';
			
			?></td></tr>
            
            <td>Course Engagement <br>(Videos)</td>
            <td><?php
            // Query Set Up
			$TotalCount = 'SELECT COUNT(*) AS Count FROM `videotracking`';
            $CompletedCount = 'SELECT COUNT(*) AS Count FROM videotracking WHERE Completed = 1';
            $NullCompleted = 'SELECT COUNT(*) AS Count FROM videotracking WHERE Completed = 0';
            
            // Result set up. 
            $TotalCount = $conn->query($TotalCount);
            $TotalCount = $TotalCount->fetch_assoc();
            $CompletedCount = $conn->query($CompletedCount);
            $CompletedCount = $CompletedCount->fetch_assoc();
            $NullCompleted = $conn->query($NullCompleted);
            $NullCompleted = $NullCompleted->fetch_assoc();
            
            // Do math here.
            $NullRatio =  $NullCompleted['Count'] * 100 /  $TotalCount['Count'];
            echo number_format($CompletedRation = $CompletedCount['Count'] * 100 /  $TotalCount['Count'], 2). '%';
            
			?></td></tr>
            
            <td>Daily Activity <br>(24hr delay)</td>
            <td><?php
            // Query Set Up
			$DailyActivity = "SELECT COUNT(*) AS Count FROM logintracking WHERE DateLog > DATE_ADD(now(), INTERVAL -2 DAY)";
            $DailyActivity = $conn->query($DailyActivity);
            $DailyActivity = $DailyActivity->fetch_assoc();
            echo $DailyActivity["Count"];
            
			?></td></tr>
            
        </table>	
        </div>
        
        <!-- Ordred Products Detials -->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Ordered Training</h2>
            <!-- Find total Moneys from Ordersplaced -->
              <form class="getMomth" method="get" action="script.php">
                <div class="form-group">
                  <select class="form-control" id="MonthSales" name="month">
                  	<option>Select Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                  </select>
                  </div>
              </form>
              <hr> 
            <h3 class="Totaltext">Total: $<span id="Total"></span></h3>
            <!-- code to find how many is in cart -->
            <span id="Products"></span>
        </div> 
        
        <!-- Total Sales-->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Total Sales</h2>
            <hr>
            <!-- code to find how many is in cart -->
            <table>
            <tr>
            	<td>Name</td>
            	<td>Count</td>
            </tr>
			<?php 
				$select = "SELECT COUNT(CataID) AS Count, PName, ProdID
				FROM orderplaced 
                WHERE Price > 0 
                AND YEAR(DateOrdered) = YEAR(CURRENT_DATE)
				GROUP BY ProdID ORDER BY Count DESC";
				$result = $conn->query($select);
				while($rows = $result->fetch_assoc()){
					$Name = $rows["PName"];
					$Count = $rows["Count"];
					?>
                        <tr>
                    		<td width="300"><?php echo $Name?></td>
                    		<td width="100"><?php echo $Count?></td>
                    	</tr>
                    
                    <?php 
				}
            ?>
            </table>
        </div> 
    </div>
    <div class="row">
    <hr>
    
    	<!-- Referral Codes-->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Referral Codes</h2>
            <hr>
            <!-- code to find how many is in cart -->
            <table>
            <tr>
            	<td>Name</td>
            	<td>Code</td>
            	<td>Discount</td>
            </tr>
			<?php 
				$select = "SELECT * FROM referralcodes ORDER BY Name ASC";
				$result = $conn->query($select);
				while($rows = $result->fetch_assoc()){
					$Name = $rows["Name"];
					$Code = $rows["Code"];
					$Discount = $rows["Discount"];
					
					?>

                    	<tr>
                    		<td width="300"><?php echo $Name?></td>
                    		<td width="100"><?php echo $Code?></td>
                            <td width="50"><?php echo $Discount *100?>%</td>
                    	</tr>
                    
                    <?php 
				}
            ?>
            </table>
        </div> 
        
        <!-- Discount Codes Codes-->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Discount Codes</h2>
            <hr>
            <!-- code to find how many is in cart -->
            <table>
            <tr>
            	<td>Name</td>
            	<td>Code</td>
            	<td>Discount</td>
            </tr>
			<?php 
				$select = "SELECT * FROM discountcode ORDER BY Name ASC";
				$result = $conn->query($select);
				while($rows = $result->fetch_assoc()){
					$Name = $rows["Name"];
					$Code = $rows["Code"];
					$Discount = $rows["Discount"];
					
					?>

                    	<tr>
                    		<td width="300"><?php echo $Name?></td>
                    		<td width="100"><?php echo $Code?></td>
                            <td width="50"><?php echo $Discount *100?>%</td>
                    	</tr>
                    
                    <?php 
				}
            ?>
            </table>
        </div>          
    	
        <!-- Commison payout-->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Commision Payout:</h2>
              <form class="getMomth" method="get" action="script/commisionpayout.php">
                <div class="form-group">
                  <select class="form-control" id="sel1" name="month">
                  	<option>Select Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                  </select>
                  </div>
              </form>
              <hr>
            <!-- code to find how many is in cart -->
            <div class="commision">
            </div>
        </div> 
        
        <!-- Discount Used -->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 overflow" style="background:#e6e6e6;">
            <h2>Discount Usage:</h2>
              <form class="getMomth" method="get" action="">
                <div class="form-group">
                  <select class="form-control" id="DiscountUsage" name="month">
                  	<option>Select Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                  </select>
                  </div>
              </form>
              <hr>
            <!-- code to find how many is in cart -->
            <div class="DiscountUsage">
            </div>
        </div>    
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
<script>
function currentMonth(){
	var d = new Date();
    var n = d.getMonth()+1;
	return n;	
}
	$( document ).ready(function() {
		$('select#sel1').val(currentMonth());
		$('select#MonthSales').val(currentMonth());
		$('select#DiscountUsage').val(currentMonth());
		$(".DiscountUsage").load("<?php echo baseurl()?>report/script/discountusage.php?month="+currentMonth());
		$(".commision").load("<?php echo baseurl()?>report/script/commisionpayout.php?month="+currentMonth());
		$("#Products").load("<?php echo baseurl()?>report/script/orderedproducts.php?month="+currentMonth());
		$("#Total").load("<?php echo baseurl()?>report/script/totalmoney.php?month="+currentMonth());
	});
	
	$('select#sel1').change(function(e){
			var value = $("select#sel1").val();
			e.preventDefault();	
			$(".commision").load("<?php echo baseurl()?>report/script/commisionpayout.php?month="+value);
		})
		
	$('select#MonthSales').change(function(e){
			var value = $("select#MonthSales").val();
			e.preventDefault();	
			$("#Products").load("<?php echo baseurl()?>report/script/orderedproducts.php?month="+value);
			$("#Total").load("<?php echo baseurl()?>report/script/totalmoney.php?month="+value);
		})
	
	$('select#DiscountUsage').change(function(e){
			var value = $("select#DiscountUsage").val();
			$(".DiscountUsage").load("<?php echo baseurl()?>report/script/discountusage.php?month="+value);
		})
</script>
</body>
</html>