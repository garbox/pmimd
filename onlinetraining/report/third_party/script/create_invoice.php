<?php include '../../../includes/functions.php';?>
<?php include '../../../includes/config.php';?>
<?php 

$data = $_REQUEST;

$select  = "SELECT 
            customers.Fname AS fname, 
            customers.Lname AS lname, 
            customers.Email AS email, 
            orderplaced.DateOrdered AS orderdate, 
            orderplaced.Price AS price, 
            orderplaced.PName AS prod_name,
            orderplaced.ID as order_ID
            FROM pmimd_prodcenter.customers 
            INNER JOIN 
            orderplaced ON customers.ID = orderplaced.CustomerID 
            WHERE DateAdded BETWEEN '".$data['startDate']."' AND '".$data['endDate']."' AND third_party = 'SA' ORDER BY orderplaced.DateOrdered ASC";

$total  = "SELECT 
            SUM(price) AS total
            FROM pmimd_prodcenter.customers 
            INNER JOIN 
            orderplaced ON customers.ID = orderplaced.CustomerID 
            WHERE DateAdded BETWEEN '".$data['startDate']."' AND '".$data['endDate']."' AND third_party = 'SA'";

$user_count  = "SELECT 
            COUNT(*) AS user_count
            FROM pmimd_prodcenter.customers 
            INNER JOIN 
            orderplaced ON customers.ID = orderplaced.CustomerID 
            WHERE DateAdded BETWEEN '".$data['startDate']."' AND '".$data['endDate']."' AND third_party = 'SA'";

$invoice_size = "SELECT COUNT(*) AS invoice_number from invoice";
$conn = Connect();
$query = $conn->query($select);
$invoice_size = $conn->query($invoice_size);
$invoice_size = $invoice_size->fetch_object();

$invoice_update = "UPDATE orderplaced SET ReceiptID = '".$order_id."' WHERE ID = '".$data->order_id."'";

$total = $conn->query($total);
$total = $total->fetch_assoc();

$user_count = $conn->query($user_count);
$user_count = $user_count->fetch_assoc();

$order_id = GetOrderID();

$invoice_insert = "INSERT INTO invoice 
(total, invoice_number, invoice_date) 
VALUES ('".$total['total']."','".$order_id."','".$data['dueDate']."')";

$conn->query($invoice_insert);                                                                                                                                   
?>

<!doctype html>
<html>
<head>
  <title>Skill Acquire: Create Invoice</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    <style>
        body{
            color:black;
        }
    </style>
</head>

<body style="width: 1000px;">
<div class="container">
    <div class="row">
        <img src="https://www.pmimd.com/images/Invoice-banner_900x150-art.jpg" alt="" >
    </div>
    <div class="row">
        <div class="col-xs-6">
            <table cellpadding="5px" border="1px" width="300px">
                <tr><td><strong>Biller</strong></td></tr>
                <tr><td>Practice Management Institute</td></tr>
                <tr><td>David T. Womack</td></tr>
                <tr><td>8242 Vicar Dr.</td></tr>
                <tr><td>San Antonio, TX 78218</td></tr>
            </table><br>
            <table cellpadding="5px" border="1px" width="300px">
                <tr><td><strong>Bill To:</strong></td></tr>
                <tr><td>Skill Acquire</td></tr>
                <tr><td>Neil Falaharty</td></tr>
                <tr><td>2035 Sunset Lake Raod, Suit B-2</td></tr>
                <tr><td>Newark, DE 19702</td></tr>
            </table>
        </div>
        <div class="col-xs-6">
            <table border="1px" cellpadding="5px" width="150px">
                <tr>
                    <td><strong>Invoice Number</strong></td>
                </tr>
                <tr>
                    <td>1000<?php echo $invoice_size->invoice_number+1?></td>
                </tr>
                <tr>
                    <td><strong>Order Number</strong></td>
                </tr>
                <tr>
                    <td><?php echo $order_id?></td>
                </tr>
                <tr>
                    <td><strong>Invoice Date</strong></td>
                </tr>
                <tr>
                    <td><?php echo date("F j, Y", strtotime($data['dueDate']))?></td>
                </tr>
                <tr>
                    <td><strong>Total Due</strong></td>
                </tr>
                <tr>
                    <td>$<?php echo $total['total']?></td>
                </tr>
            </table>

        </div>
    </div>
</div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <table cellpadding="5px" width="100%" border="1px">
                    <tr>
                        <td colspan="2"><strong>Description</strong></td>
                        <td colspan="2"><strong>Users</strong></td>
                        <td><strong>Total</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2">Online classes</td>
                        <td colspan="2"><?php echo $user_count['user_count']?></td>
                        <td>$<?php echo $total['total']?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <table  cellpadding="5px;" border="1px">
                <tr>
                    <td><strong>Name</strong></td>
                    <td><strong>Email</strong></td>
                    <td><strong>Product</strong></td>
                    <td><strong>Price</strong></td>
                    <td><strong>Date Ordered</strong></td>
                </tr>
                    <?php
                    while($order = $query->fetch_assoc()){
                        $invoice_update = "UPDATE orderplaced SET ReceiptID = '".$order_id."' WHERE ID = '".$order['order_ID']."'";
                        $conn->query($invoice_update);
                        
                    ?>
                    <tr>
                        <td><?php echo $order['fname'] ." ". $order['lname'];?></td>
                        <td><?php echo $order['email']?></td>
                        <td><?php echo $order['prod_name']?></td>
                        <td><?php echo $order['price']?></td>
                        <td><?php echo date("m/j/Y", strtotime($order['orderdate']))?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>

</body>
</html>
