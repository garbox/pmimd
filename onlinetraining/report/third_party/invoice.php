<?php session_start()?>
<?php include '../../includes/functions.php';?>
<?php include '../../includes/config.php';?>
<?php CookieforCart()?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Skill Acquire: Invoice</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
html, body{ 
	overflow-x: hidden;
}
.btn-img{
	width:auto;	
}
.shadow{
	box-shadow: 3px 3px 10px #e6e6e6;	
}
h1{
	color:#5371ad;
}
.blue{
	color:#5371AD;
}
.socialIcons{
    position:relative;
}   

a:hover{
    cursor: pointer;
}
</style>

</head>
<body>

<div class="fixed">

<!-- Nav bar -->
<div class="conatiner">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Skill Acquire: Invoice Creation</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Display -->
    <div class="container">
        <div class="row" style="margin-bottom: 25px; margin-top: 25">
            <div class="col-sm-12" style="margin-top:20px;" id="date_range">
                <form action="script/create_invoice.php" class="form-inline DateRange" method="post">
                    <label for="">Enter date range for invoice</label><br>
                    <div class="form-group">
                        <label for="">Start Date</label>
                        <input class="form-control" type="date" id="startDate" name="startDate">
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="">End Date</label>
                        <input class="form-control" type="date"  id="endDate" name="endDate">
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="">Invoice Date</label>
                        <input class="form-control" type="date"  id="dueDate" name="dueDate"><br>
                    </div>
                    <br><br>
                    <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                    </div>
            </div>
        </div>
    </div>
    
</div>


<!-- Footer -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
    <script>
        $( document ).ready(function() {
            $('select#state').val("<?php echo strtoupper($data['State'])?>");
        });
    </script>
    <script>
	$('.test').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
          url: desination,
          type: 'post',
          data: values, 
          success: function(data){
              if(data == 1){
                  $('.modal-body').html("<p>Account Updated, use link below to log into your account<br><br><a href='<?php echo baseurl()?>client/login.php'>Login Here</a>");
                  $('#myModal').modal('toggle');
              }
              else{
                $('.modal-body').html("<p>There was an error updating your account, please contact info@pmimd.com for assistance.</p>")
                $('#myModal').modal('toggle');
                  console.log(data);
              }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
		});
	
	});
</script>
</body>
</html>

