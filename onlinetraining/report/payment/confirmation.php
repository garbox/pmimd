<?php 
session_start();
      include '../../includes/config.php';
      include '../includes/reportfunctions.php';
      include 'check_out_auto.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
html, body{ 
	overflow-x: hidden;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav p {
	display:inline;	
	font-size:20px;
}
td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}
</style>
<?php //GoogleAnalytics('UA-73530823-1')?>

<script>
</script>
</head>
<body>

<!-- Nav bar -->
<div class="container-fluid">
    <div class="row">
        <?php include "../includes/custnav.php";?>    
    </div>
</div>
 
<!-- confimration display -->
<div class="container-fluid" style=" padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row" id="Cart-Container">
        <div class="container">                     
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 col-md-offset-3 col-lg-offset-3" >
              	  	<div class="Cart-Container print">
                    <div class="test"></div>
						<?php
                        foreach($_REQUEST as $name => $value){
                            $orderData[$name] = $value;
                        }
                         //GoogleEcommerce($_COOKIE["cartIDs"], $orderID, $info['req_amount']);
						 // function to determin what function to use with cardcode. 
                        echo cardCode($orderData, $_SESSION['Profile']['ID']);      
                        ?>
                    </div>
      			 </div>              
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>

