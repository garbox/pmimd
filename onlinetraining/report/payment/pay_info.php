<?php 
include '../../includes/config.php';
include '../includes/reportfunctions.php';
session_start();
ReportLogInSessionCheck();


//set global vars;
$id = $_GET['ID'];
$uniqID = uniqid();

// call classes 
$cartInfo = new CartSummary;
$CustomerInfo = $cartInfo->custinfo($id);
$summary = $cartInfo->main($id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
body{
	color:black;
}
</style>
</head>

<body>
    <!-- Nav bar -->
    <div class="container-fluid">
        <div class="row">
            <?php include "../includes/custnav.php";?>    
        </div>
    </div>
    
    <div class="container fluid">
        <div class="row">
            <div class="col-lg-7">
            <h3>Billing Info</h3>
            <hr>
                <form id="formPayInfo" action="<?php echo securebaseurl()?>report/payment/pay.php?ID=<?php echo $CustomerInfo['ID']?>" method="post">
                    <input type="hidden" name="access_key" value="7e1ae59d77d13bc8ba277f5a6d921ca9">
                    <input type="hidden" name="profile_id" value="3CC084A0-B4D7-4332-AAC7-052403E1C3A2">
                    <input type="hidden" name="transaction_uuid" value="OLC-<?php echo $uniqID;?>">
                    <input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code">
                    <input type="hidden" name="unsigned_field_names" value="card_type,card_number,card_expiry_date">
                    <input type="hidden" name="signed_date_time" value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                    <input type="hidden" name="locale" value="en">
                    <input type="hidden" name="transaction_type" size="25" value="authorization">
                    <input type="hidden" name="reference_number" size="25" value="<?php echo OrderID()?>">
                    <input type="hidden" name="amount" size="25" value="<?php echo $summary['Price']?>">
                    <input type="hidden" name="currency" size="25" value="usd">
                    <input type="hidden" name="payment_method" value="card">
                    <input type="hidden" name="bill_to_address_country" value='us'>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">First Name On Card</label>
                            <input class="form-control fname" type="text" name="bill_to_forename" placeholder="First Name" value="<?php echo $CustomerInfo['Fname']?>">
                        </div>
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Last Name On Card</label>
                            <input class="form-control lname" type="text" name="bill_to_surname" placeholder="Last Name" value="<?php echo $CustomerInfo['Lname']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Email Address</label>
                            <input class="form-control email" type="text" name="bill_to_email" placeholder="Email" value="<?php echo $CustomerInfo['Email']?>">
                      </div>
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Phone Number</label>
                            <input class="form-control phone" type="text" name="bill_to_phone" placeholder="Phone" value="<?php echo $CustomerInfo['Phone']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Billing Address</label>
                            <input class="form-control address" type="text" name="bill_to_address_line1" placeholder="Address" value="<?php echo $CustomerInfo['Address']?>">
                        </div>
                        <div class="col-sm-2">
                            <label for="" style="font-weight:normal">Billing Zip</label>
                            <input class="form-control zip" type="text" name="bill_to_address_postal_code" placeholder="Zip" value="<?php echo $CustomerInfo['Zip']?>">
                        </div>
                        <div class="col-sm-4">
                            <label for="" style="font-weight:normal">Billing City</label>
                            <input class="form-control city" type="text" name="bill_to_address_city" placeholder="City" value="<?php echo $CustomerInfo['City']?>">
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="" style="font-weight:normal">Billing State</label>
                    <select id="state" class="form-control" name="bill_to_address_state">
                        <option value="false" Selected>Please Select Your State
                        <option value="AK">Alaska
                        <option value="AL">Alabama
                        <option value="AZ">Arizona
                        <option value="AR">Arkansas
                        <option value="CA">California
                        <option value="CO">Colorado
                        <option value="CT">Connecticut
                        <option value="DE">Delaware
                        <option value="DC">District of Columbia
                        <option value="FL">Florida
                        <option value="GA">Georgia
                        <option value="HI">Hawaii
                        <option value="IA">Iowa
                        <option value="ID">Idaho
                        <option value="IL">Illinois
                        <option value="IN">Indiana
                        <option value="KS">Kansas
                        <option value="KY">Kentucky
                        <option value="LA">Louisiana
                        <option value="ME">Maine
                        <option value="MA">Massachusetts
                        <option value="MD">Maryland
                        <option value="MI">Michigan
                        <option value="MN">Minnesota
                        <option value="MS">Mississippi
                        <option value="MO">Missouri
                        <option value="MT">Montana
                        <option value="NE">Nebraska
                        <option value="NV">Nevada
                        <option value="NH">New Hampshire
                        <option value="NJ">New Jersey
                        <option value="NM">New Mexico
                        <option value="NY">New York
                        <option value="NC">North Carolina
                        <option value="ND">North Dakota
                        <option value="OH">Ohio
                        <option value="OK">Oklahoma
                        <option value="OR">Oregon
                        <option value="PA">Pennsylvania
                        <option value="RI">Rhode Island
                        <option value="SC">South Carolina
                        <option value="SD">South Dakota
                        <option value="TN">Tennessee
                        <option value="TX">Texas
                        <option value="UT">Utah
                        <option value="VT">Vermont
                        <option value="VA">Virginia
                        <option value="WA">Washington
                        <option value="WV">West Virginia
                        <option value="WI">Wisconsin
                        <option value="WY">Wyoming
                        </select>
                    </div>
                            
            </div>
            <div class="col-lg-5" >     
                <h3>Summary</h3>   
                <hr>    
                <table>
                    <tr>
                        <td>Item Count</td>
                        <td><?php echo $summary["ItemCount"]?></td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td>$<?php echo $summary["Price"]?></td>
                    </tr>
                </table>
            </div>
            
            <div class="col-lg-12" style="margin-top: 50px;">            
                <a href="<?php echo securebaseurl()?>report/payment/pay_info.php?ID=<?php echo $CustomerInfo['ID']?>">
                    <button style="width: 100%" class="btn btn-primary">Next Step</button>
                </a>
            </div>
            </form>
        </div>
    </div>
    
<script>
	// form validation. if empty throw error
	$('#formPayInfo').on('submit', function(e){
		//empty containers when exicuted to remove error messages. 
		$('.error').empty();
		
		// reset css for inputs
		$('.fname').css('border-color', '#ccc');
		$('.lname').css('border-color', '#ccc');
		$('.email').css('border-color', '#ccc');
		$('.phone').css('border-color', '#ccc');
		$('.address').css('border-color', '#ccc');	
		$('.zip').css('border-color', '#ccc');
		$('.city').css('border-color', '#ccc');
		$('.state').css('border-color', '#ccc');
		
		// make sure field isnt empty
		if( $('.fname').val() == ""){
		e.preventDefault();	
		$('.fname').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.lname').val() == ""){
		e.preventDefault();	
		$('.lname').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.email').val() == ""){
		e.preventDefault();	
		$('.email').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.phone').val() == ""){
		e.preventDefault();	
		$('.phone').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.address').val() == ""){
		e.preventDefault();	
		$('.address').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.zip').val() == ""){
		e.preventDefault();		
		$('.zip').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.city').val() == ""){
		e.preventDefault();	
		$('.city').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		
		if( $('.state').val() == "false"){
		e.preventDefault();	
		$('.state').css('border-color', 'red');
		$('.error').html('Please check fields above');
		}
		if($( "input" ).hasClass( "yes" )){
			if( !$('.yes').is(':checked')){
				if(!$('.no').is(':checked')){
				  $('.error').append('<p>Please select a shipping option</p>');
				  e.preventDefault();	
				}
			}
		}
		
	});	    
</script>
</body>
</html>