<?php
//  Google Analytics Ecommerce Script
function Google_Ecommerce_Back_Office($orderData){
    ?>
    <script>
        ga('require', 'ecommerce');
    </script>
    <?php
    $conn = Connect();
    $CartSelect = "SELECT * FROM cart WHERE Email = '".$orderData['req_bill_to_email']."'";
    $Result = $conn->query($CartSelect);
    
    while($Cart = $Result->fetch_assoc()){
        $SPSelect = "SELECT * FROM selfpaced WHERE ProdID = '".$Cart['ProdID']."'";
        $SPResult = $conn->query($SPSelect);
        
        while($SPItem = $SPResult->fetch_assoc()){
            ?>
            <script>
                ga('ecommerce:addItem', {
                  'id': '<?php echo $orderData['req_reference_number']?>',                                                // Transaction ID. Required.
                  'name': '<?php echo $SPItem['Name']?>',                                       // Product name. Required.
                  'price': '<?php echo $orderData['req_amount']?>',                 // Unit price.
                });
            </script>
            <?           
        }
    }    
    ?>
        <script>
        // Below is used for final ammount (your total revenue)
        ga('ecommerce:addTransaction', {
          'id': '<?php echo $OrderID?>',                     // Transaction ID. Required.
          'affiliation': 'PMI Online Order',   // Affiliation or store name.
          'revenue': '<?php echo $TotalAmount?>',               // Grand Total.
        });
        ga('ecommerce:send');
        </script>
    <?php 
}

function OrderPlaced($orderData){
    //Get customer info first. User Obj and find with session var with ID
    
    $recDate = ReconcileDate(DateAndTime);
    $newobject = new CartSummary;
    $custData = $newobject->custinfo($_SESSION['Profile']['ID']);
    
    //get all items in the cart with said customer. 
    $conn = Connect();
    $pmiconnect = pmimain_connect();
    $cart_select = "SELECT ProdID FROM cart WHERE Email = '".$custData['Email']."'";
    $resut = $conn->query($cart_select);
    
    $cart_data = $resut->fetch_all();
    
    //get the data from cart and apply to class info database. 
    foreach($cart_data as $key => $value){
        $selectProd = "SELECT * FROM AF_SP_Info WHERE ProdID = '".$value[0]."'";
        $result = $pmiconnect->query($selectProd);
        
        //take class info from database and unsert into orderplaced table. 
        while($row = $result->fetch_assoc()){
            $insert = "INSERT orderplaced (ProdID, CustomerID, Email, Price, CataID, PName, ReceiptID, RecDate, CardType, CardNumber) 
                VALUES ('".$row['ProdID']."','".$_SESSION['Profile']['ID']."','".$custData['Email']."','".$row['Price']."','".$row['CataID']."','".$row['Name']."','".$orderData['req_reference_number']."','".$recDate."', '".$orderData['req_card_type']."', '".$orderData['req_card_number']."')";
            
            $conn->query($insert); 
        }
    }
}

//Last function to EXE... clean the cart of users data. 
function AddProdToProtal(){
    $conn = Connect();
    $newobject = new CartSummary;
    $custData = $newobject->custinfo($_SESSION['Profile']['ID']);
    
    $select = "SELECT * FROM cart WHERE Email = '".$custData['Email']."'";
    
    $result = $conn->quer($select);
    
    $cart_data = $result->fetch_all();
    
    foreach($cart_data as $key=>$value){
        $insert = "INSERT custportal (custID, prodID) 
				VALUES ('".$_SESSION['Profile']['ID']."','".$value[0]."')";
        $conn->query($insert);
    }
    
    $delete = "DELETE FROM cart WHERE Email = '".$custData['Email']."'";
    $conn->query($delete);
    
}
?>