<?php 
include '../../includes/config.php';
include '../includes/reportfunctions.php';
session_start();
ReportLogInSessionCheck();
$id = $_GET['ID'];
// call classes 
$cartObj = new CartSummary;
$CustomerInfo = $cartObj->custinfo($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
body{
	color:black;
}
</style>
</head>

<body>
    <!-- Nav bar -->
    <div class="container-fluid">
        <div class="row">
            <?php include "../includes/custnav.php";?>    
        </div>
    </div>
    
    <div class="container fluid">
        <div class="row">
            <div class="col-lg-7">
            <h3>Customers Cart</h3>
            <hr>
            <table width="100%">
                <?php displayCartPortal($CustomerInfo['Email']);?>
            </table>
            </div>
            <div class="col-lg-5" >     
                <h3>Summary</h3>   
                <hr>    
                <?php 
                $cartInfo = $cartObj->main($id);
                ?>
                <table>
                    <tr>
                        <td>Item Count</td>
                        <td><?php echo $cartInfo['ItemCount']?></td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td>$<?php echo $cartInfo['Price']?></td>
                    </tr>
                </table>
            </div>
            
            <div class="col-lg-12" style="margin-top: 50px;">            
                <a href="<?php echo baseurl()?>report/payment/pay_info.php?ID=<?php echo $CustomerInfo['ID']?>">
                    <button style="width: 100%" class="btn btn-primary">Check Out</button>
                </a>
            </div>
        </div>
    </div>
</body>
<script>
    $("button.RemoveClass").on('click', function(){
        var prodID = $(this).attr('id');
        var email = "<?php echo $CustomerInfo['Email']?>";
        var cataID = $(this).parent('td').parent('tr').attr('id');
        
        var jsonObj = {
            "email": ""+email+"",
            "prodID": ""+prodID+"",
        }
        
        var values = $.param(jsonObj);
        console.log(values);
        var desination = "<?php echo baseurl()?>report/script/removefromcart.php";
        $.ajax({
            url: desination,
            type: 'post',
            data: values,
            success: function(data) {
                location.reload();
                console.log(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#wait').hide();
                console.log(textStatus, errorThrown);
            }
        });
    })
</script>
</html>