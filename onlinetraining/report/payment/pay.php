<?php 
include '../../includes/config.php';
include '../includes/reportfunctions.php';
include 'security.php';
session_start();
ReportLogInSessionCheck();

//set global vars;
$id = $_GET['ID'];
$uniqID = uniqid();
$_SESSION['Profile']['ID'] = $id;
// call classes 
$cartInfo = new CartSummary;
$CustomerInfo = $cartInfo->custinfo($id);
$summary = $cartInfo->main($id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
body{
	color:black;
}
</style>
</head>

<body>
    <!-- Nav bar -->
    <div class="container-fluid">
        <div class="row">
            <?php include "../includes/custnav.php";?>    
        </div>
    </div>
    
    <div class="container fluid">
        <div class="row">
            <div class="col-lg-7">
            <h3>Card Info</h3>
            <hr>
                        <form id="payform" action="https://secureacceptance.cybersource.com/silent/pay" method="post"/>
						<?php
                        foreach($_REQUEST as $name => $value) {
                        $params[$name] = $value;
                        }
                        foreach($params as $name => $value) {
                        echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
                        }
                        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
                        ?>
                        <div class="form-group">
                        <label for="" style="font-weight:normal;">Select Card Type</label>
                        <select class="form-control cardType" name="card_type">
                            <option value="" selected></option>
                            <option value="001">Visa</option>
                            <option value="002">MasterCard</option>
                            <option value="003">American Express</option>
                        </select>
                        </div>
                        <div class="form-group">
                        <label for="" style="font-weight:normal;">Card Number (with no dashes or spaces)</label>
                        <input class="form-control cardNumber" type="text" name="card_number">
                        </div>
                        <div class="form-group">
                        <label for=""  style="font-weight:normal;">Expiration Date (MM-YYYY)</label>
                        <input class="form-control cardExpire" type="text" name="card_expiry_date">
                        </div>
            </div>
            <div class="col-lg-5" >     
                <h3>Summary</h3>   
                <hr>    
                <table>
                    <tr>
                        <td>Item Count</td>
                        <td><?php echo $summary["ItemCount"]?></td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td>$<?php echo $summary["Price"]?></td>
                    </tr>
                </table>
            </div>
            
            <div class="col-lg-12" style="margin-top: 50px;">            
                <a href="<?php echo securebaseurl()?>report/payment/pay_info.php?ID=<?php echo $CustomerInfo['ID']?>">
                    <button style="width: 100%" class="btn btn-primary">Next Step</button>
                </a>
            </div>
            </form>
        </div>
    </div>   
<script> 
$('#payform').on('submit', function(e){
	//empty containers when exicuted to remove error messages. 
	$('.errors-type').empty();
	$('.errors-acct').empty();
	$('.errors-exp').empty();
	
	// reset css for inputs
	$('.cardExpire').css('border-color', '#ccc');
	$('.cardNumber').css('border-color', '#ccc');
	$('.cardType').css('border-color', '#ccc');	
	
	// make sure field isnt empty
	if( $('.cardType').val() == ""){
	e.preventDefault();	
	$('.cardType').css('border-color', 'red');
	$('.errors-type').html("Please enter card type");
	}		
	
	// make sure field length is greater then 15
	if($('.cardNumber').val().length <15){
	e.preventDefault();	
	$('.cardNumber').css('border-color', 'red');
	$('.errors-acct').html("Length of credit card number is incorrect.");	
	}
	
	// make sure field isnt empty
	if( $('.cardExpire').val() == ""){
	e.preventDefault();	
	$('.cardExpire').css('border-color', 'red');
	$('.errors-exp').html("Please enter experiation date");
	}
	
	// make sure field is the correct length. 
	if($('.cardExpire').val().length < 7){
	e.preventDefault();	
	$('.cardExpire').css('border-color', 'red');
	$('.errors-exp').html("Please check experiation date format MM-YYYY");
	}
});

/* Automaticly formats sting */
$('.cardExpire').keyup(function () {
	$(this).val($(this).val().replace('+', '-'));
	$(this).val($(this).val().replace('*', '-'));
	$(this).val($(this).val().replace('/', '-'));
    $(this).val($(this).val().replace(/(\d{2})(\d{4})/, "$1-$2"));
	});
</script>
</body>
</html>