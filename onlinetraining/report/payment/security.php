<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '97d78f9ec8854f8a982640e8885e123d9bca328c574d4dde8d45262b9166b3de650fdf4fc5de42ebbb2f73a38b5d96ca3bdcb704e0194d4babac3267041cd56cdd9261ef4bb24dbfbfd8f7a436de4513e1620a806c4b4b3cb749ed61a24ddc4d81c97f8c40d74dc4a399c6358f5d026cf1339d17aba7457385a28b3cb294e9ec');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
