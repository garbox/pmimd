<?php 
include '../../includes/config.php';
include '../includes/reportfunctions.php';
session_start();
ReportLogInSessionCheck();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}    
h2{
	color:#5371AD;
}
a.EditPro{
 font-size: 16px;       
}
.btn-success{
    background-color: #0ca24b;
}
</style>
</head>

<body>
<!-- Nav bar -->
<div class="container-fluid">
	<div class="row">
        <?php include "../includes/custnav.php";?>    
    </div>
</div>

<!-- Preimitive search function for customer -->
<div id="div1" class="hidden"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
        <h3>Customer Info</h3>
        <hr>
            <?php
                $ID = $_GET['ID'];
                $customerID = $_GET['ID'];
                $conn = Connect();
                $selectOrderID = "SELECT * FROM customers WHERE ID = '".$ID."'";
                
                $resultOrderID = $conn->query($selectOrderID);
                // gather info from receipt number (OrderID)
                while($transaction = $resultOrderID->fetch_assoc()){
                    foreach($transaction as $Customer => $value) {
                    $CustomerArray[$Customer] = $value;
                    }
                }
            ?>
            <?php 
                // Checks if customer has logged in. 
                $Select = "SELECT DATE_FORMAT(DateLog, '%m/%d/%Y At %h:%i %p') AS Date FROM logintracking WHERE CustID = '".$customerID."' ORDER BY Datelog DESC LIMIT 1";
                $LastLogIn = $conn->query($Select);
                $LastLogIn = $LastLogIn->fetch_assoc();
                $LastLogIn = $LastLogIn['Date'];
                if($LastLogIn == ""){
                    $LastLogIn = "No recored of login";   
                }
            ?>
            <table>
                <tr>
                   <td colspan="2">
                        <a class="EditPro" href="editaccount.php?ID=<?php echo $ID?>">Edit Profile</a>
                   </td>
                </tr>
            	<tr>
            		<td>Name:</td>
            		<td><?php echo ucwords(strtolower($CustomerArray['Fname'])) . " ". ucwords(strtolower($CustomerArray['Lname']))?></td>
            	</tr>
                <tr>
            		<td>Address:</td>
            		<td><?php echo ucwords(strtolower($CustomerArray['Address'])). "<br>" . ucwords(strtolower($CustomerArray['City'])). " ". strtoupper($CustomerArray['State']).", ".$CustomerArray['Zip']?></td>
            	</tr>
                <tr>
            		<td>Phone:</td>
            		<td><?php echo $CustomerArray['Phone']?></td>
            	</tr>
                <tr>
            		<td>Email:</td>
            		<td><?php echo $CustomerArray['Email']?></td>
            	</tr>
                   <?php
                    if($CustomerArray['Cert'] != 0){
                        if(GradCheck($CustomerArray) == 1){
                            ?>
                                <tr>
                                    <td>Graduate ID</td>
                                    <td style="color:#0ca24b"><?php echo $CustomerArray['Cert']?></td>
                                </tr>
                            <?php 
                        }
                        else{
                            ?>
                                <tr>
                                    <td>Graduate ID</td>
                                    <td style="color:#c01313"><?php echo $CustomerArray['Cert']?> (Not Valid)</td>
                                </tr>
                            <?php 
                        }
                    }
                    else{
                        
                    }
                    ?>
                <tr>
                    <td>Last Login:</td>
                    <td><?php echo $LastLogIn;?></td>
        	    </tr>
            </table>
            <button class="LoginTracking btn btn-primary" style="width:50%;" data-toggle="modal" data-target="#LoginTracking">Login History</button>
        </div>
        <div class="col-sm-4" >
            <h3>Course Tracking</h3>
            <hr>
            <div style=" overflow-y: scroll; height:300px;">
               <table>
                <?php VideoCompletePrecent($customerID)?>
                </table>
            </div>
            <div style="margin-top:10px;">
                <button class="btn btn-primary" style="width:50%;" data-toggle="modal" data-target="#VideoTracking">Video History</button>
            </div>
        </div>
        <div class="col-sm-4">
        <h3>Add Class</h3>
        <hr>
            <div class="col-md-12">
                <div class="form-group">
                    <input name="CustomerID" type="hidden" value="<?php echo $CustomerArray['ID']?>">
                </div>
                <div class="form-group">
                    <select class="form-control" id="classID" name="ProdID">
                        <?php 
                            class_listing();
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control price" name="Price" placeholder="Price (Total Ammount)" type="text">
                </div>
                <div class="form-group" id="con_layout">
                    <h4>Conference Info</h4><label for="" style="font-weight:normal">Dietary Preference</label> <select class="form-control" id="dietary" name="dietary">
                        <option value="none">
                            None
                        </option>
                        <option value="vegetarian">
                            Vegetarian
                        </option>
                        <option value="gluten-free">
                            Gluten-free
                        </option>
                        <option value="vegan">
                            Vegan
                        </option>
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <input name="orderID" type="hidden" value="<?php echo OrderID()?>"> 
                    <button class="btn btn-success" id="AddProdForm">Add Product</button>
                    <button class="btn btn-primary" id="CartButton">Add to cart</button>
                    <a href="../payment/summary.php?ID=<?php echo $CustomerArray['ID']?>"><button class="btn btn-danger" id="CheckOut2">Check Out</button></a>
                </div>
                <div id="wait"><img class='center-block' height="50" src='https://pmimd.com/totalaccess/img/Test.gif'></div>
            </div>
            <div class="col-lg-12">
                <h3>Customers Cart</h3>
                <hr>
                <table width="100%">
                <?php displayCartPortal($CustomerArray['Email']); ?>
                </table>
            </div>
    </div>
</div>
<hr>

<!-- Products Customers Own-->
<div class="container-fluid" style="margin-bottom:10px;">
	<div class="row">
        <!-- Class Info -->
    	<div class="col-lg-8">
           	<table width="100%">
                <tr>
                <?php 
					$ClassSelect = "SELECT *, SUM(Price) as Sum, MAX(RecDate) as RecDate FROM orderplaced WHERE CustomerID = '".$ID."' GROUP BY ProdID, ReceiptID ORDER BY DateOrdered DESC" ;
					$result = $conn->query($ClassSelect);
					if($result->num_rows>0){
						?>
                        <td>Class Name</td>
                        <td>Amount</td>
                        <td>Order ID</td>
                        <td>Date Ordered</td>
                        <td>Last Post Date</td>
                        <td>Class ID</td>
                        <?php
						while($transaction = $result->fetch_assoc()){
							foreach($transaction as $Customer => $value) {
								$CustomerArray[$Customer] = $value;
							}
							$ID = $transaction[ID];
							?>
                            <tr>
                            <td><?php echo $CustomerArray['PName']?></td>
                            <td><?php echo "$".$CustomerArray['Sum']?></td>
                            <td><?php 
							  foreach (glob("../../receipt/sp/*".$CustomerArray['ReceiptID']."*.html") as $filename) {
								  echo "<a href=".$filename.">".$CustomerArray['ReceiptID']."</a>";
							  }
							  if($filename == ""){
								  echo $CustomerArray['ReceiptID'];  
							  }
							  ?>
                            </td>
                            <td><?php echo $CustomerArray['DateOrdered']?></td>
                            <td><?php echo $CustomerArray['RecDate']?></td>
                            <td><?php echo $CustomerArray['ProdID']?></td>
                            <td><a href="<?php echo baseurl()?>report/paymenttransaction.php?CustID=<?php echo $customerID?>&ProdID=<?php echo $transaction['ProdID']?>&ReciptID=<?php echo $CustomerArray['ReceiptID']?>"><button class="btn btn-primary">Transaction History</button></a></td>
                            <script>$("#RecDate<?php echo $transaction['ID']?>" ).datepicker({ dateFormat: 'yy-mm-dd' });</script>
                            </tr>
                            <?php
						}
						
					}
					else{
					echo "<td>Customers has no classes.</td>";
					}
				?>
                </tr>
        	</table>
			<hr>
        </div>
        
        <!-- Customer Notes -->
        <div class="col-md-4">
            <div class="row">
            <h3 style="margin-top:-1px;">Customer Notes</h3>
            <hr>
                <div style="min-height: 200px; max-height:200px; overflow-y:scroll; padding:10px; ">
                <?php 
                $ID = $_GET["ID"];
                    $select = "SELECT * FROM customernotes WHERE CustomerID = ".$ID." ORDER BY Date DESC";

                    $result = $conn->query($select);

                    if($result->num_rows > 0){
                        while($row = $result->fetch_assoc()){
                            $Note = $row["Note"];
                            $User = $row["User"];
                            $Date = $row["Date"];

                            ?>
                            <p><?php echo $Note ?></p>
                            <p>-<?php echo $User ."<br> Date: ". $Date;?></p>
                            <hr>
                            <?php   
                        }
                    }
                    else{
                     echo "No customer notes"; 
                    }
                ?>
                </div>

            </div>
            <div class="row">
                <form id="NoteForm" action="../script/noteupload.php">

                <div class="form-group"><br>
                    <label for="">Enter Notes Below</label>
                    <textarea class="form-control" name="Note" id="" rows="5"></textarea>
                </div>
                <div class="form-group"><br>
                    <input name="ID" class="form-control" type="hidden" value="<?php echo $ID?>">
                    <input name="User" class="form-control User" type="hidden" value="<?php echo $_SESSION['LoginName']?>">
                    <button class="btn btn-primary" type="submit">Add Note</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Login Tracking Modal -->
<div id="LoginTracking" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo baseurl() ?>report/Script/LoginHistory.php?CustID=<?php echo $ID?>"><p>Print Friendly</p></a>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login Tracking</h4>
      </div>
      <div class="modal-body" style="overflow-y:scroll; height:350px">
        <div id="LoginTrackingData">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Video Tracking Modal -->
<div id="VideoTracking" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo baseurl() ?>report/script/videohistory.php?CustID=<?php echo $ID?>"><p>Detailed Report</p></a>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Course Tracking</h4>
      </div>
      <div class="modal-body" style="overflow-y:scroll; height:350px">
        <div id="VideoTrackingData">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Scripts -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.js"></script>
    
<script>
    // Pre-loading before functions called. 
    $("#con_layout").css('display', "none"); 
    $('#wait').hide();
    
    //add product to customers cart 
    $("#CartButton").on('click', function(e){
        $('#wait').show();
        var prodID = $('#classID').val();
        
        var email = "<?php echo $CustomerArray['Email']?>";

        var JsonObj = {
            "prodID": ""+prodID+"",
            "email": ""+email+""
        }
        var values = $.param(JsonObj); 
        var desination = "<?php echo baseurl()?>report/script/addtocart.php";
        $.ajax({
            url: desination,
            type: 'post',
            data: values,
            success: function(data) {
                $('#wait').hide();
                location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#wait').hide();
                console.log(textStatus, errorThrown);
            }
        });
    })
    
    //add product to customers cart 
    $("#CheckOut").on('click', function(e){
        $('#wait').show();
        var prodID = $('#classID').val();
        var email = "<?php echo $CustomerArray['Email']?>";
        
        var JsonObj = {
            "prodID": ""+prodID+"",
            "email": ""+email+""
        }
        var values = $.param(JsonObj); 
        var desination = "<?php echo baseurl()?>report/script/addtocart.php";
        $.ajax({
            url: desination,
            type: 'post',
            data: values,
            success: function(data) {
                window.location ='../payment/summary.php?ID='+<?php echo $CustomerArray['ID']?>;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#wait').hide();
                console.log(textStatus, errorThrown);
            }
        });
    })
    
    // add product to account
    $('#AddProdForm').on('click', function(e){
        var prodID = $('#classID').val();
        var price = $('input[name="Price"]').val();
        var orderID = $('input[name="orderID"]').val();
        
        var JsonObj = {
            "CustomerID": "<?php echo $ID?>",
            "prodID": ""+prodID+"",
            "price": ""+price+"",
            "orderID": ""+orderID+"",
            "email": "<?php echo $CustomerArray['Email']?>"
        }
        // if price is not set throw error and highlight price input. 
        if(price == ""){
            $('input[name="Price"]')
                .css('border-color', 'red');
        }
        
        // else do said action and continue purchase flow.
        else{
            if(price >0){
                var values = $.param(JsonObj); 
                var desination = "<?php echo baseurl()?>report/script/addtocart.php";
                //e.preventDefault();
                $.ajax({
                    url: desination,
                    type: 'post',
                    data: values,
                    success: function(data) {
                        console.log(data);
                        $("#div1").load('../script/expresscheckoutsession.php?price='+JsonObj['price']+'');
                        window.location ='../payment/pay_info.php?ID='+JsonObj['CustomerID'];
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
                }
            else{
                var values = $.param(JsonObj); 
                var desination = "<?php echo baseurl()?>report/script/addproduct.php";
                e.preventDefault();
                $.ajax({
                    url: desination,
                    type: 'post',
                    data: values,
                    success: function(data) {
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        }
        
    });

	$('#NoteForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                  location.reload();
                  console.log(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
		});
	
	});
    
    // Checks if its a conference and selects diet. 
    $("#classID").change(function() {
        var Data = $( this ).val();
        if(Data == '22673-1109' || Data == '22671-1108' || Data == '22672-1108'){
           $("#con_layout").css('display', "inline") 
        }
        else{
            $("#con_layout").css('display', "none"); 
        }
    });  
    
    $("button.RemoveClass").on('click', function(){
        var prodID = $(this).attr('id');
        var email = "<?php echo $CustomerArray['Email']?>";
        var cataID = $(this).parent('td').parent('tr').attr('id');
        
        var jsonObj = {
            "email": ""+email+"",
            "prodID": ""+prodID+"",
        }
        
        var values = $.param(jsonObj);
        var desination = "<?php echo baseurl()?>report/script/removefromcart.php";
        $.ajax({
            url: desination,
            type: 'post',
            data: values,
            success: function(data) {
                $('tr#'+prodID).hide();
                console.log(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#wait').hide();
                console.log(textStatus, errorThrown);
            }
        });
    })

    //pull reports for login and video tracking
    $('#LoginTrackingData').load('<?php echo baseurl() ?>report/script/loginhistory.php?CustID=<?php echo $ID?>');
    $('#VideoTrackingData').load('<?php echo baseurl() ?>report/script/videohistory.php?CustID=<?php echo $ID?>');
</script>
</body>
</html>