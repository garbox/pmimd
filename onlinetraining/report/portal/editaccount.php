<?php
include('../../includes/config.php');
include('../includes/reportfunctions.php');
session_start();
$data = $_REQUEST;
$cust_obj = new CartSummary;
$cust_data = $cust_obj->custinfo($data['ID']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMIMD: Online Training Center</title>
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<style>
	body,html{overflow-x:hidden;}.Cart-Container{background-color:#fff;padding:15px 30px}td{padding:10px!important}h1,h2,h3,h4,h5,h6{color:#999}.Cart-Container p.cartNav{display:inline;font-size:20px}.cartNavText{font-size:20px;vertical-align:middle;margin-top:-9px}.active{opacity:1}.inactive{opacity:.3}.manager{border:none}
	</style>
</head>
<body>
	<div class="conatiner" style="background-color:#fff">
		<div class="row">
			<div class="col-lg-12">
				<?php include '../includes/nav.php';?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="Cart-Container">
							<h2>Edit Account</h2>
							<form action="../script/update_account.php" class="HideForm" id="EditUser" method="post" name="EditUser">
								<label for="">Basic Info</label>
								<div class="form-group">
									<input class="form-control" name="ID" type="hidden" value="<?php echo $cust_data['ID']?>">
								</div>
								<div class="row">
								    <div class="col-md-6">
								    <div class="form-group">
                                        <label for="firstname">First Name</label>
                                        <input class="form-control" name="firstname" placeholder="First Name" value="<?php echo $cust_data['Fname']?>">
                                    </div>    
								    </div>
								    <div class="col-md-6">
								        <div class="form-group">
                                            <label for="lastname">Last Name</label>
                                            <input class="form-control" name="lastname" placeholder="Last Name" value="<?php echo $cust_data['Lname']?>">
                                        </div>
								    </div>
								</div>
								
								
								<div class="form-group">
								<label for="address">Address</label>
									<input class="form-control" name="address" placeholder="Address" type="address" value="<?php echo $cust_data['Address']?>">
								</div>
								<div class="form-group">
								<label for="phone">Phone</label>
									<input class="form-control" name="phone" placeholder="Phone Number" value="<?php echo $cust_data['Phone']?>">
								</div>
								<div class="row">
								    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input class="form-control" name="city" placeholder="City" value="<?php echo $cust_data['City']?>">
                                        </div>
								    </div>
								    <div class="col-md-4">
								        <div class="form-group">
                                            <label for="zip">Zip Code</label>
                                            <input class="form-control" name="zip" placeholder="Zip" value="<?php echo $cust_data['Zip']?>">
                                        </div>
								    </div>
								    <div class="col-md-4">
								        <div class="form-group">
                                            <label for="state">State</label>
                                            <select id="state" class="form-control" name="state">
                                                <option value="false" Selected>Please Select Your State
                                                <option value="AK">Alaska
                                                <option value="AL">Alabama
                                                <option value="AZ">Arizona
                                                <option value="AR">Arkansas
                                                <option value="CA">California
                                                <option value="CO">Colorado
                                                <option value="CT">Connecticut
                                                <option value="DE">Delaware
                                                <option value="DC">District of Columbia
                                                <option value="FL">Florida
                                                <option value="GA">Georgia
                                                <option value="HI">Hawaii
                                                <option value="IA">Iowa
                                                <option value="ID">Idaho
                                                <option value="IL">Illinois
                                                <option value="IN">Indiana
                                                <option value="KS">Kansas
                                                <option value="KY">Kentucky
                                                <option value="LA">Louisiana
                                                <option value="ME">Maine
                                                <option value="MA">Massachusetts
                                                <option value="MD">Maryland
                                                <option value="MI">Michigan
                                                <option value="MN">Minnesota
                                                <option value="MS">Mississippi
                                                <option value="MO">Missouri
                                                <option value="MT">Montana
                                                <option value="NE">Nebraska
                                                <option value="NV">Nevada
                                                <option value="NH">New Hampshire
                                                <option value="NJ">New Jersey
                                                <option value="NM">New Mexico
                                                <option value="NY">New York
                                                <option value="NC">North Carolina
                                                <option value="ND">North Dakota
                                                <option value="OH">Ohio
                                                <option value="OK">Oklahoma
                                                <option value="OR">Oregon
                                                <option value="PA">Pennsylvania
                                                <option value="RI">Rhode Island
                                                <option value="SC">South Carolina
                                                <option value="SD">South Dakota
                                                <option value="TN">Tennessee
                                                <option value="TX">Texas
                                                <option value="UT">Utah
                                                <option value="VT">Vermont
                                                <option value="VA">Virginia
                                                <option value="WA">Washington
                                                <option value="WV">West Virginia
                                                <option value="WI">Wisconsin
                                                <option value="WY">Wyoming
                                            </select>
                                        </div>
								    </div>
								</div>
								
								<label for="">Account Info<br>
								<span style="font-size:12px;color:#c01313">(If you change the email address, it will also change the user name.)</span></label>
								<div class="form-group">
									<input class="form-control" name="email" placeholder="Email" type="email" value="<?php echo $cust_data['Email']?>">
								</div>
								<div class="row">
								    <div class="col-md-6">
								        <div class="form-group">
                                        <label for="manager">Manager Name</label>
                                            <input class="form-control" name="manager" placeholder="Your Managers Name" value="<?php echo $cust_data['Manager']?>">
                                        </div>
								    </div>
								    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="practice">Practice Name</label>
                                            <input class="form-control practice" name="practice" placeholder="Practice Name" value="<?php echo $cust_data['Practice']?>">
                                        </div>
								    </div>
								</div>
								
								
								<div class="form-group">
								<label for="certID">Certification ID</label>
									<input class="form-control" name="certID" placeholder="Certification ID" value="<?php echo $cust_data['Cert']?>">
								</div><button class="btn btn-block btn-default" type="submit">Submit</button>
							</form>
							<div class="wait"><img class="center-block img-fluid" src="http://www.pmimd.com/totalaccess/img/Test.gif"></div>
							<div class="result"></div>
					</div>
				</div>
			</div>
		</div>
	</div><?php include '../Includes/Footer.html';?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script>
	<script src="http://www.pmimd.com/css/bootstrap/ProdCenter/js/bootstrap.min.js">
	</script>
	<script>
	$('.wait').hide();
	$('#EditUser').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		$(this).hide();
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                console.log(data);
	            $(".wait").hide();
                $("#EditUser").empty();
                $("#EditUser").show().html("<p>Thank you, your infomation has been updated. <\/p>");
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
        
    $( document ).ready(function() {
		$('select#state').val("<?php echo strtoupper ($cust_data["State"])?>");
	});
	</script>
</body>
</html>