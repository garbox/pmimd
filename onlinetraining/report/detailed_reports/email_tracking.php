<?php 
include '../includes/reportfunctions.php';
include '../../includes/config.php';
session_start();
ReportLogInSessionCheck();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email Conversions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
<style>
body{
	color:black;
}

h2{
	color:#5371AD;
}
</style>
</head>
<body>
<!-- Nav bar -->
<?php include "../includes/nav.php";?>

<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-11">
           	<table width="85%">
        	<tr style="border-bottom:1px solid #e6e6e6">
        		<td valign="top">First Name</td>
        		<td>Last Name</td>
        		<td>Email</td>
                <td>Date Registered</td>
                <td>Receipt</td>
                <td>Email Campaign</td>
                <td>Conversion Amount</td>
                <td></td>
        	</tr>
        	<?php 
                $conn = Connect();
                $campaign_select = "SELECT * FROM mcecommerce ORDER BY Date DESC";
                $query = $conn->query($campaign_select);
            
                while($data = $query->fetch_assoc()){
                    $customer = "SELECT * FROM customers WHERE ID = '".$data['CustomerID']."'";
                    $cust_query = $conn->query($customer);
                    
                    while($cust_data = $cust_query->fetch_assoc()){
                        ?>
                        <tr>
                        <td><?php echo $cust_data['Fname']?></td>
                        <td><?php echo $cust_data['Lname']?></td>
                        <td><?php echo $cust_data['Email']?></td>
                        <td><?php echo date('m/d/Y',strtotime($data['Date']))?></td>
                        <td><?php echo $data['OrderID']?></td>
                        <td><a target="_blank" href="http://us12.campaign-archive2.com/?u=398c28b27db7d75b66accc62a&id=<?php echo $data['CampaignID']?>"><?php echo $data['CampaignID']?></a></td>
                        <td>$<?php echo $data['Amount']?></td>
                        </tr>
                        <?php
                    }
                }
            ?>
        </tr>
        </table>

        </div>
    </div>
</div>

