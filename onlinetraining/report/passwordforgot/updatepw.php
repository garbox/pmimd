<?php 
include '../includes/reportfunctions.php';
include '../../includes/config.php';
$GenKey = $_GET['GenKey'];

$conn = Connect();
$select = "SELECT * FROM resetpw WHERE EncryptKey = '".$GenKey."'";
$result = $conn->query($select);
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report: ForgotPassword</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<div class="container-fluid" style="padding-top:30px; margin-bottom:30px; min-height:507px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Update Password</h2>
                        <hr>
                        <div class="error">   
                            <p>Seems to be an error, please try to reset password again.</p>
                            <a href="<?php echo baseurl()?>report/passwordforgot/passwordupdate.php">Forgot Password</a>
                        </div>
                        <form class="LogInForm" method="post" action="<?php echo baseurl()?>report/script/userupdatepassword.php" >
                          <div class="form-group">
                            <input type="hidden" class="form-control" name="username" value="<?php echo $row['Email']?>">
                            <input type="password" class="form-control" name="pw" placeholder="Password">
                          </div>                       
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>
</body>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
$('form.LogInForm').hide();
$('.error').hide();
</script>
<?php 
if($result->num_rows>0){
    ?>
        <script>
            $('form.LogInForm').show();
        </script>
    <?php
}
else{
    ?>
        <script>
            $('.error').show();;
        </script>
    <?php    
}


?>
<!-- Ajax -->
<script>
	$('.wait').hide();
	$('.LogInForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.LogInForm').prop('action');   		
		// get current location url			
		$('.LogInForm').hide();
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
                    window.location = '<?php echo baseurl()?>report/login.php'
				  }
				  else{
					  console.log(data);
					  $('.wait').hide();
					  $('.LogInForm').show();
					  $(".error").html("Seems to be an error, please contact support at helpdesk@pmimd.com");
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
</script>
</html>
