<?php 
include '../includes/reportfunctions.php';
include '../../includes/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report: ForgotPassword</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<div class="container-fluid" style="padding-top:30px; margin-bottom:30px; min-height:507px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Forgot Password</h2>
                        <p class="error"></p>
                        <hr>
                        <div class="wait">
                        	<img class='center-block img-fluid'src='http://pmimd.com/totalaccess/img/Test.gif'>
                        </div>
                      <form class="LogInForm" method="post" action="../script/updatepwscript.php" >
                          <div class="form-group">
                            <input type="email" class="form-control" name="username" placeholder="Username">
                          </div>                      
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div></body>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://pmimd.com/css/bootstrap/ProdCenter/js/bootstrap.min.js"></script>

<!-- Ajax -->
<script>
	$('.wait').hide();
	$('.LogInForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.LogInForm').prop('action');   		
		// get current location url			
		$('.LogInForm').hide();
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
                      console.log(data);
                      $('.wait').hide();
                      $(".error").html("An email has been sent to reset your password. Follow the instructions to update your password.");
				  }
				  else{
					  console.log(data);
					  $('.wait').hide();
					  $('.newtopmi').show();
					  $('.LogInForm').show();
					  $(".error").html("Incorrect info: Please make sure username is entered correctly.");
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
</script>
</html>
