<?php 
// output headers so that the file is downloaded rather than displayed
include('../../includes/config.php');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Roster.csv');

$CataID = $_GET['CataID'];
// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array("Class ID", "Class CataID", "Class Name", 'CertID', 'First Name', 'Last Name', 'Phone', 'Email', 'Address', 'City', 'State', 'Zip', 'Practice', 'Price', "Date Registered"));

// fetch the data
$conn = Connect();
$select = "SELECT * FROM orderplaced WHERE CataID = '".$CataID."' GROUP BY ReceiptID";
$result = $conn->query($select);

while($row = $result->fetch_assoc()){
    $ID = $row['CustomerID'];
    $Name = $row['PName'];
    $ProdID = $row['ProdID']; 
    
    switch($ProdID){
        case '22671-1108':
        $CataID = 'PKGC';
        break;
            
        case '22672-1108':
        $CataID = $CataID = 'PKGM';
        break;
            
        case '22673-1109':
        $CataID = $CataID = 'CONF';
        break;
            
        default:
    }

    $SELECT = "SELECT * FROM customers WHERE ID = '".$ID."'";
    $cust_result = $conn->query($SELECT);
    
    while($data = $cust_result->fetch_assoc()){
        $row_data = array($ProdID, $CataID, $Name, $data['Cert'], $data['Fname'], $data['Lname'], $data['Phone'], $data['Email'], $data['Address'], $data['City'], $data['State'], $data['Zip'], $data['Practice'], "$".$row['Price'], date('m/d/y', strtotime($row['DateOrdered'])));
        fputcsv($output, $row_data);
    }
}
?>