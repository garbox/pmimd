<?php session_start();
include "../includes/reportfunctions.php";
include '../../includes/config.php';

$custID = $_GET['custID'];
$conn = Connect();

class course_tracking{
    public function get_users_courses($custID, $conn){

        $vt_select = $conn->query("SELECT CourseID, LastAccess , SUM(CurrentTime) AS TimeSpent
            FROM videotracking 
            WHERE CustomerID = ".$custID."
            GROUP BY CourseID");
        return $vt_select->fetch_all();
    }
    
    public function total_course_duration($courseID, $conn){
        $Videoresult = $conn->query("SELECT SUM(Duration) AS TotalDuration, COUNT(ClassID) AS Count FROM videos WHERE ClassID = '".$courseID."'");
        return $Videoresult->fetch_assoc();
    }
    
    public function login_tracking($custID, $conn){
        $login_select = $conn->query("SELECT * FROM logintracking WHERE CustID = '".$custID."'");
        return $login_select->fetch_all();
    }
    
    public function course_info($prodID,$conn){
        $course_info = $conn->query("SELECT Name from selfpaced WHERE ProdID = ".$prodID."");
        return $course_info->fetch_assoc();
    }
    
    public function sec_time_conversion($seconds){
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        $time_array = array(
            "Hour"=>$hours,
            "Min"=>$mins,
            "Sec"=>$secs
        );
        return $time_array;
    }
}

// get customer info
$cust_data = new CartSummary();
$cust_data = $cust_data->custinfo($custID);

//get course tracking info. 
$progress_tracking = new course_tracking();
$users_courses = $progress_tracking->get_users_courses($custID, $conn);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report: Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-sm-4">
               <h3>PMI Progress Report</h3>
               <hr>
                <table>
                    <tr>
                        <td>Name:</td>
                        <td><?php echo ucwords(strtolower($cust_data['Fname'])) . " ". ucwords(strtolower($cust_data['Lname']))?></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><?php echo ucwords(strtolower($cust_data['Address'])) ."<br>". ucwords(strtolower($cust_data['City'])) . " ". strtoupper($cust_data['State']) .", ".$cust_data['Zip']?></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><?php echo $cust_data['Phone']?></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><?php echo $cust_data['Email']?></td>
                    </tr>         
                </table>
            </div>
        </div>
    </div>
    <hr>
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
               <h4>Course Completion</h4>
               <table width="100%">
                  <tr>
                      <td>Course Name</td>
                      <td>Last Accessed</td>
                      <td>Total Time Spent</td>
                      <td>Course Length</td>
                      <td>Completed</td>
                  </tr>
                  <?php
                    // interation used to display data from $users_courses. array index correlation between cols. 
                    $i = 0;
                    while($i < sizeof($users_courses)){
                        $className = $progress_tracking->course_info($users_courses[$i][0], $conn);
                        $duration_class = $progress_tracking->total_course_duration($users_courses[$i][0], $conn);
                        $time_spent = $progress_tracking->sec_time_conversion($users_courses[$i][2]);
                        $total_duration = $progress_tracking->sec_time_conversion($duration_class['TotalDuration']);
                        $completed = $duration_class['TotalDuration'] - ($users_courses[$i][2] + 60);   
                        $last_accessed = date("m/d/Y", strtotime($users_courses[$i]['1']));
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $className['Name']?></td>
                            <td><?php echo $last_accessed;?></td>
                            <td><?php echo date('H:i:s', strtotime($time_spent['Hour'].':'.$time_spent['Min'].':'.$time_spent['Sec']))?></td>
                            <td><?php echo date('H:i:s', strtotime($total_duration['Hour'].':'.$total_duration['Min'].':'.$total_duration['Sec']))?></td>
                            <td><?php 
                            if($completed <= 0){
                                echo "Yes";
                                }
                                else{echo "No";}
                            ?></td>
                        </tr>
                        <?php
                    }
                  ?>
                </table>
                <hr>
            </div>
            <div class="col-xs-12 col-sm-5 col-sm-offset-1">
               <h4>Login History</h4>
               <table>
                  <tr>
                      <td>Date</td>
                      <td>Time</td>
                  </tr>
                   
                <?php 
                   $login_tracking = $progress_tracking->login_tracking($custID, $conn);
                    while($i < sizeof($login_tracking)){
                        $date = date("m/d/Y", strtotime($login_tracking[$i][2])); 
                        $time = date("H:i:s", strtotime($login_tracking[$i][2]));
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $date?></td>
                            <td><?php echo $time?></td>
                        </tr>
                        <?php 
                    }
                ?>
                </table>
            </div>

        </div>
    </div>
</body>
</html>