<?php     
include "../../includes/config.php";

class ForgotPWSequenceAddCust{
	
	//step One
	//function to create key.
	private function KeyGen(){
		$GneKey = md5(uniqid());
		return $GneKey;
	}
	
	//step two
	//Take the genKey and store in DB. 
	private function ForgotPWAction($email){
		//generate key with function. 
		$GneKey = new ForgotPWSequenceAddCust();
		$GneKey = $GneKey->KeyGen();
		
		$conn = Connect();
		$insert = "INSERT INTO resetpw (EncryptKey, Email) VALUE ('".$GneKey."', '".$email."')";
		$conn->query($insert);
		return $GneKey;
	}
	
	//Step Three
	//Gather all info, send email and exe above functions. 
	public function UserAuthAndEmailSend($email){
		//check if user is in DB
		$conn = Connect();
		$select = "SELECT Email FROM customers WHERE Email = '".$email."'";
		$result = $conn->query($select);
		
		if($result->num_rows>0){
		
		// call action function.
		$ForgotPW = new ForgotPWSequenceAddCust();	
		$ForgotPW = '<a class="mcnButton " title="Rest Password" href="'.baseurl().'scripts/genkeycheck.php?GenKey='.$ForgotPW->ForgotPWAction($email).'" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Reset Password</a>';

		
		// call and get mandrill template and send it with keygen as link.
		try {
		require_once '../../mandrill-api-php/src/Mandrill.php';
		$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
		$template_name = 'forgotpw-template';
		// content area, name = mcedit in main source code
		// content  = whats going in that container.
		$template_content = array(
			array(
				'name' => 'fname',
				'content' => $fullname
			),
			array(
				'name' => 'GenKey',
				'content' => $ForgotPW
			),				
		);
		$message = array(
			'html' => '<p>Product Center: Message Recieved</p>',
			'text' => 'Example text content',
			'subject' => 'PMI Portal Password Reset',
			'from_email' => 'webmaster@pmimd.net',
			'from_name' => 'pmiMD',
			'to' => array(
				array(
					'email' => $email, //email address
					'name' => $fullname, // name of client
					'type' => 'to'
				)
			),
			'important' => false,
			'track_opens' => false,
			'track_clicks' => false,
			'auto_text' => null,
			'auto_html' => null,
			'inline_css' => true,
			'url_strip_qs' => null,
			'preserve_recipients' => null,
			'view_content_link' => null,
			'bcc_address' => '', // bcc email address
			'tracking_domain' => null,
			'signing_domain' => null,
			'return_path_domain' => null,
			'merge' => true,
			'merge_language' => 'mailchimp',
			'global_merge_vars' => array(
				array(
					'name' => 'merge1',
					'content' => 'merge1 content'
				)
			),
			// Image needed for tracking opens
			'images' => array(
				array(
					'type' => 'image/png',
					'name' => 'IMAGECID',
					'content' => 'ZXhhbXBsZSBmaWxl'
				)
			)
		);
		//async is for bulk sending.
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = '2015-01-01';
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
		
		} 
		catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id ''
			throw $e;
		}
		}
	}
	
	//Delete key when key is correct
	//Embeded in GenKeyCheck function
	private function DeleteGenKey($GenKey){
		$conn = Connect();
		$drop = "DELETE FROM resetpw WHERE EncryptKey ='".$GenKey."'";
		
		$conn->query($drop);
	}
	
	//Step Four
	//Take code from email and check it is correct
	public function GenKeyCheck($GenKey){
		$conn = Connect();
		$select = "SELECT * FROM resetpw WHERE EncryptKey = '".$GenKey."'";
		$result = $conn->query($select);
		
		while($row = $result->fetch_assoc()){
			$email = $row['Email'];	
		}
		if($result->num_rows>0){
			$_SESSION['rest'] = 'set';
			$_SESSION['email'] = $email;
			$realKey = true;	
			

			$drop = new ForgotPWSequence();
			$drop->DeleteGenKey($GenKey);	
		}
		else{
			$realKey = false;
		}
		return $realKey;
	}
	
	//final step 
	//accept PW and change in DB. 
	public function ChangePW($pw, $email){
			$conn = Connect();
			$update = "UPDATE customers SET Password = '".$pw."' WHERE Email = '".$email."'";
			$conn->query($update);
	}
}

function Check_Grad_ID($GradID, $Fname, $Lname){
    // THis function will take 3 params to check if grad ID is active. and will return 1 for ture 0 for false.
    
    $conn = pmimain_connect();
    $gradCheck = "SELECT GradID FROM Current_Grad_Check WHERE GradID = ".$GradID." AND GradFirstName = '".$Fname."' AND GradLastName = '".$Lname."' GROUP BY GradID";
    
    $result = $conn->query($gradCheck);
    
    if($result->num_rows > 0){
        return 1;
    }
    else{
        return 0;
    }
}

$result = $_REQUEST;
$tempPass = md5(rand(500, 700));
$conn = Connect();

$insert = "INSERT INTO customers (Cert, Fname, Lname, Address, City, Zip, State, Email, Phone, Practice, Manager, Password)
            VALUES ('".$result['certID']."',
            '".ucfirst(strtolower($result['fname']))."',
            '".ucfirst(strtolower($result['lname']))."',
            '".$result['address']."',
            '".ucwords(strtolower($result['city']))."',
            '".$result['zip']."',
            '".$result['state']."',
            '".strtolower($result['email'])."',
            '".$result['phone']."',
            '".ucwords(strtolower($result['practice']))."',
            '".ucwords(strtolower($result['manager']))."',
            '".$tempPass."')";


$conn->query($insert);
$password = new ForgotPWSequenceAddCust;
$password->UserAuthAndEmailSend($result['email']);

$select = "SELECT ID FROM customers WHERE Email = '".$result['email']."' AND Fname = '".$result['fname']."' AND Lname = '".$result['lname']."'";

$result = $conn->query($select);

$id = $result->fetch_assoc();

echo $id['ID'];
?>
