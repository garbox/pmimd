<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';
session_start();
ReportLogInSessionCheck();
if($_SESSION['Role'] == "CS" or $_SESSION['Role'] == "Manager" ){
    header('Location: http://www.pmimd.com/onlinetraining/report/customersearch.php');
}
$conn = Connect();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
.ui-datepicker{
    width:inherit;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
 border:none;
    
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
    padding:5px;   
}
</style>

</head>
<body>
<!-- Nav bar -->
<?php include "includes/nav.php";?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12" style="margin-top:20px;">
            <form action="" class="form-inline DateRange">
                <div class="form-group">
                Start Date: <input class="form-control" type="text" id="startDate">
                </div>
                <div class="form-group">
                End Date: <input class="form-control" type="text"  id="endDate"><br>
                </div>
                <div class="form-group">
                <button class="btn btn-primary">Submit</button>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div id="AjaxLoad">
            
            </div>
        </div>
        <div class="col-xs-4">
            <div id="QuickReports">

            </div>
        </div>        
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#startDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
    $( "#endDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>
<script>
function currentMonth(){
	var d = new Date();
    var n = d.getMonth()+1;
	return n;	
}
</script>
<!-- Ajax -->
<script>
	$('.DateRange').on('submit', function(e){
        e.preventDefault();	
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();
		$("#AjaxLoad").load("<?php echo baseurl()?>report/script/accountingreport.php?startDate="+startDate+"&endDate="+endDate);
        $("#QuickReports").load("<?php echo baseurl()?>report/script/totalpostreport.php?startDate="+startDate+"&endDate="+endDate);

	});
</script>
</body>
</html>