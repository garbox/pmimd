<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';

session_start();
ReportLogInSessionCheck();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<!-- Nav bar -->
<div class="container-fluid">
	<div class="row">
        <?php include "includes/custnav.php";?>
    </div>
</div>

<!-- Preimitive search function for customer -->
<div class="container-fluid">
	<div class="row">
    	<div class="col-lg-12">
           	<table width="100%">
        	<tr style="border-bottom:1px solid #e6e6e6">
        		<td valign="top">First Name</td>
        		<td>Last Name</td>
        		<td>Address</td>
        		<td>City</td>
        		<td>State</td>
        		<td>Zip</td>
        		<td>Email</td>
        		<td>Phone</td>
        		<td>Practice</td>
                <td>Manager</td>
                <td colspan="2">Order Info</td>
        	</tr>
        <?php
		$find = $_GET['find'];
		$conn = Connect();
		$selectOrderID = "SELECT * FROM customers WHERE Fname LIKE '%".$find."%'
						OR Fname LIKE '%".$find."%'
						OR Lname LIKE '%".$find."%'
						OR State LIKE '%".$find."%'
						OR City LIKE '%".$find."%'
						OR Zip LIKE '%".$find."%'
						OR Address LIKE '%".$find."%'
						OR Email LIKE '%".$find."%'
						OR Manager LIKE '%".$find."%'
						OR Practice LIKE '%".$find."%'
						OR Phone LIKE '%".$find."%' ORDER BY ID ASC" ;
		
		
		$resultOrderID = $conn->query($selectOrderID);
		// gather info from receipt number (OrderID)
		while($rowCustomer = $resultOrderID->fetch_assoc()){
				foreach($rowCustomer as $Customer => $value) {
					$CustomerArray[$Customer] = $value;
				}
			?>
            <tr>
        		<td><?php echo ucwords(strtolower($CustomerArray['Fname']));?></td>
        		<td><?php echo ucwords(strtolower($CustomerArray['Lname']));?></td>
        		<td><?php echo ucwords(strtolower($CustomerArray['Address']));?></td>
        		<td><?php echo ucwords(strtolower($CustomerArray['City']));?></td>
                <td><?php echo strtoupper($CustomerArray['State']);?></td>
                <td><?php echo ucwords(strtolower($CustomerArray['Zip']));?></td>
                <td><?php echo ucwords(strtolower($CustomerArray['Email']));?></td>
                <td><?php echo ucwords(strtolower($CustomerArray['Phone']));?></td>
               	<td><?php echo ucwords(strtolower($CustomerArray['Practice']));?></td>
                <td><?php echo ucwords(strtolower($CustomerArray['Manager']));?></td>
                <td><a href="<?php echo baseurl()?>/report/portal/index.php?ID=<?php echo $CustomerArray['ID']?>"><button class="btn btn-primary">View Info</button></a></td>
                <?php 
				$selectOrder = "SELECT * FROM orderplaced WHERE CustomerID = '".$CustomerArray['ID']."'";
				$resultOrder = $conn->query($selectOrder);
					if($resultOrder->num_rows>0){
						?>
                        <td width="100">Ordered <span style="color:green" class="glyphicon glyphicon-ok"></span></td>
                        <?php
					}
				?>
        	</tr>
            <tr>
            <?php
			$code = $find;
		}
		?>
        </tr>
        </table>

        </div>
    </div>
</div>

