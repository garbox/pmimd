<?php 
include 'includes/reportfunctions.php';
include '../includes/config.php';
session_start();
ReportLogInSessionCheck();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2, h3, h4{
	color:#5371AD;
}
    
</style>
</head>

<body>
<?php include "includes/nav.php"?>

<div class="container-fluid">
    <div class="row">
       <!-- Enter User Info -->
        <div class="col-sm-4 col-sm-offset-4">
           <h3>Enter Customers Info</h3>
           <hr>
            <form action="script/addcustomer.php" method="post" id="custinfoform">
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label for="" style="font-weight:normal">First Name</label>
                        <input class="form-control fname" type="text" name="fname" placeholder="First Name" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="" style="font-weight:normal">Last Name</label>
                        <input class="form-control lname" type="text" name="lname" placeholder="Last Name" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label for="" style="font-weight:normal">Email Address</label>
                        <input class="form-control email" type="text" name="email" placeholder="Email" required>
                  </div>
                    <div class="col-sm-6">
                        <label for="" style="font-weight:normal">Phone Number</label>
                        <input class="form-control phone" type="text" name="phone" placeholder="Phone" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label for="" style="font-weight:normal">Address</label>
                        <input class="form-control address" type="text" name="address" placeholder="Address" required>
                    </div>
                    <div class="col-sm-2">
                        <label for="" style="font-weight:normal">Zip</label>
                        <input class="form-control zip" type="text" name="zip" placeholder="Zip" required>
                    </div>
                    <div class="col-sm-4">
                        <label for="" style="font-weight:normal">City</label>
                        <input class="form-control city" type="text" name="city" placeholder="City" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" style="font-weight:normal">State</label>
                    <select id="state" class="form-control" name="state" required>
                        <option value="false" Selected>Please Select Your State
                        <option value="AK">Alaska
                        <option value="AL">Alabama
                        <option value="AZ">Arizona
                        <option value="AR">Arkansas
                        <option value="CA">California
                        <option value="CO">Colorado
                        <option value="CT">Connecticut
                        <option value="DE">Delaware
                        <option value="DC">District of Columbia
                        <option value="FL">Florida
                        <option value="GA">Georgia
                        <option value="HI">Hawaii
                        <option value="IA">Iowa
                        <option value="ID">Idaho
                        <option value="IL">Illinois
                        <option value="IN">Indiana
                        <option value="KS">Kansas
                        <option value="KY">Kentucky
                        <option value="LA">Louisiana
                        <option value="ME">Maine
                        <option value="MA">Massachusetts
                        <option value="MD">Maryland
                        <option value="MI">Michigan
                        <option value="MN">Minnesota
                        <option value="MS">Mississippi
                        <option value="MO">Missouri
                        <option value="MT">Montana
                        <option value="NE">Nebraska
                        <option value="NV">Nevada
                        <option value="NH">New Hampshire
                        <option value="NJ">New Jersey
                        <option value="NM">New Mexico
                        <option value="NY">New York
                        <option value="NC">North Carolina
                        <option value="ND">North Dakota
                        <option value="OH">Ohio
                        <option value="OK">Oklahoma
                        <option value="OR">Oregon
                        <option value="PA">Pennsylvania
                        <option value="RI">Rhode Island
                        <option value="SC">South Carolina
                        <option value="SD">South Dakota
                        <option value="TN">Tennessee
                        <option value="TX">Texas
                        <option value="UT">Utah
                        <option value="VT">Vermont
                        <option value="VA">Virginia
                        <option value="WA">Washington
                        <option value="WV">West Virginia
                        <option value="WI">Wisconsin
                        <option value="WY">Wyoming
                        </select>
                </div>
                <hr>
                    <h4>Other Information</h4>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="" style="font-weight:normal">Certification ID</label>
                            <input class="form-control email" type="text" name="certID" placeholder="CertID">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Practice Name</label>
                            <input class="form-control email" type="text" name="practice" placeholder="Practice Name">
                        </div>
                        <div class="col-sm-6">
                            <label for="" style="font-weight:normal">Manager</label>
                            <input class="form-control email" type="text" name="manager" placeholder="Manger">
                        </div>
                    </div>
                        <button type="submit" class="btn btn-primary" style="width: 100%">Submit</button>
        </div>
            
        </form>
               
<!-- Modal -->
<div id="SecuessModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Account Created</h4>
      </div>
      <div class="modal-body">
        <p>Account has been created successfully!</p>
        <p>Please inform the customer they will recieve an email to reset their password.</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" id="ModalButton">I have informed the customer.</button>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>/assets/js/bootstrap.min.js"></script>
<script>
    $("#confernece_info").css('display', "none"); 
    $("#con_layout").removeClass("col-lg-6").addClass("col-lg-12");
    
    $( "#classID" ).change(function() {
        var Data = $( this ).val();
        if(Data == '22673-1109' || Data == '22671-1108' || Data == '22672-1108'){
           $("#confernece_info").css('display', "inline-block") 
           $("#con_layout").removeClass("col-lg-12").addClass("col-lg-6");
        }
        else{
            $("#confernece_info").css('display', "none"); 
            $("#con_layout").removeClass("col-lg-6").addClass("col-lg-12");
        }
    });  
    
    function modelPopUp(data){
        $('#SecuessModel').modal('show');
        $('#ModalButton').on("click", function(){
            window.location = '<?php echo baseurl();?>report/portal/index.php?ID=' + data;
        })
    }
    // Below is the json obj that will send all the data to cybersource. 
    $('#custinfoform').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $("#custinfoform").prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                  modelPopUp(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	});
</script>
</body>
</html>
