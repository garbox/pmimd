<?xml version='1.0' encoding='UTF-8' ?>
<rss version='2.0'>

<channel>
  <title>Practice Management Institue: Webinar Schedule </title>
  <link>https://www.pmimd.com/audio/</link>
  <description>PMI Webianrs</description>
  <event>
    <item>
      <title>Winning Carrier Denial Appeals</title>
      <date>2018-03-21<date/>
      <time>12:00<time/>
      <description>This webinar could save your practice thousands of dollars recouped from denied claims.<description/>
      <link>https://www.pmimd.com/programs/a-wcdaudio.asp<link/>
      <endtime>13:30<endtime/>
      <cost>199</cost>
    </item>
  </event>
</channel>
</rss>