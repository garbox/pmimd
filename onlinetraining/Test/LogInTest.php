<?php include '../includes/Functions.php';?>
<?php 
session_start();
CookieforCart();	
SessionCheck();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<style>

html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.Cart-Container p.cartNav {
	display:inline;	
	font-size:20px;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

a:hover{
	text-decoration:none;	
}
</style>
</head>

<!-- Google Analytics Script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73530823-1', 'auto');
  ga('send', 'pageview');

</script>


<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../Includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block">
                <img src="http://pmimd.com/productcenter/images/smallHeader.png" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- PMI Logo -->
<div class="container-fluid" style="padding-top:30px; background-color:#e6e6e6;">
    <div class="row">
        <div class="container">
      		<div align="center"> 
            <img src="http://www.pmimd.com/images/PMILogoBlack.png" alt="">
           </div>
    	</div>
 	</div>            
 </div>   
 
<!-- Login form -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:507px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Login</h2>
                        <p class="error"></p>
                        <hr>
                        <div class="wait">
                        	<img class='center-block img-fluid'src='http://pmimd.com/totalaccess/img/Test.gif'>
                        </div>
                        <form class="LogInForm" method="post" action="Auth2.php" >
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?php echo $_SESSION['email'];?>">
                          </div>
                           <div class="form-group">
                            <input type="password" class="form-control" name="pw" placeholder="Password">
                          </div>                         
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                        <a href="ForgottenPassWord/ForgotPassword.php"><p style="font-size:14px;margin-top:5px;">Forgot Password?</p></a>
                        <hr>
                        <div class="newtopmi">
                            <a href="CreateAccount.php">
                            <h4 align="center">Create a new account</h4></a>
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include '../Includes/Footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://pmimd.com/css/bootstrap/ProdCenter/js/bootstrap.min.js"></script>

<!-- Ajax --><script>
	$('.wait').hide();
	$('.LogInForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.LogInForm').prop('action');   		
		// get current location url			
		$('.LogInForm').hide();
		$('.newtopmi').hide();
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
					console.log(data);
				  }
				  else{
					console.log(data);
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
</script>


</body>
</html>
  <link rel="stylesheet" href="http://pmimd.com/css/bootstrap/ProdCenter/css/bootstrap.css">
  <link rel="stylesheet" href="http://pmimd.com/css/bootstrap/ProdCenter/css/style.css">

