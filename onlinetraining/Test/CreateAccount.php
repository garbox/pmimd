<?php include '../includes/Functions.php';?>
<?php CookieforCart();
session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://pmimd.com/css/bootstrap/ProdCenter/css/bootstrap.css">
  <link rel="stylesheet" href="http://pmimd.com/css/bootstrap/ProdCenter/css/style.css">
<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.Cart-Container p {
	display:inline;	
	font-size:20px;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

</style>
</head>

<!-- Google Analytics Script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73530823-1', 'auto');
  ga('send', 'pageview');

</script>

<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block"><img src="http://pmimd.com/productcenter/images/smallHeader.png" alt="" width="100%"></div>
            </div>
        </div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>
 
<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:538px">
    <div class="row">
        <div class="container"> 
            <div class="row"> 
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
              	  	<div class="Cart-Container">
                    <h2 align="center">Hi! Welcome to PMI!</h2>
                    <hr>
                    <div class="success"></div>
                        <form class="submitform" method="post" action="AddUser.php" >
                        <label for="">Basic Info</label>
                          <div class="form-group">
                            <input type="text" class="form-control fname" name="firstname" placeholder="First Name">
                          </div>
                          <div class="form-group">
                          	<input type="text" class="form-control lname" name="lastname" placeholder="Last Name">
                          </div> 
                          <div class="form-group">
                            <input type="text" class="form-control phone" name="phone" placeholder="Phone Number">
                          </div> 
                          <div class="form-group">
                            <input type="address" class="form-control address" name="address" placeholder="Address">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control city" name="city" placeholder="City">
                          </div>  
                          <div class="form-group">
                            <input type="text" class="form-control zip" name="zip" placeholder="Zip">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control state" name="state" placeholder="State">
                          </div>
                          <label for="">Account Info</label>
                          <div class="form-group">
                            <input type="email" class="form-control email" name="email" placeholder="Email" value="<?php echo $_SESSION['email'];?>">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control pw" name="pw" placeholder="Password">
                          </div>
                          <label for="">Practice Info</label>
                          <div class="form-group">
                            <input type="text" class="form-control" name="manager" placeholder="Your Managers Name">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="practice" placeholder="Practice Name">
                          </div>  
                          <label for="">Certification Information</label><br>
                          <p style="color:#c01313; font-size:12px;">Reqired for PMI discount</p>
                          <div class="form-group">
                            <input type="text" class="form-control" name="certID" placeholder="Certification ID Number">
                          </div>                                                     
                          <button type="submit" class="btn btn-default btn-block">Submit</button>
                        </form>
                        <div class="error bg-danger" style="padding:5px; margin-top:10px"></div>
                    </div>
        		  </div>                
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include '../Includes/Footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://pmimd.com/css/bootstrap/ProdCenter/js/bootstrap.min.js"></script>

<script>
/* Ajax */
$('.phone').keyup(function () {
	$(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3"));
});
$('.error').hide();
$('.submitform').on('submit', function(e){	
	var inputEmpty = false;
	//empty containers when exicuted to remove error messages. 
	$('.error').empty();
	
	// reset css for inputs
	$('.fname').css('border-color', '#ccc');
	$('.lname').css('border-color', '#ccc');
	$('.email').css('border-color', '#ccc');
	$('.phone').css('border-color', '#ccc');
	$('.address').css('border-color', '#ccc');	
	$('.zip').css('border-color', '#ccc');
	$('.city').css('border-color', '#ccc');
	$('.state').css('border-color', '#ccc');
	
	// make sure field isnt empty
	if( $('.fname').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.fname').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.lname').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.lname').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.email').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.email').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.phone').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.phone').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.address').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.address').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.zip').val() == ""){
	e.preventDefault();		
	inputEmpty = true;
	$('.zip').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.city').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.city').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.state').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.state').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.pw').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.pw').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	/* Ajax Section */
	if(inputEmpty == false){
		var values = $('.submitform').serialize();
		// get value of action attribute   
		var desination = $('.submitform').prop('action');   		
		// get current location url	
		$('.submitform').hide();		
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
					$('.wait').show();
				  	$('.success').show().html('<p style="font-size:16px;">Your account has been created. Please login with your new password and username by clicking the button below.</p><br><br><a href="http://www.pmimd.com/productcenter/Client/Login.php"><button class="btn btn-primary">Login</button></a>');	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	}
});

</script>
</body>
</html>

