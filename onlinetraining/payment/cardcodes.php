<!-- 
Reason Code Source File

The point of this code is to create functions for the marjor reason codes for Cybersource. 
This will allow us to attach madrill templates to these functions with ease. 
Each one gets a specific email relating to the content. 
This will be shown on the Confirmation.php under Payment in product center. 

Kyle Metivier
Created: 10/18/2016
Edit: 2/14/2017
-->
<?php 
//Temp function for cmc message. remove once message is no longer needed. 
function cmca_message(){
    $conn = Connect();
    $select_query ="SELECT * FROM cart WHERE CookieID = '".$_COOKIE['cartIDs']."' AND ProdID = 165";
    $result = $conn->query($select_query);
    if($result->num_rows > 0){
        $count_select ="SELECT * FROM orderplaced WHERE ProdID = 165";
        $result = $conn->query($count_select);
        $count = $result->num_rows +1;
        return "<tr><td valign='top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'><p>Please note that the CMCA-E/M content will not be available to access online until early April. You will receive an email notifying you when it has been uploaded. Your course manual will be mailed in early April. If you have difficulty accessing the content or have any questions, contact PMI at 800-259-5562 or email: info@pmiMD.com. Customer service is available 8-5 Central, M-F.<br><br>Right now you are currently number ".$count.".</p></td></tr>";
    }
}

// main function that will take reasoncode and suplly the corect respounce via swich statement.
function cardCode($orderData, $id){
	switch($orderData['reason_code']){
		// Secuess Payment.
		case "100":
		code100($orderData, $id);
		break;
		
		//missing fields
		case "101":
		code101();
		break;
		
		//Invalid Data
		case "102":
		code102($ASCArray);
		break;
		
		//Duplicate Transaction, Check your account for products. 
		case "104":
		code104();
		break;
		
		//Time Out
		case "150":
		code150();
		break;
		
		//Time Out
		case "151":
		code150();
		break;
		
		// Expird Card
		case "202":
		code202();
		break;
        
        // Expird Card
		case "203":
		code203();
		break;
		
		// No funds
		case "204":
		code204();
		break;
		
		//Stolen Or lost card
		case "205":
		code205();
		break;
		
		//Decline - Issuing bank unavailable.
		case "207":
		code207();
		break;
		
		//Decline - Inactive card
		case "208":
		code208();
		break;
		
		//Decline - card verification number 
		case "209":
		code209();
		break;
		
		//Decline - card verification number 
		case "211":
		code209();
		break;
		
		//credit limit maxed
		case "210":
		code210();
		break;
		
		//Decline - Invalid account number
		case "231":
		code231();
		break;
		
		//address Error Check address
		case "450":
		code450();
		break;
		
		//address Error Check address
		case "451":
		code450();
		break;
		
		//address Error Check address
		case "452":
		code450();
		break;
		
		//address Error Check address
		case "453":
		code450();
		break;
		
		//address Error Check address
		case "454":
		code450();
		break;
		
		//address Error Check address
		case "455":
		code450();
		break;
		
		//address Error Check address
		case "456":
		code450();
		break;
		
		//address Error Check address
		case "457":
		code450();
		break;
		
		//address Error Check address
		case "458":
		code450();
		break;
		
		//address Error Check address
		case "459":
		code450();
		break;
		
		//General Error, try again or contact helpdesk. 
		default:
		?>
		<h2>Uh-Oh</h2>
		<p>Looks like there is a problem with this card. Please try a different payment method.</p><br>
		<a href="<?php echo securebaseurl()?>payment/pay_info.php?"<?php echo $_SESSION['Profile']['ID'];?>><button class="btn btn-primary">Try again</button></a>
		<?php	
	}
}


// This function is exe when cardCode() reasoncode is 100. 
// This includes empty cart, add prods to customer portal and add prods to orders placed. 
// also creates a html of the recipt and adds to payments/receipts/sp
// need to figure out a better file naming system incase user needs receipt at a later time. 
function PDFReceipt($orderData){
	$name = $orderData['req_bill_to_forename'] ." ". $orderData['req_bill_to_surname'];
    $address = $orderData['req_bill_to_address_line1'] . "<br>" . $orderData['req_bill_to_address_city'] ." ". $orderData['req_bill_to_address_state']. ", " . $orderData['req_bill_to_address_postal_code'];
    
	$FileName = "SP-Receipt-".$_SESSION['OrderIDShort']."-".date("mdY-His");
	$prodDisplayEmail = ProdEmailDisplay();
	$myfile = fopen("../receipt/sp/".$FileName.".html", "w") or die(print_r(error_get_last()));
	$txt = "<!DOCTYPE html><html xmlns=http://www.w3.org/1999/xhtml xmlns:o=urn:schemas-microsoft-com:office:office xmlns:v=urn:schemas-microsoft-com:vml><head><!--[if gte mso 15]><xml><o:officedocumentsettings><o:allowpng><o:pixelsperinch>96</o:pixelsperinch></o:officedocumentsettings></xml><![endif]--><meta charset=UTF-8><meta content='IE=edge'http-equiv=X-UA-Compatible><meta content='width=device-width,initial-scale=1'name=viewport><title>*|SUBJECT|*</title><style>p{margin:10px 0;padding:0}table{border-collapse:collapse}h1,h2,h3,h4,h5,h6{display:block;margin:0;padding:0}a img,img{border:0;height:auto;outline:0;text-decoration:none}#bodyCell,#bodyTable,body{height:100%;margin:0;padding:0;width:100%}#outlook a{padding:0}img{-ms-interpolation-mode:bicubic}table{mso-table-lspace:0;mso-table-rspace:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}a,blockquote,li,p,td{mso-line-height-rule:exactly}a[href^=sms],a[href^=tel]{color:inherit;cursor:default;text-decoration:none}a,blockquote,body,li,p,table,td{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}#bodyCell{padding:10px}.templateContainer{max-width:600px!important}a.mcnButton{display:block}.mcnImage{vertical-align:bottom}.mcnTextContent{word-break:break-word}.mcnTextContent img{height:auto!important}.mcnDividerBlock{table-layout:fixed!important}#bodyTable,body{background-color:#eee}#bodyCell{border-top:0}.templateContainer{border:0}h1{color:#5371ad;font-family:Verdana,Geneva,sans-serif;font-size:26px;font-style:normal;font-weight:400;line-height:125%;letter-spacing:normal;text-align:left}h2{color:#5371ad;font-family:Verdana,Geneva,sans-serif;font-size:22px;font-style:normal;font-weight:400;line-height:125%;letter-spacing:normal;text-align:left}h3{color:#5371ad;font-family:Verdana,Geneva,sans-serif;font-size:20px;font-style:normal;font-weight:400;line-height:125%;letter-spacing:normal;text-align:left}h4{color:#202020;font-family:Helvetica;font-size:18px;font-style:normal;font-weight:700;line-height:125%;letter-spacing:normal;text-align:left}#templatePreheader{background-color:#FAFAFA;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:0;padding-bottom:0}#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{color:#656565;font-family:Verdana,Geneva,sans-serif;font-size:12px;line-height:150%;text-align:left}#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{color:#656565;font-weight:400;text-decoration:underline}#templateHeader{background-color:#FFF;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:10px;padding-bottom:10px}#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{color:#999;font-family:Verdana,Geneva,sans-serif;font-size:16px;line-height:150%;text-align:left}#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{color:#5371af;font-weight:400;text-decoration:none}#templateBody{background-color:#FFF;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:2px solid #EAEAEA;padding-top:0;padding-bottom:9px}#templateBody .mcnTextContent,#templateBody .mcnTextContent p{color:#999;font-family:Verdana,Geneva,sans-serif;font-size:16px;line-height:150%;text-align:left}#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{color:#5371ad;font-weight:400;text-decoration:underline}#templateFooter{background-color:#414549;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px}#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{color:#999;font-family:Verdana,Geneva,sans-serif;font-size:12px;line-height:150%;text-align:center}#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{color:#656565;font-weight:400;text-decoration:underline}@media only screen and (min-width:768px){.templateContainer{width:600px!important}}@media only screen and (max-width:480px){a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}}@media only screen and (max-width:480px){body{width:100%!important;min-width:100%!important}}@media only screen and (max-width:480px){#bodyCell{padding-top:10px!important}}@media only screen and (max-width:480px){.mcnImage{width:100%!important}}@media only screen and (max-width:480px){.mcnBoxedTextContentContainer,.mcnCaptionBottomContent,.mcnCaptionLeftImageContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightImageContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionTopContent,.mcnCartContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageGroupContentContainer,.mcnRecContentContainer,.mcnTextContentContainer{max-width:100%!important;width:100%!important}}@media only screen and (max-width:480px){.mcnBoxedTextContentContainer{min-width:100%!important}}@media only screen and (max-width:480px){.mcnImageGroupContent{padding:9px!important}}@media only screen and (max-width:480px){.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{padding-top:9px!important}}@media only screen and (max-width:480px){.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent,.mcnImageCardTopImageContent{padding-top:18px!important}}@media only screen and (max-width:480px){.mcnImageCardBottomImageContent{padding-bottom:9px!important}}@media only screen and (max-width:480px){.mcnImageGroupBlockInner{padding-top:0!important;padding-bottom:0!important}}@media only screen and (max-width:480px){.mcnImageGroupBlockOuter{padding-top:9px!important;padding-bottom:9px!important}}@media only screen and (max-width:480px){.mcnBoxedTextContentColumn,.mcnTextContent{padding-right:18px!important;padding-left:18px!important}}@media only screen and (max-width:480px){.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{padding-right:18px!important;padding-bottom:0!important;padding-left:18px!important}}@media only screen and (max-width:480px){.mcpreview-image-uploader{display:none!important;width:100%!important}}@media only screen and (max-width:480px){h1{font-size:22px!important;line-height:125%!important}}@media only screen and (max-width:480px){h2{font-size:20px!important;line-height:125%!important}}@media only screen and (max-width:480px){h3{font-size:18px!important;line-height:125%!important}}@media only screen and (max-width:480px){h4{font-size:16px!important;line-height:150%!important}}@media only screen and (max-width:480px){.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{font-size:14px!important;line-height:150%!important}}@media only screen and (max-width:480px){#templatePreheader{display:block!important}}@media only screen and (max-width:480px){#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{font-size:14px!important;line-height:150%!important}}@media only screen and (max-width:480px){#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{font-size:16px!important;line-height:150%!important}}@media only screen and (max-width:480px){#templateBody .mcnTextContent,#templateBody .mcnTextContent p{font-size:16px!important;line-height:150%!important}}@media only screen and (max-width:480px){#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{font-size:14px!important;line-height:150%!important}}</style><center><table border=0 cellpadding=0 cellspacing=0 width=100% align=center height=100% id=bodyTable><tr><td valign=top id=bodyCell align=center><!--[if gte mso 9]><table border=0 cellpadding=0 cellspacing=0 width=600 style=width:600px align=center><tr><td valign=top align=center width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=templateContainer width=100%><tr><td valign=top id=templatePreheader><table border=0 cellpadding=0 cellspacing=0 class=mcnCodeBlock width=100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner><table border=0 cellpadding=0 cellspacing=0 class=responsive-table width=100% align=center><tr><td bgcolor=#414549 width=25% height=10><td bgcolor=#5371AD width=25%><td bgcolor=#0CA24B width=25%><td bgcolor=#C01313 width=25%></table></table><tr><td valign=top id=templateHeader><table border=0 cellpadding=0 cellspacing=0 class=mcnImageBlock width=100% style=min-width:100%><tbody class=mcnImageBlockOuter><tr><td valign=top class=mcnImageBlockInner style=padding:0><table border=0 cellpadding=0 cellspacing=0 class=mcnImageContentContainer width=100% style=min-width:100% align=left><tr><td valign=top class=mcnImageContent style=padding-right:0;padding-left:0;padding-top:0;padding-bottom:0;text-align:center><img align=center alt=''class=mcnImage src=https://gallery.mailchimp.com/398c28b27db7d75b66accc62a/images/32df9074-fd2d-4b56-a560-49bf9c830002.png style=max-width:600px;padding-bottom:0;display:inline!important;vertical-align:bottom width=600></table></table><tr><td valign=top id=templateBody><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerBlock width=100% style=min-width:100%><tbody class=mcnDividerBlockOuter><tr><td class=mcnDividerBlockInner style=min-width:100%;padding:18px><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerContent width=100% style='min-width:100%;border-top:2px solid #EAEAEA'><tr><td><span></span></table></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><h2 class=null><span style=font-size:18px>Billing Info:</span></h2></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><div style=text-align:left><span mc:edit=fullName>".$name."</span><br><span mc:edit=address>".$address."</span><br><span mc:edit=phone>".$orderData['req_bill_to_phone']."</span><br>Order ID: <span mc:edit=orderID>".$orderData['req_reference_number']."</span></div></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerBlock width=100% style=min-width:100%><tbody class=mcnDividerBlockOuter><tr><td class=mcnDividerBlockInner style=min-width:100%;padding:18px><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerContent width=100% style='min-width:100%;border-top:2px solid #EAEAEA'><tr><td><span></span></table></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><h2 class=null><span style=font-size:18px>What you ordered:</span></h2></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><span mc:edit=cart>".$prodDisplayEmail."</span></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerBlock width=100% style=min-width:100%><tbody class=mcnDividerBlockOuter><tr><td class=mcnDividerBlockInner style=min-width:100%;padding:18px><table border=0 cellpadding=0 cellspacing=0 class=mcnDividerContent width=100% style='min-width:100%;border-top:2px solid #EAEAEA'><tr><td><span></span></table></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><h2 class=null><span style=font-size:18px>How to access pmiMD Protal</span></h2></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px>Some paragraph on how to access pmiMD portal and a link for the profile also.</table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table><table border=0 cellpadding=0 cellspacing=0 class=mcnButtonBlock width=100% style=min-width:100%><tbody class=mcnButtonBlockOuter><tr><td valign=top class=mcnButtonBlockInner style=padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px align=right><table border=0 cellpadding=0 cellspacing=0 class=mcnButtonContentContainer style=border-collapse:separate!important;border-radius:0;background-color:#5371AD><tr><td valign=middle class=mcnButtonContent style=font-family:Arial;font-size:16px;padding:10px align=center><a href=http://pmimd.com/onlinetraining/Client/Portal.php style=font-weight:400;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#FFF class=mcnButton target=_blank title='pmiMD Profile'>pmiMD Profile</a></table></table><tr><td valign=top id=templateFooter><table border=0 cellpadding=0 cellspacing=0 class=mcnCodeBlock width=100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner><table width=66% align=center><tr><td align=center><a href=http://www.pmimd.com/programs/locator.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Live Classes |</a><td align=center><a href=http://www.pmimd.com/audio/default.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Live Webinar |</a><td align=center><a href=http://www.pmimd.com/certify/default.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Certifications |</a><td align=center><a href=http://www.pmimd.com/certify/products2.1.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Self-Paced</a></table><table align=center><tr><td align=center><a href=http://www.pmimd.com/ style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>PMIMD |</a><td align=center><a href=http://www.pmimd.com/LV2016 style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Conference |</a><td align=center><a href=*|UNSUB|* style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Unsubscribe |</a><td align=center><a href=http://www.pmimd.com/subscribe2.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Subscribe |</a><td align=center><a href=http://www.pmimd.com/privacy.asp style='text-decoration:none;color:#fff;font-family:Segoe,'Segoe UI','DejaVu Sans','Trebuchet MS',Verdana,sans-serif'>Privacy</a></table></table><table border=0 cellpadding=0 cellspacing=0 class=mcnTextBlock width=100% style=min-width:100%><tbody class=mcnTextBlockOuter><tr><td valign=top class=mcnTextBlockInner style=padding-top:9px><!--[if mso]><table border=0 cellpadding=0 cellspacing=0 width=100% style=width:100% align=left><tr><![endif]--><!--[if mso]><td valign=top width=600 style=width:600px><![endif]--><table border=0 cellpadding=0 cellspacing=0 class=mcnTextContentContainer width=100% style=max-width:100%;min-width:100% align=left><tr><td valign=top class=mcnTextContent style=padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px><div style=text-align:center>© 2016 Practice Management Institute®. All rights reserved.<br>CPT<sup>®</sup> is a registered trademark of the American Medical Association. All rights reserved.</div></table><!--[if mso]><![endif]--><!--[if mso]><![endif]--></table></table><!--[if gte mso 9]><![endif]--></table></center>";
	fwrite($myfile, $txt);
	fclose($myfile);
}

// **************** Start of card Codes Function *******************
// Accepted Payment
// Send Receipt via email and stored PDF on server
function code100($orderData, $id){ 
?>
    <!-- HTML Message -->
    <h2>Thank You!</h2>
    <p>Your order is complete.<br>Click the button below to view/access your purchase.</p><br>
    <a href="<?php echo baseurl()?>client/portal.php"><button class="btn btn-primary">Current Content</button></a><br><br>
    <p>Order number: <?php echo $orderData['req_reference_number']?></p>
	<?php
    //crate object for cart
    $cart_obj = new CartSummary;
    
    // declear vars for cart obj
    $cust_data = $cart_obj->custinfo($id);
    $cart_data = $cart_obj->cartinfo($cust_data['Email']); // Cart Info
    $cart_summary = $cart_obj->main($id);
	$emailCart = $cart_obj->CartSummaryDisplay($cart_data, $cart_summary);
	
    //general user infomation into vars
    $name = $cust_data['Fname']. " ". $cust_data['Lname'];
    $address = $cust_data['Address'] . "<br>" .$cust_data['City'] . ", " .$cust_data['State'] . " " .$cust_data['Zip'];
    
	//Mandrill Send-Template API
	try {
		require_once '../mandrill-api-php/src/Mandrill.php';
		$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
		$template_name = 'prodcenter-reciept';
		// content area, name = mcedit in main source code
		// content  = whats going in that container.
		$template_content = array(
			array(
				'name' => 'fullName',
				'content' => $name
			),
			array(
				'name' => 'address',
				'content' => $address
			),
			array(
				'name' => 'phone',
				'content' => $cust_data['Phone']
			),
			array(
				'name' => 'cart',
				'content' => ProdEmailDisplay()
			),	
			array(
				'name' => 'orderID',
				'content' => $orderData['req_reference_number']
			),
			array(
				'name' => 'shipping',
				'content' => $shippingInfo
			),
            array(
				'name' => 'date',
				'content' => date("m/j/Y")
			),
            array(
				'name' => 'cmca_count',
				'content' => cmca_message()
			),
										
		);
		$message = array(
			'html' => '<p>Your Receipt</p>',
			'text' => 'Example text content',
			'subject' => 'pmiMD: Your Order Receipt',
			'from_email' => 'receipt@pmimd.com',
			'from_name' => 'pmiMD Portal',
			'to' => array(
				array(
					'email' => $orderData['req_bill_to_email'], //email address
					'name' => $name, // name of client
					'type' => 'to'
				),
				array(
					'email' => 'jward@pmimd.com', //email address
					'name' => 'Jeanie Ward', // name of client
					'type' => 'to'
				),
				array(
					'email' => 'emolina@pmimd.com', //email address
					'name' => 'Elizabeth Molina', // name of client
					'type' => 'to'
					)
				
			),
			'important' => false,
			'track_opens' => true,
			'track_clicks' => true,
			'auto_text' => null,
			'auto_html' => null,
			'inline_css' => true,
			'url_strip_qs' => null,
			'preserve_recipients' => null,
			'view_content_link' => null,
			'bcc_address' => 'info@pmimd.com', // bcc email address
			'tracking_domain' => null,
			'signing_domain' => null,
			'return_path_domain' => null,
			'merge' => true,
			'merge_language' => 'mailchimp',
			'global_merge_vars' => array(
				array(
					'name' => 'merge1',
					'content' => 'merge1 content'
				)
			),
			'images' => array(
				array(
					'type' => 'image/png',
					'name' => 'IMAGECID',
					'content' => 'ZXhhbXBsZSBmaWxl'
				)
			)
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = '2015-01-01';
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
		
	} 
	catch(Mandrill_Error $e) {
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
	}
                                  
    //Tracking Functions                              
    McEcommerce($orderData, $_COOKIE['mc_cid']);  
    Tracking_Code($orderData, $_COOKIE['tc']);      
    
    // Order Processing Functions. 
	ShippingInfoStore($orderData);
    PDFReceipt($orderData);
	DiscountUsage($_SESSION['RCode'],$_SESSION['Profile']['ID']);
    ReferralComp($_SESSION['RCode'], $_SESSION['Profile']['ID']);
	OrderPlaced($orderData, $id);
	AddProdToProtal($_COOKIE['cartIDs']);
}

// Missing Info
function code101($orderData){
	?>
	<h2>Oops</h2>
	<p>Something is missing. Please review your order below.</p><br>
    <p>Error message: <?php echo $orderData['message']?></p>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Review Order</button></a>
	<?php
}

// Invalid Data
function code102($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like there is some invalid data.<br>Please check to ensure all information is in the right format.<br><br>[hint: check <?php echo str_replace(',', ", ", str_replace('_', ' ', $AscArray['invalid_fields']." "))?> ]</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Check your info here</button></a>
    <?php
}

// Duplicate order (Same merc ID, customer needs to refresh Payinfo.php page)
function code104(){	
	?>
	<h2>Hmm</h2>
	<p>Looks like this order was already submited. Please click button below.<br> If you get stuck, please feel to contact us at 800-259-5562</p><br>
	<a href="../Client/Portal.php"><button class="btn btn-primary">Your PMI Profile</button></a>
	<?php
}

// Timeout Try again 
// CODE: 150 , 151
function code150($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like we are having trouble processing your payment.<br>Please wait a few min and try again by clicking below.<br>If you need help, contact us at 800-259-5562 for assistance.</p><br>
    <p>Error message: <?php echo $orderData['message']?></p>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Card Expired
function code202(){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like your card has expired, please check your payment info again or try another card.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Check payment info</button></a>
	<?php
}

// General Decline Try another Card
function code203($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like there was a general error, please try using another card.</p><br>
    <p>Error message: <?php echo $orderData['message']?></p>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Insufficient Funds
function code204(){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like is insufficient funds, please try another card.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Lost or stolen card (Possible Fraud)
function code205(){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like this card was reported lost or stolen. Please call your credit card institution to have this resolved, or try another card. </p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Unable to reach Bank
function code207(){	
	?>
	<h2>Uh-Oh</h2>
	<p>It looks like we are having connection issues with your financial institution. Please try again.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Card Not Active Customer Needs to Activate Card. 
function code208($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like this card has not been activated yet. Please try another card or contanct your finacial insitution to activate it.U</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Click here to try again</button></a>
	<?php
}

// Wrong CVN Number
// Code 209 , 211
function code209($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like your we need a Card Verification Number from you. Please give us a call at 1-800-259-5562</p><br>
	<?php
}

// Credit Limit Reached
function code210($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like this card has reached its credit limit. Please try another card.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Try again</button></a>
	<?php
}

// Invalid Card Number (Ask customer to check card number)
function code231($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like the card number you entered is incorrect. Please verify your infomation and try again.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Check account number</button></a>
	<?php
}

// General Address Error 
// Codes: 450, 451, 452, 453, 454, 455, 456, 457, 458, 459
function code450($orderData){	
	?>
	<h2>Uh-Oh</h2>
	<p>Looks like the address you submitted is incorrect. Please ensure address entered is the same as the billing address.</p><br>
	<a href="https://www.pmimd.com/onlinetraining/Payment/PayInfo.php"><button class="btn btn-primary">Check address info.</button></a>
	<?php
}
?>