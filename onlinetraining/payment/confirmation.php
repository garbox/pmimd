<?php include '../includes/functions.php';
    include '../includes/config.php';
	include 'cardcodes.php'?>
<?php CookieforCart();

session_start();
$browser_info = $_SERVER['HTTP_USER_AGENT'];
$directory = $_SERVER['SCRIPT_NAME'];
$conn = Connect();
$insert = "INSERT INTO browser_info (browser, url_location) VALUES ('".$browser_info."','".$directory."')";
$conn->query($insert);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav p {
	display:inline;	
	font-size:20px;
}
td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}
</style>
<?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Confirmation</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Shopping Cart Nav -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container">
      		<div class="CartNav"> 
            	<div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-ok cartNavText" style="color:#414549;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> <span class="hidden-sm hidden-xs">Checking Order</span></p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-search cartNavText" style="color:#5371ad;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> Billing Info</p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-credit-card cartNavText" style="color:#0ca24b;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs">  <span class="hidden-sm hidden-xs">Card Info</span></p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 active" align="center">
                        <span class="glyphicon glyphicon-thumbs-up cartNavText" style="color:#c01313;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs">  <span class="hidden-sm hidden-xs">Success</span></p>
                    </div>
              	</div>
           </div>
    	</div>
 	</div>            
 </div> 
 
<!-- confimration display -->
<div class="container-fluid" style=" padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row" id="Cart-Container">
        <div class="container">                     
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 col-md-offset-3 col-lg-offset-3" >
              	  	<div class="Cart-Container print">
                    <div class="test"></div>
						<?php
                        foreach($_REQUEST as $name => $value){
                            $orderData[$name] = $value;
                        }
                        //$orderData['reason_code'] = 100;
                        GoogleEcommerce($_COOKIE["cartIDs"], $orderID, $info['req_amount']);
						 // function to determin what function to use with cardcode. 
                        echo cardCode($orderData, $_SESSION['Profile']['ID']);      
                        ?>
                    </div>
      			 </div>              
            </div>
        </div>
    </div>
</div>


<!-- Footer -->
<?php include '../includes/footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo securebaseurl()?>assets/js/bootstrap.min.js"></script>
</body>
</html>

