<?php include '../includes/functions.php';
include '../includes/config.php';
session_start();
LoginCheckCartFlow();
$uniqID = uniqid();

//Below is for summary, This allows functions to fire before displaying.
$test = new Discounts();
$show = $test->TotalCostShow($_SESSION['RCode'] );	
$Dshow = $test->DiscountShow($_SESSION['RCode'] );

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo securebaseurl();?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo securebaseurl();?>assets/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo securebaseurl()?>assets/js/bootstrap.min.js"></script>

<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

.CartNav{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav p {
	display:inline;	
	font-size:20px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}
.btn-primary{
	background-color:#5371ad;	
}
.btn-primary:hover{
	background-color:#3A65A5;
}
.btn-Continue:hover{
	background-color:#3A65A5;
}
.btn-Continue>h4{
	color:#414549;
}
.btn-Continue{
	background-color:#DFDFDF;	
	border:1px solid #DFDFDF;
}
.btn-Continue:hover>h4{
	color:white;	
}

a.CartNavLink{
	color:#999999;
}
a.CartNavLink:hover{
	text-decoration:none;
}
label{
	font-weight:normal;
}
</style>
<?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Billing Information</h1>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Shopping Cart Nav -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container">
      		<div class="CartNav"> 
            	<div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <a class="CartNavLink" href="<?php echo baseurl()?>YourCart.php">
                        	<span class="glyphicon glyphicon-ok cartNavText" style="color:#414549;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Checking Order</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 active" align="center">
                        <a class="CartNavLink" href="<?php echo securebaseurl()?>payment/payinfo.php">
                        	<span class="glyphicon glyphicon-search cartNavText" style="color:#5371ad;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Billing Info</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-credit-card cartNavText" style="color:#0ca24b;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs">  Card Info</p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-thumbs-up cartNavText" style="color:#c01313;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> Success</p>
                    </div>
              	</div>
           </div>
    	</div>
 	</div>            
 </div> 
 
<!-- PayInfo Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row">
        <div class="container"> 
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
              	  	<div class="Cart-Container">
    					<h2>Bill-To Info</h2> 
                        <hr>
                        <form id="formPayInfo" action="<?php echo securebaseurl();?>payment/pay.php" method="post">
                            <input type="hidden" name="access_key" value="a9c153ccd3f53afe8740609528aff618">
                            <input type="hidden" name="profile_id" value="87B69B24-9CD6-4A79-9240-DC7A4039B4C5">
                            <input type="hidden" name="transaction_uuid" value="<?php echo "SP-".$uniqID ?>">
                            <input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code">
                            <input type="hidden" name="unsigned_field_names" value="card_type,card_number,card_expiry_date">
                            <input type="hidden" name="signed_date_time" value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                            <input type="hidden" name="locale" value="en">
                            <input type="hidden" name="transaction_type" size="25" value="authorization">
                            <input type="hidden" name="reference_number" size="25" value="<?php echo $_SESSION['OrderIDShort'] = OrderID()?>">
                            <input class="amount" type="hidden" name="amount" size="25" value="<?php echo $show?>">
                            <input type="hidden" name="currency" size="25" value="usd">
                            <input type="hidden" name="payment_method" value="card">
                            <input type="hidden" name="bill_to_address_country" value='us'>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="" style="font-weight:normal">First Name On Card</label>
                                    <input class="form-control fname" type="text" name="bill_to_forename" placeholder="First Name" value="<?php echo $_SESSION['Profile']['fname']?>">
                                </div>
                                <div class="col-sm-6">
                                    <label for="" style="font-weight:normal">Last Name On Card</label>
                                    <input class="form-control lname" type="text" name="bill_to_surname" placeholder="Last Name" value="<?php echo $_SESSION['Profile']['lname']?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="" style="font-weight:normal">Email Address</label>
                                    <input class="form-control email" type="text" name="bill_to_email" placeholder="Email" value="<?php echo $_SESSION['Profile']['email']?>">
                              </div>
                                <div class="col-sm-6">
                                    <label for="" style="font-weight:normal">Phone Number</label>
                                    <input class="form-control phone" type="text" name="bill_to_phone" placeholder="Phone" value="<?php echo $_SESSION['Profile']['phone']?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="" style="font-weight:normal">Billing Address</label>
                                    <input class="form-control address" type="text" name="bill_to_address_line1" placeholder="Address" value="<?php echo $_SESSION['Profile']['address']?>">
                                </div>
                                <div class="col-sm-2">
                                    <label for="" style="font-weight:normal">Billing Zip</label>
                                    <input class="form-control zip" type="text" name="bill_to_address_postal_code" placeholder="Zip" value="<?php echo $_SESSION['Profile']['zip']?>">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" style="font-weight:normal">Billing City</label>
                                    <input class="form-control city" type="text" name="bill_to_address_city" placeholder="City" value="<?php echo $_SESSION['Profile']['city']?>">
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="" style="font-weight:normal">Billing State</label>
                            <select id="state" class="form-control" name="bill_to_address_state">
                                <option value="false" Selected>Please Select Your State
                                <option value="AK">Alaska
                                <option value="AL">Alabama
                                <option value="AZ">Arizona
                                <option value="AR">Arkansas
                                <option value="CA">California
                                <option value="CO">Colorado
                                <option value="CT">Connecticut
                                <option value="DE">Delaware
                                <option value="DC">District of Columbia
                                <option value="FL">Florida
                                <option value="GA">Georgia
                                <option value="HI">Hawaii
                                <option value="IA">Iowa
                                <option value="ID">Idaho
                                <option value="IL">Illinois
                                <option value="IN">Indiana
                                <option value="KS">Kansas
                                <option value="KY">Kentucky
                                <option value="LA">Louisiana
                                <option value="ME">Maine
                                <option value="MA">Massachusetts
                                <option value="MD">Maryland
                                <option value="MI">Michigan
                                <option value="MN">Minnesota
                                <option value="MS">Mississippi
                                <option value="MO">Missouri
                                <option value="MT">Montana
                                <option value="NE">Nebraska
                                <option value="NV">Nevada
                                <option value="NH">New Hampshire
                                <option value="NJ">New Jersey
                                <option value="NM">New Mexico
                                <option value="NY">New York
                                <option value="NC">North Carolina
                                <option value="ND">North Dakota
                                <option value="OH">Ohio
                                <option value="OK">Oklahoma
                                <option value="OR">Oregon
                                <option value="PA">Pennsylvania
                                <option value="RI">Rhode Island
                                <option value="SC">South Carolina
                                <option value="SD">South Dakota
                                <option value="TN">Tennessee
                                <option value="TX">Texas
                                <option value="UT">Utah
                                <option value="VT">Vermont
                                <option value="VA">Virginia
                                <option value="WA">Washington
                                <option value="WV">West Virginia
                                <option value="WI">Wisconsin
                                <option value="WY">Wyoming
                                </select>
                            </div>
                            <?php ShippingInfoSP() ?>
                            <div class="form-group">
                            <input class="form-control btn btn-primary submit" type="submit" id="submit" name="submit" value="Continue"/>
                            </div>
                        </form>
                        <p>Editing above info will not change your account info.</p>
                        <div class="error bg-danger"></div>
                    </div>
       			  </div>    
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              	  	<div class="Cart-Container">
    					<h2>Summary</h2>
                        <hr>
                        <p>Item Count: 
                        	<span class="count">
							<?php 
								$_SESSION['ItemCount'] = ItemCount(); 
								echo $_SESSION['ItemCount']?>
                            </span>
                        </p>
                        <p class="discount">
                        <?php 
						if($_SESSION['Discount']== "1"){
							if($Dshow>0){
							echo "Discount: <span style='color:#0ca24b'>-$".$Dshow."</span>";
							}
						} 
						?>
                        </p>
                        <p>Total: $
                        <span class="cost">
							<?php 						
								echo $show;
							?>
                        </span>
                        </p>
                    </div>
      			 </div>                 
            </div>
        </div>
    </div>
</div>

<!-- Continue shopping button above footer -->
<div class="container-fluid">
	<div class="row">
		<div style="background-color:#5371ad" align="center">
        	<a href="<?php echo baseurl();?>"><button class="btn btn-Continue" style="width:100%"><h4>Continue Shopping</h4></button></a>
        </div>
	</div>
</div>

<!-- Footer -->
<?php include '../includes/footer.html';?>

<!-- Scripts -->
<script>
    
/* Ajax */
	$( document ).ready(function() {
		$('select#state').val("<?php echo $test = strtoupper ($_SESSION['Profile']['state'])?>");
	});
	
	// form validation. if empty throw error
	$('#formPayInfo').on('submit', function(e){
		//empty containers when exicuted to remove error messages. 
		$('.error').empty();
		
		// reset css for inputs
		$('.fname').css('border-color', '#ccc');
		$('.lname').css('border-color', '#ccc');
		$('.email').css('border-color', '#ccc');
		$('.phone').css('border-color', '#ccc');
		$('.address').css('border-color', '#ccc');	
		$('.zip').css('border-color', '#ccc');
		$('.city').css('border-color', '#ccc');
		$('.state').css('border-color', '#ccc');
		
		// make sure field isnt empty
		if( $('.fname').val() == ""){
		e.preventDefault();	
		$('.fname').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.lname').val() == ""){
		e.preventDefault();	
		$('.lname').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.email').val() == ""){
		e.preventDefault();	
		$('.email').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.phone').val() == ""){
		e.preventDefault();	
		$('.phone').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.address').val() == ""){
		e.preventDefault();	
		$('.address').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.zip').val() == ""){
		e.preventDefault();		
		$('.zip').css('border-color', 'red');
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.city').val() == ""){
		e.preventDefault();	
		$('.error').html('<p style="color: white; padding:5px;">Please check fields above</p>');
		}
		
		if( $('.state').val() == "false"){
		e.preventDefault();	
		$('.state').css('border-color', 'red');
		$('.error').html('<p style="padding:5px;">Please check fields above</p>');
		}
		if($( "input" ).hasClass( "yes" )){
			if( !$('.yes').is(':checked')){
				if(!$('.no').is(':checked')){
				  $('.error').append('<p>Please select a shipping option</p>');
				  e.preventDefault();	
				}
			}
		}
		
	});	
	
	// change format of phone number. 
	$('.phone').keyup(function () {
    $(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
	});
</script>
</body>
</html>

