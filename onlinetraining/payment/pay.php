<?php include '../includes/functions.php';
include '../includes/config.php';
  include 'security.php'?>
<?php CookieforCart();
session_start();
//Below is for summary, This allows functions to fire before displaying.
$test = new Discounts();
$show = $test->TotalCostShow($_SESSION['RCode'] );	
$Dshow = $test->DiscountShow($_SESSION['RCode'] );
$radio = $_POST['optradio'];

if($radio == "YES"){
	$_SESSION["ShippingInfo"] = array(
	'ShipAddress' => $_POST['ShipAddress'],
	'ShipFname' => $_POST['ShipFname'],
	'ShipLname' => $_POST['ShipLname'],
	'ShipCity' => $_POST['ShipCity'],
	'ShipState' => $_POST['ShipState'],
	'ShipZip' => $_POST['ShipZip'],
	'ShipPhone' =>$_POST['ShipPhone']);
}
?>
    
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo securebaseurl()?>assets/css/style.css">
<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

.CartNav{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav p {
	display:inline;	
	font-size:20px;
}
td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}
.btn-primary{
	background-color:#5371ad;	
}
.btn-primary:hover{
	background-color:#3A65A5;
}
.btn-Continue:hover{
	background-color:#3A65A5;
}
.btn-Continue>h4{
	color:#414549;
}
.btn-Continue{
	background-color:#DFDFDF;	
	border:1px solid #DFDFDF;
}
.btn-Continue:hover>h4{
	color:white;	
}
a.CartNavLink{
	color:#999999;
}
a.CartNavLink:hover{
	text-decoration:none;
}
</style>
<?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Card Infomation</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Shopping Cart Nav -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container">
      		<div class="CartNav"> 
            	<div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <a class="CartNavLink" href="http://www.pmimd.com/onlinetraining/YourCart.php">
                        	<span class="glyphicon glyphicon-ok cartNavText" style="color:#414549;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Checking Order</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <a class="CartNavLink" href="https://www.pmimd.com/onlinetraining/payment/payinfo.php">
                        	<span class="glyphicon glyphicon-search cartNavText" style="color:#5371ad;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Billing Info</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 active" align="center">
                        <a class="CartNavLink" href="https://www.pmimd.com/onlinetraining/Payment/Pay.php">
                        	<span class="glyphicon glyphicon-credit-card cartNavText" style="color:#0ca24b;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs">  Card Info</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-thumbs-up cartNavText" style="color:#c01313;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> Success</p>
                    </div>
              	</div>
           </div>
    	</div>
 	</div>            
 </div> 
 
<!-- Card Info Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row">
        <div class="container">                     
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 ">
                	<div class="Cart-Container">
                    	<h2>Card Info</h2>
                        <hr>
                        <!-- Insert Form for CC processing. -->
                        <form id="payform" action="https://secureacceptance.cybersource.com/silent/pay" method="post"/>
						<?php
                        foreach($_REQUEST as $name => $value) {
                        $params[$name] = $value;
                        }
                        foreach($params as $name => $value) {
                        echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
                        }
                        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
                        ?>
                        <div class="form-group">
                        <label for="" style="font-weight:normal;">Select Card Type</label>
                        <select class="form-control cardType" name="card_type">
                            <option value="" selected></option>
                            <option value="001">Visa</option>
                            <option value="002">MasterCard</option>
                            <option value="003">American Express</option>
                        </select>
                        </div>
                        <div class="form-group">
                        <label for="" style="font-weight:normal;">Card Number (with no dashes or spaces)</label>
                        <input class="form-control cardNumber" type="text" name="card_number">
                        </div>
                        <div class="form-group">
                        <label for=""  style="font-weight:normal;">Expiration Date (MM-YYYY)</label>
                        <input class="form-control cardExpire" type="text" name="card_expiry_date">
                        </div>
                        <div class="form-group">
                        	<button class="form-control btn btn-primary" type="submit" id="submit" value="Confirm">Confirm</button>
                        </div>
                        </form>  
                        <br>
                        <p>Powered by: Cyber Source</p>
                        <div class="errors-type bg-danger"></div>
                        <div class="errors-acct bg-danger"></div>
                        <div class="errors-code bg-danger"></div>
                        <div class="errors-exp bg-danger"></div>
             		</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              	  	<div class="Cart-Container">
    					<h2>Summary</h2>
                        <hr>
                        <p>Item Count: 
                        <span class="count">
							<?php 
								$_SESSION['ItemCount'] = ItemCount(); 
								echo $_SESSION['ItemCount']?>
                            </span>
                        </p>
                        <p class="discount">
                        <?php 
						if($_SESSION['Discount']== "1"){
							if($Dshow>0){
							echo "Discount: <span style='color:#0ca24b'>-$".$Dshow."</span>";
							}
						} 
						?>
                        </p>
                        <p>Total: $
                        <span class="cost">
							<?php 						
								echo $show;
							?>
                        </span>
                        </p>
                    </div>
      			 </div>      
            </div>
        </div>
    </div>
</div> 

<!-- Continue shopping button above footer -->
<div class="container-fluid">
	<div class="row">
		<div style="background-color:#5371ad" align="center">
        	<a href="http://www.pmimd.com/onlinetraining"><button class="btn btn-Continue" style="width:100%"><h4>Continue Shopping</h4></button></a>
        </div>
	</div>
</div>

<!-- Footer -->
<?php include '../includes/footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- Form validation -->
<!-- this will check the inputs if they are either empty or not of correct length. -->
<script> 
$('#payform').on('submit', function(e){
	//empty containers when exicuted to remove error messages. 
	$('.errors-type').empty();
	$('.errors-acct').empty();
	$('.errors-exp').empty();
	
	// reset css for inputs
	$('.cardExpire').css('border-color', '#ccc');
	$('.cardNumber').css('border-color', '#ccc');
	$('.cardType').css('border-color', '#ccc');	
	
	// make sure field isnt empty
	if( $('.cardType').val() == ""){
	e.preventDefault();	
	$('.cardType').css('border-color', 'red');
	$('.errors-type').html("Please select your card");
	}		
	
	// make sure field length is greater then 15
	if($('.cardNumber').val().length <15){
	e.preventDefault();	
	$('.cardNumber').css('border-color', 'red');
	$('.errors-acct').html("Length of credit card number is incorrect.");	
	}
	
	// make sure field isnt empty
	if( $('.cardExpire').val() == ""){
	e.preventDefault();	
	$('.cardExpire').css('border-color', 'red');
	$('.errors-exp').html("Please enter expiration date");
	}
	
	// make sure field is the correct length. 
	if($('.cardExpire').val().length < 7){
	e.preventDefault();	
	$('.cardExpire').css('border-color', 'red');
	$('.errors-exp').html("Please check expiration date format MM-YYYY");
	}
});

/* Automaticly formats sting */
$('.cardExpire').keyup(function () {
        $(this).val($(this).val().replace('+', '-'));
        $(this).val($(this).val().replace('*', '-'));
        $(this).val($(this).val().replace('/', '-'));
        $(this).val($(this).val().replace(/(\d{2})(\d{4})/, "$1-$2"));
	});
</script>

<script>
console.log('<?php echo ShippingInfoEmail()?>');
    
</script>
</body>
</html>

