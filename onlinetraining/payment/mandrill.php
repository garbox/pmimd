<?php 
	//Mandrill Send-Template API
	try {
		require_once '../mandrill-api-php/src/Mandrill.php';
		$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
		$template_name = 'welcome-email-coding-leader';
		// content area, name = mcedit in main source code
		// content  = whats going in that container.
		$template_content = array(
			array(
				'name' => 'totalCost',
				'content' => '<p> Total: '.$totalCost.'</p>'
			),
			array(
				'name' => 'name',
				'content' => '<p> Name: '.$name.'</p>'
			),
			array(
				'name' => 'address',
				'content' => '<p> Address: '.$address.'</p>'
			),
			array(
				'name' => 'orderID',
				'content' => '<p> OrderID: '.$orderID.'</p>'
			),				
		);
		$message = array(
			'html' => '<p>Your Receipt</p>',
			'text' => 'Example text content',
			'subject' => 'Mandrill System Test, Sent To You Via API!!!!',
			'from_email' => 'webmaster@pmimd.net',
			'from_name' => 'PMI Mandrill System Check',
			'to' => array(
				array(
					'email' => $email, //email address
					'name' => $name, // name of client
					'type' => 'to'
				)
			),
			'important' => false,
			'track_opens' => true,
			'track_clicks' => true,
			'auto_text' => null,
			'auto_html' => null,
			'inline_css' => true,
			'url_strip_qs' => null,
			'preserve_recipients' => null,
			'view_content_link' => null,
			'bcc_address' => 'kmetivier@pmimd.com', // bcc email address
			'tracking_domain' => null,
			'signing_domain' => null,
			'return_path_domain' => null,
			'merge' => true,
			'merge_language' => 'mailchimp',
			'global_merge_vars' => array(
				array(
					'name' => 'merge1',
					'content' => 'merge1 content'
				)
			),
			// Image needed for tracking opens
			'images' => array(
				array(
					'type' => 'image/png',
					'name' => 'IMAGECID',
					'content' => 'ZXhhbXBsZSBmaWxl'
				)
			)
		);
		//async is for bulk sending.
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = '2015-01-01';
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
		
	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id ''
		throw $e;
	}
	?>