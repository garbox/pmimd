<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '7454a8cbf9ca40aaa86269ad5e78de73eccabd1f0ae44b18bac1f2b2198c31914621a7cb8ec24b05b2c46a74fd751ba8e64a34342b28432fab055ac8c85e09e72678289deaee4fce84b679d6ebde119cb43911daedaa4e4e82e0c98cb9904f33f6e37b88c7cf488abd36fd84b8af614e9a48ee06a1cd4096949332de53052b67');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
