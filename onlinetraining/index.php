<?php session_start()?>
<?php include 'includes/functions.php';?>
<?php include 'includes/config.php';?>
<?php CookieforCart()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Online Training Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
html, body{ 
	overflow-x: hidden;
}
.btn-img{
	width:auto;	
}
.shadow{
	box-shadow: 3px 3px 10px #e6e6e6;	
}
h1{
	color:#5371ad;
}
.blue{
	color:#5371AD;
}
.socialIcons{
    position:relative;
}   

    a:hover{
        cursor: pointer;
    }
</style>
<?php GoogleAnalytics('UA-73530823-1')?>

</head>
<body>

<div class="fixed">

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner">
    <div class="row">
        <div class="col-lg-12">
		<?php include 'includes/nav.php';?>
        </div>
    </div>
</div>

</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block"><img src="<?php echo baseurl()?>images/OnlineTrainingBannerLarge.png" alt="" width="100%"></div>
            </div>
        </div>
    </div>
</div>
<div id="searchPlacement">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
            <a id="All" class="navbar-brand hidden-xs hidden-sm">All Classes</a>
            <a  id="All" class="navbar-brand hidden-md hidden-lg">All</a>
            <a id="Featured" class="navbar-brand">Featured</a>
            <a id="Certification" class="navbar-brand hidden-xs hidden-sm">Certifications</a>
            <a id="Certification" class="navbar-brand hidden-md hidden-lg">Certs</a>
            <a id="Core" class="navbar-brand" >Core</a>
            <a id="Packages" class="navbar-brand">Packages</a>
        </nav>
    </div>    
</div>

<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:15px;">
    <div class="row">
        <div id="Featured_section">
        <div class="container" style="background-color:white; margin-top:15px; margin-bottom: 30px">
            <div class="col-lg-12" style="padding:15px;">
                <h1>Featured Topics</h1>
                <p>Take a look at some of our highest rated and attended topics.</p>
                <hr>
            </div>
            <?php ProdPicks();?>
        </div>
        </div>
        <div id="Core_section">
        <div class="container" style="background-color:white; margin-top:15px;  margin-bottom: 30px">           
            <div class="col-lg-12" style="padding:15px;">
                <h1>Core topics</h1>
                <p>Recorded sessions are adapted from PMI’s live class curriculum and include a digital course manual.</p>
                <hr>
            </div>
            <? ProdCore();?>
        </div>
        </div>
        <div id="Certification_section">
            <div class="container" style="background-color:white; margin-top:15px;  margin-bottom: 30px">  
                <div class="col-lg-12" style="padding:15px;">
                    <h1>Certifications</h1>
                    <p>Sessions are adapted from live class lectures and include a physical course manual, support via discussion forum, and proctored exam.</p>
                    <hr>
                </div>
                <? ProdCert();?>
            </div>
        </div>
        <div id="Packages_section">
        <div class="container" style="background-color:white; margin-top:15px; margin-bottom: 30px">  
            <div class="col-lg-12" style="padding:15px;">
                <h1>Training packages</h1>
                <p>Get more for less when you customize your learning track with these package options.</p>
                <hr>
            </div>
            <? ProdServices();?>
		</div>
        </div>
	</div>
</div>

<!-- Footer -->
<?php include 'includes/footer.html';?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
  <script>
  console.log(<?php echo $_COOKIE['mc_cid']?>)  
  </script>
    
    <script>
        $("#All").on("click", function(e){
            e.preventDefault;
            $("#Packages_section").show();
            $("#Certification_section").show();
            $("#Core_section").show();
            $("#Featured_section").show();
        })
        $("#Featured").on("click", function(e){
            e.preventDefault;
            $("#Packages_section").hide();
            $("#Certification_section").hide();
            $("#Core_section").hide();
            $("#Featured_section").show();
        })
        $("#Certification").on("click", function(e){
            e.preventDefault;
            $("#Packages_section").hide();
            $("#Certification_section").show();
            $("#Core_section").hide();
            $("#Featured_section").hide();
        })
        $("#Core").on("click", function(e){
            e.preventDefault;
            $("#Packages_section").hide();
            $("#Certification_section").hide();
            $("#Core_section").show();
            $("#Featured_section").hide();
        })
        $("a#Packages").on("click", function(e){
            e.preventDefault;
            $("#Packages_section").show();
            $("#Certification_section").hide();
            $("#Core_section").hide();
            $("#Featured_section").hide();
        })
    </script>
</body>
</html>

