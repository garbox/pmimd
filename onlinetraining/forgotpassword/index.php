<?php include '../includes/functions.php';
include '../includes/config.php';
session_start();
CookieforCart();	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Forgot Password</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
<style>

html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.Cart-Container p.cartNav {
	display:inline;	
	font-size:20px;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

a:hover{
	text-decoration:none;	
}
</style>

<?php GoogleAnalytics('UA-73530823-1')?>

</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav Bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block"><img src="<?php echo baseurl()?>images/PMIProtalBanner.png" alt="" width="100%"></div>
            </div>
        </div>
    </div>
</div>
 
<!-- Login form -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-sm-offset-3 col-md-offset-3 col-lg-offset-4">
              	  	<div class="Cart-Container">
    					<h3 align="center">Password Reset Error</h3>
                        <hr>
                        <p>Seems the reset password link is either expired or not working correclty. <br><br>Please try reseting your password again by clicking the button below</p>                        
                    	<a href="<?php echo baseurl()?>forgottenpassword/forgotpassword.php"><button class=" btn btn-primary">Reset Password</button></a>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include '../includes/footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<!-- Ajax -->
<script>
	$('.error').hide();
	$('.wait').hide();
	$('.GenKey').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.GenKey').prop('action');   		
		$('.wait').show();	
		$('.GenKey').hide();	
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == 1){
					   window.location = '<?php echo baseurl()?>forgotpassword/changepw.php';
				  }
				  else{
					 $('.GenKey').show();
					 $('.wait').hide();
					$('input.genKey').css("border-color",'red');
					$('.error').show().html('<p align="center" class="bg-danger">Wrong secret code, please try again</p>');

				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  console.log(textStatus, errorThrown);
				}
		});
	
	});
</script>

</body>
</html>

