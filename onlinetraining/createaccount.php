<?php include 'includes/functions.php';?>
<?php include 'includes/config.php';?>
<?php CookieforCart();
session_start();?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.Cart-Container p {
	display:inline;	
	font-size:20px;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

</style>
</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include 'includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Create An Account</h1>
                </div>
            </div>
        </div>
    </div>
</div>


 
<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:538px">
    <div class="row">
        <div class="container"> 
            <div class="row"> 
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
              	  	<div class="Cart-Container">
                    <h2 align="center">Hi! Welcome to PMI!</h2>
                    <p align="center" style="font-size: 14px;">Please fill out the information to create an account with us.</p>
                    <hr>
                    <div class="success"></div>
                        <form class="submitform" method="post" action="scripts/adduser.php" >
                        <label for="">Basic Info</label>
                          <div class="form-group">
                            <input type="text" class="form-control fname" name="firstname" placeholder="First Name">
                          </div>
                          <div class="form-group">
                          	<input type="text" class="form-control lname" name="lastname" placeholder="Last Name">
                          </div> 
                          <div class="form-group">
                            <input type="text" class="form-control phone" name="phone" placeholder="Phone Number">
                          </div> 
                          <div class="form-group">
                            <input type="address" class="form-control address" name="address" placeholder="Address">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control city" name="city" placeholder="City">
                          </div>  
                          <div class="form-group">
                            <input type="text" class="form-control zip" name="zip" placeholder="Zip">
                          </div>
                          <div class="form-group">
                              <select id="state" class="form-control" name="bill_to_address_state">
                                <option value="false" Selected>Please Select Your State
                                <option value="AK">Alaska
                                <option value="AL">Alabama
                                <option value="AZ">Arizona
                                <option value="AR">Arkansas
                                <option value="CA">California
                                <option value="CO">Colorado
                                <option value="CT">Connecticut
                                <option value="DE">Delaware
                                <option value="DC">District of Columbia
                                <option value="FL">Florida
                                <option value="GA">Georgia
                                <option value="HI">Hawaii
                                <option value="IA">Iowa
                                <option value="ID">Idaho
                                <option value="IL">Illinois
                                <option value="IN">Indiana
                                <option value="KS">Kansas
                                <option value="KY">Kentucky
                                <option value="LA">Louisiana
                                <option value="ME">Maine
                                <option value="MA">Massachusetts
                                <option value="MD">Maryland
                                <option value="MI">Michigan
                                <option value="MN">Minnesota
                                <option value="MS">Mississippi
                                <option value="MO">Missouri
                                <option value="MT">Montana
                                <option value="NE">Nebraska
                                <option value="NV">Nevada
                                <option value="NH">New Hampshire
                                <option value="NJ">New Jersey
                                <option value="NM">New Mexico
                                <option value="NY">New York
                                <option value="NC">North Carolina
                                <option value="ND">North Dakota
                                <option value="OH">Ohio
                                <option value="OK">Oklahoma
                                <option value="OR">Oregon
                                <option value="PA">Pennsylvania
                                <option value="RI">Rhode Island
                                <option value="SC">South Carolina
                                <option value="SD">South Dakota
                                <option value="TN">Tennessee
                                <option value="TX">Texas
                                <option value="UT">Utah
                                <option value="VT">Vermont
                                <option value="VA">Virginia
                                <option value="WA">Washington
                                <option value="WV">West Virginia
                                <option value="WI">Wisconsin
                                <option value="WY">Wyoming
                                </select>
                          </div>
                          <label for="">Account Info</label>
                          <div class="form-group">
                            <input type="email" class="form-control email" name="email" placeholder="Email" value="<?php echo $_SESSION['email'];?>">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control pw" name="pw" placeholder="Create Password">
                          </div>
                          <label for="">Practice Info</label>
                          <div class="form-group">
                            <input type="text" class="form-control" name="manager" placeholder="Your Managers Name">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="practice" placeholder="Practice Name">
                          </div>  
                          <label for="">Certification Information</label><br>
                          <p style="font-size:12px;">Enter your PMI Certification ID number to receive the certified professional discount.</p>
                          <div class="form-group" style="padding-top:10px;">
                            <input type="text" class="form-control" name="certID" placeholder="Certification ID Number">
                          </div>                                                     
                          <button type="submit" class="btn btn-default btn-block">Submit</button>
                        </form>
                        <div class="error bg-danger" style="padding:5px; margin-top:10px"></div>
                    </div>
        		  </div>                
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include 'includes/footer.html';?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script>
/* Ajax */
$('.phone').keyup(function () {
	$(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3"));
});
$('.error').hide();
$('.submitform').on('submit', function(e){	
	var inputEmpty = false;
	//empty containers when exicuted to remove error messages. 
	$('.error').empty();
	
	// reset css for inputs
	$('.fname').css('border-color', '#ccc');
	$('.lname').css('border-color', '#ccc');
	$('.email').css('border-color', '#ccc');
	$('.phone').css('border-color', '#ccc');
	$('.address').css('border-color', '#ccc');	
	$('.zip').css('border-color', '#ccc');
	$('.city').css('border-color', '#ccc');
	$('.state').css('border-color', '#ccc');
	
	// make sure field isnt empty
	if( $('.fname').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.fname').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.lname').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.lname').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.email').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.email').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.phone').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.phone').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.address').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.address').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.zip').val() == ""){
	e.preventDefault();		
	inputEmpty = true;
	$('.zip').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.city').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.city').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.state').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.state').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	if( $('.pw').val() == ""){
	e.preventDefault();	
	inputEmpty = true;
	$('.pw').css('border-color', 'red');
	$('.error').show().html('Please check fields above');
	}
	
	/* Ajax Section */
	if(inputEmpty == false){
		var values = $('.submitform').serialize();
		// get value of action attribute   
		var desination = $('.submitform').prop('action');   		
		// get current location url	
		$('.submitform').hide();		
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  // if data returns 1 then new account has been created, if 0 then email address is alredy in system and recomend forgot password.
					if(data == '1'){
						console.log(data);
						$('.wait').hide();
				  		$('.success').show().html('<p style="font-size:16px;">Your account has been created. Please login with your new password and username by clicking the button below.</p><br><br><a href="<?php echo baseurl()?>Client/Login.php"><button class="btn btn-primary">Login</button></a>');
					}
					else{
						console.log(data);
						$('.submitform').show();
						$('.error').show().html('<p style="font-size:16px;">It seems this email address is already in our system. Please use a different email or use <a style="font-size:16px;" href="<?php echo baseurl()?>ForgottenPassWord/ForgotPassword.php">This Link</a> to reset your password');

					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	}
});



</script>
</body>
</html>

