<?php session_start()?>
<?php include 'includes/functions.php';?>
<?php include 'includes/config.php';?>
<?php CookieforCart();

//Below is for summary, This allows functions to fire before displaying.

if (isset($_GET['email'])){
    $_SESSION['Profile']['email'] = $_GET['email'];
} 
$test = new Discounts();
$show = $test->TotalCostShow($_SESSION['RCode'] );	
$Dshow = $test->DiscountShow($_SESSION['RCode'] );
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
<style>
html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

.CartNav{
	background-color:white;	
	padding: 15px 30px;
}
.CartNav p {
	display:inline;	
	font-size:20px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

.btn-primary{
	background-color:#5371ad;	
}
.btn-primary:hover{
	background-color:#3A65A5;
}
.btn-Continue:hover{
	background-color:#3A65A5;
}
.btn-Continue>h4{
	color:#414549;
}
.btn-Continue{
	background-color:#DFDFDF;	
	border:1px solid #DFDFDF;
}
.btn-Continue:hover>h4{
	color:white;	
}
a.CartNavLink{
	color:#999999;
}

a.CartNavLink:hover{
	text-decoration:none;
}
    td{
        border-bottom: 1px solid #e6e6e6;
    }
</style>

<?php GoogleAnalytics('UA-73530823-1')?>

</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include 'includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Your Cart</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Shopping Cart Nav -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container">
      		<div class="CartNav"> 
            	<div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 active" align="center">
                        <a class="CartNavLink" href="yourcart.php">
                        	<span class="glyphicon glyphicon-ok cartNavText" style="color:#414549;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Checking Order</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <a class="CartNavLink" href="payment/payinfo.php">
                        	<span class="glyphicon glyphicon-search cartNavText" style="color:#5371ad;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Billing Info</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-credit-card cartNavText" style="color:#0ca24b;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs">  Card Info</p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-thumbs-up cartNavText" style="color:#c01313;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> Success</p>
                    </div>
              	</div>
           </div>
    	</div>
 	</div>            
 </div>  
 
<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row">
        <div class="container">                     
            <div class="row">

                <div class="col-sm-6 col-xs-12 col-md-8 col-lg-8">
                	<div class="Cart-Container">
                    	<h2>Summary</h2>

                        <table width="100%">
                            <?php NewCartDisplay($_SESSION['Profile']['Email'])?>
                        </table>
             		</div>
                </div> 

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              	  	<div class="Cart-Container">
    					<h2>Ready to check out?</h2>
                        <hr>
                        <p>Item Count: 
                        	<span class="count">
							<?php 
								$_SESSION['ItemCount'] = ItemCount(); 
								echo $_SESSION['ItemCount'];
                            ?>
                           
                            </span>
                        </p>
                        <p class="referral"></p>
                        <p class="discount">
                        <?php 
						if($_SESSION['Discount']== "1"){
							if($Dshow>0){
							echo "Discount: <span style='color:#0ca24b'>-$".$Dshow."</span>";
							}
						} 
						?>
                        </p>
                        <p>Total: $
                        <span class="cost">
							<?php 						
								echo $show;
							?>
                        </span>
                        </p>
						<p><a data-toggle="modal" data-target="#myModal" href="" class="" style="padding-left:0px;font-size:15px; margin-bottom:15px;">Have a referral or discount code?</a></p>
                        <a href="<?php echo baseurl()?>payment/payinfo.php">
                        <button id="checkoutbutton" class="btn btn-primary" style="width:100%">Proceed to checkout</button></a>
                    </div>
      			 </div>              
            </div>
        </div>
    </div>
</div>

<!-- Continue shopping button above footer -->
<div class="container-fluid">
	<div class="row">
		<div style="background-color:#5371ad" align="center">
        	<a href="<?php echo baseurl()?>"><button class="btn btn-Continue" style="width:100%"><h4>Continue Shopping</h4></button></a>
        </div>
	</div>
</div>
<!-- Footer -->
<?php include 'includes/footer.html';?>

<!-- Modal Content -->
<!-- Referal Code Modal -->
<div id="myModal" class="modal fade" role="dialog" style="margin-top:150px;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Referral/Discount Code</h4>
      </div>
      <div class="modal-body">
        
        <form class="DiscountCode" action="scripts/discountcode.php" method="get" class="form-group">
        <p>Enter discount or referral code below</p>
            <div class="form-group">
                <input class="form-control" name="DiscountCode" type="text">
            </div>
            <div class="form-group">
                <button class="form-control btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
            <div class="error bg-danger" style="color:black;"></div>
            <div class="success bg-success" style="color:black;"></div>
                    <p style="font-size:12px">PMI Certified Professionals with an active ID# recorded in their PMI Profile will automatically receive 10% off the purchase total. Only one discount or referral code may be used. If multiple discounts are in effect, the highest discount will be applied to the purchase, less any applicable taxes.</p>

      </div>
    </div>
  </div>
</div>
<!-- Modal for email -->
<div class="modal fade" id="emailModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Save/Retrieve Your Cart</h3>
        <button id="closebutton" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="scripts/email_cart_set.php" id="email_form">
           <label for="email">Enter your email address.</label>
            <input type="email" class="form-control" name='email' placeholder="Enter Email">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="btn_submit">Submit</button>
        </form>
        <div class="holding"></div>
      </div>
    </div>
  </div>
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
<script>

    $("#closebutton").on('click', function(e){
        var id = $(this).attr('id');
		var values = 1;
		// get value of action attribute   
		var desination = 'scripts/email_modal_cookie.php'   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
    })
/* Ajax */
    
	$('.RemoveProd').on('submit', function(e){
		var id = $(this).attr('id');
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.RemoveProd').prop('action');   		
		// get current location url			
		console.log(desination);
		// prevent page from leaving to desination
		e.preventDefault();	
		
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){	
				$('tr#'+id).hide(); 
				$('span.count').load( "<?php echo baseurl()?>scripts/itemcount.php" );
				$('p.discount').load( "<?php echo baseurl()?>scripts/discount.php" );
				$('span.cost').load( "<?php echo baseurl()?>scripts/totalcost.php" );			  
			  },
			  error: function(){
			  } 
		});
	
	});
	
	$('.DiscountCode').on('submit', function(e){
		$('.error').hide();
		var id = $(this).attr('id');
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.DiscountCode').prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		
		$.ajax({
			  url: desination,
			  type: 'get',
			  data: values, 
			  success: function(data){
				  data = parseInt(data);
				  if(data == '1'){
					  $('.success').html("<p>Referal/Discount code added.</p>");
					  $('.DiscountCode').hide();
					  $('p.discount').load( "<?php echo baseurl()?>scripts/discount.php" );
					  $('span.cost').load( "<?php echo baseurl()?>scripts/totalcost.php" );	
				  }
				  else{
					$('.error').show().html("<p>Incorrect referreal code, please try again.</p><p>If you are still having issues with the discount code, please give us a call at <br>1-800-259-5562");
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
    
    <?php 
    if(isset($_SESSION['Profile']['email']) || $_COOKIE['modal_close'] == 1){}
        else{
    ?>
    $(window).on('load',function(){
        $('#emailModal').modal('show');
    });
    <?php } ?>
    
    $('#email_form').on('submit', function (e){
	var values = $(this).serialize();
		var desination = $(this).prop('action');   		
		e.preventDefault();	
		console.log(values);
		$.ajax({
			  url: desination,
			  type: 'post',
		  data: values, 
			  success: function(data){	
                 if(data == 1){
                      location.reload();
                 }
                 else{
                     location.reload();
                  }
                
		  },
			  error: function(){
              } 
		});
   })
    
</script>

<?php if($_SESSION['ItemCount'] == 0){
?>
<script>
    $('#checkoutbutton').remove();
</script>
<?php
}
?>

</body>
</html>



