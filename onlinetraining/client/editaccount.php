<?php include '../includes/Functions.php';
include('../includes/config.php');
session_start();
LoginCheckProtPage();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMIMD: Product Center</title>
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<style>
	body,html{overflow-x:hidden;background-color:#e6e6e6}.Cart-Container{background-color:#fff;padding:15px 30px}td{padding:10px!important}h1,h2,h3,h4,h5,h6{color:#999}.Cart-Container p.cartNav{display:inline;font-size:20px}.cartNavText{font-size:20px;vertical-align:middle;margin-top:-9px}.active{opacity:1}.inactive{opacity:.3}.manager{border:none}
	</style>
    <?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>
	<div class="contianer">
		<div class="row-fluid">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlack"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlue"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiGreen"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiRed"></div>
		</div>
	</div>
	<div class="conatiner" style="background-color:#fff">
		<div class="row">
			<div class="col-lg-12">
				<?php include '../includes/nav.php';?>
			</div>
		</div>
	</div>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">PMI Portal</h1>
                </div>
            </div>
        </div>
    </div>
</div>
  <noscript>
              <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
                    <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: You no do have JavaScript enabled on your system. We recommend enabling JavaScript for full course tracking features.</strong></p>
            </div></div></div>
  </noscript>
   <!--[if IE 9 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </strong></p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 8 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 7 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 6 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
	<div class="container-fluid" style="background-color:#e6e6e6;padding-top:30px;margin-bottom:30px;min-height:554px">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
						<div class="Cart-Container">
							<?php include '../includes/portalnav.html';?>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="Cart-Container">
							<h2>Edit Account</h2>
							<form action="<?php echo baseurl()?>scripts/edituser.php" class="HideForm" id="EditUser" method="post" name="EditUser">
								<label for="">Basic Info</label>
								<hr>
								<input class="form-control" name="ID" type="hidden" value="<?php echo $_SESSION['Profile']['ID']?>">
								<div class="row">
                                            
								    <div class="col-md-6">
								        <div class="form-group">
                                        <label for="firstname">First Name</label>
                                            <input class="form-control" name="firstname" placeholder="First Name" value="<?php echo $_SESSION['Profile']['fname']?>" disabled>
                                        </div>
								    </div>
								    <div class="col-md-6">
								        <div class="form-group">
                                        <label for="lastname">Last Name</label>
                                            <input class="form-control" name="lastname" placeholder="Last Name" value="<?php echo $_SESSION['Profile']['lname']?>" disabled>
                                        </div>
								    </div>
								</div>
								<div class="form-group">
								<label for="address">Address</label>
									<input class="form-control" name="address" placeholder="Address" type="address" value="<?php echo $_SESSION['Profile']['address']?>">
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <div class="form-group">
                                        <label for="cuty">City</label>
                                            <input class="form-control" name="city" placeholder="City" value="<?php echo $_SESSION['Profile']['city']?>">
                                        </div>
								    </div>
								    <div class="col-md-4">
								        <div class="form-group">
                                        <label for="zip">Zip Code</label>
                                            <input class="form-control" name="zip" placeholder="Zip" value="<?php echo $_SESSION['Profile']['zip']?>">
                                        </div>
								    </div>
								    <div class="col-md-4">
								        <div class="form-group">
                                        <label for="state">State</label>
                                            <input class="form-control" name="state" placeholder="State" value="<?php echo $_SESSION['Profile']['state']?>">
                                        </div>
								    </div>
								</div>
								
								<div class="form-group">
								    <label for="phone">Phone Number</label>
									<input class="form-control" name="phone" placeholder="Phone Number" value="<?php echo $_SESSION['Profile']['phone']?>">
								</div>
								
								<label for="">Account Info<br>
								<hr>
								<span style="font-size:12px;color:#c01313">(If you change the email address, it will also change your user name.)</span></label>
								<div class="form-group">
								<label for="email">Email</label>
									<input class="form-control" name="email" placeholder="Email" type="email" value="<?php echo $_SESSION['Profile']['email']?>" disabled>
								</div>								
								<label for="">Practice Info</label>
								<hr>
								<div class="form-group">
								<label for="manager">Your Manager's Name</label>
									<input class="form-control" name="manager" placeholder="Your Managers Name" value="<?php echo $_SESSION['Profile']['manager']?>">
								</div>
								<div class="form-group">
								<label for="practice">Practice Name</label>
									<input class="form-control practice" name="practice" placeholder="Practice Name" value="<?php echo $_SESSION['Profile']['practice']?>">
								</div>
								<div class="form-group">
                                <label for="certID">Graduate ID</label>
								    <input class="form-control certID" name="certID" placeholder="Graduate ID" value="<?php echo $_SESSION['Profile']['Cert']?>">
								</div>
								<button class="btn btn-block btn-default" type="submit">Submit</button>
							</form>
							<div class="wait"><img class="center-block img-fluid" src="https://www.pmimd.com/totalaccess/img/Test.gif"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?php include '../includes/footer.html';?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script>
	<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js">
	</script>
	<script>
	$('.wait').hide();
	$('#EditUser').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		$("#EditUser").hide();
        $(".wait").show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
                      $('.wait').hide();
					$("#EditUser").show().html("<p>Thank you, your infomation has been updated. <br><br> Infomation will be changed upon next login.</p>");
				  }
				  else{
					  console.log(data);
					  $('.wait').hide();
					  $("#EditUser").show().html('<p class="bg-danger">Looks like there was some issues trying to save the info, please refresh the page and try again.</p>')
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
	</script>
</body>
</html>