<?php include '../includes/functions.php';?>
<?php include '../includes/config.php';?>

<?php 
session_start();
CookieforCart();	
SessionCheck();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
<style>

html, body{ 
	overflow-x: hidden;
	background-color:#e6e6e6;
}
.Cart-Container{
	background-color:white;	
	padding: 15px 30px;
}

td{
	padding:10px!important;
}
h1,h2,h3,h4,h5,h6{
	color:#999999;
}

.Cart-Container p.cartNav {
	display:inline;	
	font-size:20px;
}
.cartNavText{
	font-size:20px; 
	vertical-align:middle;
	margin-top:-9px;
}
.active{
	opacity:1;	
}
.inactive{
	opacity:.3;
}

a:hover{
	text-decoration:none;	
}
</style>

<?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav -->
<div class="conatiner" style="background-color:white;">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Login</h1>
                </div>
            </div>
        </div>
    </div>
</div>
 
<!-- Login form -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; min-height:565px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Login</h2>
                        <p class="error"></p>
                        <hr>
                        <div class="wait">
                        	<img class='center-block img-fluid'src='https://pmimd.com/totalaccess/img/Test.gif'>
                        </div>
                        <form class="LogInForm" method="post" action="../scripts/auth.php" >
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?php echo $_SESSION['email'];?>">
                          </div>
                           <div class="form-group">
                            <input type="password" class="form-control" name="pw" placeholder="Password">
                          </div>                         
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                        <a href="<?php echo baseurl()?>forgotpassword/forgotpassword.php"><p style="font-size:14px;margin-top:5px;">Forgot Password?</p></a>
                        <hr>
                        <div class="newtopmi">
                            <a href="<?php echo baseurl()?>createaccount.php">
                            <h4 align="center">Create a new account</h4></a>
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div style="position: fixed; width: 100%" >
<?php include '../includes/footer.html';?>
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<!-- Ajax --><script>
	$('.wait').hide();
	$('.LogInForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('.LogInForm').prop('action');   		
		// get current location url			
		$('.LogInForm').hide();
		$('.newtopmi').hide();
		$('.wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
					window.location = '<?php echo baseurl()?>client/portal.php'
				  }
				  else{
					  console.log(data);
					  $('.wait').hide();
					  $('.newtopmi').show();
					  $('.LogInForm').show();
					  $(".error").html("<p>Incorrect info: Please make sure username and password is entered correctly.</p>");
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
</script>
</body>
</html>