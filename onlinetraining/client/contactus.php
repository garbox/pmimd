<?php include '../includes/Functions.php';
include('../includes/config.php');
session_start();
LoginCheckProtPage()?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMIMD: Product Center</title>
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<style>
	body,html{overflow-x:hidden;background-color:#e6e6e6}.Cart-Container{background-color:#fff;padding:15px 30px}td{padding:10px!important}h1,h2,h3,h4,h5,h6{color:#999}.Cart-Container p.cartNav{display:inline;font-size:20px}.cartNavText{font-size:20px;vertical-align:middle;margin-top:-9px}.active{opacity:1}.inactive{opacity:.3}.manager{border:none}
	</style>
    <?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>
	<div class="contianer">
		<div class="row-fluid">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlack"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlue"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiGreen"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiRed"></div>
		</div>
	</div>
	<div class="conatiner" style="background-color:#fff">
		<div class="row">
			<div class="col-lg-12">
				<?php include '../includes/nav.php';?>
			</div>
		</div>
	</div>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">PMI Portal</h1>
                </div>
            </div>
        </div>
    </div>
</div>
  <noscript>
              <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
                    <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: You no do have JavaScript enabled on your system. We recommend enabling JavaScript for full course tracking features.</strong></p>
            </div></div></div>
  </noscript>
   <!--[if IE 9 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </strong></p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 8 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 7 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 6 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
	<div class="container-fluid" style="background-color:#e6e6e6;padding-top:30px;margin-bottom:30px;min-height:562px">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
						<div class="Cart-Container">
							<?php include '../includes/portalnav.html';?>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="Cart-Container">
							<h3>Message Us</h3>
							<div class="wait"><img class="center-block img-fluid" src="https://www.pmimd.com/totalaccess/img/Test.gif"></div>
							<form action="../Scripts/Contactus.php" class="MessageUs" method="post">
								<div class="form-group">
									<textarea cols="52" name="message" rows="5"></textarea>
								</div><button class="btn btn-block btn-default" type="submit">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?php include '../includes/Footer.html';?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script>
	<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
	<script>
	$(".wait").hide(),$(".MessageUs").on("submit",function(s){var e=$(this).serialize(),a=$(".MessageUs").prop("action");$(".MessageUs").hide(),$(".wait").show(),s.preventDefault(),$.ajax({url:a,type:"post",data:e,success:function(s){1==s?window.location="Portal.php":($(".wait").hide(),$(".MessageUs").empty(),$(".MessageUs").show().html("<p>Thanks for contacting us! A representative will get back to you within 2 business days. If you need immediate assistance, call 800-259-5562 M-F, 8 a.m. to 5 p.m. Central Time.<p>"))},error:function(s,e,a){$(".wait").hide(),$(".MessageUs").empty(),$(".MessageUs").show().html("<p>Looks like there was an issue submitting your message. Please contact PMI at info@pmimd.com for help.<\/p>"),console.log(e,a)}})})
	</script>
</body>
</html>