<?php include '../includes/functions.php';
include('../includes/config.php');
?>

<?php
session_start();
$CustID = $_SESSION['Profile']['ID'];
$VideoID = $_GET['VideoID'];
$course_id = $_GET['ProdID'];

$conn = Connect();

// Select info from both video tracking and video tables for general info on video. 
$tracking_query = "SELECT * FROM videotracking WHERE CustomerID = '".$CustID."' AND VideoID = '".$VideoID."'";
$video_query = "SELECT ClassID, Duration, FileName FROM videos where ID = ".$VideoID."";
$video_history_query = "INSERT INTO videohistory (cust_id, course_id, video_id) VALUES(".$_SESSION['Profile']['ID'].",".$course_id.",".$VideoID.")";

//query calls
$tracking_data = $conn->query($tracking_query);
$video_data = $conn->query($video_query);
$video_data = $video_data->fetch_object();

//if not tracking data, insert data and call it again for data. else use data
if($tracking_data->num_rows == 0){
    $insert_tracking = "INSERT INTO videotracking (CustomerID, VideoID, CourseID, LastAccess, VideoDuration) VALUES ('".$CustID."', '".$VideoID."', '".$course_id."', '".date("Y/m/j")."', '".$video_data->Duration."')";
    $conn->query($insert_tracking);
    $tracking_data = $conn->query($tracking_query);
}

//get data in object form
$tracking_data = $tracking_data->fetch_object();

LoginCheckProtPage()?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMIMD: Product Center</title>
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<style>
	body, html {
		overflow-x: hidden;
		background-color: #e6e6e6
	}
	.Cart-Container {
		background-color: #fff;
		padding: 15px 30px
	}
	.CartNav {
		background-color: #fff;
		padding: 15px 30px
	}
	.CartNav p {
		display: inline;
		font-size: 20px
	}
	td {
		padding: 10px!important
	}
	h1, h2, h3, h4, h5, h6 {
		color: #999
	}
	.cartNavText {
		font-size: 20px;
		vertical-align: middle;
		margin-top: -9px
	}
	.active {
		opacity: 1
	}
	.inactive {
		opacity: .3
	}
	.col-lg-3:hover {
		background-color: #F3F3F3
	}	
    li.ClassResource{
        list-style: none;
        margin-left:5px;
    }
    video{
        padding:30px 0px;   
        width:90%;
    }
    </style>
    
    	
</head>
<body>
	<div class="contianer">
		<div class="row-fluid">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlack"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlue"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiGreen"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiRed"></div>
		</div>
	</div>
	<div class="conatiner" style="background-color:#fff">
		<div class="row">
			<div class="col-lg-12">
				<?php include '../includes/nav.php';?>
			</div>
		</div>
	</div>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Course Content</h1>
                </div>
            </div>
        </div>
    </div>
</div>
  <noscript>
              <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
                    <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: You no do have JavaScript enabled on your system. We recommend enabling JavaScript for full course tracking features.</strong></p>
            </div></div></div>
  </noscript>
   <!--[if IE 9 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </strong></p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 8 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 7 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <!--[if IE 6 ]>
    <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
	            <p align='center' style="margin-top:7px;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features. </p>
	    </div></div></div>
    <![endif]--> 
    <div class="container">
    <div align="center">
    <video id="myVideo" controls controlsList="nodownload" >
      <source src="https://www.pmimd.com/mp4/<?php echo $video_data->FileName?>" type="video/mp4">
      Your browser does not support HTML5 video.
    </video>
    <div id="VideoFinished">
    </div>
</div>
    </div>
    <?php include '../includes/footer.html';?>
    <script></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
    <script> 
        //document.addEventListener('contextmenu', event => event.preventDefault());
        var VideoTag = document.getElementById("myVideo");
        VideoTag.currentTime = <?php echo $tracking_data->CurrentTime?>;  
        
        // all this function does is check the server is video is already completed if so, display message. 
        function PreCompleteCheck(Completed){
            if(Completed == '1'){
                $("#VideoFinished").empty().html("<div style='padding:15px; margin-bottom:15px; background-color:#5371ad;'><span style='color:white; font-size:18px'>Video Finished</span></div>")
            }
            
        }
        
        // This function checks to see if user has finished video and 
        // also set the video as completed for good once they have completed. 
        function CompletedCheck(CurrentTime, Duration, Completed){
            if(Completed != '1'){
                if (CurrentTime + 120 >= Duration ) {
                    $("#VideoFinished").empty().html("<div style='padding:15px; margin-bottom:15px; background-color:#5371ad;'><span style='color:white; font-size:18px'>Video Finished</span></div>")
                    Completed = '1';
                }
                else{
                    Completed = '0';
                }
            }
            else{
                $("#VideoFinished").html("<div style='padding:15px; margin-bottom:15px; background-color:#5371ad;'><span style='color:white; font-size:18px'>Video Finished</span></div>")
            }
            return Completed;
        }
        
        // function that loops every 5 sec to send info to the server. 
        // This will update the customers current time and last date accessed to the smallest degree of day. 
        // json is set to querystring to send to the location via ajax. 
        function Interval(VideoID, CustID) {
            
            //set current time from video. 
            var CurrentTime = VideoTag.currentTime;
            var Duration = VideoTag.duration;
            var Completed = CompletedCheck(CurrentTime, Duration, Completed);
            //jason format (Delievered to server)
            var JsonObj = {
                    "CurrentTime": "" + CurrentTime + "",
                    "VideoID": "" + VideoID + "",
                    "CustID": "" + CustID + "",
                    "Duration": ""+Duration+"",
                    "Completed": "" + Completed + "",
                    "CourseID":"<?php echo $video_data->ClassID?>"
                }
                console.log(JsonObj);
            // querystring json & set desination for data   
            var values = $.param(JsonObj);
            var desination = '../scripts/videotracking.php';
            
            // Deliver package via ajax
            $.ajax({
                url: desination,
                type: 'post',
                data: values,
                success: function(data) {
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
        
        // Seting of vars, disable right click. set video time. 
        CurrentTime = VideoTag.currentTime;
        VideoID = <?php echo $VideoID?>;
        CustID = <?php echo $CustID?>;
        Completed = <?php echo $tracking_data->Completed;?>;
        PreCompleteCheck(Completed);
        setInterval(function() { Interval(VideoID, CustID) }, 2000); 
    </script>
</body>
</html>