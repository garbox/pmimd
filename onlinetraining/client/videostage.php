<?php include '../includes/functions.php';
include('../includes/config.php');?>

<?php CookieforCart();
session_start();
$prodID = $_POST['ProdID'];
$Name = $_POST['Name'];
$email = $_SESSION['Profile']['email'];


LoginCheckProtPage()?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMIMD: Product Center</title>
	<meta charset="utf-8">
	<meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
	<style>
	body, html {
		overflow-x: hidden;
		background-color: #e6e6e6
	}
	.Cart-Container {
		background-color: #fff;
		padding: 15px 30px
	}
	.CartNav {
		background-color: #fff;
		padding: 15px 30px
	}
	.CartNav p {
		display: inline;
		font-size: 20px
	}
	td {
		padding: 10px!important
	}
	h1, h2, h3, h4, h5, h6 {
		color: #999
	}
	.cartNavText {
		font-size: 20px;
		vertical-align: middle;
		margin-top: -9px
	}
	.active {
		opacity: 1
	}
	.inactive {
		opacity: .3
	}
	.col-lg-3:hover {
		background-color: #F3F3F3
	}	
    li.ClassResource{
        list-style: none;
        margin-left:5px;
    }
    </style>
    <?php GoogleAnalytics('UA-73530823-1')?>
</head>
<body>
	<div class="contianer">
		<div class="row-fluid">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlack"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiBlue"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiGreen"></div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pmiRed"></div>
		</div>
	</div>
	<div class="conatiner" style="background-color:#fff">
		<div class="row">
			<div class="col-lg-12">
				<?php include '../includes/nav.php';?>
			</div>
		</div>
	</div>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Course Content</h1>
                </div>
            </div>
        </div>
    </div>
</div>
  <noscript>
              <div class='container-fluid' style="background-color:#c01313; color:#fff"><div class='row'><div class='col-lg-12'>
                    <p align='center' style="margin-top:7px;"><strong>IMPORTANT NOTICE: You no do have JavaScript enabled on your system. We recommend enabling JavaScript for full course tracking features.</strong></p>
            </div></div></div>
  </noscript>
	<div class="container-fluid" style="background-color:#e6e6e6;padding-top:30px;margin-bottom:30px;min-height:562px">
		<div class="row">
			<div class="container" style="background-color:#fff">
				<div class="row">
					<div class="col-xs-8">
						<h3><?php echo $Name?></h3>
					</div>
				</div>
                <hr>
				<div class="row">
                <!--[if IE 9 ]>
                            <div class='col-lg-12' style='background-color:#c01313; margin-bottom:15px'>
                                <p align='center' style="margin-top:7px; color:white;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features.<br><br>By continuing, your agreeing that we will not track you for CEUs and will not be given any CEUs for this course. </p>
                            </div>
                <![endif]-->
                <!--[if IE 7 ]>
                            <div class='col-lg-12' style='background-color:#c01313; margin-bottom:15px'>
                                <p align='center' style="margin-top:7px; color:white;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features.<br><br>By continuing, your agreeing that we will not track you for CEUs and will not be given any CEUs for this course. </p>
                            </div>
                <![endif]--> 
                <!--[if IE 6 ]>
                            <div class='col-lg-12' style='background-color:#c01313; margin-bottom:15px'>
                                <p align='center' style="margin-top:7px; color:white;">IMPORTANT NOTICE: Our system has detected that you are using an older version of Internet Explorer. We recommend IE10 or above for full course tracking features.<br><br>By continuing, your agreeing that we will not track you for CEUs and will not be given any CEUs for this course. </p>
                            </div>
                <![endif]-->
					<?php 
                        echo PDFDisplay($prodID);
						$test = new VideoStage();
						$test->DisplayVideo($email, $prodID);
					?>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div align="center" style="background-color:#5371ad">
				<a href="<?php echo baseurl()?>client/custclasses.php"><button class="btn btn-primary" style="width:100%">
				<h4 style="color:#fff">Your Self-Paced Classes</h4></button></a>
			</div>
		</div>
	</div><?php include '../includes/footer.html';?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
	<script>
        $(function(){
            var JsonObj = {
                    "prodID": "<?php echo $prodID;?>",
                    "custID": "<?php echo $_SESSION['Profile']['ID']?>"
                }
            var values = $.param(JsonObj);
            $.ajax({
                  url: '<?php echo baseurl()?>scripts/course_tracking.php',
                  type: 'post',
                  data: values, 
                  success: function(){ 
                    
                    },
                  error: function(xhr){
                    console.log(xhr)
                  } 
            });
        })
	$(".RemoveProd").on("submit",function(o){var e=$(this).serialize(),t=$(".RemoveProd").prop("action");o.preventDefault(),$.ajax({url:t,type:"post",data:e,success:function(){location.reload()},error:function(){}})})
	</script>
</body>
</html>