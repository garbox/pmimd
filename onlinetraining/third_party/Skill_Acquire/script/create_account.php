<?php include '../../../includes/config.php';?>
<?php include '../../../includes/functions.php';?>
<?php 

$uri_data = $_REQUEST;
$conn = Connect();

$insert = "INSERT INTO customers (Fname, Lname, Address, City, Zip, State, Email, Phone, third_party)
VALUES(
'".$uri_data['firstname']."',
'".$uri_data['lastname']."',
'".$uri_data['address']."',
'".$uri_data['city']."',
'".$uri_data['zip']."',
'".$uri_data['state']."',
'".$uri_data['email']."',
'".$uri_data['phone']."',
'SA')";

if($conn->query($insert)== TRUE){
    $select = "SELECT * FROM customers WHERE third_party = 'SA' ORDER BY ID DESC LIMIT 1";
    $query = $conn->query($select);
    $cust_data = $query->fetch_assoc();    
    
    //select prod for orderpalced data. 
    $select_prod = "SELECT * FROM selfpaced WHERE ProdID = '".$uri_data['prod_id']."'";
    $query = $conn->query($select_prod);
    $prod_info = $query->fetch_assoc();
    
    //insert product into customer portal. 
    $insert = "INSERT custportal (custID, prodID) 
        VALUES ('".$cust_data['ID']."','".$prod_info['ProdID']."')";
    $conn->query($insert);
    
    //insert into order placed table.
    $insert_order = "INSERT orderplaced (ProdID, CustomerID, Email, Price, CataID, PName, ReceiptID, DateOrdered, RecDate) 
                    VALUES ('".$prod_info['ProdID']."',
                    '".$cust_data['ID']."',
                    '".$cust_data['Email']."',
                    '".$prod_info['Price']*.8."',
                    '".$prod_info['CataID']."',
                    '".$prod_info['Name']."',
                    '".uniqid()."',
                    '".date('y-m-d')."', 
                    '".date('y-m-d')."')";				
    $conn->query($insert_order);
    
    //print data for ajax.
    echo $cust_data['ID'];
    }
else{
    echo $conn->error;
}


?>