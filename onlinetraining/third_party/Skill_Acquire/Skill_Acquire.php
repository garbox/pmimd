<?php session_start()?>
<?php include '../../includes/functions.php';?>
<?php include '../../includes/config.php';?>
<?php CookieforCart()?>
<?php $info = $_REQUEST;

$conn = Connect();
$select = "SELECT * FROM customers WHERE ID = ".$info['uid']."";
    $data = $conn->query($select);
    $data = $data->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Skill Acquire: Edit Account</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
html, body{ 
	overflow-x: hidden;
}
.btn-img{
	width:auto;	
}
.shadow{
	box-shadow: 3px 3px 10px #e6e6e6;	
}
h1{
	color:#5371ad;
}
.blue{
	color:#5371AD;
}
.socialIcons{
    position:relative;
}   

a:hover{
    cursor: pointer;
}
</style>
<?php GoogleAnalytics('UA-73530823-1')?>

</head>
<body>

<div class="fixed">

<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../../includes/nav.php';?>
        </div>
    </div>
</div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Skill Acquire: Create Account</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding:15px;">
    <div class="row">
        <div class="container">
            <div class="col-xs-6 col-xs-offset-3" style="background-color:white; margin-top:15px; margin-bottom: 30px; padding: 15px;">
                <form id="submit" action="<?php echo baseurl()?>third_party/Skill_Acquire/script/sa_update.php" method="post" name="EditUser">
                    <input class="form-control" name="ID" type="hidden" value="<?php echo $data['ID']?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="firstname">First Name</label>
                                <input class="form-control" name="firstname" placeholder="First Name" value="<?php echo $data['Fname']?>" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="lastname">Last Name</label>
                                <input class="form-control" name="lastname" placeholder="Last Name" value="<?php echo $data['Lname']?>" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="address">Address</label>
                        <input class="form-control" name="address" placeholder="Address" type="address" value="<?php echo $data['Address']?>">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="cuty">City</label>
                                <input class="form-control" name="city" placeholder="City" value="<?php echo $data['City']?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="zip">Zip Code</label>
                                <input class="form-control" name="zip" placeholder="Zip" value="<?php echo $data['Zip']?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="state">State</label>
                                <select id="state" class="form-control" name="state">
                                    <option value="false" Selected>State
                                    <option value="AK">Alaska
                                    <option value="AL">Alabama
                                    <option value="AZ">Arizona
                                    <option value="AR">Arkansas
                                    <option value="CA">California
                                    <option value="CO">Colorado
                                    <option value="CT">Connecticut
                                    <option value="DE">Delaware
                                    <option value="DC">District of Columbia
                                    <option value="FL">Florida
                                    <option value="GA">Georgia
                                    <option value="HI">Hawaii
                                    <option value="IA">Iowa
                                    <option value="ID">Idaho
                                    <option value="IL">Illinois
                                    <option value="IN">Indiana
                                    <option value="KS">Kansas
                                    <option value="KY">Kentucky
                                    <option value="LA">Louisiana
                                    <option value="ME">Maine
                                    <option value="MA">Massachusetts
                                    <option value="MD">Maryland
                                    <option value="MI">Michigan
                                    <option value="MN">Minnesota
                                    <option value="MS">Mississippi
                                    <option value="MO">Missouri
                                    <option value="MT">Montana
                                    <option value="NE">Nebraska
                                    <option value="NV">Nevada
                                    <option value="NH">New Hampshire
                                    <option value="NJ">New Jersey
                                    <option value="NM">New Mexico
                                    <option value="NY">New York
                                    <option value="NC">North Carolina
                                    <option value="ND">North Dakota
                                    <option value="OH">Ohio
                                    <option value="OK">Oklahoma
                                    <option value="OR">Oregon
                                    <option value="PA">Pennsylvania
                                    <option value="RI">Rhode Island
                                    <option value="SC">South Carolina
                                    <option value="SD">South Dakota
                                    <option value="TN">Tennessee
                                    <option value="TX">Texas
                                    <option value="UT">Utah
                                    <option value="VT">Vermont
                                    <option value="VA">Virginia
                                    <option value="WA">Washington
                                    <option value="WV">West Virginia
                                    <option value="WI">Wisconsin
                                    <option value="WY">Wyoming
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone Number</label>
                        <input class="form-control" name="phone" placeholder="Phone Number" value="<?php echo $data['Phone']?>">
                    </div>
                    <div class="form-group">
                    <label for="email">Email</label>
                        <input class="form-control" name="email" placeholder="Email" type="email" value="<?php echo $data['Email']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" class="form-control pw" name="pw" placeholder="Create Password">
                      </div>
                    <button class="btn btn-block btn-default" type="submit">Submit</button>
                </form>
            </div>
            
        </div>
        </div>
	</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your Account</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
<?php echo $data['State']?>
  </div>
</div>
<!-- Footer -->
<?php include '../../includes/footer.html';?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
    <script>
        $( document ).ready(function() {
            $('select#state').val("<?php echo strtoupper($data['State'])?>");
        });
    </script>
    <script>
	$('#submit').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
          url: desination,
          type: 'post',
          data: values, 
          success: function(data){
              if(data == 1){
                  $('.modal-body').html("<p>Account Updated, use link below to log into your account<br><br><a href='<?php echo baseurl()?>client/login.php'>Login Here</a>");
                  $('#myModal').modal('toggle');
              }
              else{
                $('.modal-body').html("<p>There was an error updating your account, please contact info@pmimd.com for assistance.</p>")
                $('#myModal').modal('toggle');
                  console.log(data);
              }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
		});
	
	});
</script>
</body>
</html>

