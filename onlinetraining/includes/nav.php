<head>
<style>
.dropdown:hover .dropdown-menu {
	display: block;
}
</style>
</head>
<div class="container-fluid" style="background-color: white;">
<nav class="navbar">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo baseurl()?>"><img src="https://www.pmimd.com/images/pmilogo.gif" alt="" height="40" style="margin-top:-10px;"></a>
                <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button"><span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="http://www.pmimd.com">Home</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="http://www.pmimd.com/certify">Certification<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="http://www.pmimd.com/certify/certified-medical-coder-certification.asp">Certified Medical Coder (CMC)</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/certify/certified-medical-insurance-specialist-certification.asp">Certified Medical Insurance Specialist (CMIS)</a>
                            </li>
                            <li>
                                 <a href="http://www.pmimd.com/certify/certified-medical-office-manager-certification.asp">Certified Medical Office Manager (CMOM)</a>
                            </li>
                            <li>
                                 <a href="http://www.pmimd.com/certify/certified-medical-compliance-officer-certification.asp">Certified Medical Compliance Officer (CMCO)</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/certify/ceutracker.asp">CEU Tracker</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/certify/FAQs.asp">FAQs</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="http://www.pmimd.com/programs/default.asp">Education<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo baseurl()?>">Online Training</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/programs/locator.asp">Class Locator</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/programs/default.asp">Class Descriptions</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/lcenters/default.asp">Learning Centers</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle"  href="http://www.pmimd.com/about/default.asp">About PMI <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="http://www.pmimd.com/instructors.php">PMI Instructors</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/pmi_news.php">PMI In The News</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/blog">PMI Blog</a>
                            </li>
                            <li>
                                <a href="http://www.pmimd.com/contact.asp">Contact Us</a>
                            </li>
                        </ul>
                    </li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <a href='<?php echo baseurl()?>client/portal.php'>PMI Portal</a>
                    </li>
                    <li class="hidden-lg hidden-md">
                        <a href="<?php echo baseurl()?>yourcart.php" style="padding: 0px 10px;"><img alt="" height="50px" src="<?php echo baseurl()?>images/ShoppingCart.gif"></a>
                    </li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <form action="<?php echo baseurl()?>search.php" class="navbar-form" method="get" role="search" style="width:250px; padding-left:30px;">
                            <div class="input-group">
                               <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit" style="color:white; background-color:#5371ad; border:1px solid #5371ad"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                                <input style="border-color: #5371ad" class="form-control" list="type" name="find" placeholder="Find a course" type="text">
                                    <option value="Coding">
                                        </option>
                                    <option value="Billing">
                                        </option>
                                    <option value="Compliance">
                                        </option>
                                        <option value="Management">
                                        </option>
                                    <option value="ICD-10">
                                        </option>
                                </datalist>
                            </div>
                        </form>
                    </li>
                </ul>
                <div class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                    <ul class="nav navbar-nav">
                        <li class="hidden-md">
                            <form action="<?php echo baseurl()?>search.php" class="navbar-form" method="get" role="search" class="col-lg-12 col-md-12">
                                <div class="input-group">
                                   <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit" style="color:white; background-color:#5371ad; border:1px solid #5371ad"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                    <input style="border-color: #5371ad" class="form-control" list="type" name="find" placeholder="Find a course" type="text"> 
                                    <datalist id="type">
                                        <option value="Coding"></option>
                                        <option value="Billing"></option>
                                        <option value="Compliance"></option>
                                        <option value="Management"></option>
                                        <option value="ICD-10"></option>
                                    </datalist>
                                    
                                </div>
                            </form>
                        </li>
                        <li>
                            <a href="<?php echo baseurl()?>yourcart.php" style="padding: 0px 10px;"><img alt="" height="50px" src="https://www.pmimd.com/onlinetraining/images/ShoppingCart.gif"></a>
                        </li>
                        <li>
                        	<?php NavLoginCheck()?>
                        </li>
                    </ul>
                </div>
            </div>
</nav>
</div>