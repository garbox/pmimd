<?php
// ------------- Sessions  -------------
if($_SERVER["HTTPS"] != "on"){
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

//check sessionn and send them to login page in they have no session
function SessionCheck(){
	//if seesion is set.. do nothing, else send user to login page. 
	if(isset($_SESSION['custID'])){
		header( 'http://www.pmimd.com/onlinetraining/payment/payinfo.php') ;
	}
	else{
	}
}

// ------------- Product Displays and Search  -------------

// Cache Test Function
//displays all products in order of type acending order
function Prod(){
	//Connect to DB and select all from SP where type is like certification.
	$conn = Connect();
	$select = "SELECT SQL_CACHE * FROM selfpaced WHERE Display = 1";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_object()){
							
				// Insert Code Here with PHP Inserts
				// Prod Container 
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	// print is nothing was found. 
	else{
		echo "Nothing was found";	
	}
}

//displays all Cert products in order of type acending order
function ProdCert(){
	//Connect to DB and select all from SP where type is like certification.
	$conn = Connect();
	$select = "SELECT * FROM selfpaced WHERE Type LIKE '%Certification%' AND Display = '1'";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_object()){
			// Associate Array
				
				// Insert Code Here with PHP Inserts
				// Prod Container 
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="certification/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="certification/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	// print is nothing was found. 
	else{
		echo "<p>Not available at the moment.</p>";	
	}
	
}

//displays all Top Picks products in order of type acending order
function ProdPicks(){
	// Connect to server while selecting all data. 
	$conn = Connect();
	$select = "SELECT * FROM selfpaced WHERE Type LIKE '%Featured%' AND Display = '1' ORDER BY Name ASC";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		//fetch data
		while($row = $result->fetch_object()){      
				
				// Insert Code Here with PHP Inserts
				// container layout 
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	// nothing found print below. 
	else{
		echo "<p>Not available at the moment.</p>";	
	}
	
}

//displays all Services products in order of type acending order
function ProdServices(){
	$conn = Connect();
	$select = "SELECT * FROM selfpaced WHERE Type LIKE '%Services%' AND Display = '1'";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_object()){            
				
				// Insert Code Here with PHP Inserts
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	else{
		echo "<p>Not available at the moment.</p>";	
	}
	
}

//displays all Core products in order of type acending order
function ProdCore(){
	$conn = Connect();
	$select = "SELECT * FROM selfpaced WHERE Type LIKE '%Core%' AND Display = '1'";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_object()){            
				
				// Insert Code Here with PHP Inserts
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	else{
		echo "<p>Not available at the moment.</p>";	
	}
	
}

//search function
function ProdSearch(){
	$find = $_GET['find'];
	$conn = Connect();
	
	// check if type var is empty, if so do full search
		$rowCount = "SELECT * FROM selfpaced WHERE Display = '1' AND Type LIKE '%".$find."%'
		OR (Display = '1' AND Name LIKE '%".$find."%')
		OR (Display = '1' AND ShortDescs LIKE '%".$find."%')
		OR (Display = '1' AND CartDescs LIKE '%".$find."%')
		OR (Display = '1' AND FullDesc LIKE '%".$find."%')
		OR (Display = '1' AND ProdID LIKE '%".$find."%')
		OR (Display = '1' AND InstName LIKE '%".$find."%')
		OR (Display = '1' AND CataID LIKE '%".$find."%')
        OR (Display = '1' AND Type LIKE '%".$find."%')";
	
	$result = $conn->query($rowCount);
	
	if($result->num_rows>0){
		while($row = $result->fetch_object()){
				
				// Insert Code Here with PHP Inserts
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>">
                                <img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>" alt="<?php echo $row->Name?>">
                            </a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                                <h4 align="center" class="blue"><?php echo $row->Name?></h4>
                            </div>
                            <p class="text"><?php echo $row->CartDescs?></p>
                            <div  style="margin-top:15px;">

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                                    <h3 style="margin-top:2px">$<?php echo $row->Price?></h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="productpage/index.php?prodID=<?php echo $row->ProdID?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	else{
		echo "<p>Nothing was found, please try a different search term.</p>";	
	}
	
}


//displays all Core products in order of type acending order
function ProdReimbursment(){
	$conn = Connect();
	$select = "SELECT * FROM selfpaced WHERE Type LIKE '%Reimbursement%' AND Display = '1'";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			// Associate Array Testing for faster excution
			// There was no difference meaning the slowdown is the communction between client side and server
				$ProdArray = array(
							'pname' => $row['Name'],
							'price' => $row['Price'],
							'img' => $row['Image'],
							'details' => $row['DetailsLink'],
							'Buy' => $row['PurchaseLink'],
							'ceus' => $row['CEUs'],
							'CartDescs' => $row['CartDescs'],
							'prodID' => $row['ProdID'],
							'cataID' => $row['CataID'],
							);
							
							echo $ProdArray['CataID'.$count]
            
				
				// Insert Code Here with PHP Inserts
				?>
                <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
                    <div class="ProdCont shadow">
                        <div class="imgContainer">
                            <a href="productpage/index.php?prodID=<?php echo $ProdArray['prodID']?>"><img class="imgPlacement imgSize center-block" src="http://www.pmimd.com/products/images/<?php echo $ProdArray['img']?>" alt="<?php echo $ProdArray['pname']?>"></a>
                        </div>    
                        <div class="prodContent">
                        	<div class="headerSize">
                            <h4 align="center"><?php echo $ProdArray['pname']?></h4>
                            </div>
                            <p class="text"><?php echo$ProdArray['CartDescs']?></p>
                            <div  style="margin-top:15px;">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                            <h3 style="margin-top:2px ;color:#999999">$<?php echo $ProdArray['price']?></h3>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <a href="productpage/index.php?prodID=<?php echo $ProdArray['prodID']?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
		}
	}
	else{
		echo "<p>Not available at the moment.</p>";	
	}
	
}

// ------------- Marketing Functions -------------

//needs edits display in order not how they are put into the function. 
//Marketing for recomended products.
function RecProd($prodID, $prodID2, $prodID3){
	$RecProd = array($prodID, $prodID2, $prodID3);
	$conn = Connect();
	$select = "SELECT ProdID, Name, Image FROM selfpaced WHERE ProdID='".$RecProd[0]."' AND Display='1' OR ProdID='".$RecProd[1]."'AND Display='1' OR ProdID='".$RecProd[2]."'AND Display='1' ";
	$count = 0;
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		
		while($row = $result->fetch_assoc()){
				$id = $row['ProdID'];
				$pname = $row['Name'];
				$img = $row['Image'];
				?>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" style="padding:5px;">
                    <a href="../productpage/index.php?prodID=<?php echo $id?>">
                    	<img  alt="<?php echo $pname?>" class="center-block img-fluid recomended" src="https://www.pmimd.com/products/images/<?php echo $img?>">
                    </a>
                    <p align="center"><?php  echo $pname?></p>
                </div>
                <?php
        }
		
	}
		
}

// ------------- Shopping Cart Functions -------------

// Function determins the pattern of AF and returns a 1 or 0
// This will allow to use both af and ProdID in the same function 
// and reduce database duplicates.
function af_pattern($prodID){
    return preg_match('/[0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]/', $prodID);
}

//add product to cart
function AddToCart(){
	// This functions take Cookie ID and also Prod ID and stores in into DB for shopping cart
	//Check if cookie is set
	
	// Vars
	$prodID = $_POST['ProdID'];		
	$cartID = $_COOKIE["cartIDs"];
        if(isset($_SESSION['Profile']['email'])){
        $email = $_SESSION['Profile']['email'];
    }
    else{
        $email = 1;
    }
	

	// Connection/ query
	$conn = Connect();
	$insert = "INSERT INTO cart (CookieID,ProdID,Email) VALUES ('".$cartID."', '".$prodID."', '".$email."')";
	
	// Move data to DB
	if($conn->query($insert)){
	echo $prodID.'<br>';	
	echo $cartID.'<br>';		
	echo $email.'<br>';		
	}
	else{
	echo "Error: " . $sql . "<br>" . $conn->error;	
	}
}

//Main function to display cart from cookie. 
function DisplayCart($prodID, $conn, $id){
        // Select from db
        $selectProd = "SELECT * FROM selfpaced WHERE ProdID = '".$prodID."'";
        
        //connect
        $results = $conn->query($selectProd);

        //check
        if($results->num_rows>0){
            while($rows = $results->fetch_assoc()){
                $img = $rows['Image'];
                $pname = $rows['Name'];
                $price = $rows['Price'];
                ?>
                <tr class="Remove" id="<?php echo $id?>">
                    <td> 
                    <form class="RemoveProd" id="<?php echo $id?>" action="scripts/removeprodcart.php" method="post">
                        <input type="hidden" name="cartID" value="<?php echo $_COOKIE['cartIDs']?>">
                        <input type="hidden" name="prodID" value="<?php echo $prodID?>">
                        <input type="hidden" name="ID" value="<?php echo $id?>">
                        <button class='Delete'><span class='glyphicon glyphicon-remove' style='color:#c01313; padding:5px;'></span></button>                            
                    </form>
                    </td>
                    <td><a href="productpage/index.php?prodID=<?php echo $prodID?>"><img src="https://www.pmimd.com/products/images/<?php echo $img?>" height='45px'></a></td>
                    <td colspan="3"><?php echo $pname?></td>
                    <td>$<?php echo number_format($price,2)?></td>
                </tr>
                <?php	
        }
    }	
}

//Main function to display cart from cookie. 
function DisplayCartAF($prodID, $id){
        $pmi_connect = pmimain_connect();
        $selectProd = "SELECT * FROM AF_Location_Info WHERE PrgID = '".$prodID."'";

        //connect
        $results = $pmi_connect->query($selectProd);
        //check
        if($results->num_rows>0){
            while($rows = $results->fetch_assoc()){
                $prg_date = date('M d, Y', strtotime($rows['prgdate']));
                $prg_name = $rows['prnname'];
                $prg_price = number_format($rows['pagbrochurefee'], 2);
                $prg_city = ucwords(strtolower($rows['CadCity']));
                $prg_state = strtoupper($rows['CadState']);
                $prg_code = $rows['PrnCode'];
                ?>
                <tr class="Remove" id="<?php echo $id?>">
                    <td>
                        <form class="RemoveProd" id="<?php echo $id?>" action="scripts/removeprodcart.php" method="post">
                            <input type="hidden" name="cartID" value="<?php echo $_COOKIE['cartIDs']?>">
                            <input type="hidden" name="prodID" value="<?php echo $prodID?>">
                            <input type="hidden" name="ID" value="<?php echo $id?>">
                            <button class='Delete'><span class='glyphicon glyphicon-remove' style='color:#c01313; padding:5px;'></span></button>
                        </form>
                    </td>
                    <td class="hidden-sm-down"><a href="http://www.pmimd.com/programs/<?php echo $prg_code?>.asp"><img src='https://www.pmimd.com/images/icon_live_training.gif' alt='' height='45'></a></td>
                    <td><?php echo $prg_name?></td>
                    <td><?php echo $prg_city . ", " .$prg_state?></td>
                    <td width="120"><?php echo $prg_date?></td>
                    <td><?php echo "$".$prg_price?></td>
                </tr>
                <?php 
            }
        }
}

// this function will combine both af and prodid cart info into one function and will sort between the two with 
// with a pattern function. AF have a patterns of xxxxx-xxxx. If prodID dosnt follow this pattern its safe to say its a sp prod id.
// and does not containe a location. 
function NewCartDisplay($email){
	// Vars	
	$cartID = $_COOKIE["cartIDs"];
	// Connection/ query
	$conn = Connect();
    //$pmi_connect = pmimain_connect();
       $selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'"; 
	   
			
	// SELECT data query
	$result = $conn->query($selectCart);	
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$prodID = $row['ProdID'];
			$id = $row['ID'];
            
            $bool = af_pattern($prodID);
            
            if($bool == 1){
                DisplayCartAF($prodID,$id);
            }
            else{
                DisplayCart($prodID, $conn, $id);
            }
        }
    }
	else{
		echo "There is nothing in your cart";	
	}	
    
}

//Function that goes with in the recipt email. This lays out the tables for the products ordred. 
//Discount and total are pulled from Discount class for correct values are already calculated. 
function ProdEmailDisplay(){
	// Vars	
	$cartID = $_COOKIE["cartIDs"];
	$Total = new Discounts;
	$Discount = $Total->DiscountShow($_SESSION['RCode']);
	$TotalShow = $Total->TotalCostShow($_SESSION['RCode']);
	
		
	// Connection/ query
	$conn = Connect();
    $PmiCOnnect = pmimain_connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'";
	
	// Move data to DB
	$result = $conn->query($selectCart);	
	
	// open table	
	$ProdList = $ProdList .= "<table border='0' cellpadding='5' cellspacing='0' width='600' id='emailContainer'>";
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
				$prodID = $row['ProdID'];
				
				// Select from db
				$selectProd = "SELECT * FROM AF_SP_Info WHERE AF = '".$prodID."'";
				
				//connect
				$results = $PmiCOnnect->query($selectProd);
				//check
				if($results->num_rows>0){
					while($rows = $results->fetch_assoc()){
						$pname = $rows['Name'];
						$price = $rows['Price'];
						$ProdList = $ProdList .= "<tr><td class='mcnTextContent' align='left' valign='top'>".$pname."</td><td class='mcnTextContent' align='center' valign='top'>$".number_format($price,2)."</td></tr>";	
					}
					
				}
		}
	}
	// close table
	$total = array_sum($total);
	//add discount if there is one.
	if($_SESSION['Discount'] == '1'){
		$ProdList = $ProdList .= "<tr><td class='mcnTextContent' align='left' valign='top'><h3>Discount</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".$Discount."</h3></td></tr>";	
	}
	$ProdList = $ProdList .= "<tr><td class='mcnTextContent' align='left' valign='top'><h3>Total</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".$TotalShow."</h3></td></tr>";	
	$ProdList = $ProdList .= "</table>";
	return $ProdList;
}

function ProdEmailDisplay2(){
	// Vars	
	$cartID = $_COOKIE["cartIDs"];
	$Total = new Discounts;
	$Discount = $Total->DiscountShow($_SESSION['RCode']);
	$TotalShow = $Total->TotalCostShow($_SESSION['RCode']);
    
    echo $Discount;
	
		
	// Connection/ query
	$conn = Connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."'";
	
	// Move data to DB
	$result = $conn->query($selectCart);	
	
	// open table	
	$ProdList = $ProdList .= "<table border='0' cellpadding='5' cellspacing='0' width='600' id='emailContainer'>";
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
				$prodID = $row['ProdID'];
            
            // If prodID is a AF, go to AF database. 
            if(af_pattern($prodID) ==1){
				// Select from db
				$selectProd = "SELECT * FROM AF_Location_Info WHERE PrgID = '".$prodID."'";
				$conn = pmimain_connect();
				//connect
				$results = $conn->query($selectProd);
				//check
				if($results->num_rows>0){
					while($rows = $results->fetch_assoc()){
						$pname = $rows['PrnName'];
						$price = number_format($rows['PagBrochureFee'], 2);
						$ProdList = $ProdList .= "<tr><td width='5%'><img src='http://www.pmimd.com/images/icon_live_training.gif' height='40px'></td><td class='mcnTextContent' align='left' valign='top'>".$pname."</td><td class='mcnTextContent' align='right' valign='top'>$".$price."</td></tr>";	
					}
					
				}
            }
            //else go to SP database
            else{
				// Select from db
				$selectProd = "SELECT * FROM selfpaced WHERE ProdID = '".$prodID."'";
				$conn = Connect();
				//connect
				$results = $conn->query($selectProd);
				//check
				if($results->num_rows>0){
					while($rows = $results->fetch_assoc()){
						$pname = $rows['Name'];
						$price = $rows['Price'];
						$ProdList = $ProdList .= "<tr><td width='5%'><img src='http://www.pmimd.com/products/images/".$rows['Image']."' height='40px'></td><td class='mcnTextContent' align='left' valign='top'>".$pname."</td><td class='mcnTextContent' align='right' valign='top'>$".$price."</td></tr>";	
					}
					
				}
            }
		}
	}
	// close table
	$total = array_sum($total);
	//add discount if there is one.
	if($_SESSION['Discount'] == '1'){
		$ProdList = $ProdList .= "<tr><td colspan='2' class='mcnTextContent' align='left' valign='top'><h3>Discount</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".$Discount."</h3></td></tr>";	
	}
	$ProdList = $ProdList .= "<tr><td colspan='2' class='mcnTextContent' align='left' valign='top'><h3>Total</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".$TotalShow."</h3></td></tr>";	
	$ProdList = $ProdList .= "</table>";
	return $ProdList;
}

//Function that applies discout to user if they are certified
function Discount(){
	// Vars	
	$cartID = $_COOKIE["cartIDs"];
	$total = array();
	
	// Connection/ query
	$conn = Connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'";
	
	// exec query
	$result = $conn->query($selectCart);		
	
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$prodID = $row['ProdID'];
			
			// Select from db
			$selectProd = "SELECT * FROM selfpaced WHERE ProdID = '".$prodID."'";
			
			//connect
			$results = $conn->query($selectProd);
			
			//check
			if($results->num_rows>0){
				while($rows = $results->fetch_assoc()){
					$price = $rows['Price'];
					array_push($total, $price);			
				}
			}
		}
	}
	$total = array_sum($total);
	
	//If Certified, apply discount. 
	if($_SESSION['Profile']['Cert'] == '1'){
		$total = $total * .1;
		$total = number_format($total, 2, '.', '');
		$display = "<p class='discount'>Discount<span style='color:#0ca24b;'> - $".$total."</span></p>";
	}
	else{
		$display = NULL;	
	}
	
	return $display;
		
}

// Discount that gets shown on the email if they have a discount. 
function DiscountEmail(){
		// Vars	
	$cartID = $_COOKIE["cartIDs"];
	$total = array();
	
	// Connection/ query
	$conn = Connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'";
	
	// exec query
	$result = $conn->query($selectCart);		
	
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$prodID = $row['ProdID'];
			
			// Select from db
			$selectProd = "SELECT * FROM selfpaced WHERE ProdID = '".$prodID."'";
			
			//connect
			$results = $conn->query($selectProd);
			
			//check
			if($results->num_rows>0){
				while($rows = $results->fetch_assoc()){
					$price = $rows['Price'];
					array_push($total, $price);			
				}
			}
		}
	}
	$total = array_sum($total);
	
	//If Certified, apply discount. 
	if($_SESSION['Profile']['Cert'] == '1'){
		$total = $total * .1;
		$total = number_format($total, 2, '.', '');
		$display = "<tr><td class='mcnTextContent' align='left' valign='top'><h3>Discount</h3></td><td class='mcnTextContent' align='right' valign='top'><h3 style='color:#0ca24b'>$".$total."</h3></td></tr>";
		
	}
	else{
		$display = NULL;	
	}
	
	return $display;
		
}

//This function show to count in the shopping cart.
function ItemCount(){

	$cartID = $_COOKIE['cartIDs'];
	
	// Connection/ query
	$conn = Connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'";
	
	// Move data to DB
	$result = $conn->query($selectCart);
	$count = $result->num_rows;
	
	return $count;	
	
}

// Removes Product from Cart per user request. 
// Tied in with Ajax
function RemoveProd(){
	$ID = $_POST['ID'];
	$cartID = $_COOKIE['cartIDs'];
	
	echo $prodID;
	echo $cartID;
	$conn = Connect();
	$Delete = "DELETE FROM cart WHERE ID = '".$ID."' AND CookieID = '".$cartID."'";	
	
	$conn->query($Delete);
}

// Make Cookies 
function CookieforCart(){
        //header( 'Location: http://www.pmimd.com/Maintenance.html' ) ;
        $data = $_REQUEST;
        if($data['tc']!=''){
            setcookie('tc', $data['tc'], time() + (86400 * 7), "/");
        }
    
        if($data['mc_cid']!=''){
            setcookie('mc_cid', $data['mc_cid'], time() + (86400 * 7), "/");
        }
            
		$PMI = "cartIDs";
		$cartID = uniqid();
		
		if(!isset($_COOKIE[$PMI])) {
			setcookie($PMI, $cartID, time() + (86400 * 7), "/"); // 86400 = 1 day
		}
}

// Total Cost in cart
function TotalCost(){
	// Vars	
	$cartID = $_COOKIE['cartIDs'];
	$total = array();
	
	// Connection/ query
	$conn = Connect();
    $pmiconnect = pmimain_connect();
	$selectCart = "SELECT * FROM cart WHERE CookieID = '".$cartID."' OR Email = '".$_SESSION['Profile']['email']."'";
	
	// exec query
	$result = $conn->query($selectCart);		
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$prodID = $row['ProdID'];
			// Select from db
            
                $selectProd = "SELECT * FROM AF_SP_Info WHERE AF = '".$prodID."'";
                $results = $pmiconnect->query($selectProd);
                if($results->num_rows>0){
                    while($rows = $results->fetch_assoc()){
                        $price = $rows['Price'];
                        array_push($total, $price);	
                    }
			    }		
		}
	}
	$total = array_sum($total);

	$total = number_format($total, 2, '.', '');
	return $total;
}

// Function checks cart classID to see if it requires a different shipping address. 
function ShippingInfoSP(){
	// Connect and find all product numbers in cart. 
	$conn = Connect();
	$SelectProdID = "SELECT ProdID FROM cart WHERE CookieID	= '".$_COOKIE["cartIDs"]."'";	
	$resultProdID = $conn->query($SelectProdID);
	
	while($rowProdID = $resultProdID->fetch_assoc()){
		$ProdID = $rowProdID['ProdID'];
		
		$SelectCataID = "SELECT CataID FROM selfpaced WHERE ProdID = '".$ProdID."'";
		$resultCataID = $conn->query($SelectCataID);
		
		while($rowCataID = $resultCataID->fetch_assoc()){
			$CataID = $rowCataID['CataID'];
			
			if($count <1){
				if($CataID == "SP-CMCA" || $CataID == "SP-CMC" || $CataID == "SP-CMOM" || $CataID == "SP-CMIS" || $CataID == "SP-CMCO" || $CataID == "SP-MT"){
                   	?>
                    <p>Shipping address different from billing?</p>
                    <div class="radioError">
                    <label class="radio-inline"><input type="radio" class="yes" value="YES" name="optradio">Yes</label>
                    <label class="radio-inline"><input type="radio" class="no" value="NO" name="optradio">No</label><br>
                    </div>
                    <span class="test"></span>
                    <script>
					$( 'input[type=radio]' ).click(function() {
						var myClass = $(this).attr("class");
						var val = $("."+myClass+"").val();
						
						if(val == "YES"){
						$('.test').html("<br><div class=form-group><div style='border-top:1px solid #e6e6e6;padding-top:10px'><div class=form-group><label>First Name</label><input class='form-control email'name=ShipFname placeholder='First Name'value='<?php echo $_SESSION['ShippingInfo']['ShipFname']?>'></div><div class=form-group><label>Last Name</label><input class='form-control email'name=ShipLname placeholder='Last Name'value='<?php echo $_SESSION['ShippingInfo']['ShipLname']?>'></div><div class=form-group><label>Address</label><input class='form-control lname'name=ShipAddress placeholder=Address value='<?php echo $_SESSION['ShippingInfo']['ShipAddress']?>'></div><div class=form-group><label>City</label><input class='form-control email'name=ShipCity placeholder=City value='<?php echo $_SESSION['ShippingInfo']['ShipCity']?>'></div><div class=form-group><label>State</label><input class='form-control email'name=ShipState placeholder=State value='<?php echo $_SESSION['ShippingInfo']['ShipState']?>'></div><div class=form-group><label>Zip</label><input class='form-control email'name=ShipZip placeholder=Zip value='<?php echo $_SESSION['ShippingInfo']['ShipZip']?>'></div><div class=form-group><div class=form-group><label>Phone Number</label><input class='form-control phone'name='ShipPhone' placeholder='Phone Nubmer'value='<?php echo $_SESSION['ShippingInfo']['ShipPhone']?>'></div></div>");
						}
						else{
							$('.test').empty();
						}
					});
					</script><br>
					<?php
					$count++;
				}
			}
		}		
	}
}

// Function that returns info to the email with their shipping address. 
function ShippingInfoEmail(){
	if(!empty($_SESSION['ShippingInfo']['ShipFname'])){
	$ShippingInfoReturn .= "<h3>Shipping Infomation</h3><br>";
	$ShippingInfoReturn .= $_SESSION['ShippingInfo']['ShipFname'] . " ". $_SESSION['ShippingInfo']['ShipLname'] . "<br>";
    $ShippingInfoReturn .= $_SESSION['ShippingInfo']['ShipAddress'] . "<br>";
    $ShippingInfoReturn .= $_SESSION['ShippingInfo']['ShipCity'].", ".$_SESSION['ShippingInfo']['ShipState'] . " " . $_SESSION['ShippingInfo']['ShipZip'] ."<br>";
    $ShippingInfoReturn .= $_SESSION['ShippingInfo']['ShipPhone'];
	}
	return $ShippingInfoReturn;
}

// this is the discount class. This allows us to create multible discount and then check to see what one is greater and apply. 
// there is a public var called discount, this is used to store the % of discount. 
// then this will be applied to the BiggestDiscountCheck() functuon and returns biggest discount. 
// Finally it will be apllied to the total cost and how much user has saved. 
class Discounts{
	//global array to be used in all functions. 
	private $discount = array();
	
	//Cert Discount, checks upon log-in and saved as session cookie. 
	private function CertDiscount($GradID){
        $pmiconnect = pmimain_connect();
        $GradSelect = "SELECT GradID FROM Current_Grad_Check 
                WHERE GradFirstName = '".$_SESSION['Profile']['fname']."' 
                AND GradLastName = '".$_SESSION['Profile']['lname']."' 
                AND GradID = '".$GradID."' GROUP BY GradID";
        $result = $pmiconnect->query($GradSelect);
        if($result->num_rows >0){
            $discountValue = .1	;
            $_SESSION['Discount'] = "1";
        }
        return $discountValue;
    }
    
	//referal code, if true needs to be stored in DB when check out is completed for commision. 
	// Possible Null Function 
	private function ReferalDiscount($code){	
		session_start();	
		$conn = Connect();
		
		$Select = "SELECT Discount, Name 
				  FROM referralcodes 
				  WHERE Code = '".$code."'";
		
		$result = $conn->query($Select);
		
		if($result->num_rows>0){
			while($row = $result->fetch_assoc()){
				$ReferalDiscount = $row['Discount'];
				$_SESSION["ReferalMult"] = $ReferalDiscount;
				$_SESSION['Discount'] = "1";
				$_SESSION['RCode'] = $code;	
			}
		}
		else {
			$ReferalDiscount = NULL;
		}
		
		return $ReferalDiscount;
	}
	
	// Pams 50% Discount code that applies to all calsses but cert and ta
	private function PJ50($code){
				$conn = Connect();
				$selectCartInfo = "SELECT * FROM cart WHERE CookieID = '".$_COOKIE['cartIDs']."'";
				
				$resultCartProdID = $conn->query($selectCartInfo);
				
				$prodIDs = array();
				while($rowProdID = $resultCartProdID->fetch_assoc()){
					array_push($prodIDs, $rowProdID["ProdID"]);
				}
				
				$count = 0;

				while(sizeof($prodIDs) >= $count){
					if($prodIDs[$count] == 119 || $prodIDs[$count] == 120 || $prodIDs[$count] == 121 || $prodIDs[$count] == 122){
						$ReferalDiscount = 0;
						$count = sizeof($prodIDs) + 1;
					}
					else{
						$ReferalDiscount = .5;
						$count++;	
					}
				}
				return $ReferalDiscount;
				
			}
	
	// discount codes, if true apply discount and possible store in DB
	private function DiscountCodes($code){
		session_start();	
		$conn = Connect();
		
		$Select = "SELECT Discount, Name 
				  FROM discountcode 
				  WHERE Code = '".$code."'";
		
		$result = $conn->query($Select);
		
		if($result->num_rows>0){
			if(strtoupper($code) == "PJ50"){
				$ReferalDiscount = $this->PJ50($code);
			}
			else{
				while($row = $result->fetch_assoc()){
					$ReferalDiscount = $row['Discount'];
					$_SESSION["ReferalMult"] = $ReferalDiscount;
					$_SESSION['Discount'] = "1";
				}
			}
		}
		else {
			$ReferalDiscount = NULL;
		}
		
		return $ReferalDiscount;
		
	}
			
	//checks if the referal code is valid and returns value. Also sets Referal code as a session when valid, 
	//thus users only needs to enter code once for the session. when they close all browsers, code is lost and needs to be entered agian. 
	// Possible Null Function
	public function ReferalDiscountBool($code){		
		$conn = Connect();
		
		$Select = "SELECT Discount, Name 
				  FROM referralcodes 
				  WHERE Code = '".$code."'
				  UNION ALL 
				  SELECT Discount, Name 
				  FROM discountcode
				  WHERE Code = '".$code."'";
		
		$result = $conn->query($Select);
		
		if($result->num_rows>0){
			while($row = $result->fetch_assoc()){
				$ReferalDiscount = 1;
				$_SESSION['Discount'] = 1;	
				$_SESSION['RCode'] = $code;
			}
		}
		else {
			$ReferalDiscount = 0;
		}
		return $ReferalDiscount;
	}
	
	//check global array for the biggest discount. 
	private function BiggestDiscountCheck($code){	
		// create instances 	
		$cert = $this->CertDiscount($_SESSION['Profile']['Cert']);
		$referal = $this->ReferalDiscount($code);
		$discount = $this->DiscountCodes($code);
		
		//push items to global array. 
		array_push($this->discount, $cert, $referal, $discount);
		
		rsort($this->discount);
		//sort array decending
		$_SESSION["ReferalMult"] = $this->discount[0];
		
		return $this->discount[0] + $memorialDiscount;
	}
	
	//returns the value of the discount mult. and show discount ammount. 
	public function DiscountShow($code){	
		$discountmulit = $this->BiggestDiscountCheck($code);
		
		$totalCost = TotalCost();
                
		$discountShow = $totalCost * $discountmulit;
		$discountShow = $discountShow + $memorialDiscount;
		//if($_SESSION['Discount'] == "1"){
			return number_format($discountShow, 2, '.', '')."</p>";
		//}
	}
	
	//function that shows the total cost with discount.
	public function TotalCostShow($code){
		$discountmulit = $this->BiggestDiscountCheck($code);
		$totalCost = TotalCost();
		$discountShow = $totalCost * $discountmulit;
		$total = $totalCost - $discountShow;

		return number_format($total, 2, '.', '');
	}
}

// ------------- User Account Functions -------------

//auth the user and returns var.
function Auth(){
	session_start();
	//start session and compture login info
	$pass = md5($_POST['pw']); 
	$user = $_POST['email'];
	
	//connect to DB
	$conn = Connect();
	
	//Select all quries
	$select = "SELECT ID, Cert, Email, Password, Fname, Lname, Address, City, State, Zip, Phone, Manager, Practice, Fname FROM customers WHERE Email ='".$user."' AND Password = '".$pass."'";
	$results = $conn->query($select);
	
	$data = $results->num_rows;
	//search in db for user and passwrod, also store quires in var to store in sessions.
	if ($results->num_rows > 0) {
		while($row = $results->fetch_assoc()){
			$_SESSION['Profile'] = array(
				'ID' => $row['ID'],
				'Cert' => $row['Cert'],
				'fname' => $row['Fname'],
				'lname' => $row['Lname'],
				'address' => $row['Address'],
				'city' => $row['City'],
				'zip' => $row['Zip'],
				'state' => $row['State'],
				'email' => $row['Email'],
				'phone' => $row['Phone'],
				'practice' => $row['Practice'],
				'manager' => $row['Manager']);
				$bool= true;
				$_SESSION['LoginCheck'] = "yes";
                LoginTracking($row['ID']);
			
		}
	}
	else{
		$bool = false;
		$_SESSION['LoginCheck'] = "no";
	}
	echo $bool;
	
			
}

//auth the user and returns var.
//More Sucure Password System
function Auth2(){
	session_start();
	//start session and compture login info
	$pass = $_POST['pw']; 
	$user = $_POST['email'];
	
	//connect to DB
	$conn = Connect();
	
	//Select all quries
	$select = "SELECT ID, Cert, Email, Password, Fname, Lname, Address, City, State, Zip, Phone, Manager, Practice, Password, Fname FROM customers WHERE Email ='".$user."'";
	$results = $conn->query($select);
	
	$data = $results->num_rows;
	while($row = $results->fetch_assoc()){
		$pw = $row["Password"];
		
		if (password_verify($pass, $pw))
		{ 
		  echo "Verified";
		}
		else
		{
		  echo "Somethings Wrong";
		}	
	}		
}

// Add user to db.
//More Sucure Password System
function AddUser2(){
	$firstName = $_POST['firstname'];
	$lastName = $_POST['lastname'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$city = $_POST['city'];
	$zip = $_POST['zip'];
	$state = $_POST['state'];
	$email = $_POST['email'];
	$pw = password_hash($_POST['pw'], PASSWORD_BCRYPT);
	$manager = $_POST['manager'];
	$practice = $_POST['practice'];
	$cert = CertCheck($_POST['certID']);
	
	$conn = Connect();
	
	$select = "SELECT ID FROM customers";
	$insert = "INSERT INTO customers (ID, Cert, Fname, Lname, Address, City, Zip, State, Email, Phone, Practice, Manager, Password) 
				VALUES ('".$ID."', '".$cert."', '".$firstName."', '".$lastName."', '".$address."', '".$city."', '".$zip."', '".$state."', '".$email."', '".$phone."', '".$practice."',  '".$manager."', '".$pw."')";
				
	$result = $conn->query($insert);
	$result = $conn->query($select);
	
	$id = $result->num_rows;
	$_SESSION['custID'] = $id;
		
}

function LoginTracking($CustID){
    $conn = Connect();
    $Insert = "INSERT INTO logintracking (CustID) VALUES ('".$CustID."')";
    $conn->query($Insert);
    
}

// Add user to db. and checks if email is already in the DB. If so asked them to reset their password. 
function AddUser(){
	$firstName = $_POST['firstname'];
	$lastName = $_POST['lastname'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$city = $_POST['city'];
	$zip = $_POST['zip'];
	$state = $_POST['state'];
	$email = $_POST['email'];
	$pw = md5($_POST['pw']);
	$manager = $_POST['manager'];
	$practice = $_POST['practice'];
	$cert = $_POST['certID'];
	
	$conn = Connect();
	
	$emailCheck = "SELECT Email FROM customers WHERE Email = '".$email."'";
	$EcheckResult = $conn->query($emailCheck);
	if($EcheckResult->num_rows>0){
		echo $NewEmail = 0;	
	}
	else{
	$select = "SELECT ID FROM customers";
	$insert = "INSERT INTO customers (ID, Cert, Fname, Lname, Address, City, Zip, State, Email, Phone, Practice, Manager, Password) 
				VALUES ('".$ID."', '".$cert."', '".$firstName."', '".$lastName."', '".$address."', '".$city."', '".$zip."', '".$state."', '".$email."', '".$phone."', '".$practice."',  '".$manager."', '".$pw."')";
				
	$result = $conn->query($insert);
	$result = $conn->query($select);
	
	$id = $result->num_rows;
	$_SESSION['custID'] = $id;
		echo $NewEmail= 1;
	}
}

// Edit Users Account. 
// Not working correctly
// dosnt store info to DB. after first submit
function EditUser(){
	$UserUpdate = array (
		'address' => $_POST['address'],
		'city' => $_POST['city'],
		'zip' => $_POST['zip'],
		'state' => $_POST['state'],
		'phone' => $_POST['phone'],
		'manager' => $_POST['manager'],
		'practice' => $_POST['practice'],
		'ID' => $_POST['ID']
	);	
	
    echo $UserUpdate['fname'];
	$conn = Connect();
	$update = "UPDATE customers SET 
		Address = '".$UserUpdate['address']."', 
		City = '".$UserUpdate['city']."',
		Zip = '".$UserUpdate['zip']."',
		State = '".$UserUpdate['state']."',
		Phone = '".$UserUpdate['phone']."',
		Manager = '".$UserUpdate['manager']."',
		Practice = '".$UserUpdate['practice']."'
		WHERE ID = ".$UserUpdate['ID']."";
    
	if ($conn->query($update) === TRUE) {
		echo "1";
	} 
	else {
    	echo "Error updating record: " . $conn->error;
	}
}

// function to check certification in database to see if they are true. 
// Not Functioning
function CertCheck($certID){
	$conn = Connect();
	$select = "SELECT GradID FROM certsid WHERE GradID = '".$certID."'";
	$result = $conn->query($select);
	if($result->num_rows>0){
		$data = 1	;
	}
	else{
		$data = 0;
	}
	
	return $data;
}

//logs customer out be deleting session
// Simple yet Effective.
function Logout(){
	unset($_SESSION['LoginCheck']);
	unset($_SESSION['Profile']);
	unset($_SESSION['Discount']);
	unset($_SESSION['RCode']);
}

// Pull classes from DB that customer has bought. 
// Stores based off of user id, this prevents "account lost" when they change email
function CustClasses(){
	$count = 0;
	$conn = Connect();
	$select = "SELECT custID, prodID FROM custportal WHERE custID = '".$_SESSION['Profile']['ID']."'";
	$result = $conn->query($select);
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$ClassID = $row['prodID'];
			
			$selectProd = "SELECT Name, Image, CataID, ProdID FROM selfpaced WHERE ProdID = '".$ClassID."'";
			
			$resultProd = $conn->query($selectProd);
			if ($resultProd->num_rows > 0) {
				echo "<table>";
				while($row = $resultProd->fetch_assoc()){
					$ProdArray = array(
						'pname' => $row['Name'],
						'img' => $row['Image'],
						'cataID' => $row['CataID'],
						'prodID'=> $row['ProdID']
					);
					//count++ for ID
					$count++;
					//Insert HTML Code
					?>
                    <tr id="<?php echo $count?>">
                    	<td>
                        	<img src="https://www.pmimd.com/products/images/<?php echo $ProdArray['img']?>" alt="" height="50px;">
                        </td>
                    	<td width="350px">
                        	<p><?php echo $ProdArray['pname']?></p>
                        </td>
                    	<td>
                        <!-- Needs ACtion for form -->
                            <form id="VideoLinks" action="videostage.php" method="post" style="display:inline; max-width:100px">
                                <input type="hidden" name="ProdID" value="<?php echo $ProdArray['prodID']?>">
                                <input type="hidden" name="Name" value="<?php echo $ProdArray['pname']?>">
                                <button id="BuyButton" type="submit" class="btn btn-default" style="width:100px; background-color:#5371ad; color:white">Your Videos</button>
                            </form>
                        </td>
                        <td>
                        <!-- Needs ACtion for form -->
                            <form class="Archive" id="<?php echo $count?>" action="../scripts/archive.php" method="post" style="display:inline; max-width:100px">
                                <input type="hidden" name="prodID" value="<?php echo $ProdArray['prodID']?>">
                                <button id="BuyButton" type="submit" class="btn btn-primary" style="width:100px; color:white">Archive</button>
                            </form>
                        </td>
                    </tr>
                    <?php 
					//END HTML Code
				}
			}
		}
	}
	else{
		echo "You currently have no active classes.";
	}
	echo "</table>";
}

//Pulls info from Arcive DB for the users, stored via User ID. 
function ArchClasses(){
	$count = 0;
	$conn = Connect();
	
	$select = "SELECT custID, prodID FROM custarchive WHERE custID = '".$_SESSION['Profile']['ID']."'";
	
	$result = $conn->query($select);
	
	if($result->num_rows>0){
		while($row = $result->fetch_assoc()){
			$ClassID = $row['prodID'];
			
			$selectProd = "SELECT Name, Image, CataID, ProdID FROM selfpaced WHERE ProdID = '".$ClassID."'";
			
			$resultProd = $conn->query($selectProd);
			if ($resultProd->num_rows > 0) {
				echo "<table>";
				while($row = $resultProd->fetch_assoc()){
					$ProdArray = array(
						'pname' => $row['Name'],
						'img' => $row['Image'],
						'cataID' => $row['CataID'],
						'prodID'=> $row['ProdID']
					);
					//count++ for ID
					$count++;
					//Insert HTML Code
					?>
                    <tr id="<?php echo $count?>">
                    	<td>
                        	<img src="https://www.pmimd.com/products/images/<?php echo $ProdArray['img']?>" alt="" height="50px;">
                        </td>
                    	<td width="350px">
                        	<p><?php echo $ProdArray['pname']?></p>
                        </td>
                    	<td>
                        <!-- Needs ACtion for form -->
                            <form id="VideoLinks" action="VideoStage.php" method="post" style="display:inline; max-width:100px">
                                <input type="hidden" name="ProdID" value="<?php echo $ProdArray['prodID']?>">
                                <input type="hidden" name="Name" value="<?php echo $ProdArray['pname']?>">
                                <button id="BuyButton" type="submit" class="btn btn-default" style="width:100px; background-color:#5371ad; color:white">Your Videos</button>
                            </form>
                        </td>
                        <!-- Needs ACtion for form -->
                        <td>
                           <form class="Archive" id="<?php echo $count?>" action="../scripts/CurrentContent.php" method="post" style="display:inline;">
                                <input type="hidden" name="prodID" value="<?php echo $ProdArray['prodID']?>">
                                <button id="BuyButton" type="submit" class="btn btn-primary" style=" color:white">Move to Current Content</button>
                            </form>
                        </td>
                    </tr>
                    <?php 
					//END HTML Code
				}
			}
		}
	}
	else{
		echo "You have not archived any classes yet.";
	}
	echo "</table>";
}

// Contact us with Mandrill API.
function ContactUs($message){

		$phone = $_SESSION['Profile']['phone'];
		$email = $_SESSION['Profile']['email'];
		$fullname = $_SESSION['Profile']['fname'] ." ". $_SESSION['Profile']['lname']; 
	
	// Email template function
	try {
	require_once '../mandrill-api-php/src/Mandrill.php';
	$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
	$template_name = 'general-email-prodcenter';
	// content area, name = mcedit in main source code
	// content  = whats going in that container.
	$template_content = array(
		array(
			'name' => 'message',
			'content' => $message
		),
		array(
			'name' => 'fullname',
			'content' => $fullname
		),
		array(
			'name' => 'email',
			'content' => $email
		),
		array(
		
			'name' => 'phone',
			'content' => $phone
		),						
	);
	$message = array(
		'html' => 'Product Center: Message Received',
		'text' => 'Product Center: Message Received',
		'subject' => 'Product Center: Message Received',
		'from_email' => 'info@pmimd.net',
		'from_name' => 'pmiMD Product Center',
		'to' => array(
			array(
				'email' => 'info@pmimd.com', //email address
				'name' => $fullname, // name of client
				'type' => 'to'
			)
		),
		'important' => false,
		'track_opens' => false,
		'track_clicks' => false,
		'auto_text' => null,
		'auto_html' => null,
		'inline_css' => true,
		'url_strip_qs' => null,
		'preserve_recipients' => null,
		'view_content_link' => null,
		'bcc_address' => '', // bcc email address
		'tracking_domain' => null,
		'signing_domain' => null,
		'return_path_domain' => null,
		'merge' => true,
		'merge_language' => 'mailchimp',
		'global_merge_vars' => array(
			array(
				'name' => 'merge1',
				'content' => 'merge1 content'
			)
		),
		// Image needed for tracking opens
		'images' => array(
			array(
				'type' => 'image/png',
				'name' => 'IMAGECID',
				'content' => 'ZXhhbXBsZSBmaWxl'
			)
		)
	);
	//async is for bulk sending.
	$async = false;
	$ip_pool = 'Main Pool';
	$send_at = '2015-01-01';
	$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
	
	} 
	catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id ''
		throw $e;
	}	
}

//The process of removed Products from CustomerPortal to the archive DB. Product is deleted from Customer Portal DB. 
function Archive(){
	$prodID = $_POST['prodID'];
	$conn = Connect();
	
	$insert = "INSERT INTO custarchive (custID, prodID) VALUES ('".$_SESSION['Profile']['ID']."','".$prodID."')";
	$delete = "DELETE FROM custportal WHERE custID = '".$_SESSION['Profile']['ID']."' AND prodID = '".$prodID."'";
	$conn->query($insert);
	$conn->query($delete);	
}

// Move archived items to current content. 
function CurrentContentMove(){
	$prodID = $_POST['prodID'];
	$conn = Connect();
	
	$insert = "INSERT INTO custportal (custID, prodID) VALUES ('".$_SESSION['Profile']['ID']."','".$prodID."')";
	$delete = "DELETE FROM custarchive WHERE custID = '".$_SESSION['Profile']['ID']."' AND prodID = '".$prodID."'";
	$conn->query($insert); 
	$conn->query($delete);	
}

function PDFDisplay($prodID){
	$conn = Connect();
	$select = "SELECT *	FROM courseresources WHERE ProdID = '".$prodID."'";
	$result = $conn->query($select);
	
    if($result->num_rows > 0){
        echo "<div class='col-md-2'> 
        <h4>Course Resources:</h4>";   

        while($row = $result->fetch_assoc()){
            $ClassRes = $row["ClassResource"];
            $ResLocation = $row["ResourceLocation"];
            if(!empty($ClassRes)){
                echo "<li class='ClassResource'><a style=' font-size:16px;'target='_blank'href='http://www.pmimd.com/onlinetraining/SP-PDF/".$ResLocation."'>".$ClassRes."</a></li>";
            }
	}
        echo "</div><div class='col-md-10'>";
    }
    else{
        echo "<div class='col-md-12'>";
    }
		
}

//displays videos from DB if user is enrolled. 
//has validation if user tries to change prodID
class VideoStage{
	
	// function to check DB if customer has class in their protal. 
	// Insure customers cant change var values and get a product they didnt reg for. 
	private function VerifyVideoAllowance($email, $prodID){
		$conn = Connect();
		$select = "SELECT email, prodID FROM custportal WHERE email = '".$email."' AND prodID = '".$prodID."'";
		$result = $conn->query($select);
		
		if($result->num_rows>0){
				$display = true;
		}
		else{
			$display = false;
		}
	    return $display;
	}
	
	// This checker checks to see if $video has a value. 
	// ensures broken links are not displayed due to empty colums. 
	private function VideoLayout($VideoID, $Link, $count, $title, $prodID){
		
		if(!empty($Link)){
		// spit out html for layout	
		?>
        <div class="col-md-3 col-sm-4 col-xs-6 col-lg-3" align="center" style="padding:20px; margin-bottom:20px;"> 
            <p><?php echo $title ?></p>
        	<a href="http://www.pmimd.com/onlinetraining/client/videohub.php?VideoID=<?php echo $VideoID?>&ProdID=<?php echo $prodID?>" target="_blank"><img src="../images/PLayButton.gif" alt="" height="50px"></a><br><br>
        </div>
        <?php
		}
		else{
            //do nothing/
		}
	}
	
	//Uses functions from above to display vidoes. 
	public function DisplayVideo($email, $prodID){
		$display = new VideoStage();
		$display->VerifyVideoAllowance($email, $prodID);
		$count = 1;
		
		if($display == true){
			$conn = Connect();
			if($prodID == 119){
				$select = "SELECT ID, FileName, Title FROM videos WHERE ClassID = '".$prodID."' OR ClassID = 124 ORDER BY ID";
			}
			else{
				$select = "SELECT ID, FileName, Title FROM videos WHERE ClassID = '".$prodID."' ORDER BY ID";
			}
			
			$result = $conn->query($select);
			
            if($result->num_rows >1){
			while($row = $result->fetch_assoc()){
				$video = array(
					'FileName' => $row['FileName'],
                    'ID' =>$row['ID']
				);
				$title = $row['Title'];

				$ShowVideo = new VideoStage();
				$ShowVideo->VideoLayout($video['ID'], $video['FileName'], $count, $title, $prodID);
				$count++;
			}
            }
            else{
                echo "<p>Content Coming Soon. <br>If this is an error, please contact us at <a style='font-size:15px;' href='mailto:support@pmimd.com?Subject=Missing%20Content%20ProdID%20".$prodID."'>support@pmimd.com</a></p>";
            }
				
		}
			
	}
}

// Functions to forgotpassword 
class ForgotPWSequence{
	
	//step One
	//function to create key.
	private function KeyGen(){
		$GneKey = md5(uniqid());
		return $GneKey;
	}
	
	//step two
	//Take the genKey and store in DB. 
	private function ForgotPWAction($email){
		//generate key with function. 
		$GneKey = new ForgotPWSequence();
		$GneKey = $GneKey->KeyGen();
		
		$conn = Connect();
		$insert = "INSERT INTO resetpw (EncryptKey, Email) VALUE ('".$GneKey."', '".$email."')";
		$conn->query($insert);
		return $GneKey;
	}
	
	//Step Three
	//Gather all info, send email and exe above functions. 
	public function UserAuthAndEmailSend($email){
		//check if user is in DB
		$conn = Connect();
		$select = "SELECT Email FROM customers WHERE Email = '".$email."'";
		$result = $conn->query($select);
		
		if($result->num_rows>0){
		
		// call action function.
		$ForgotPW = new ForgotPWSequence();	
		$ForgotPW = '<a class="mcnButton " title="Rest Password" href="http://pmimd.com/onlinetraining/Scripts/GenKeyCheck.php?GenKey='.$ForgotPW->ForgotPWAction($email).'" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Reset Password</a>';

		
		// call and get mandrill template and send it with keygen as link.
		try {
		require_once '../mandrill-api-php/src/Mandrill.php';
		$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
		$template_name = 'forgotpw-template';
		// content area, name = mcedit in main source code
		// content  = whats going in that container.
		$template_content = array(
			array(
				'name' => 'fname',
				'content' => $fullname
			),
			array(
				'name' => 'GenKey',
				'content' => $ForgotPW
			),				
		);
		$message = array(
			'html' => '<p>Product Center: Message Recieved</p>',
			'text' => 'Example text content',
			'subject' => 'PMI Portal Password Reset',
			'from_email' => 'webmaster@pmimd.com',
			'from_name' => 'pmiMD',
			'to' => array(
				array(
					'email' => $email, //email address
					'name' => $fullname, // name of client
					'type' => 'to'
				)
			),
			'important' => false,
			'track_opens' => false,
			'track_clicks' => false,
			'auto_text' => null,
			'auto_html' => null,
			'inline_css' => true,
			'url_strip_qs' => null,
			'preserve_recipients' => null,
			'view_content_link' => null,
			'bcc_address' => '', // bcc email address
			'tracking_domain' => null,
			'signing_domain' => null,
			'return_path_domain' => null,
			'merge' => true,
			'merge_language' => 'mailchimp',
			'global_merge_vars' => array(
				array(
					'name' => 'merge1',
					'content' => 'merge1 content'
				)
			),
			// Image needed for tracking opens
			'images' => array(
				array(
					'type' => 'image/png',
					'name' => 'IMAGECID',
					'content' => 'ZXhhbXBsZSBmaWxl'
				)
			)
		);
		//async is for bulk sending.
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = '2015-01-01';
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
		
		} 
		catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id ''
			throw $e;
		}
		}
	}
	
	//Delete key when key is correct
	//Embeded in GenKeyCheck function
	private function DeleteGenKey($GenKey){
		$conn = Connect();
		$drop = "DELETE FROM resetpw WHERE EncryptKey ='".$GenKey."'";
		
		$conn->query($drop);
	}
	
	//Step Four
	//Take code from email and check it is correct
	public function GenKeyCheck($GenKey){
		$conn = Connect();
		$select = "SELECT * FROM resetpw WHERE EncryptKey = '".$GenKey."'";
		$result = $conn->query($select);
		
		while($row = $result->fetch_assoc()){
			$email = $row['Email'];	
		}
		if($result->num_rows>0){
			$_SESSION['rest'] = 'set';
			$_SESSION['email'] = $email;
			$realKey = true;	
			
			$drop = new ForgotPWSequence();
			$drop->DeleteGenKey($GenKey);	
		}
		else{
			$realKey = false;
		}
		return $realKey;
	}
	
	//final step 
	//accept PW and change in DB. 
	public function ChangePW($pw, $email){
			$conn = Connect();
			$update = "UPDATE customers SET Password = '".$pw."' WHERE Email = '".$email."'";
			var_dump($conn->query($update));
	}
}

// ------------- Completed Checkout Functions -------------

// Once order approved, items are installed on customers portal with proper links for access. 
// Expires cookie function in there.. not working. 
function AddProdToProtal($custID){
	$conn = Connect();
	$select = "SELECT * FROM cart WHERE CookieID = '".$custID."'";
	
	$result = $conn->query($select);
	
	if($result->num_rows > 0){
		while($rows = $result->fetch_assoc()){
			$prodID = $rows['ProdID'];
			$email = $_SESSION['email'];	
			
            if($prodID == 162){
                $conn->query("INSERT INTO custportal (custID, prodID) 
                    VALUES ('".$_SESSION['Profile']['ID']."','146'");
                $conn->query("INSERT INTO custportal (custID, prodID) 
                    VALUES ('".$_SESSION['Profile']['ID']."','147'");
                $conn->query("INSERT INTO custportal (custID, prodID) 
                    VALUES ('".$_SESSION['Profile']['ID']."','149'");
                $conn->query("INSERT INTO custportal (custID, prodID) 
                    VALUES ('".$_SESSION['Profile']['ID']."','156'");
            }
            else{
                $insert = "INSERT INTO custportal (custID, prodID) 
                    VALUES ('".$_SESSION['Profile']['ID']."','".$prodID."')";

                if ($conn->query($insert) === TRUE) {
                } else {
                    echo "There was an error entering product into your portal, please contact support@pmimd.com";
                }
            }
		}
	}
    
	// Expires Cookie
	ExpireCookie();
}

// Once order Approved, items are stored into orderplaced table for recored. Gathers Prod Name, Customer Email, Catalog ID, & Price
// Has the MC Class Object for Marketing Automation. 
function OrderPlaced($OrderArray, $ReceiptID){
    // build vars
	session_start();
    $recDate =  ReconcileDate(DateandTime());
	// open connection and select from cart with cookieID
	$conn = Connect();
    $pmiconnect = pmimain_connect();
	$select = "SELECT * FROM cart WHERE CookieID = '".$_COOKIE['cartIDs']."'";
	
	$result = $conn->query($select);
	
	if($result->num_rows > 0){
		while($rows = $result->fetch_assoc()){
			//get Prod IDs
			$prodID = $rows['ProdID'];
			
			// Get SP prod Info and assign them to vars. 
			$selectSP = "SELECT * FROM AF_SP_Info WHERE AF = '".$prodID."'";
			
			$resultSP = $pmiconnect->query($selectSP);
			if($resultSP->num_rows > 0){
				// Vars below for catalog ID, Prod Name, Price, Customer Email address, And date Ordered. 
				while($rowSP = $resultSP->fetch_assoc()){
					$cataID = $rowSP['CataID'];
					$name = $rowSP['Name'];
					$price = $rowSP['Price'];
					$email = $_SESSION['Profile']['email'];
					$date = date(ymd);
					
					// checks if they have certs... if so give discount. 
					if($_SESSION["Discount"] == '1'){
						$price = $price - ($price * $_SESSION["ReferalMult"]);
					}
					
					// Insert Info into orderplaced Table.
					$insert = "INSERT INTO orderplaced (ProdID, CustomerID, Email, Price, CataID, PName, ReceiptID, DateOrdered, RecDate, CardType, CardNumber) 
						VALUES ('".$prodID."','".$_SESSION['Profile']['ID']."','".$email."','".$price."','".$cataID."','".$name."','".$OrderArray['req_reference_number']."','".$date."', '".$recDate."', '".$OrderArray['req_card_type']."', '".$OrderArray['req_card_number']."')";
					
					// query for insert.
					$resultI = $conn->query($insert);
					
					// check if they ordered PMCC or MLM for Marketing Automation. 
					$MCObject = new Mailchimp();
					if($cataID == 'SP-PMCC'){
						$MCObject->MCAddMember('b68b12eba5');
					}
					else if($cataID == 'SP-MLM'){
						$MCObject->MCAddMember('72bade8760');						
					}
				}
			}
			else{
			 echo "not working";
			}
		}
	}	

}

// Expires Cookie, Sets a new cookie. Needs Work
function ExpireCookie(){
	$cookieID = $_COOKIE['cartIDs'];
	$conn = Connect();
	$delete = "DELETE FROM cart WHERE CookieID = '".$cookieID."'";
	setcookie("mc_cid", "", time()-3600);
	$conn->query($delete);
	unset($_SESSION["ShippingInfo"]);
}

// If corrent referral code is used.. data is stored in a DB for compensation. 
function ReferralComp($RCode, $CustID, $Receipt_id){
		$Amount = TotalCost();
		$conn = Connect();
		// get the muilt for the customer from DB.. 
		// this allows for referral to still get their ammount even if its not the hightest discount. 
		// Also makes sure we dont give them more then they are allowed. 
		$select = "SELECT Discount FROM referralcodes WHERE Code = '".$RCode."'";
		
		$Sresult = $conn->query($select);
		if($Sresult->num_rows>0){
			while($rows = $Sresult->fetch_assoc()){
				$mult = $rows['Discount'];	
			}
				
			//set ammount value of referal code ammount. 
			$Amount = $Amount * $mult;
			
			if($Amount >0){		
			//once completed inset into DB. and unset bool and code session.  
			$insert = "INSERT INTO referralcommision (Code, Amount, CustomerID, Receipt) VALUES('".$RCode."','".$Amount."', '".$CustID."', '".$Receipt_id."')";
			$conn->query($insert);
			unset($_SESSION['RCode']);
			}
		}
}

//Stores the discount unto the server to track
function DiscountUsage($RCode, $CustID){
		$Amount = TotalCost();
		$conn = Connect();
		// get the muilt for the customer from DB.. 
		// this allows for referral to still get their ammount even if its not the hightest discount. 
		// Also makes sure we dont give them more then they are allowed. 
		$select = "SELECT Discount FROM referralcodes WHERE Code = '".$RCode."'
		UNION 
		SELECT Discount FROM discountcode WHERE Code = '".$RCode."'";
		
		$Sresult = $conn->query($select);
		if($Sresult->num_rows>0){
			while($rows = $Sresult->fetch_assoc()){
				$mult = $rows['Discount'];	
			}
				
			//set ammount value of referal code ammount. 
			$Amount = $Amount * $mult;
			
			if($Amount >0){		
			//once completed inset into DB. and unset bool and code session.  
			$insert = "INSERT INTO discountusage (Code, Amount, CustomerID) VALUES('".$RCode."','".$Amount."', '".$CustID."')";
			$conn->query($insert);
			unset($_SESSION['RCode']);
			}
		}
}

//Stores the discount unto the server to track
function DiscountTracker($RCode, $CustID){	
		$Amount = TotalCost();
		$conn = Connect();
		// get the muilt for the customer from DB.. 
		// this allows for referral to still get their ammount even if its not the hightest discount. 
		// Also makes sure we dont give them more then they are allowed. 
		$select = "SELECT Discount FROM discountcodes WHERE Code = '".$RCode."'";
		
		$Sresult = $conn->query($select);
		if($Sresult->num_rows>0){
			while($rows = $Sresult->fetch_assoc()){
				$mult = $rows['Discount'];	
			}
				
			//set ammount value of referal code ammount. 
			$Amount = $Amount * $mult;
			
			if($Amount >0){		
			//once completed inset into DB. and unset bool and code session.  
			$insert = "INSERT INTO referralcommision (Code, Amount, CustomerID) VALUES('".$RCode."','".$Amount."', '".$CustID."')";
			$conn->query($insert);
			unset($_SESSION['RCode']);
			}
		}	
}

// Enter Shipping Info into Database. 
function ShippingInfoStore($orderID){
	$conn = Connect();
	if(isset($_SESSION['ShippingInfo'])){
		$InsertShippingInfo = "INSERT INTO shippinginfo (CustomerID, Fname, Lname, Address, City, State, Zip, OrderID) 
			  VALUES ('".$_SESSION["Profile"]['ID']."',
					  '".$_SESSION['ShippingInfo']['ShipFname']."', 
					  '".$_SESSION['ShippingInfo']['ShipLname']."', 
					  '".$_SESSION['ShippingInfo']['ShipAddress']."', 
					  '".$_SESSION['ShippingInfo']['ShipCity']."', 
					  '".$_SESSION['ShippingInfo']['ShipState']."', 
					  '".$_SESSION['ShippingInfo']['ShipZip']."',
					  '".$orderID['req_reference_number']."')";
										
		$conn->query($InsertShippingInfo);
	}
}

// ------------- Navigations Functions -------------

// Customizes the nav menu to display if account is logged in. 
function NavLoginCheck(){
	if($_SESSION['LoginCheck'] == "yes" ){
		?>
        <li class="dropdown">
            <a href='<?php echo baseurl()?>/client/portal.php'>Hello, <?php echo $_SESSION['Profile']['fname']?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li>
                <a href="<?php echo baseurl()?>/client/portal.php">PMI Portal</a>
                </li>
                <li>
                <a href="<?php echo baseurl()?>/scripts/logout.php">Log Out</a>
                </li>
            </ul>
        </li>
		<?php
	}
	else{
		echo "<a href='".baseurl()."/client/login.php'>PMI Portal</a>";	
	}
}

// Function for prot page. If not logged in.. send them to login page no exceptions
function LoginCheckProtPage(){
	if($_SESSION['LoginCheck'] != "yes" ){
		header( 'Location: https://www.pmimd.com/onlinetraining/login.php' ) ;
	}
	else{
	}
}

// Function for cart flow, gives users option to skip login. 
function LoginCheckCartFlow(){
	if($_SESSION['LoginCheck'] == "yes"){
	}
	else{
		header( 'Location: '.baseurl().'login.php' ) ;
	}
	
}

function OrderID(){
    $orderid = GetOrderID();
    $conn = Connect();
    $newid = 0;
    while($newid == 0){
        $select = "SELECT ReceiptID FROM orderplaced WHERE ReceiptID = '".$orderid."'";
        $result = $conn->query($select);
    
        if($result->num_rows >0){
            $orderid = GetOrderID();
        }
        else{
            $newid = 1;
            return $orderid;
        }
        
    }
}

function GetOrderID(){
    //update Need to check to make sure order id isnt used before. 
    
	$count = 0;
	while($count<8){
    $int = rand(0,36);
    $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $rand_letter .= $a_z[$int];
	$count++;
	}
	return $rand_letter;
}

// Google analytics Script
function GoogleAnaltyicsScript(){
	
	echo "
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73530823-1', 'auto');
  ga('send', 'pageview');
</script>";
	
}

//  Google Analytics Ecommerce Script
function GoogleEcommerce($CartCookie, $OrderID, $TotalAmount){
    ?>
    <script>
        ga('require', 'ecommerce');
    </script>
    <?php
    $conn = Connect();
    $CartSelect = "SELECT * FROM cart WHERE CookieID = '".$CartCookie."'";
    $Result = $conn->query($CartSelect);
    
    while($Cart = $Result->fetch_assoc()){
        $SPSelect = "SELECT * FROM selfpaced WHERE ProdID = '".$Cart['ProdID']."'";
        $SPResult = $conn->query($SPSelect);
        
        while($SPItem = $SPResult->fetch_assoc()){
            ?>
            <script>
                ga('ecommerce:addItem', {
                  'id': '<?php echo $OrderID?>',                                                // Transaction ID. Required.
                  'name': '<?php echo $SPItem['Name']?>',                                       // Product name. Required.
                  'price': '<?php echo (1-$DiscountCode)*$SPItem['Price']?>',                 // Unit price.
                });
            </script>
            <?php           
        }
    }    
    ?>
        <script>
        // Below is used for final ammount (your total revenue)
        ga('ecommerce:addTransaction', {
          'id': '<?php echo $OrderID?>',                     // Transaction ID. Required.
          'affiliation': 'PMI Online Order',   // Affiliation or store name.
          'revenue': '<?php echo $TotalAmount?>',               // Grand Total.
        });
        ga('ecommerce:send');
        </script>
    <?php 
}

function GoogleAnalytics($GoogleCode){
    if($_SERVER['REMOTE_ADDR'] == '67.78.62.2501'){

    }
    else{
      ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
          
          ga('create', '<?php echo $GoogleCode?>', 'auto');
          ga('send', 'pageview');  
        </script>
      <?php   
    }
}

// -------------------- Mailchump's API Libaray------------------

//This is the main class for connection to mailchimp. with in it are instances do many things
class Mailchimp{
	//Connection for the API, Paramters Needed, APIkey, URL, Request as POST PUT GET etc....)
	private function APIConnection($url, $Request, $data){
		
		$ch = curl_init($url);
	
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . '2c230f72ec2cf86b57a2f8d36d60194d-us12');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);   
		
		// Results 
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
        $return_data = array(
            'http_code' => $httpCode,
            'data'      => $result
        );
		
		return $return_data;
	}
    
    private function APIConnection_no_input($url, $Request){
		
		$ch = curl_init($url);
	
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . '2c230f72ec2cf86b57a2f8d36d60194d-us12');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
		
		// Results 
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
        $return_data = array(
            'http_code' => $httpCode,
            'data'      => $result
        );
		
		return $return_data;
	}
	
	// List ID c843b5346c SP-Survery
	// List ID b68b12eba5 PMCC List
	// List ID 72bade8760 MLM List
	public function MCAddMember($listId) {
				
		$memberID = md5(strtolower($_SESSION['Profile']['email']));
        $url = 'https://us12.api.mailchimp.com/3.0/lists/' . $listId . '/members/' .$memberID;
		
		
		$json = json_encode([
			'email_address' => $_SESSION['Profile']['email'],
			'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
			'merge_fields'  => [
				'FNAME'     => $_SESSION['Profile']['fname'],
				'LNAME'     => $_SESSION['Profile']['lname'],
				'ZIP' 		=> $_SESSION['Profile']['zip']
			]
		]);
		
		// Connection for API. 
		$APIConnect = new Mailchimp();
		$APIConnect->APIConnection($url, "PUT", $json);                                                                    
	}
    
    public function MCAddMemberToList($listId, $custData, $lead_Code) {
				
		$memberID = md5(strtolower($custData['email']));
        $url = 'https://us12.api.mailchimp.com/3.0/lists/' . $listId . '/members/' .$memberID;
		
		
		$json = json_encode([
			'email_address' => $custData['email'],
			'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
			'merge_fields'  => [
				"FNAME"=> $custData['fname'],
                "LNAME" => $custData['lname'],
                "LEAD_CODE"=> $_SESSION['lead_code'],
                "ZIP"=> $custData['zip'],
                "JOB_TITLE" => $custData['jobtitle']
			]
		]);
		
		// Connection for API. 
		$APIConnect = new Mailchimp();
		$APIConnect->APIConnection($url, "PUT", $json);                                                                    
	}
    
    public function MC_update_list($data) {
				
		$memberID = md5(strtolower($data['email']));
		$url = 'https://us12.api.mailchimp.com/3.0/lists/cc2535b0e1/members/' .$memberID;
		
		$json = json_encode([
			'email_address' => $data['email'],
			'status_if_new' => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
			'merge_fields'  => [
				'FNAME'     => $data['fname'],
				'LNAME'     => $data['lname'],
                'ADDRESS'  => $data['address'],
                'CITY'      => $data['city'],
				'ZIP' 		=> $data['zip']
			]
		]);
		
		// Connection for API. 
		$APIConnect = new Mailchimp();
		return $APIConnect->APIConnection($url, "PUT", $json);                                                                    
	}
    
    public function MCEcommerce_disabled($StoreID, $OrderID, $TotalSale) {
		
            $memberID = "4548cef640e47fed66ff5b97680a45dc";
            $campignID = "2ac9af9ed7";
            $url = 'https://us12.api.mailchimp.com/3.0/ecommerce/stores/'.$StoreID.'/orders';
            
            $json = json_encode([
                  'id' => $OrderID,
                  'customer' => [
                    'id' => $memberID
                  ],
                  'campaign_id' => $campignID,
                  'checkout_url' => 'https://www.pmimd.com/onlinetraining/payment/confirmation.php',
                  'currency_code' => 'USD',
                  'order_total' => $TotalSale,
            ]);
            // Connection for API. 
            $APIConnect = new Mailchimp();
            echo $APIConnect->APIConnection($apiKey, $url, "PUT", $json);  
                                                                  
	}	
    
    public function Get_List_data($list_id){
        $url = 'https://us12.api.mailchimp.com/3.0/lists/'.$list_id;
        $APIConnect = new Mailchimp();
        return $APIConnect->APIConnection_no_input($url, "GET");  
        
    }
}

//------------------- Reconcile Functions --------------------------
//function that figures out he predetermined rec/post date for accounting. 
function ReconcileDate($Array){
                $day = $Array[0];
                $DayOfWeek = date('w', strtotime($Array[0]));
                date(G)-5;
                
                if($DayOfWeek == "5" && $Array[1] < 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+3 days"));
                }
                elseif($DayOfWeek == "5" && $Array[1] > 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+4 days"));
                }
                elseif($DayOfWeek == "6"){
                    $RecDate = date('Y-m-d',strtotime($day ."+3 days"));
                }
                elseif($DayOfWeek == "0"){
                    $RecDate = date('Y-m-d',strtotime($day ."+2 days"));
                }
                elseif($Array[1] > 15){
                    $RecDate = date('Y-m-d',strtotime($day ."+2 days"));
                }
                else{
                    $RecDate = date('Y-m-d',strtotime($day ."+1 days"));
                }
                return $RecDate;
            }
            
// puts date and time in array for faster processing. 
function DateandTime(){
                $DateTimeArray = array(date("Y-m-d"), date(G)-5);
                return $DateTimeArray;
            }
            
function VideoTrackerUpdate(){
    $conn= Connect();
    
    $get_video_info = 'SELECT * FROM videos';
    
    $result = $conn->query($get_video_info);
    
    if($result->num_rows>0){
        foreach($result as $data){
            $update_video_info = 'UPDATE videotracking
                          SET VideoDuration='.$data['Duration'].'
                          WHERE VideoID='.$data['ID'].'';
                          
             $conn->query($update_video_info);
        }
           
    }
}

function McEcommerce($orderData, $mc_cid){
    $conn=Connect();
    
    if(isset($_COOKIE['mc_cid'])){
    $insert = "INSERT INTO mcecommerce (CustomerID, CampaignID, Amount, OrderID)
                VALUES ('".$_SESSION['Profile']['ID']."', '".$mc_cid."', '".$orderData['req_amount']."', '".$orderData['req_reference_number']."')";
    $conn->query($insert);
    }
}

class CartSummary{
     
    // get cart info 
    public function cartinfo($email){
        $conn = Connect();
        $select = "SELECT ProdID FROM cart WHERE Email = '".$email."'";
        $result = $conn->query($select);
        return $result->fetch_all();
    }
    
    //gather customer info 
    public function custinfo($id){
        $conn = Connect();
        $select = "SELECT * FROM customers WHERE ID = '".$id."'";
        $result = $conn->query($select);
        return $result->fetch_assoc(); 
    }
    
    // get price of said product
    private function priceTotal($ProdID){
        $conn = pmimain_connect();
        $priceArray = array();
        $select = "SELECT Price FROM AF_SP_Info WHERE AF = '".$ProdID."'";
        $result = $conn->query($select);
        return $result->fetch_assoc();
    }
    
    //find item count. 
    private function itemcount($email){
        $conn = Connect();
        $select = "SELECT COUNT(*) AS Count FROM cart WHERE Email = '".$email."'";
        $result = $conn->query($select);
        $count = $result->fetch_assoc();
        return $count['Count'];
    }
    
    //check for discount (if they are currently certified)
    private function discount($fname, $lname, $certID){
            $pmiconnect = pmimain_connect();
            $GradSelect = "SELECT GradID FROM Current_Grad_Check 
                    WHERE GradFirstName = '".$fname."' 
                    AND GradLastName = '".$lname."' 
                    AND GradID = '".$certID."' GROUP BY GradID";
            $result = $pmiconnect->query($GradSelect);
            if($result->num_rows >0){
                return 1;
            }
            else{
                return 0;
            }
    }
    
    public function CartSummaryDisplay($cart_data, $cart_summary){
        $conn = pmimain_connect();
        $EmailPackage .= "<table border='0' cellpadding='5' cellspacing='0' width='600' id='emailContainer'>";
        for($x = 0; $x < sizeof($cart_data); $x++){
            $select = "SELECT * FROM AF_SP_Info WHERE AF = '".$cart_data[$x][0]."'";
            $result = $conn->query($select);

            while($data = $result->fetch_assoc()){
                $EmailPackage .= "<tr><td class='mcnTextContent' align='left' valign='top'>".$data['Name']."</td><td class='mcnTextContent' align='center' valign='top'>$".number_format($data["Price"],2)."</td></tr>";
            }
        }
        $EmailPackage .= "<tr><td style='padding:15px;'></td><td></td></tr>";
        $EmailPackage .= "<tr><td class='mcnTextContent' align='left' valign='top'><h3>Total</h3></td><td class='mcnTextContent' align='right' valign='top'><h3>$".number_format($cart_summary['Price'], 2)."</h3></td></tr>";
        $EmailPackage .="</table>";
        
        return $EmailPackage;
    }
    
    //main function that produces data needed for display. 
    public function main($id){
        $customerArray = $this->custinfo($id);
        $itemCount = $this->itemcount($customerArray['Email']);
        $cartData = $this->cartinfo($customerArray['Email']);
        $discount = $this->discount($customerArray['Fname'], $customerArray['Lname'], $customerArray['Cert']);
        $AssocPrice = array();

        foreach($cartData as $cartData){
            array_push($AssocPrice, $this->priceTotal($cartData[0]));
        }
        for($x=0; $x<sizeof($AssocPrice); $x++){
                $price = $price + $AssocPrice[$x]['Price'];
        }
        if($discount == 1){
            $discountprice = number_format($price * .1,2);
            $price = number_format($price *.9, 2);
        }
        if(isset($_SESSION['CartPrice'])){
            $price = $_SESSION['CartPrice'];
        }
        
        return array("Discount" =>$discount, "Price"=>$price, "ItemCount"=>$itemCount);
    }
}

function email_check_cart(){
    if(isset($_SESSION['Profile']['Email'])){
        return $_SESSION['Profile']['Email'];
    }
    else{
        return 1;
    }
}

function Tracking_Code($orderData, $tc_code){
    $conn = Connect();
    $isert = "INSERT INTO tc_conversion (custID, conversion, orderID, tracking_code)
            VALUES ('".$_COOKIE['Profile']['ID']."','".$orderData["req_amount"]."','".$orderData["req_reference_number"]."','".$tc_code."',)";
    $conn->query($insert);    
}

function htmlconvert($str){
    echo htmlspecialchars_decode(htmlspecialchars($str,ENT_QUOTES),ENT_NOQUOTES);
}
?>