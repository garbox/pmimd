<head>
<style>
.dropdown:hover .dropdown-menu {
	display: block;
}
</style>
</head>
<nav class="navbar">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button"><span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="https://www.pmimd.com/">Home</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Certification<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="https://www.pmimd.com/certify/certified-medical-coder-certification.asp">Certified Medical Coder (CMC)</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/certify/certified-medical-insurance-specialist-certification.asp">Certified Medical Insurance Specialist (CMIS)</a>
                            </li>
                            <li>
                                 <a href="https://www.pmimd.com/certify/certified-medical-office-manager-certification.asp">Certified Medical Office Manager (CMOM)</a>
                            </li>
                            <li>
                                 <a href="https://www.pmimd.com/certify/certified-medical-compliance-officer-certification.asp">Certified Medical Complience Officer (CMCO)</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/certify/ceutracker.asp">CEU Tracker</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/certify/FAQs.asp">FAQs</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Education<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="https://www.pmimd.com/programs/locator.asp">Class Locator</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/programs/default.asp">Class Discriptions</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/lcenters/default.asp">Learning Centers</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">About PMI <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="https://www.pmimd.com/about/default.asp">Meet The Faculty</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/pminews.asp">PMI In The News</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/blog">PMI Blog</a>
                            </li>
                            <li>
                                <a href="https://www.pmimd.com/contact.asp">Contact Us</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="https://www.pmimd.com/productcenter/">Products Page</a>
                    </li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <a href='http://www.pmimd.com/onlinetraining/Client/Portal.php'>PMI Portal</a>
                    </li>
                    <li class="hidden-lg hidden-md">
                        <a href="https://www.pmimd.com/productcenter/YourCart.php" style="padding: 0px 10px;"><img alt="" height="50px" src="https://www.pmimd.com/onlinetraining/images/ShoppingCart.gif"></a>
                    </li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <form action="Search.php" class="navbar-form" method="get" role="search" style="width:250px; padding-left:30px;">
                            <div class="input-group">
                                <input class="form-control" list="type" name="find" placeholder="Search" type="text"> <datalist id="type">
                                    <option value="Coding">
                                        </option>
                                    <option value="Billing">
                                        </option>
                                    <option value="Compliance">
                                        </option>
                                    <option value="Management">
                                        </option>
                                    <option value="ICD-10">
                                        </option>
                                </datalist>
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
                <div class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                    <ul class="nav navbar-nav">
                        <li>
                            <form action="https://www.pmimd.com/onlinetraining/search.php" class="navbar-form" method="get" role="search">
                                <div class="input-group">
                                    <input class="form-control" list="type" name="find" placeholder="Search" type="text"> <datalist id="type">
                                        <option value="Coding">
                                            </option>
                                        <option value="Billing">
                                            </option>
                                        <option value="Compliance">
                                            </option>
                                        <option value="Management">
                                            </option>
                                        <option value="ICD-10">
                                            </option>
                                    </datalist>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li>
                            <a href="https://www.pmimd.com/productcenter/YourCart.php" style="padding: 0px 10px;"><img alt="" height="50px" src="https://pmimd.com/ProductCenter/images/ShoppingCart.gif"></a>
                        </li>
                        <li>
                        	<a href='https://www.pmimd.com/productcenter/Client/Portal.php'>PMI Portal</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>