function RemoveProd(){
	$('#RemoveProd').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('#RemoveProd').prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		e.preventDefault();	
		
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(){ 
			  location.reload();
				},
			  error: function(){
			  } 
		});
				
	});
}