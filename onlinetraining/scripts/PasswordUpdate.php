<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online Training Report: ForgotPassword</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.pmimd.com/css/bootstrap/ProdCenter/css/bootstrap.css">
    <link rel="stylesheet" href="http://www.pmimd.com/css/bootstrap/ProdCenter/css/style.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<div class="container-fluid" style="padding-top:30px; margin-bottom:30px; min-height:507px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Forgot Password</h2>
                        <p class="error"></p>
                        <hr>
                        <form class="LogInForm" method="post" action="updatepwscript.php" >
                          <div class="form-group">
                            <input type="email" class="form-control" name="username" placeholder="Username">
                          </div>                       
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>
</body>
</html>
