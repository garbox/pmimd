<?php include "../../includes/functions.php";
<?php include "../../includes/config.php";
$conn = Connect();

$custID = $_GET['CustID'];

// Query for Video Tracking
$Select = "SELECT CourseID, LastAccess , SUM(CurrentTime) AS TimeSpent
            FROM videotracking 
            WHERE CustomerID = '".$custID."'
            GROUP BY CourseID";
$Result = $conn->query($Select);

if($Result->num_rows>0){
    echo '<table><tr><td>Class Name</td><td>Last Accessed </td><td>Total Time Spent</td><td>Course Length</td><td>Completed</td></tr>';
    while($ResultData = $Result->fetch_assoc()){
        // declare Vars
        $CourseID = $ResultData['CourseID'];
        $LastAccess = $ResultData['LastAccess'];
        $TimeSpent = $ResultData['TimeSpent'];
        
        // Query for Selfpaced courses
        $SelectProdName = "SELECT Name FROM selfpaced WHERE ProdID = '".$CourseID."'";
        $result = $conn->query($SelectProdName);
        $ProdName = $result->fetch_assoc();
        // declare Vars 
        $ProdName = $ProdName['Name'];
        
        //Query for Videos
        $VideoSelect = "SELECT SUM(Duration) AS TotalDuration, COUNT(ClassID) AS Count FROM videos WHERE ClassID = '".$CourseID."'";
        $Videoresult = $conn->query($VideoSelect);
        $TotalDuration = $Videoresult->fetch_assoc();
        // declare Vars 
        $TotalDuration =  $TotalDuration['TotalDuration'];
        $VideoCount =  $TotalDuration['Count'];
        
        ?>
        	<tr>
        		<td><?php echo $ProdName?></td>
                <td><?php echo $LastAccess?></td>
                <td><?php echo $TimeSpent + $VideoCount?></td>
                <td><?php echo $TotalDuration?></td>
                <td><?php 
                $completed = $TotalDuration-$TimeSpent;
                if($completed == 0){
                    echo "Yes";
                    }
                    else{echo "No";}
                
                ?></td>
        	</tr>

        <?php
    }
    echo "</table>";
}

?>