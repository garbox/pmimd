<?php include 'includes/functions.php';?>
<?php include 'includes/config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PMIMD: Product Center</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">

<style>
html, body{ 
	overflow-x: hidden;
}

.btn-img{
	width:auto;	
}
.shadow{
	box-shadow: 3px 3px 10px #999999;	
}
</style>
<?php GoogleAnaltyicsScript()?>
</head>

<body>
<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner">
    <div class="row">
        <div class="col-lg-12">
		<?php include 'includes/nav.php';?>
        </div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block"><img src="https://pmimd.com/images/SP-Banner.png" alt="" width="100%"></div>
            </div>
        </div>
    </div>
</div>
<div id="searchPlacement"></div>



<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; min-height:281px">
    <div class="row">
        <div class="container"> 
            <div class="row">
			<? ProdSearch();?>
            </div>
    	</div>
    </div>
</div>
<!-- Footer -->
<?php include 'includes/footer.html';?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.js"></script>

</body>
</html>

