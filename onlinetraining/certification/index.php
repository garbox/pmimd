<?php include '../includes/functions.php';
 include '../includes/config.php';
CookieforCart();	
session_start();
$ProdID = $_GET['prodID'];
 
    $conn = Connect();
    $CMS = CMS();
    $faculty_conn = pmimd_faculty();

    $select = "SELECT * FROM selfpaced WHERE ProdID='".$ProdID."'";	
    $cms_select = "SELECT * FROM CMS WHERE prod_id ='".$ProdID."'";	
    
	
	$result = $conn->query($select);
    $cms = $CMS->query($cms_select);
    
    $row = $result->fetch_object();
    $cms_data = $cms->fetch_object();

    $faculty_select = "SELECT * FROM faculty WHERE ID = '".$row->Inst_ID."'";
    $faculty_query = $faculty_conn->query($faculty_select);
    $faculty_data = $faculty_query->fetch_object();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pname?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $row->Type?>">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
	hr{
		all:none;  
	}
	html, body{ 
		overflow-x: hidden;
	}
	h3{
		color:#5371ad;
	}
	.padimg{
		padding:15px; 
		width:120px;
	}
	.conpad{
		padding-top:15px;
		padding-bottom:15px;  
	}
	.recomended{
		margin:auto;
		padding:15px;
		height:120px;  
		width:auto;
		border-radius:200px;
	}
	btn-default{
		background-color:#C01313!important;  
	}
	btn-default:hover{
		background-color:#A31010!important
	}	
	.roundInfoGraph{
		height:120px;
		width:120px;
		border:1px solid #999999;
		display:inline-block;
		border-radius:60px; 
		margin:10px; 
	}
	.roundInfoGraph>h3{
		margin-top:35px; 
	}
	p.Info{
		font-size:18px;  
	}
	ul,p{
		font-size:15px!important;	
	}
    p>a{
        font-size:15px!important;	
    }
</style>

<?php GoogleAnaltyicsScript()?>

</head>
<body>
<!-- Strip -->
<div class="contianer">
    <div class="row-fluid">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Nav bar -->
<div class="conatiner">
    <div class="row">
        <div class="col-lg-12">
		<?php include '../includes/nav.php';?>
        </div>
    </div>
</div>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Online Training Course</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
    <div class="row">
        <div class="container conpad" style="background-color:#fff">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left:31px;">
                    <h3 style="font-size:26px;"><img class="img-circle" src="https://www.pmimd.com/products/images/<?php echo $row->Image;?>" alt="" height="50px"> <?php echo $row->Name; ?><hr></h3>
                </div>              
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center" style="min-height:200px; padding-left:31px;">	
                    <p><?php echo $row->ShortDescs;?></p>
                    <div id="content"></div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div align="center">
                    	<div class="roundInfoGraph">
                        	<h3>CEUs</h3>
                       		<p class="Info"><?php echo $row->CEUs;?></p>
                        </div>
                        <div class="roundInfoGraph">
                        	<h3>Length</h3>
                        	<p class="Info"><?php echo $row->Length;?> min</p>
                        </div>
                        <div class="roundInfoGraph">
                        	<h3>Price</h3>
                        	<p class="Info">$<?php echo $row->Price?></p>
                        </div>            
                    </div>
                    <div align="center" style="padding-top:20px;">
                   		<!-- Buy -->
                        <form id="BuyProd" action="../scripts/cartscript.php" method="post" style="display:inline">
                        <input type="hidden" name="ProdID" value="<?php echo $row->ProdID;?>">
                        <button id="BuyButton" type="submit" class="btn btn-default" style="width:200px; background-color:#c01313; color:white">Buy</button>
                        </form>
                        
                        <!-- Add To Cart -->
                        <form id="AddProd" action="../scripts/cartscript.php" method="post" style="display:inline">
                        <input type="hidden" name="ProdID" value="<?php echo $row->ProdID;?>">
                        <button type="submit" class="btn btn-default" style="width:200px; background-color:#0ca24b; color:white">Add to Cart</button>
                        </form>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<!-- Row 2 of content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
    <div class="row">
        <div class="container" style="background-color:#fff">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding:15px;">
                    <ul class="nav nav-tabs hidden-xs hidden-sm">
                        <?php if($cms_data->full_desc != ""){?>
                            <li class="active">
                                <a data-toggle="tab" href="#Course">Course Summary</a>
                            </li>
                        <?php }?>
                        
                        <?php if($cms_data->benifits != ""){?>
                            <li>
                                <a data-toggle="tab" href="#Benefits">Benefits</a>
                            </li>
                        <?php }?>
                        
                        <?php if($cms_data->curriculm != ""){?>
                            <li>
                                <a data-toggle="tab" href="#Curriculum">Curriculum</a>
                            </li>
                        <?php }?>
                        
                        <li>
                            <a data-toggle="tab" href="#Instructor">Instructor</a>
                        </li>
                        
                        <?php if($cms_data->format != ""){?>
                            <li>
                                <a data-toggle="tab" href="#Format">Format</a>
                            </li>
                        <?php }?>
                        
                        <?php if($cms_data->faq != ""){?>
                            <li>
                                <a data-toggle="tab" href="#FAQ">FAQ</a>
                            </li>
                        <?php }?>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Course" style="padding:15px;">
                            <?php echo $cms_data->full_desc;?>
                        </div>
                        
                        <div class="tab-pane" id="Instructor" style="padding:15px;">
                            <table cellpadding="15px;">
                                <tr>
                                    <td><img src="https://www.pmimd.com/images/instructors2/<?php echo $faculty_data->image?>" alt=""></td>
                                    <td valign="top">
                                        <h3><?php echo $faculty_data->fname." ".$faculty_data->lname?></h3>
                                        <p><?php echo $faculty_data->credentials?></p>
                                    </td>
                                </tr>
                            </table>
                            <hr>
                            <?php echo $faculty_data->bio?>
                        </div>
                        <?php if($cms_data->benifits != ""){?>
                        <div class="tab-pane" id="Benefits" style="padding:15px;">
                            <?php echo $cms_data->benifits;?>
                        </div>
                        <?php }?>
                        
                         <?php if($cms_data->curriculm != ""){?>
                        <div class="tab-pane" id="Curriculum" style="padding:15px;">
                            <?php echo $cms_data->curriculm;?>
                        </div>
                        <?php }?>
                        
                        <?php if($cms_data->format != ""){?>
                        <div class="tab-pane" id="Format" style="padding:15px;">
                            <?php echo $cms_data->format;?>
                        </div>
                        <?php }?>
                        
                        <?php if($cms_data->faq != ""){?>
                        <div class="tab-pane" id="FAQ" style="padding:15px;">
                            <?php echo $cms_data->faq;?>
                        </div>
                        <?php }?>
                	</div>               
            	</div>
        	</div>
    	</div>
    </div>
</div>

<!-- Products Like This -->
<div class="container-fluid conpad" style="background-color:#E6E6E6" >
    <div class="row">
        <div class="container conpad" style="background-color:#FFFFFF">
            <div class="row">
                <div style="padding-bottom:25px;">
                    <h3 align="center">Other classes like this.</h3>
                </div>
                    <?php RecProd($row->Featured1, $row->Featured2, $row->Featured3);?>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include '../includes/footer.html';?>
    
<!-- Buy -->
<form id="BuySubmit" action="<?php echo baseurl()?>payment/payinfo.php" method="post">
    <input type="hidden" name="img" value="<?php echo $row->Image;?>">
    <input type="hidden" name="pname" value="<?php echo $row->Pname;?>">
    <input type="hidden" name="price" value="<?php echo $row->Price;?>">
</form>

<!-- Add To Cart -->

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

 <script>

/* Ajax */
var count = 0;
$('#AddProd').on('submit', function(e){
	var values = $(this).serialize();
	// get value of action attribute   
	var desination = $('#AddProd').prop('action');   		
	// get current location url			
	
	// prevent page from leaving to desination
	e.preventDefault();	
	
	$.ajax({
		  url: desination,
		  type: 'post',
		  data: values, 
		  success: function(data){ 
			$('#content').html("<br><p class='bg-success' align='center' style='font-size:20px; color:white; background-color:#5371ad'>Product has been added</p>");
			},
		  error: function(xhr){
			  console.log(data);
			  $('#content').html("<br><p class='bg-success' align='center' style='font-size:20px; color:white; background-color:#5371ad'>"+xhr.status + " " + xhr.statusText+"</p>");
		  } 
	});

});
$('#BuyProd').on('submit', function(e){
	var values = $(this).serialize();
	// get value of action attribute   
	var desination = $('#AddProd').prop('action');   		
	// get current location url			
	
	// prevent page from leaving to desination
	e.preventDefault();	

	$.ajax({
		  url: desination,
		  type: 'post',
		  data: values, 
		  success: function(){ 
			window.location = '<?php echo baseurl()?>yourcart.php';
			},
		  error: function(xhr){
			  $('#content').html("<br><p class='bg-success' align='center' style='font-size:20px; color:white; background-color:#5371ad'>"+xhr.status + " " + xhr.statusText+"</p>");
		  } 
	});

});
</script>

 
</body>
</html>