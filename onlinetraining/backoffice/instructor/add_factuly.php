<?php include '../../includes/functions.php'?>
<?php include '../../includes/config.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Add: Faculty</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
  
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>
	tinymce.init({
	selector: "textarea",
	plugins: [
                "link lists",
				"code"
                ],
	toolbar: 'undo redo alignleft aligncenter alignright removeformat bullist numlist styleselect code',
	setup: function (editor) {
		editor.on('change', function () {
			tinymce.triggerSave();
		});
	}
	});
    </script>

</head>

<body>

<!-- Strip -->
<?php include("../includes/nav.php");?>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Add: Instructor</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="SubmitInfo" action="script/add.php" method="post">
<div class="container" id="content" style="margin-top: 15px; min-height: 600px">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-4">
           <label for="">First Name</label>
            <input class="form-control fname" name="fname" type="text" >
        </div>
        <div class="col-sm-4">
           <label for="">Last Name</label>
            <input class="form-control lname" name="lname" type="text" >
        </div>
        <div class="col-sm-4">
           <label for="">Credentials</label>
            <input class="form-control" name="credentials" type="text" >
        </div>
    </div>
    <div class="row" style="margin-bottom: 15px">
      <div class="col-xs-12">
       <label for="">Bio</label>
        <textarea class="form-control" name="bio" cols="25" rows="10">

        </textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
           <label for="">Faculty Image</label>
            <input class="form-control"  name="image" type="text" >
        </div>
        <div class="col-sm-2">
           <label for="">Corp Image</label>
            <input class="form-control" name="corpImg" type="text" >
        </div>
        <div class="col-sm-2">
           <label for="">Active</label>
            <select class="form-control" name="active" id="Active">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <div class="col-sm-2">
           <label for="">Group</label>
            <select class="form-control" name="facgroup" id="facgroup">
                <option value="Faculty">Faculty</option>
                <option value=AssocInt>Associate Instructor</option>
                <option value="CorpGroup">Corporation Group</option>
            </select>
        </div>
        <div class="col-sm-2">
           <label for="">PDF File</label>
            <input class="form-control" name="PDF_File" type="text" >
        </div>
        <div class="col-sm-2">
           <label for="">Role</label>
            <input class="form-control" name="role" type="text" >
        </div>
    </div>
</div>
<br>
<div class="subbutton">
    <input type="submit" value="Submit" class="btn btn-danger" style="width:100%">
</div>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
<script>   
        inputEmpty = false;
        $('#SubmitInfo').on('submit', function(e){
            if( $('.fname').val() == ""){
            inputEmpty = true;
            e.preventDefault();	
            $('.fname').css('border-color', 'red');
            }

            if( $('.lname').val() == ""){
            inputEmpty = true;
            e.preventDefault();	
            $('.lname').css('border-color', 'red');
            }
            
            if(inputEmpty == false){
                var values = $(this).serialize();
                // get value of action attribute   
                var desination = $('#SubmitInfo').prop('action');   		
                // get current location url			

                // prevent page from leaving to desination
                event.preventDefault()
                $('.subbutton').empty();
                $('.subbutton').html("<div align='center' style='background-color:white;'><img src='https://www.ielts.org/images/loading-icon.gif'/></div>");

                $.ajax({
                      url: desination,
                      type: 'post',
                      data: values, 
                      success: function(data){ 
                          console.log(data)
                          $('.subbutton').empty();
                          $('.subbutton').html("<div style='background-color:#0ca24b; padding:15px;'><h3 align='center' style='color:white'>Success!</h3><p align='center' style='color:white'>Refresh page for updates</p></div>");
                          setTimeout(function() {
                                location.assign('<?php echo baseurl()?>backoffice/instructor/update_instructor.php?ID='+data);
                            }, 2000);


                        },
                      error: function(xhr){
                          $('div.hide').show();
                          $('#error').html("<p class='bg-danger'>There was an error please try again.</p>");
                      } 
                    });
                }
        });
</script>
<?php include '../../includes/footer.html';?>
</body>
</html>
