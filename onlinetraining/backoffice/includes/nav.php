<head>
<style>
.dropdown:hover .dropdown-menu {
	display: block;
}
.navbar{
    background-color: #ffffff;
}
</style>
</head>
         <nav class="navbar" style="background-color: white;">
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button"><span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span> <span class="icon-bar" style="background-color:#5371ad"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo baseurl() ."backoffice"?>">Main Menu</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo baseurl() ."backoffice/instructor"?>">Instructor
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo baseurl() ."backoffice/instructor/add_factuly.php"?>">Add instructor</a></li>
                            <li><a href="<?php echo baseurl() ."backoffice/instructor"?>">Edit instructor</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo baseurl() ."backoffice/otc"?>">OTC
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo baseurl() ."backoffice/otc/videoupload.php"?>">New Video</a></li>
                            <li><a href="<?php echo baseurl() ."backoffice/otc/upload.php"?>">New Class</a></li>
                            <li><a href="<?php echo baseurl() ."backoffice/otc"?>">Update Class</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo baseurl() ."backoffice/inthenews"?>">In The News
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo baseurl() ."backoffice/inthenews/new_news.php"?>">Add News</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo baseurl() ."backoffice/certifications"?>">Certifications</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>