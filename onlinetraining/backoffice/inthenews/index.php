<?php include '../../includes/functions.php'?>
<?php include '../../includes/config.php';?>

<?php 
//get all of the articles query
$i = 0;
$conn = pmimain_connect();
$select = "SELECT * from pmi_news ORDER BY newsDate DESC";
$result = $conn->query($select);
$rows = $result->num_rows;
$data = $result->fetch_all();
$split_row = floor($result->num_rows/2+1);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Backoffice: In The News</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
</head>

<body>
<?php include("../includes/nav.php");?>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Backoffice: In The News</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container" style="margin-top: 15px;">
    <div class="row">
       <div class="col-md-6">
           <table>
           <tr>
               <td>Title</td>
               <td colspan="2">Date</td>
           </tr>
            <?php
            while($i < $split_row && $i <= $rows){
                ?>
                    <tr>
                        <td><?php echo $data[$i][2]?></td>
                        <td><?php echo date('m/d/Y', strtotime($data[$i][1]))?></td>
                        <td><a href="update_news.php?ID=<?php echo $data[$i][0]?>"><button class="btn btn-primary">Edit</button></a></td>
                    </tr>
                <?
                $i++;
            }
            ?>      
           </table>
       </div>
       <div class="col-md-6">
           <table>
           <tr>
               <td>Title</td>
               <td colspan="2">Date</td>
           </tr>
            <?php
            while($i >= $split_row && $i < $rows){
                ?>
                    <tr>
                        <td><?php echo $data[$i][2]?></td>
                        <td><?php echo date('m/d/Y', strtotime($data[$i][1]))?></td>
                        <td><a href="update_news.php?ID=<?php echo $data[$i][0]?>"><button class="btn btn-primary">Edit</button></a></td>
                    </tr>
                <?
                $i++;
            }
            ?>      
           </table>
       </div>
    </div>
</div>

  <script async src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script async src="assets/js/bootstrap.min.js"></script>
</body>
</html>
