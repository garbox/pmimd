<?php session_start()?>
<?php include '../../includes/functions.php'?>
<?php include '../../includes/config.php';?>
<?php 
    $ID = $_GET['ID'];

?>


<?php 

    $conn = pmimain_connect();
    $select = 'SELECT * FROM pmi_news WHERE ID = "'.$ID.'"';
    $result = $conn->query($select);
    $article_data = $result->fetch_object();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Edit: PMI In The News</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>
	tinymce.init({
	selector: "textarea",
	plugins: [
                "link lists",
				"code",
                "wordcount"
                ],
	toolbar: 'undo redo alignleft aligncenter alignright removeformat bullist numlist styleselect code',
	setup: function (editor) {
		editor.on('change', function () {
			tinymce.triggerSave();
		});
	}
	});
</script>
</head>

<body>
<?php include("../includes/nav.php");?>
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Edit News</h3>
                </div>
            </div>
        </div>
    </div>
</div>
    
<form id="submit" action="script/update_news.php" method="post">
<div class="container" style="margin-top: 15px">
    <div class="row">
        <div class="col-sm-12">
           <label for="">Title</label>
            <input class="form-control" name="title" type="text" value="<?php echo $article_data->newsTitle?>" width="100%">
        </div>
    </div>
    <div class="row">
        <p>
           <div class="col-sm-4">
              <label for="">Author</label>
               <input class="form-control" name="author" type="text" value="<?php echo $article_data->newsAuthor?>">
           </div>
           <div class="col-sm-4">
              <label for="">Publisher</label>
               <input class="form-control" name="publisher" type="text" value="<?php echo $article_data->newPublisher?>">
           </div>
           <div class="col-sm-4">
              <label for="">Date</label>
               <input class="form-control" name="date" type="date" value="<?php echo $article_data->newsDate?>">
           </div> 
        </p>
    </div>
    <hr>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12" class="">
           <article>
             <label for="">Teaser</label>
              <textarea name="teaser" id="" cols="50" rows="10">
               <?php echo $article_data->newsTeaser?>
               </textarea>
           </article>
        </div>
    </div>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12" class="">
           <article>
             <label for="">Body</label>
              <textarea name="body" id="" cols="50" rows="10">
               <?php echo $article_data->newsBody?>
               </textarea>
           </article>
        </div>
    </div>
    <div class="row">
        <h3 align="center">Non-Displayed Items</h3>
        <hr>
        <div class="col-sm-4">
           <label for="">Link</label>
            <input class="form-control" name="link" type="text" value="<?php echo $article_data->newsLink?>">
        </div>
        <div class="col-sm-4">
           <p>Display</p>
                <select class="form-control" name="Display" id="Display">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
        </div>
        <div class="col-sm-4">
           <label for="">Publisher Image</label>
            <input class="form-control" name="pub_img" type="text" value="<?php echo $article_data->newsImage?>">
        </div>
    </div>
</div>
<br>
<div class="subbutton">
    <input type="hidden" name="ID" value="<?php echo $ID?>">
    <input type="submit" value="Submit" class="btn btn-danger" style="width:100%">
</div>
</form>
<?php include 'onlinetraining/includes/footer.html';?>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<script>
$('select#Display').val(<?php echo $article_data->display?>);
</script>
    
<script>
	$('#submit').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $(this).prop('action');   		
		// get current location url			
		
		// prevent page from leaving to desination
		event.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                 alert(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	});
</script>
</body>
</html>
