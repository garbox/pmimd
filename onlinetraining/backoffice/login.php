<?php session_start();?>
<?php include '../includes/config.php';?>
<?php include '../includes/functions.php'?>
<?php echo $_SESSION['Login']?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Backoffice: Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
body{
	color:black;
}
div.overflow{
    height: 500px;
    overflow-y: scroll;	
}
h2{
	color:#5371AD;
}
</style>
</head>
<body>
<div class="container-fluid" style="padding-top:30px; margin-bottom:30px; min-height:507px">
    <div class="row">
        <div class="container">                     
            <div class="row"> 
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 col-md-offset-4 col-lg-offest-4">
              	  	<div class="Cart-Container">
    					<h2 align="center">Backoffice Login</h2>
                        <p class="error"></p>
                        <hr>
                        <div id="wait">
                        	<img class='center-block img-fluid'src='https://pmimd.com/totalaccess/img/Test.gif'>
                        </div>
                        <form id="LogInForm" method="post" action="<?php echo baseurl()?>backoffice/script/auth.php" >
                          <div class="form-group">
                            <input type="email" class="form-control" name="username" placeholder="Username">
                          </div>
                           <div class="form-group">
                            <input type="password" class="form-control" name="pw" placeholder="Password">
                          </div>                         
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                        <a href="<?php echo baseurl()?>/report/passwordforgot/passwordupdate.php"><p style="font-size:14px;margin-top:5px;">Forgot Password?</p></a>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>
</body>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo baseurl()?>asset/js/bootstrap.min.js"></script>

<!-- Ajax -->
<script>
	$('#wait').hide();
	$('#LogInForm').on('submit', function(e){
		var values = $(this).serialize();
		// get value of action attribute   
		var desination = $('#LogInForm').prop('action');   		
		// get current location url			
		$('#LogInForm').hide();
		$('.newtopmi').hide();
		$('#wait').show();
		
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
				  if(data == '1'){
                    window.location = '<?php echo baseurl()?>backoffice'
				  }
				  else{
					  console.log(data);
					  $('#wait').hide();
					  $('.newtopmi').show();
					  $('#LogInForm').show();
					  $(".error").html("Incorrect info: Please make sure username and password is entered correctly.");
				  }
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	});
</script>
</html>
