<?php 
// This script inserts a new product to the product center. 
include '../../../includes/functions.php';
include '../../../includes/config.php';
$prodID = $_POST['ProdID'];

$conn = Connect();
$CMS = CMS();

// CMS data
$ShortDec = htmlspecialchars_decode(htmlspecialchars($_POST['ShortDec'], ENT_QUOTES));
$FullDesc = htmlspecialchars_decode(htmlspecialchars($_POST['full_desc'], ENT_QUOTES));
$CartDec = htmlspecialchars_decode(htmlspecialchars($_POST['CartDec'], ENT_QUOTES));
$CEU = $_POST['CEU'];
$Length = $_POST['Length'];
$Price = $_POST['Price'];
$Benifits = htmlspecialchars_decode(htmlspecialchars($_POST['benifits'], ENT_QUOTES));
$Curriculum = htmlspecialchars_decode(htmlspecialchars($_POST['curriculum'], ENT_QUOTES));
$Format = htmlspecialchars_decode(htmlspecialchars($_POST['format'], ENT_QUOTES));
$FAQ = htmlspecialchars_decode(htmlspecialchars($_POST['faq'], ENT_QUOTES));
$cata = $_POST['Categories'];

//OTC data
$PreReq = $Format;
$ProdName = $_POST['ProdName'];
$Featured1 = $_POST['Featured1'];
$Featured2 = $_POST['Featured2'];
$Featured3 = $_POST['Featured3'];
$CataID = $_POST['CataID'];
$Type = $_POST['Type'];
$Display = $_POST['Display'];
$PDF = $_POST['PDF'];
$ProdImg = $_POST['ProdImg'];
$AF = $_POST['ActionForm'];
$ClassMat = $_POST['ClassMaterials'];
$Inst_ID = $_POST['Inst_ID'];


//SEO Data;
$social_title = htmlspecialchars_decode(htmlspecialchars($_POST['social_title'],ENT_QUOTES));
$social_desc = htmlspecialchars_decode(htmlspecialchars($_POST['social_desc'],ENT_QUOTES));
$social_img = $_POST['social_img'];
$page_title = htmlspecialchars_decode(htmlspecialchars($_POST['page_title'],ENT_QUOTES));
$page_keyword = $_POST['page_keywords'];
$page_desc = htmlspecialchars_decode(htmlspecialchars($_POST['page_desc'],ENT_QUOTES));

$UpdateSP = "UPDATE selfpaced SET 
Name = '".$ProdName."',
ShortDescs = '".$ShortDec."',
CEUs = '".$CEU."',
Length = '".$Length."',
Price = '".$Price."',
Inst_ID = '".$Inst_ID."',
InstructorImg = '".$InstImg."',
InstName = '".$InstName."',
Prerequisites = '".$PreReq."',
FullDesc = '".$FullDesc."',
Featured1 = '".$Featured1."',
Featured2 = '".$Featured2."',
Featured3 = '".$Featured3."',
CataID = '".$CataID."',
CartDescs = '".$CartDec."',
Type = '".$cata."',
Display = '".$Display."',
PDFs = '".$PDF."',
Instructor = '".$InstDec."',
Image = '".$ProdImg."',
ActionForm = '".$AF."',
ClassMaterial = '".$ClassMat."'
WHERE ProdID = '".$prodID."'";

$UpdateCMS = "UPDATE CMS SET 
full_desc = '".$FullDesc."',
cart_desc = '".$CartDec."',
short_desc = '".$ShortDec."',
ceus = '".$CEU."',
length = '".$Length."',
price = '".$Price."',
course_summary = '".$FullDesc."',
benifits = '".$Benifits."',
format = '".$Format."',
curriculm = '".$Curriculum."',
faq = '".$FAQ."'
WHERE prod_id = ".$prodID."";

if($conn->query($UpdateSP) == TRUE && $CMS->query($UpdateCMS) == TRUE){
    $select_seo = "SELECT ProdID FROM seo_info WHERE ProdID = '".$prodID."'";
    $result = $conn->query($select_seo);
    
    $row = $result->num_rows;
    
    if($row > 0){
        $update_seo = "UPDATE seo_info SET
        social_desc = '".$social_desc."',
        social_title = '".$social_title."',
        social_img = '".$social_img."',
        page_title = '".$page_title."',
        page_desc = '".$page_desc."',
        page_keywords = '".$page_keyword."'
        WHERE ProdID = '".$prodID."'";
        $conn->query($update_seo);
        echo "Update";
    }
    else{
        $insert_seo = "INSERT INTO seo_info (ProdID, social_desc, social_title, social_img, page_title, page_desc, page_keywords)
VALUES ('".$prodID."', '".$social_desc."', '".$social_title."', '".$social_img."', '".$page_title."', '".$page_desc."', '".$page_keyword."')";
        $conn->query($insert_seo);
        echo "Insert";
    }
}
else{
    echo "OLC" .$conn->error;
    echo "CMS" .$CMS->error;
}


?>