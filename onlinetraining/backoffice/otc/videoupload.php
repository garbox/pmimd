<!DOCTYPE html>
<?php 
include '../../includes/functions.php';
include '../../includes/config.php';
?>

<?php
  function ClassListing(){
      $conn = Connect();
      $select = "SELECT * FROM selfpaced";
      $result = $conn->query($select);

      if($result->num_rows > 0){
          while($row = $result->fetch_assoc()){
              ?>
              <option value="<?php echo $row['ProdID']?>"><?php echo $row['Name']?></option>
              <?php
          }
      }
  }  
?>
<html lang="en">
<head>
    <title>Products Upload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	tinymce.init({
	selector: "textarea",
	plugins: "link",
 	 menubar: "insert",
	setup: function (editor) {
		editor.on('change', function () {
			tinymce.triggerSave();
		});
	}
	});
</script>
  
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
	hr{
		all:none;  
	}
	html, body{ 
		overflow-x: hidden;
	}
	h3{
		color:#5371ad;
	}
	.padimg{
		padding:15px; 
		width:120px;
	}
	.conpad{
		padding-top:15px;
		padding-bottom:15px;  
	}
	
	.recomended{
		margin:auto;
		padding:15px;
		height:150px;  
		width:150px;
		border-radius:200px;
	}
	
	btn-default{
		background-color:#C01313!important;  
	}
	
	btn-default:hover{
		background-color:#A31010!important
	}	
	
	.roundInfoGraph{
		height:120px;
		width:120px;
		border:1px solid #999999;
		display:inline-block;
		border-radius:60px; 
		margin:10px; 
	}
	
	.roundInfoGraph>h3{
		margin-top:35px; 
	}
	
	p.Info{
		font-size:18px;  
	}
	
	input.infograph{
		width:50px;
	}
</style>

</head>
<body> 
<!-- Form Start -->
<div id="error"></div>
<div class="hiddenCon">

<?php include("../includes/nav.php");?>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 center-block" style="padding-top:15px; padding-bottom:15px">
                	<h2 align="center" style="color:white; margin:0">File Upload</h2>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- Non-displayed info -->
<div class="container-fluid conpad" style="min-height: 520px;">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
			<div class="row">
				<h3 align="center" style="padding-bottom:15px;">Non-Displayed Info</h3>
                <div class="col-xs-4" style="padding:10px;">
                    <p>Title</p>
                    <input id="title" class="form-control" name="Title" type="text" >
                </div>
                <div class="col-xs-4" style="padding:10px;">
					<p>File Name</p>
                        <input type="file" name="my-file-selector" id="my-file-selector">
				</div>  
				<div class="col-xs-4" style="padding:10px;">
					<p>Class ID</p>
					<select name="classID" id="classID" class="form-control">
					    <?php ClassListing()?>
					</select>
				</div>  
                <div class="col-xs-4" style="padding:10px;">
					<p>Duration</p>
					<input id="duration" class="form-control" name="Duration" type="text">
				</div>  
			</div>
		</div>
	</div>
	<div class="row">
       <div class="container">
            <div class="col-lg-12" id="Display_Video">

            </div>  
       </div>
	</div>
</div>                    
<!-- Button for form to submit -->
<div style="width:100%">
<input type="hidden" name="ProdID" >
<div class="subbutton">
    <button id='submutButton' class="btn btn-danger" style="width:100%">Submit </button>
</div>

<!-- Footer -->

<?php include '../includes/footer.html';?>
</div>
</div>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<script>

    
$('#submutButton').on('click', function(e){
    ClassID = $( "#classID" ).val(); 
    Title = $( "#title" ).val(); 
    Fileupload = $( "#my-file-selector" ).val(); 
    Duration = $( "#duration" ).val(); 
    
    // Destination 
	var desination = 'script/uploadvideo.php';  
    // get info via json 				
    var JsonObj = {
        "ClassID": ""+ClassID+"",
        "Title": ""+Title+"",
        "FileUpload": ""+Fileupload+"",
        "Duration": ""+Duration+""
    }
    // asign data to var values. 
    var values = $.param(JsonObj);            
    console.log(values);
    // prevent page from leaving to desination
	e.preventDefault();	
    
    // Empty button and add loading icon. 
	$('.subbutton').empty();
	$('.subbutton').html("<div align='center' style='background-color:white;'><img src='https://www.ielts.org/images/loading-icon.gif'/></div>");

	// Start Ajax Function. 
	$.ajax({
		  url: desination,
		  type: 'post',
		  data: values, 
		  success: function(data){ 
          console.log(data);
			  $('.subbutton').empty();
			  $('.subbutton').html("<div style='background-color:#0ca24b; padding:15px;'><h3 align='center' style='color:white'>Success!</h3><p align='center' style='color:white'>Refresh page for updates</p></div>");

			},
		  error: function(xhr){
			  $('div.hide').show();
			  $('#error').html("<p class='bg-danger'>There was an error please try again.</p>");
		  } 
	});

});

$('#classID').on('change', function(){
    var classID = $(this).val();
    $('#Display_Video').load('script/display_video.php?classID='+ classID);
    
});
    


    
</script>
</body>
</html>