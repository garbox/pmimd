<!DOCTYPE html>
<?php 
include '../../includes/functions.php';
include '../../includes/config.php';
?>
<html lang="en">
<head>
    <title>Products Upload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	tinymce.init({
	selector: "textarea.textarea",
	plugins: [
        "link lists",
    ],
          convert_urls: false,
	toolbar: 'undo redo alignleft aligncenter alignright removeformat bullist numlist styleselect',
	setup: function (editor) {
		editor.on('change', function () {
			tinymce.triggerSave();
		});
	}
	});
</script>
  
<style>
	hr{
		all:none;  
	}
	html, body{ 
		overflow-x: hidden;
	}
	h3{
		color:#5371ad;
	}
	.padimg{
		padding:15px; 
		width:120px;
	}
	.conpad{
		padding-top:15px;
		padding-bottom:15px;  
	}
	
	.recomended{
		margin:auto;
		padding:15px;
		height:150px;  
		width:150px;
		border-radius:200px;
	}
	
	btn-default{
		background-color:#C01313!important;  
	}
	
	btn-default:hover{
		background-color:#A31010!important
	}	
	
	.roundInfoGraph{
		height:120px;
		width:120px;
		border:1px solid #999999;
		display:inline-block;
		border-radius:60px; 
		margin:10px; 
	}
	
	.roundInfoGraph>h3{
		margin-top:35px; 
	}
	
	p.Info{
		font-size:18px;  
	}
	
	input.infograph{
		width:50px;
	}
</style>
</head>
<body>
<!-- Form Start -->
<div id="error"></div>
<div class="hiddenCon">
<form id="SubmitInfo" action="script/uploadtodb.php" method="post">

<?php include("../includes/nav.php");?>
<!-- Header -->

<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Add OTC Class</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
	<div class="row">
		<div class="container conpad" style="background-color:#fff">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left:31px;">
					<h3 style="font-size:26px;">
                    	<img alt="" class="img-circle" height="50px" src="https://www.pmimd.com/products/images/<?php echo $img?>"> 
                    	<input size="60%" placeholder="Product Name" name="ProdName" type="text" value="<?php echo $pname?>">
					<hr>
					<h3 style="font-size:26px;"></h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center" style="min-height:200px; padding-left:31px;">
					<textarea class="textarea" cols="70" id="" name="ShortDec" rows="10" ><?php echo $ShortDec?></textarea>
					<div id="content"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div align="center">
						<div class="roundInfoGraph">
							<h3>CEUs</h3>
                            <input class="infograph" name="CEU" placeholder="CEU" type="text" value="<?php echo $CEUs?>">
						</div>
						<div class="roundInfoGraph">
							<h3>Length</h3>
                            <input class="infograph" name="Length" placeholder="Length" type="text" value="<?php echo $Length?>">min
						</div>
						<div class="roundInfoGraph">
							<h3>Price</h3>
                            $<input class="infograph" name="Price" placeholder="Price" type="text" value="<?php echo $Price?>">
						</div>
					</div>
					<div align="center" style="padding-top:20px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Row 2 of content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
	<div class="row">
		<div class="container" style="background-color:#fff">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding:15px;">
					<ul class="nav nav-tabs hidden-xs hidden-sm">
						<li class="active">
							<a data-toggle="tab" href="#Description">Class Infomation</a>
						</li>
						<li>
							<a data-toggle="tab" href="#Instructor">Instructor</a>
						</li>
						<li>
							<a data-toggle="tab" href="#Prerequisites">Prerequisites</a>
						</li>
					</ul>
                    
					<div class="tab-content">
                    	<!-- Class Descriptions -->
						<div class="tab-pane active" id="Description" style="padding:15px;">
							<textarea class="textarea" id="txtContent" cols="70" name="ClassDec" rows="10"><?php echo $FullDec?></textarea>
						</div>
                    	<!-- Instructor Descriptions -->
						<div class="tab-pane" id="Instructor" style="padding:15px;">
							<table cellpadding="15px;">
								<tr>
									<td><img alt="" src="https://www.pmimd.com/images/instructors2/Blank.jpg"></td>
									<td valign="top">
                                    	<input placeholder="Instructor Img File" name="InstImg" type="text" value="<?php echo $InImg?>"><br><br>
                                        <p>Instructor Name</p>
										<textarea class="textarea" placeholder="Instructor Name" name="InstName" type="text"><?php echo $InstName?></textarea>
									</td>
								</tr>
							</table>
							<hr>
                            <p>Instructor Description</p>
							<textarea class="textarea" cols="70" id="" name="InstDec" rows="10"><?php echo $Ints?></textarea>
						</div>
                        <!-- Prerequisites -->
						<div class="tab-pane" id="Prerequisites" style="padding:15px;">
                            <p>Prerequisites</p>
							<textarea class="textarea" cols="70" id="" name="PreReq" rows="10"><?php echo $pre?></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Products Like This -->
<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
			<div align="center" class="row">
				<div style="padding-bottom:25px;">
					<h3 align="center">Other classes like this.</h3>
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 1" name="Featured1" type="text" value="<?php echo $Feat1?>">
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 2" name="Featured2" type="text"  value="<?php echo $Feat2?>">
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 3" name="Featured3" type="text"  value="<?php echo $Feat3?>">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Non-displayed info -->
<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
			<div class="row">
				<h3 align="center" style="padding-bottom:15px;">Non-Displayed Info</h3>
                <div class="col-lg-6">
					<p>Cart Description</p>
                    <textarea class="form-control textarea" rows="6" name="CartDec" ><?php echo $CartDec?></textarea>
				</div>
                <div class="col-lg-6">
					<p>Keywords</p>
                    <textarea class="form-control textarea"  rows="6" name="Type"><?php echo $Type?></textarea>
				</div>
				<div class="col-lg-4">
                	<br>
					<p>Catalog ID</p>
					<input class="form-control" name="CataID" type="text" value="<?php echo $cataID?>">
				</div>
				
				
				<div class="col-lg-4"><br>
					<p>Display</p>
					<select class="form-control" name="Display" id="Display">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
					</select>
				</div>
				<div class="col-lg-4"><br>
					<p>PDF File Name</p>
					<input class="form-control" name="PDF" type="text" value="<?php echo $PDF?>">
				</div>
				<div class="col-lg-4"><br>
					<p>Prod Image Name</p>
					<input class="form-control" name="ProdImg" type="text" value="<?php echo $img?>">
				</div>
               <div class="col-lg-4"><br>
					<p>Action Form </p>
					<input class="form-control" name="ActionForm" type="text" value="<?php echo $AF?>">
		       </div>
               <div class="col-lg-4"><br>
					<p>Class Materials (PDF File)</p>
					<input class="form-control" name="ClassMaterials" type="text" value="<?php echo $ClassMat?>">
		       </div>
			</div>
		</div>
	</div>
</div>                    

<!-- SEO info -->
<!--<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
            <div class="row">
                <h3 align="center" style="padding-bottom:15px;">SEO Fields</h3>
                <hr>
                <div class="col-xs-12"><h3>Social Media</h3></div>

                <div class="col-md-6">
                    <p>Social Medial Title</p>
                    <input class="form-control"type="text" name="social_title"><br>
                </div>
                <div class="col-md-6">
                    <p>Social Media Img</p>
                    <input class="form-control"type="text" name="social_img"><br>
                </div>
                <div class="col-md-12">
                    <p>Social Media Description</p>
                    <textarea class="form-control" type="text" name="social_desc"></textarea><br><br>
                </div>
                <div class="col-xs-12"><h3>Page Info</h3></div>
                <div class="col-md-6">
                    <p>Page Title</p>
                    <input class="form-control"type="text" name="page_title">
                </div>
                <div class="col-md-6">
                    <p>Page Description</p>
                    <input class="form-control"type="text" name="page_desc"><br>
                </div>
                <div class="col-md-12">
                    <p>Page Keywords</p>
                    <textarea class="form-control"type="text" name="page_keywords"></textarea>
                </div>
            </div>
		</div>
	</div>
</div>-->

<!-- Button for form to submit -->
<input type="hidden" name="ProdID" value="<?php echo $prodID?>">
<div class="subbutton">
    <input type="submit" value="Submit" class="btn btn-danger" style="width:100%">
</div>

</form>


<!-- Footer -->
<?php include '../includes/footer.html';?>
</div>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<script>

$('#SubmitInfo').on('submit', function(e){
	var values = $(this).serialize();
	// get value of action attribute   
	var desination = $(this).prop('action');   		
	// get current location url			
	
	// prevent page from leaving to desination
    e.preventDefault();
	$('.subbutton').empty();
	$('.subbutton').html("<div align='center' style='background-color:white;'><img src='https://www.ielts.org/images/loading-icon.gif'/></div>");

	
	$.ajax({
		  url: desination,
		  type: 'post',
		  data: values, 
		  success: function(data){ 
			  $('.subbutton').empty();
			  $('.subbutton').html("<div style='background-color:#0ca24b; padding:15px;'><h3 align='center' style='color:white'>Success!</h3><p align='center' style='color:white'>Refresh page for updates</p></div>");
              window.location = 'https://www.pmimd.com/onlinetraining/backoffice/otc/Update.php?prodID='+data;
			},
		  error: function(xhr){
			  $('div.hide').show();
			  $('#error').html("<p class='bg-danger'>There was an error please try again.</p>");
		  } 
	});

});
</script>
</body>
</html>