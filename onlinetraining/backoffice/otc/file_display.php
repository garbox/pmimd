<?php include '../../includes/functions.php'?>
<?php include '../../includes/config.php';?>
<?php
$prodID = $_GET['prodID'];

function get_files($prodID){
    $conn = Connect();
    $select = "SELECT * FROM courseresources WHERE ProdID = '".$prodID."'";
    $query = $conn->query($select);
    while($data = $query->fetch_object()){
        ?>
        <tr id="<?php echo $data->ID?>">
            <td><?php echo $data->ClassResource?></td>
            <td><a href="../../sp-pdf/<?php echo $data->ResourceLocation?>"><?php echo $data->ResourceLocation?></a></td>
            <!--<td><button id="<?php //echo $data->ID?>" class="btn btn-danger delete">Delete</button></td>-->
        </tr>
        <?php
    }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>OTC: Files</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
</head>
    <style>
        td>a{
            font-size: 15px;
        }
    </style>
<body>

<?php include("../includes/nav.php");?>
<!-- Header -->
    <div class="container-fluid" style="background-color:#3A65A5">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 center-block background-img-blank">
                    <h3 class="banner-text" align="center">OTC: Files</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Add a New File</h4>
                <form action="script/file_upload.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="prodID" value="<?php echo $prodID?>">
                    <div class="col-md-4">
                        <label for="">Display Name</label>
                        <input type="text" class="form-control" name="display_name" id="display_name" placeholder="Display Name">
                    </div>
                    <div class="col-md-3">
                        <label for="">File</label>
                        <input type="file" class="form-control-file" name="fileToUpload" id="fileToUpload" >
                    </div>
                    <div class="col-md-3"><br>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>  
                </form>
            </div>
        </div>
    </div>-->
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Available Files</h4>
                <table>
                    <tr>
                        <td><h4>Display Name</h4></td>
                        <td colspan="2"><h4>File Name</h4></td>
                    </tr>
                    <?php get_files($prodID)?> 
                </table>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

    <script>
        
	$('button.delete').on('click', function(e){
		var val = $(this).attr('id');
		var desination = "script/delete_file.php";
        var JsonObj = {
                    "id": "" + val + ""
                    }
        var values = $.param(JsonObj);
		// prevent page from leaving to desination
		e.preventDefault();	
		$.ajax({
			  url: desination,
			  type: 'post',
			  data: values, 
			  success: function(data){
                  $('tr#'+val).hide();
				},
				error: function(jqXHR, textStatus, errorThrown) {
				  	console.log(textStatus, errorThrown);
				}
		});
	
	});
    </script>
</body>