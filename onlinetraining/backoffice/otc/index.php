<?php include '../../includes/functions.php'?>
<?php include '../../includes/config.php';?>

<?php 
//get all of the articles query

$i= 0;
$conn = Connect();
$select = "SELECT * from selfpaced ORDER BY Name";
$result = $conn->query($select);
$rows = $result->num_rows;
$data = $result->fetch_all();
$split_row = floor($result->num_rows/2+1);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Backoffice: OLC</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
</head>

<body>

<?php include("../includes/nav.php");?>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Backoffice: OTC</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
       <div class="col-md-6">
           <table>
           <tr>
               <td colspan="2">Name</td>
           </tr>
            <?php
            while($i < $split_row && $i <= $rows){
                ?>
                    <tr>
                        <td><?php echo $data[$i][2]?></td>
                        <td><a href="update.php?prodID=<?php echo $data[$i][0]?>"><button class="btn btn-primary">Edit</button></a></td>
                        <td><a href="file_display.php?prodID=<?php echo $data[$i][0]?>"><button class="btn btn-seccuess">Files</button></a></td>
                    </tr>
                <?
                $i++;
            }
            ?>      
           </table>
       </div>
       <div class="col-md-6">
           <table>
           <tr>
               <td colspan="2">Name</td>
           </tr>
            <?php
            while($i >= $split_row && $i < $rows){
                ?>
                    <tr>
                        <td><?php echo $data[$i][2]?></td>
                        <td><a href="update.php?prodID=<?php echo $data[$i][0]?>"><button class="btn btn-primary">Edit</button></a></td>
                        <td><a href="file_display.php?prodID=<?php echo $data[$i][0]?>"><button class="btn btn-seccuess">Files</button></a></td>
                    </tr>
                <?
                $i++;
            }
            ?>      
           </table>
       </div>
    </div>
</div>

  <script async src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>
</body>
</html>
