<?php 
// This script inserts a new product to the product center. 
include '../../../includes/functions.php';
include '../../../includes/config.php';
$prodID = $_POST['ProdID'];

$ProdName = $_POST['ProdName'];
$ShortDec = htmlspecialchars_decode(htmlspecialchars($_POST['ShortDec'], ENT_QUOTES));
$CEU = $_POST['CEU'];
$Length = $_POST['Length'];
$Price = $_POST['Price'];
$ClassDec = htmlspecialchars_decode(htmlspecialchars($_POST['full_desc'], ENT_QUOTES));
$InstImg = $_POST['InstImg'];
$InstName = $_POST['InstName'];
$InstDec = htmlspecialchars_decode(htmlspecialchars($_POST['InstDec'],ENT_QUOTES));
$PreReq = htmlspecialchars_decode(htmlspecialchars($_POST['PreReq'],ENT_QUOTES));
$Featured1 = $_POST['Featured1'];
$Featured2 = $_POST['Featured2'];
$Featured3 = $_POST['Featured3'];
$CataID = $_POST['CataID'];
$CartDec = $_POST['CartDec'];
$Display = $_POST['Display'];
$PDF = $_POST['PDF'];
$ProdImg = $_POST['ProdImg'];
$AF = $_POST['ActionForm'];
$ClassMat = $_POST['ClassMaterials'];
$Benifits = $_POST['benifits'];
$Curriculum = $_POST['curriculum'];
$Format = $_POST['format'];
$FAQ = $_POST['faq'];
$Inst_ID = $_POST['Inst_ID'];
$Cata = $_POST['Categories'];

//SEO Data;
$social_title = htmlspecialchars_decode(htmlspecialchars($_POST['social_title'],ENT_QUOTES));
$social_desc = htmlspecialchars_decode(htmlspecialchars($_POST['social_desc'],ENT_QUOTES));
$social_img = $_POST['social_img'];
$page_title = htmlspecialchars_decode(htmlspecialchars($_POST['page_title'],ENT_QUOTES));
$page_keyword = $_POST['page_keywords'];
$page_desc = htmlspecialchars_decode(htmlspecialchars($_POST['page_desc'],ENT_QUOTES));

$conn = Connect();
$cms = CMS();

$Update = "UPDATE selfpaced SET 
Name = '".$ProdName."',
ShortDescs = '".$ShortDec."',
FullDesc = '".$ClassDec."',
Inst_ID = '".$Inst_ID."',
CEUs = '".$CEU."',
Length = '".$Length."',
Price = '".$Price."',
FullDesc = '".$ClassDec."',
CataID = '".$CataID."',
CartDescs = '".$CartDec."',
Display = '".$Display."',
Featured1 = '".$Featured1."',
Featured2 = '".$Featrued2."',
Featured3 = '".$Featured3."',
Type = '".$Cata ."'
WHERE ProdID = '".$prodID."'";

if($conn->query($Update) ==TRUE){
    
    $select_seo = "SELECT ProdID FROM seo_info WHERE ProdID = '".$prodID."'";
    $result = $conn->query($select_seo);
    
    $row = $result->num_rows;
    
    if($row > 0){
        $update_seo = "UPDATE seo_info SET
        social_desc = '".$social_desc."',
        social_title = '".$social_title."',
        social_img = '".$social_img."',
        page_title = '".$page_title."',
        page_desc = '".$page_desc."',
        page_keywords = '".$page_keyword."'
        WHERE ProdID = '".$prodID."'";
        $conn->query($update_seo);
        $conn->query($update_seo);
        echo "Update";
    }
    else{
        $insert_seo = "INSERT INTO seo_info (ProdID, social_desc, social_title, social_img, page_title, page_desc, page_keywords)
VALUES ('".$prodID."', '".$social_desc."', '".$social_title."', '".$social_img."', '".$page_title."', '".$page_desc."', '".$page_keyword."')";
        $conn->query($insert_seo);
        echo "Insert";
    }
}
else{
    echo $conn->error;
}


?>