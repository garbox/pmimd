<table>
    <tr>
        <td>Video Name</td>
        <td>File Name</td>
        <td>Duration</td>
    </tr>
    <?php
        include("../../../includes/config.php");
        $data = $_REQUEST;
        $conn = Connect();

        $select = "SELECT * FROM videos WHERE ClassID = '".$data['classID']."'";
        $result = $conn->query($select);

        while($row = $result->fetch_assoc()){
    ?>
    <tr>
        <td><?php echo $row['Title']?></td>
        <td><?php echo $row['FileName']?></td>
        <td><?php echo gmdate("H:i:s", $row['Duration'])?></td>
        <td><button id="<?php echo $row['ID']?>" class="btn btn-danger remove">Remove</button></td>
    </tr>
    <?php
        }
    ?>
</table>
<script>
    
$('.remove').on('click',function(e){
    ID = $(this).attr("id");
    
    destination = 'script/remove_video.php'; 
    
    var JsonObj = {
        "ID": ""+ID+""
    }
    
    // asign data to var values. 
    var values = $.param(JsonObj);
    
    $.ajax({
      url: destination,
      type: 'post',
      data: values, 
      success: function(data){ 
          console.log($(this));
      $("#"+ID).parent().parent().remove();
          
      },
      error: function(xhr, data){
          console.log(data)
      } 
    });
})
    
</script>