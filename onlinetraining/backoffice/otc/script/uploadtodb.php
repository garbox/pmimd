<?php 
// This script inserts a new product to the product center. 
include '../../../includes/functions.php';
include '../../../includes/config.php';


$data = $_REQUEST;

$conn = Connect();

$Insert = "INSERT INTO selfpaced 
(Name,
ShortDescs,
CEUs,
Length,
Price,
FullDesc,
InstructorImg,
InstName,
Instructor,
Prerequisites,
Featured1,
Featured2,
Featured3,
CataID,
CartDescs,
Type,
Display,
PDFs,
Image) 

VALUES ('".htmlspecialchars_decode(htmlspecialchars($data['ProdName'], ENT_QUOTES))."',
'".htmlspecialchars_decode(htmlspecialchars($data['ShortDec'], ENT_QUOTES))."',
'".$data['CEU']."',
'".$data['Length']."',
'".$data['Price']."',
'".htmlspecialchars_decode(htmlspecialchars($data['ClassDec'], ENT_QUOTES))."',
'".$data['InstImg']."',
'".htmlspecialchars_decode(htmlspecialchars($data['InstName'], ENT_QUOTES))."',
'".htmlspecialchars_decode(htmlspecialchars($data['InstDec'], ENT_QUOTES))."',
'".htmlspecialchars_decode(htmlspecialchars($data['PreReq'], ENT_QUOTES))."',
'".$data['Featured1']."',
'".$data['Featured2']."',
'".$data['Featured3']."',
'".$data['CataID']."',
'".htmlspecialchars_decode(htmlspecialchars($data['CartDec'], ENT_QUOTES))."',
'".$data['Type']."',
'".$data['Display']."',
'".$data['PDF']."',
'".$data['ProdImg']."' )";

if ($conn->query($Insert) === TRUE) {
    $select = "SELECT ProdID FROM selfpaced ORDER BY ProdID DESC LIMIT 1";
    $result = $conn->query($select);
    $data = $result->fetch_object();
    echo $data->ProdID;
}
else {
    echo "Error: " . $Insert . "<br>" . $conn->error;
}
?>