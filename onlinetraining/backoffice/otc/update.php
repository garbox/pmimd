<?php 
include '../../includes/functions.php';
include '../../includes/config.php';

$prodID = $_GET['prodID'];

if($prodID == 119 || $prodID == 120 || $prodID == 121 || $prodID == 122 || $prodID == 165){
    header("Location: https://www.pmimd.com/onlinetraining/backoffice/certifications/update_cert.php?prodID=".$prodID);
}
$conn = Connect();
$faculty = pmimd_faculty();

$select = "SELECT * FROM selfpaced WHERE ProdID = '".$prodID."'"; 
$seo_data = "SELECT * FROM seo_info WHERE ProdID = '".$prodID."'";
$fac_select = "SELECT * FROM faculty ORDER BY fname ASC";

$result = $conn->query($select);
$seo_info = $conn->query($seo_data);
$seo_info = $seo_info->fetch_object();
$row = $result->fetch_object();
$fac = $faculty->query($fac_select);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Products Upload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
    
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	tinymce.init({
	selector: "textarea.textarea",
	plugins: [
                "link lists",
				"code"
                ],
        convert_urls: false,
	toolbar: 'undo redo alignleft aligncenter alignright removeformat bullist numlist styleselect code link',
	setup: function (editor) {
		editor.on('change', function () {
			tinymce.triggerSave();
		});
	}
	});
</script>
  
<style>
	hr{
		all:none;  
	}
	html, body{ 
		overflow-x: hidden;
	}
	h3{
		color:#5371ad;
	}
	.padimg{
		padding:15px; 
		width:120px;
	}
	.conpad{
		padding-top:15px;
		padding-bottom:15px;  
	}
	
	.recomended{
		margin:auto;
		padding:15px;
		height:150px;  
		width:150px;
		border-radius:200px;
	}
	
	btn-default{
		background-color:#C01313!important;  
	}
	
	btn-default:hover{
		background-color:#A31010!important
	}	
	
	.roundInfoGraph{
		height:120px;
		width:120px;
		border:1px solid #999999;
		display:inline-block;
		border-radius:60px; 
		margin:10px; 
	}
	
	.roundInfoGraph>h3{
		margin-top:35px; 
	}
	
	p.Info{
		font-size:18px;  
	}
	
	input.infograph{
		width:50px;
	}
</style>
</head>
<body>
<!-- Form Start -->
<div id="error"></div>
<div class="hiddenCon">
    <!-- script/updatetodb.php -->
<form id="SubmitInfo" action="script/updatetodb.php" method="post">


<?php include("../includes/nav.php");?>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Update OTC Class</h3>
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="searchPlacement">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
            <a href="https://www.pmimd.com/onlinetraining/backoffice/otc/file_display.php?prodID=<?php echo $prodID?>" class="navbar-brand">Course Manuals</a>
            <div class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                <a style="color:#c01313;" href="" class="navbar-brand" id='remove'>Remove</a>
            </div>
        </nav>
    </div>    
</div>
    
<!-- Content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
	<div class="row">
		<div class="container conpad" style="background-color:#fff">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left:31px;">
					<h3 style="font-size:26px;">
                    	<img alt="" class="img-circle" height="50px" src="https://www.pmimd.com/products/images/<?php echo $row->Image?>"> 
                    	<input size="60%" placeholder="Product Name" name="ProdName" type="text" value="<?php echo $row->Name?>">
					<hr>
					<h3 style="font-size:26px;"></h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center" style="min-height:200px; padding-left:31px;">
					<textarea class="textarea" cols="70" id="" name="ShortDec" rows="10" ><?php echo $row->ShortDescs?></textarea>
					<div id="content"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div align="center">
						<div class="roundInfoGraph">
							<h3>CEUs</h3>
                            <input class="infograph" name="CEU" placeholder="CEU" type="text" value="<?php echo $row->CEUs?>">
						</div>
						<div class="roundInfoGraph">
							<h3>Length</h3>
                            <input class="infograph" name="Length" placeholder="Length" type="text" value="<?php echo $row->Length?>">min
						</div>
						<div class="roundInfoGraph">
							<h3>Price</h3>
                            $<input class="infograph" name="Price" placeholder="Price" type="text" value="<?php echo $row->Price?>">
						</div>
					</div>
					<div align="center" style="padding-top:20px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Row 2 of content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
    <div class="row">
        <div class="container" style="background-color:#fff">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding:15px;">
                    <ul class="nav nav-tabs hidden-xs hidden-sm">
                        <li class="active">
                            <a data-toggle="tab" href="#Course">Class Information</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Instructor">Instructor</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Prerequisites">Prerequisites</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Course" style="padding:15px;">
                            <textarea class="textarea form-control" name="full_desc" id="" cols="30" rows="10"><?php echo $row->FullDesc?></textarea>
                        </div>
                        
                        <div class="tab-pane" id="Instructor" style="padding:15px;">
                            <select class="form-control" name="Inst_ID" id="Instructor">
                                <?php
                                while($fac_info = $fac->fetch_object()){
                                    echo "<option value='".$fac_info->ID."'>".$fac_info->fname." ".$fac_info->lname."</option>";
                                }
                                ?>
                                
                            </select>
                        </div>
                        <div class="tab-pane" id="Prerequisites" style="padding:15px;">
                            <textarea class="textarea form-control" name="pre_req" id="" cols="30" rows="10"><?php echo $row->Prerequisites;?></textarea>
                        </div>
                	</div>               
            	</div>
        	</div>
    	</div>
    </div>
</div>

<!-- Products Like This -->
<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
			<div align="center" class="row">
				<div style="padding-bottom:25px;">
					<h3 align="center">Other classes like this.</h3>
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 1" name="Featured1" type="text" value="<?php echo $row->Featured1?>">
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 2" name="Featured2" type="text"  value="<?php echo $row->Featured2?>">
				</div>
				<div class="col-lg-4">
					<input placeholder="Marketing Spot 3" name="Featured3" type="text"  value="<?php echo $row->Featured3?>">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Non-displayed info -->
<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
			<div class="row">
				<h3 align="center" style="padding-bottom:15px;">Non-Displayed Info</h3>
                <div class="col-lg-12">
					<p>Cart Description</p>
                    <textarea class="form-control" rows="6" name="CartDec" ><?php echo $row->CartDescs?></textarea>
				</div>
				<div class="col-lg-4">
                	<br>
					<p>Catalog ID</p>
					<input class="form-control" name="CataID" type="text" value="<?php echo $row->CataID?>">
				</div>
				<div class="col-lg-4"><br>
					<p>Display</p>
					<select class="form-control" name="Display" id="Display">
					    <option value="1">Yes</option>
					    <option value="0">No</option>
					</select>
				</div>
				<div class="col-lg-4"><br>
					<p>Categories</p>
					<select class="form-control" name="Categories" id="Categories">
                        <option value="Core">Core</option>
                        <option value="Featured">Featured</option>
                        <option value="Certification">Certification</option>
                        <option value="Packages">Packages</option>
                    </select>
				</div>
				<div class="col-lg-4"><br>
					<p>Prod Image Name</p>
					<input class="form-control" name="ProdImg" type="text" value="<?php echo $row->Image?>">
				</div>
               <div class="col-lg-4"><br>
					<p>Action Form </p>
					<input class="form-control" name="ActionForm" type="text" value="<?php echo $row->AF?>">
		       </div>
                <div class="col-lg-4"><br>
					<p>PDF File Name</p>
					<input class="form-control" name="PDF" type="text" value="<?php echo $row->PDF?>">
				</div>
			</div>
		</div>
	</div>
</div>    
    
<!-- SEO info -->
<div class="container-fluid conpad" style="background-color:#E6E6E6">
	<div class="row">
		<div class="container conpad" style="background-color:#FFFFFF">
            <div class="row">
                <h3 align="center" style="padding-bottom:15px;">SEO Fields</h3>
                <hr>
                <div class="col-xs-12"><h3>Social Media</h3></div>

                <div class="col-md-6">
                    <p>Social Medial Title</p>
                    <input class="form-control"type="text" name="social_title" value="<?php echo $seo_info->social_title?>"><br>
                </div>
                <div class="col-md-6">
                    <p>Social Media Img</p>
                    <input class="form-control"type="text" name="social_img" value="<?php echo $seo_info->social_img?>"><br>
                </div>
                <div class="col-md-12">
                    <p>Social Media Description</p>
                    <textarea class="form-control" type="text" name="social_desc"><?php echo $seo_info->social_desc?></textarea><br><br>
                </div>
                <div class="col-xs-12"><h3>Page Info</h3></div>
                <div class="col-md-12">
                    <p>Page Title</p>
                    <input class="form-control"type="text" name="page_title" value="<?php echo $seo_info->page_title?>">
                </div>
                <div class="col-md-6">
                    <p>Page Description</p>
                    <textarea class="form-control" name="page_desc"><?php echo $seo_info->page_desc?></textarea>
                </div>
                <div class="col-md-6">
                    <p>Page Keywords</p>
                    <textarea class="form-control"type="text" name="page_keywords"><?php echo $seo_info->page_keywords?></textarea>
                </div>
            </div>
		</div>
	</div>
</div>               

<!-- Button for form to submit -->
<input type="hidden" name="ProdID" value="<?php echo $prodID?>">
<div class="subbutton">
    <input type="submit" value="Submit" class="btn btn-danger" style="width:100%">
</div>

</form>

<!-- Footer -->
<?php include '../../includes/footer.html';?>
</div>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

<script>
$('select#Display').val(<?php echo $row->Display?>);
$('select#Categories').val('<?php echo $row->Type?>');
$('select#Instructor').val(<?php echo $row->Inst_ID?>);
$('#SubmitInfo').on('submit', function(e){
	var values = $(this).serialize();
	// get value of action attribute   
	var desination = $('#SubmitInfo').prop('action');   		
	// get current location url			
	
	// prevent page from leaving to desination
	event.preventDefault()
	$('.subbutton').empty();
	$('.subbutton').html("<div align='center' style='background-color:white;'><img src='https://www.ielts.org/images/loading-icon.gif'/></div>");

	
	$.ajax({
		  url: desination,
		  type: 'post',
		  data: values, 
		  success: function(data){ 
			  $('.subbutton').empty();
			  $('.subbutton').html("<div style='background-color:#0ca24b; padding:15px;'><h3 align='center' style='color:white'>Success!</h3><p align='center' style='color:white'>Refresh page for updates</p></div>");
              console.log(data);
			  location.reload();
			  
			},
		  error: function(xhr){
			  $('div.hide').show();
			  $('#error').html("<p class='bg-danger'>There was an error please try again.</p>");
		  } 
	});

});
$('#remove').on("click", function(e){
    e.preventDefault();
    alert("Remove Prodcuct clicked.");

})
</script>
</body>
</html>