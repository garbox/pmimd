<?php session_start()?>
<?php include '../includes/functions.php'?>
<?php include '../includes/config.php';?>
<?php
function LogInSessionCheck(){
    
    if($_SESSION['Login'] == 1 ){
        // Do nothing
    }
    else{
        //Redirect to login page. 
          header('Location: https://www.pmimd.com/onlinetraining/backoffice/login.php'); 
    }
}
?>
<?php LogInSessionCheck()?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Backoffice</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo baseurl()?>assets/css/style.css">
  
<style>
    div.button_link>a:hover{
        text-decoration: none;
    }
    
    h1,h2,h3,h4,h5,h6{
        color:#5371ad;
    }
    body{
        background-color: #e6e6e6;
    }
</style>
</head>

<body>

<?php include("includes/nav.php");?>


<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h3 class="banner-text" align="center">Backoffice</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" >
       
        <div class="col-sm-4 button_link" style="margin-top: 30px;">
           <a href="<?php echo baseurl()?>backoffice/instructor">
            <div style="border: 2px solid #e6e6e6; width: 100%; height: 100%;min-height: 150px; background-color:white;">
               <h3 align="center">Instructor</h3>
               <p align="center">Edit all facutly, corporate, and associate instructors</p>
            </div>
            </a>
        </div>
        
        <div class="col-sm-4 button_link" style="margin-top: 30px;">
           <a href="<?php echo baseurl()?>backoffice/otc">
            <div style="border: 2px solid #e6e6e6; width: 100%; height: 100%;min-height: 150px; background-color:white;">
               <h3 align="center">OTC</h3>
               <p align="center">Edit all selfpaced products and all of the infomation that goes with it.</p>
            </div>
            </a>
        </div>
        <div class="col-sm-4 button_link" style="margin-top: 30px;">
           <a href="<?php echo baseurl()?>backoffice/inthenews">
            <div style="border: 2px solid #e6e6e6; width: 100%; height: 100%;min-height: 150px; background-color:white;">
               <h3 align="center">In The News</h3>
               <p align="center">Edit all publications that are on the In The News Sections of PMI.</p>
            </div>
            </a>
        </div>   
        <div class="col-sm-4 button_link" style="margin-top: 30px;">
           <a href="<?php echo baseurl()?>backoffice/certifications">
            <div style="border: 2px solid #e6e6e6; width: 100%; height: 100%;min-height: 150px; background-color:white;">
               <h3 align="center">Certifications</h3>
               <p align="center">In the works</p>
            </div>
            </a>
        </div>     
    </div>
</div>

<script async src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script async src="<?php echo baseurl()?>assets/js/bootstrap.min.js"></script>

</body>
</html>
