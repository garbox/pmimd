<?php
header("Content-Type:application/json");
include "config.php";

$conn = pmimain_connect();
$api_con = api_log();

// use webinar view and short by date.
$query = "SELECT * FROM webinar
WHERE  (date > CURRENT_DATE()) AND (program_code != 'A-TA')
GROUP BY AF
ORDER BY date ASC";

$api_log = "INSERT INTO log (type, method) VALUES ('webinar_api','GET')";

$result = $conn->query($query);
$api_con->query($api_log);


while($data = $result->fetch_object()){
    $webinar[$data->AF] = [
        "name"=> $data->name,
        "instructor"=> $data->instructor,
        "price"=> number_format($data->price, 2),
        "more_info_link" => strtolower("https://www.pmimd.com/programs/".$data->program_code."audio.asp"),
        "buy_link" => "https://www.pmimd.com/advanced.asp?topic=".$data->program_ID."",
        "start_time" => $data->start_time,
        "end_time" => $data->end_time,
        "date"=> $data->date
    ];
}
$full_list['webinars'] = $webinar;

echo json_encode($full_list);
?>
