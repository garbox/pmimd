<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller{
    public function __construct(){
        parent::__construct();
        
        $this->Cookie_Set->set_cookies();
   }
    
    public function index(){
        //load headers and such
        $this->load->view('header/scripts.php');
        $this->load->view('header/strip_top.php');
        $this->load->view('header/nav.php');
        
        //load models 
        $this->load->model('Prod');
        
        $prods["featured"] = $this->Prod->featured_prod();
        $prods["core"] = $this->Prod->core_prod();
        $prods["certs"] = $this->Prod->cert_prod();
        $prods["packages"] = $this->Prod->package_prod();
        
        //load page body with data
        $this->load->view("main_prod_display.php", $prods);
        
        //load footer
        $this->load->view('footer/footer.php');
    }
}
?>