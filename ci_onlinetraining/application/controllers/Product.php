<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller{
    public function __construct(){
       parent::__construct();
        
       $this->load->model('Prod');
        $this->Cookie_Set->set_cookies();
   }
    
    public function index($prodID){
        $data['prod_info'] = $this->Prod->prod_display($prodID);
        $data['featured_prod'] = $this->Prod->recomended_products($data['prod_info'][0]["ProdID"]);
        $this->Prod->class_not_in_olc_redirect($prodID);
        
        $this->load->view('header/scripts.php');
        $this->load->view('header/strip_top.php');
        $this->load->view('header/nav.php');
        
        //load page body with data
        $this->load->view("product_page.php", $data);
        
        //load footer
        $this->load->view('footer/footer.php');
    }
}
?>