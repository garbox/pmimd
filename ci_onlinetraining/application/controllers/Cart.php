<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        
        //set cookie if not set. 
        $this->Cookie_Set->set_cookies();
        
        //load global models
        $this->load->model('Cart_Model');
        $this->load->model('Prod');
    }
    
    public function index(){
        //logic/Models
        $data['cart_data'] = $this->Cart_Model->get_cart_data($_COOKIE["cart_id"]);
        $data['total_cost'] = $this->Cart_Model->total_price($data['cart_data']);
        
        //seo data
        $seo['title'] = "Your Cart";
        
        //header items
        $this->load->view('header/scripts.php', $seo);
        $this->load->view('header/strip_top.php');
        $this->load->view('header/nav.php');
        
        //body
        $this->load->view('cart.php', $data);
        
        //footer
        $this->load->view('footer/footer.php');
    }
}
?>