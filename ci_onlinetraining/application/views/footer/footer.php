<div class="container-fluid" style="background-color:#414549; color:white; padding-top:25px; padding-bottom:25px;">
	<div class="container">
    	<div class="row footer" align="left">
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a href="http://pmimd.com/certify/">Certification</a></p>
                  <a class="footiem" href="http://pmimd.com/certify/certified-medical-coder-certification.asp">Coding</a><br>
                  <a class="footiem"  href="http://pmimd.com/certify/certified-medical-insurance-specialist-certification.asp">Billing</a><br>
                  <a class="footiem"  href="http://pmimd.com/certify/certified-medical-office-manager-certification.asp">Management</a><br>
                  <a class="footiem"  href="http://pmimd.com/certify/certified-medical-compliance-officer-certification.asp">Compliance</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a href="http://www.pmimd.com/programs/default.asp">Curriculum</a></p>
                  <a class="footiem"  href="http://www.pmimd.com/programs/default.asp">Learning Topics</a><br>
                  <a  class="footiem" href="http://www.pmimd.com/programs/locator.asp">Class Locator</a><br>
                  <a class="footiem"  href="http://pmimd.com/DLCON">National Conference</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a>Meet the Team</a></p>
                  <a class="footiem"  href="http://www.pmimd.com/host/default.asp">Host PMI Training</a><br>
                  <a class="footiem"  href="http://www.pmimd.com/testimonials.asp">Success Stories</a><br>
                  <a class="footiem"  href="http://www.pmimd.com/policies.asp">Policies</a><br>
                  <a class="footiem"  href="http://www.pmimd.com/contact.asp">Contact PMI</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a>Resources</a></p>
                  <a class="footiem"  href="http://www.pmimd.com/certify/pmi-certification-renewal.asp">Certification Renewal</a><br>
                  <a class="footiem"  href="http://www.pmimd.com/audio/totalaccess.asp">LMS Subscription</a><br>
                  <a class="footiem"  href="http://www.pmimd.com/certify/ceutracker.asp">CEU Tracker</a><br>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footerMenu" style="padding-top:15px;">
          <a href="http://www.pmimd.com"><img class="center-block " src="https://www.pmimd.com/images/pmilogowhite.png" width="50%" alt=""></a>
          </div>         
      </div>
    </div>
	<div class="container">
    	<div class="row footer" align="center">
        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span style="color:#999999; font-size:12px"><br>
                </span><p><span style="color:#999999; font-size:12px">CPT® is a registered trademark of the American Medical Association. All rights reserved.<br>
                ©<?php echo date("Y")?> Practice Management Institute® |  All rights reserved.</span></p>
            </div>
        </div>
    </div>
</div> 