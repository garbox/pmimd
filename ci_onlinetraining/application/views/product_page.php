<?php 
$prod_info = $prod_info[0];
//
?>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Online Training Course</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
    <div class="row">
        <div class="container conpad" style="background-color:#fff">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left:31px;">
                    <h3 style="font-size:26px;"><img class="img-circle" src="https://www.pmimd.com/products/images/<?php echo $prod_info["Image"];?>" alt="" height="50px"> <?php echo $prod_info["Name"]; ?><hr></h3>
                </div>              
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center" style="min-height:200px; padding-left:31px;">	
                    <p><?php echo $prod_info['ShortDescs'];?></p>
                    <div id="content"></div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div align="center">
                    	<div class="roundInfoGraph">
                        	<h3>CEUs</h3>
                       		<p class="Info"><?php echo $prod_info["CEUs"];?></p>
                        </div>
                        <div class="roundInfoGraph">
                        	<h3>Length</h3>
                        	<p class="Info"><?php echo $prod_info["Length"];?> min</p>
                        </div>
                        <div class="roundInfoGraph">
                        	<h3>Price</h3>
                        	<p class="Info">$<?php echo $prod_info["Price"]?></p>
                        </div>            
                    </div>
                    <div align="center" style="padding-top:20px;">
                   		<!-- Buy -->
                        <form id="BuyProd" action="../scripts/cartscript.php" method="post" style="display:inline">
                        <input type="hidden" name="ProdID" value="<?php echo $prod_info["ProdID"];?>">
                        <button id="BuyButton" type="submit" class="btn btn-default" style="width:200px; background-color:#c01313; color:white">Buy</button>
                        </form>
                        
                        <!-- Add To Cart -->
                        <form id="AddProd" action="../scripts/cartscript.php" method="post" style="display:inline">
                        <input type="hidden" name="ProdID" value="<?php echo $prod_info["ProdID"];?>">
                        <button type="submit" class="btn btn-default" style="width:200px; background-color:#0ca24b; color:white">Add to Cart</button>
                        </form>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<!-- Row 2 of content -->
<div class="container-fluid conpad" style="background-color:#e6e6e6">
    <div class="row">
        <div class="container" style="background-color:#fff">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding:15px;">
                    <ul class="nav nav-tabs hidden-xs hidden-sm">
                        <li class="active">
                            <a data-toggle="tab" href="#Description" >Class Information</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Instructor">Instructor</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Prerequisites">Prerequisites</a>
                        </li>
                    </ul>
                    <ul class="nav nav-tabs hidden-lg hidden-md">
                        <li class="active">
                            <a data-toggle="tab" href="#Description" >Info</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Instructor">Inst</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Prerequisites">Pre-Req</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Description" style="padding:15px;">
                            <?php echo $prod_info["FullDesc"]?>
                        </div>
                        <div class="tab-pane" id="Instructor" style="padding:15px;">
                        <table cellpadding="15px;">
                        	<tr>
                        		<td><img src="https://www.pmimd.com/images/instructors2/<?php echo $prod_info["InstructorImg"]?>" alt=""></td>
                        		<td valign="top"><?php echo $prod_info["InstName"]?></td>
                        	</tr>
                        </table>
                            <hr>
                            <?php echo $prod_info["Instructor"]?>
                        </div>
                        <div class="tab-pane" id="Prerequisites" style="padding:15px;">
                        <?php echo $prod_info["Prerequisites"];?>
                        </div>
                	</div>               
            	</div>
        	</div>
    	</div>
    </div>
</div>

<!-- Products Like This -->
<div class="container-fluid conpad" style="background-color:#E6E6E6" >
    <div class="row">
        <div class="container conpad" style="background-color:#FFFFFF">
            <div class="row">
                <div style="padding-bottom:25px;">
                    <h3 align="center">Other classes like this.</h3>
                </div>
                    <?php 
                        $i = 0;
                        $f = 1;
                        while($i < sizeof($featured_prod)){
                        ?>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" style="padding:5px;">
                                <a href="<?php echo base_url()?>index.php/Product/index/<?php echo $featured_prod[$f][0]['ProdID']?>">
                                    <img alt="<?php echo $featured_prod[$f][0]['Name']?>" class="center-block img-fluid recomended" src="https://www.pmimd.com/products/images/<?php echo $featured_prod[$f][0]['Image']?>">
                                </a>
                                <p align="center"><?php  echo $featured_prod[$f][0]['Name']?></p>
                            </div>
                        <?php
                            $i++;
                            $f++;
                        }
                    ?>
            </div>
        </div>
    </div>
</div>
