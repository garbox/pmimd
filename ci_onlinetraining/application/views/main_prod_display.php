<?php 
function prod_display($data){
$i = 0;
while($i <sizeof($data)){
?>
    <div class="col-xs-12 col-sm-6 col-lg-4 prodPadding">
        <div class="ProdCont shadow">
            <div class="imgContainer">
                <a href="<?php echo base_url()?>index.php/Product/index/<?php echo $data[$i]['ProdID']?>"><img class="imgPlacement imgSize center-block" src="https://www.pmimd.com/products/images/<?php echo $data[$i]['Image']?>" alt="<?php echo $data[$i]['Name']?>"></a>
            </div>    
            <div class="prodContent">
                <div class="headerSize">
                <h4 align="center"><?php echo $data[$i]['Name']?></h4>
                </div>
                <p class="text"><?php echo $data[$i]['CartDescs']?></p>
                <div  style="margin-top:15px;">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="center">
                <h3 style="margin-top:2px">$<?php echo $data[$i]['Price']?></h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <a href="<?php echo base_url()?>index.php/Product/index/<?php echo $data[$i]['ProdID']?>"><img class="center-block" src="https://www.pmimd.com/products/images/details.jpg" alt=""></a>
                </div>
                </div>
            </div>
        </div>
    </div>
<?php $i++;}}?>

<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center-block"><img src="<?php echo base_url()?>assets/images/OnlineTrainingBannerLarge.png" alt="" width="100%"></div>
            </div>
        </div>
    </div>
</div>

<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container" style="background-color:white;"> 
            <div class="col-lg-12" style="padding:15px;">
                <h1>Featured Topics</h1>
                <p>Take a look at some of our highest rated and attended topics.</p>
                <hr>
                <?php prod_display($featured)?>
            </div>
        </div>
        <div class="container" style="background-color:white; margin-top:20px;">           
            <div class="col-lg-12" style="padding:15px;">
                <h1>Core topics</h1>
                <p>Recorded sessions are adapted from PMI’s live class curriculum and include a digital course manual.</p>
                <hr>
                <?php prod_display($core)?>
            </div>
        </div>
        <div id="Certifications"></div>
        <div class="container" style="background-color:white; margin-top:20px;">  
            <div class="col-lg-12" style="padding:15px;">
                <h1>Certifications</h1>
                <p>Sessions are adapted from live class lectures and include a physical course manual, support via discussion forum, and proctored exam.</p>
                <hr>
                <?php prod_display($certs)?>
            </div>
        </div>
        <div class="container" style="background-color:white; margin-top:20px; margin-bottom: 30px">  
            <div class="col-lg-12" style="padding:15px;">
                <h1>Training packages</h1>
                <p>Get more for less when you customize your learning track with these package options.</p>
                <hr>
                <?php prod_display($packages)?>
            </div>
		</div>
	</div>
</div>


