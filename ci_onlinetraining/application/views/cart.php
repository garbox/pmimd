
<?php print_r($cart_data)?>
<!-- Header -->
<div class="container-fluid" style="background-color:#3A65A5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-block background-img-blank">
                <h1 class="banner-text" align="center">Your Cart</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Shopping Cart Nav -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px;">
    <div class="row">
        <div class="container">
      		<div class="CartNav"> 
            	<div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 active" align="center">
                        <a class="CartNavLink" href="yourcart.php">
                        	<span class="glyphicon glyphicon-ok cartNavText" style="color:#414549;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Checking Order</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <a class="CartNavLink" href="payment/payinfo.php">
                        	<span class="glyphicon glyphicon-search cartNavText" style="color:#5371ad;"></span>
                        	<p style="text-align: center" class="hidden-sm hidden-xs"> Billing Info</p>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-credit-card cartNavText" style="color:#0ca24b;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs">  Card Info</p>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inactive" align="center">
                        <span class="glyphicon glyphicon-thumbs-up cartNavText" style="color:#c01313;"></span>
                        <p style="text-align: center" class="hidden-sm hidden-xs"> Success</p>
                    </div>
              	</div>
           </div>
    	</div>
 	</div>            
 </div>  

<!-- Product Display -->
<div class="container-fluid" style="background-color:#e6e6e6; padding-top:30px; margin-bottom:30px; min-height:490px">
    <div class="row">
        <div class="container">                     
            <div class="row">

                <div class="col-sm-6 col-xs-12 col-md-8 col-lg-8">
                	<div class="Cart-Container">
                    	<h2>Summary</h2>
                        <table width="100%">
                            <?php
                                if($cart_data == 0){
                                    echo("<p>No items in your cart</p>");
                                    $item_count = 0;
                                    $total_cost = 0;
                                }
                                else{
                                    $i = 0;
                                    ?>
                                        <tr>
                                            <td>Name</td>
                                            <td colspan="2">Price</td>
                                        </tr>
                                    <?php
                                    while($i < sizeof($cart_data)){
                                        ?>
                                            <tr>
                                                <td><?php echo $cart_data[$i][0]['Name']?></td>
                                                <td>$<?php echo $cart_data[$i][0]['Price']?></td>
                                                <td><button class="btn btn-danger">Remove</button></td>
                                            </tr>
                                        <?php
                                            $i++;
                                    }
                                }
                            ?>
                        </table>
             		</div>
                </div> 

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              	  	<div class="Cart-Container">
    					<h2>Ready to check out?</h2>
                        <hr>
                        <p>Item Count: 
                        	<span class="count">
                                <?php echo sizeof($cart_data)?>
                            </span>
                        </p>
                        <p class="referral"></p>
                        <p class="discount">
                        </p>
                        <p>Total: 
                        <span class="cost">
                            $<?php echo $total_cost?>
                        </span>
                        </p>
						<p><a data-toggle="modal" data-target="#myModal" href="" class="" style="padding-left:0px;font-size:15px; margin-bottom:15px;">Have a referral or discount code?</a></p>
                        <a href="<?php echo base_url()?>payment/payinfo.php">
                        <button id="checkoutbutton" class="btn btn-primary" style="width:100%">Proceed to checkout</button></a>
                    </div>
      			 </div>              
            </div>
        </div>
    </div>
</div>

<!-- Continue shopping button above footer -->
<div class="container-fluid">
	<div class="row">
		<div style="background-color:#5371ad" align="center">
        	<a href="<?php echo base_url()?>"><button class="btn btn-Continue" style="width:100%"><h4>Continue Shopping</h4></button></a>
        </div>
	</div>
</div>