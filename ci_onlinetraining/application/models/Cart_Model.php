<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart_Model extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('Prod');
        $this->db_olc = $this->load->database('olc', TRUE);
    }
    
    public function get_cart_data($cart_id){
        $data = $this->db_olc->get_where("cart", array("CookieID" => $cart_id));
        $result = $data->result_array();
        
        if(sizeof($result) > 0){
            $i = 0;
            $array_data = array();
            while($i < sizeof($result)){
                $array_data[$i] = $this->Prod->prod_display($result[$i]["ProdID"]);
                $i++;
            }
            return $array_data;
        }
        else{
            return 0;
        }        
    }
    
    public function total_price($cart_data){
        $i = 0;
        $total_cost = 0;
        while($i < sizeof($cart_data)){
            $total_cost = $total_cost + $cart_data[$i][0]['Price'];
            $i++;
        }
        return $total_cost;
    }
}
?>