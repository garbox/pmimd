<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prod extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->db_olc = $this->load->database('olc', TRUE);
    }
    
    //get info from database about featured prods
    public function featured_prod(){
        $result = $this->db_olc->query('SELECT * FROM selfpaced WHERE Type LIKE "%Top Picks%" AND Display = 1');
        return $result->result_array();
    }
    
    //get info from database about core products. 
    public function core_prod(){
        $result = $this->db_olc->query('SELECT * FROM selfpaced WHERE Type LIKE "%Core%" AND Display = 1');
        return $result->result_array();
    }
    
    //get info from db about certs
    public function cert_prod(){
        $result = $this->db_olc->query('SELECT * FROM selfpaced WHERE Type LIKE "%SP-Certification%" AND Display = 1');
        return $result->result_array();
    }
    
    // get info from db about our services and packages
    public function package_prod(){
        $result = $this->db_olc->query('SELECT * FROM selfpaced WHERE Type LIKE "%Services%" AND Display = 1');
        return $result->result_array();
    }
    
    // display all info for one product 
    // used with view: product_page.php)
    public function prod_display($data){
        $this->db_olc->where("ProdID", $data);
        $result = $this->db_olc->get('selfpaced');
        return $result->result_array();
    }
    
    //display recomened products for said product
    //should get name and info from said products. 
    public function recomended_products($prodID){
        $featured_prod = array();
        $this->db_olc->select("Featured1, Featured2, Featured3");
        $result = $this->db_olc->get_where("selfpaced", array('ProdID' => $prodID));
        $data = (array) $result->row();
        $i = 0;
        $f = 1;
        while($i < sizeof($data)){
            $result = $this->db_olc->get_where("selfpaced", array("ProdID" => $data["Featured".$f]));
            $featured_prod[$f] = $result->result_array();
            $i++;
            $f++;
        }
        return $featured_prod;
    }
    
    //thre are a few classes they are not in the OLC yet. if so, redirect them to differnt location. 
    public function class_not_in_olc_redirect($prodID){
         if($prodID == 132){
            header("Location: http://www.pmimd.com/audio/CEUpkg.asp"); 
         }
          if($prodID == 134){
            header("Location: http://www.pmimd.com/icd10/index.asp");   
         }
          if($prodID == 123){
            header("Location: http://www.pmimd.com/totalaccess");   
         }
    }
}

?>