<?php 
//include files
include '../class.php';
include '../email_template/temps.php';
include '../../../onlinetraining/includes/config.php';

$request_data = $_REQUEST;

class target_emails {
    
    private function SQL_where_statment($dataArray){
        for($x = 0; $x< sizeof($dataArray); $x++){
            if($x < 1){
                $query .= "AF = '".$dataArray[$x]."'"; 
            }
            else{
                $query .= " OR AF = '".$dataArray[$x]."'";
            }

        }
        return $query;
    }

    private function Display_classes($query){
        $conn = pmimain_connect();
        $select = "SELECT * FROM AF_SP_Info WHERE ".$query." GROUP BY Date , CataID";
        $result = $conn->query($select);
            while($data = $result->fetch_assoc()){
                switch ($data['CataID']){
                    case 'CMC':
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Begins:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/certify/pmi-certification-live-classroom-training.asp?show=cmc#schedule' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";  

                        break;
                    case 'CMCE':
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Begins:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/certify/pmi-certification-live-classroom-training.asp?show=cmc#schedule' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";  

                        break;
                    case 'CMIS':
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Begins:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/certify/pmi-certification-live-classroom-training.asp?show=cmis#schedule' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";  

                        break;
                    case 'CMOM':
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Begins:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/certify/pmi-certification-live-classroom-training.asp?show=cmom#schedule' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";  

                        break;
                    case 'CMCO':
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Begins:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/certify/pmi-certification-live-classroom-training.asp?show=cmco#schedule' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";  

                        break;
                    default:
                        $email_data .= "<table border='0' cellpadding='0' cellspacing='0' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody class='mcnTextBlockOuter'><tr><td class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='top'> <!--[if mso]><table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'><tr> <![endif]--><!--[if mso]><td valign='top' width='600' style='width:600px;'> <![endif]--><table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnTextContentContainer' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #999999;font-family: Verdana, Geneva, sans-serif;font-size: 16px;line-height: 150%;text-align: left;' valign='top'><table style='margin-top: 15px;margin-bottom: 15px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td rowspan='2' valign='top' width='45%'><p>".  $data['Name']."</p></td><td colspan='2'><p>Date:&nbsp;".  date('F', strtotime($data['Date'])) .' '. date('j', strtotime($data['Date']))."</p></td></tr><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' valign='middle'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='5' cellspacing='5' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%'><tbody><tr><td align='center' bgcolor='#5371AD' style='padding: 6px 14px 6px 14px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'> <a href='http://www.pmimd.com/programs/".$data['CataID'].".asp' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Live Training</a></td>";                   

                }

               $olc_query ="SELECT * FROM AF_SP_Info WHERE CataID = 'SP-".$data['CataID']."' AND AF NOT LIKE '21%' AND AF NOT LIKE '22%'" ; 
               $result_olc = $conn->query($olc_query); 
                if($result_olc->num_rows>0){ 
                    $result_olc = $result_olc->fetch_assoc();
                    $email_data .="<td style='padding: 2;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>&nbsp;</td><td align='center' bgcolor='#0ca24b' style='padding: 6px 14px 6px 14px;'><a href='http://www.pmimd.com/onlinetraining/productpage/index.php?prodID=".$result_olc['AF']."' style='font-size: 16px; font-family: Verdana, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;' target='_blank'>Online Training</a></td>";

                } 
                else{}
                $email_data .="</tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><!--[if mso]></td> <![endif]--><!--[if mso]></tr></table> <![endif]--></td></tr></tbody></table> ";      
            }

        //return $email_data;
        return $email_data;
    }

    private function insert_campaign_id($campaign_id, $af_array, $start_date, $end_date){
        for($x = 0; $x< sizeof($af_array); $x++){
            if($x < 1){
                $query .= " cdPrgID = '".$af_array[$x]."' AND cdDateToSend BETWEEN '".$start_date."' AND '".$end_date."' "; 
            }
            else{
                $query .= " OR cdPrgID = '".$af_array[$x]."' AND cdDateToSend BETWEEN '".$start_date."' AND '".$end_date."' "; 
            }
        }
        $conn = pmimain_connect();

        $update = "UPDATE AFCollateral_Distribution SET mc_campaign_id = '".$campaign_id."'
        WHERE ".$query."";
        echo $update;
        $conn->query($update);
    }

    function create_target_email(){
        // fwr class 
        $mc_obj = new Mailchimp;
        $template = new email_template;

        // Get data from form, and explode AF data.
        $AF_array = explode(" ",$GLOBALS['request_data']['ActionForm']);
        $city = $GLOBALS['request_data']['city'];
        $new_array = SQL_where_statment($AF_array);
        $class_html = Display_classes($new_array);

        $create_campaign = json_decode($mc_obj->CreateCampiagn($GLOBALS['request_data']), true);
        $get_cam_id = json_decode($mc_obj->getInfo('campaigns/'.$create_campaign['id']), true);
        $html_data = $template->target_email($class_html,$city);
        $put_campaign = json_decode($mc_obj->put_campaign('campaigns/'.$get_cam_id['id'].'/content', $html_data),true);

        //insert campaign ID for tracking of email count. 
        insert_campaign_id($get_cam_id['id'], $AF_array, $GLOBALS['request_data']['start_date'], $GLOBALS['request_data']['end_date']);

        echo $html_data; 
    }
}

class webinar_email{
    private function get_webianrs(){
        $conn = pmimain_connect();
        
        $select = "SELECT * FROM webinar
                    WHERE  (date > CURRENT_DATE()+3) AND (program_code != 'A-TA')
                    GROUP BY AF
                    ORDER BY date ASC LIMIT 3";
        $query = $conn->query($select);
        
        return $query->fetch_all($resulttype = MYSQLI_ASSOC);
    }
    
    public function create_webinar_campaign(){
        $mc_obj = new Mailchimp;
        $template = new email_template;
        $class_obj = new webinar_email;
        
        //create and assign vars
        $obj = $class_obj->get_webianrs();
        $featured = (object)$obj[0];
        $first_listing = (object)$obj[1];
        $second_listing = (object)$obj[2];
        
        $create_campaign = json_decode($mc_obj->webinar_Campiagn($GLOBALS['request_data']), true);
        $get_cam_id = json_decode($mc_obj->getInfo('campaigns/'.$create_campaign['id']), true);
        $html_data = $template->webinar_email($featured, $first_listing, $second_listing);
        $put_campaign = json_decode($mc_obj->put_campaign('campaigns/'.$get_cam_id['id'].'/content', $html_data),true);
        
        echo $html_data;
    }
    
}

class regional_email{
    
}

switch($request_data['TemplateID']){
    //Target template
    case '75861':
        echo "Target Tempalte";
        break;
        
    //Webinar template    
    case '75809':
        //create objects 
        $class_webinar = new webinar_email();
        $class_webinar->create_webinar_campaign();
        
        break;
        
    //Regional Emails    
    case '73545':
        echo "Regional Template";
        break;
        
    // invalid template ID    
    default:
        echo '<h4 style="color:white; padding:10px;" class="bg-danger" align="center">Invalid template found</h4>';
}


?>
