<?php
class Mailchimp{
	//Connection for the API, Paramters Needed, APIkey, URL, Request as POST PUT GET etc....)
	private function APIConnection($url, $Request, $data=null){

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . 'API KEY');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// Results
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		return $result;
	}

	// List ID c843b5346c SP-Survery
	// List ID b68b12eba5 PMCC List
	// List ID 72bade8760 MLM List
	public function MCAddMember($listId) {

		$memberID = md5(strtolower($_SESSION['Profile']['email']));
		$url = 'https://us12.api.mailchimp.com/3.0/lists/' . $listId . '/members/' .$memberID;

		$json = json_encode([
			'email_address' => $_SESSION['Profile']['email'],
			'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
			'merge_fields'  => [
				'FNAME'     => $_SESSION['Profile']['fname'],
				'LNAME'     => $_SESSION['Profile']['lname'],
				'ZIP' 		=> $_SESSION['Profile']['zip']
			]
		]);

		// Connection for API.
		$APIConnect = new Mailchimp();
		$APIConnect->APIConnection($url, "PUT", $json);
	}

    public function MCEcommerce($StoreID, $OrderID, $TotalSale) {

            $memberID = "4548cef640e47fed66ff5b97680a45dc";
            $campignID = "2ac9af9ed7";
            $url = 'https://us12.api.mailchimp.com/3.0/ecommerce/stores/'.$StoreID.'/orders';

            $json = json_encode([
                  'id' => $OrderID,
                  'customer' => [
                    'id' => $memberID
                  ],
                  'campaign_id' => $campignID,
                  'checkout_url' => 'https://www.pmimd.com/onlinetraining/payment/confirmation.php',
                  'currency_code' => 'USD',
                  'order_total' => $TotalSale,
            ]);
            // Connection for API.
            $APIConnect = new Mailchimp();
            echo $APIConnect->APIConnection($apiKey, $url, "PUT", $json);

	}

    public function CreateCampiagn($data){
		$url = 'https://us12.api.mailchimp.com/3.0/campaigns/';

		$json = json_encode([
            'type'=> 'regular',
            'recipients'=> [
                'list_id'=> 'cc2535b0e1',
                'segment_opts'=>[
                    'saved_segment_id'=> (int)$data['SegmentID']
                ]
            ],
            'settings'=> [
                'subject_line'=> $data['EmailSubject'],
                'title'=> $data['CampaignName'],
                'from_name'=> $data['FromName'],
                'reply_to'=> $data['FromEmail'],
                'inline_css'=> true,
                'folder_id'=>$data['FolderID'],
            ],
            'tracking'=> [
                'opens'=> true,
                'html_clicks'=> true,
            ],
        ]);
		// Connection for API.
		$APIConnect = new Mailchimp();
		return $APIConnect->APIConnection($url, "POST", $json);
    }

    public function getInfo($folder){
        $url = 'https://us12.api.mailchimp.com/3.0/'.$folder;
        $APIConnect = new Mailchimp();
		return $APIConnect->APIConnection_no_data($url, "GET");
    }

    public function put_campaign($folder, $html_data){
        $url = 'https://us12.api.mailchimp.com/3.0/'.$folder;

        $json = json_encode([
            'html'=> $html_data,
        ]);


        $APIConnect = new Mailchimp();
		return $APIConnect->APIConnection($url, "PUT", $json);
    }
}
?>
