<?php 
include 'mailchimp_class.php';

$mc_obj = new Mailchimp;
$getfolders = json_decode($mc_obj->getInfo('campaign-folders?count=25'));
$getList = json_decode($mc_obj->getInfo('lists/cc2535b0e1/segments?since_created_at=2015-01-01&count=54'));
//$getTemp = json_decode($mc_obj->getInfo('templates?count=15&folder_id=15bc3c66de'),true);
?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Cache-control" content="public">
  <link rel="stylesheet" href="https://10.0.0.15/onlinetraining/assets/css/bootstrap.css">
  <link rel="stylesheet" href="https://10.0.0.15/onlinetraining/assets/css/style.css">
  
<title>Create Campaign</title>

</head>

<body>
<div align="center" class="wait" style="height:50px">
    <img class='center-block'src='https://www.pmimd.com/totalaccess/img/Test.gif'>
</div>
<form id="createcampign" name="createcampign" method="post" action="scripts/createcampign.php">
    <div class="container-fluid" style="margin-top: 30px;">
        <div class="row">

            <div class="col-sm-4">
                <lable>Folder Selection</lable>
                <select class="form-control" id="folder_id" name="FolderID">
                        <?php
                        $folder_data = (object)$getfolders->folders;
                        foreach($folder_data as $data){
                            echo "<option value='".$data->id."'>".$data->name;
                        }
                        ?>
                    </select>
            </div>

            <div class="col-sm-4" id="segments">
                <lable>Segment Selection</lable>
                <select class="form-control" name="SegmentID" id="SegmentID">
                    <?php
                    $data = $getList->segments;
                    for($x = 0; $x <sizeof($data); $x++){
                        $seg_data = (object)$data[$x];
                        echo "<option value='".$seg_data->id."'>".$seg_data->name." | Member Count: ". $seg_data->member_count;
                    }
                    ?>   
                </select> 
            </div>

          <div class="col-sm-4">
                <lable>Template Selection</lable>
                <select class="form-control" id="TemplateID" name="TemplateID">
                        <option value='75861'>
                            Targeted Template
                        </option>
                       <option value='75809'>
                            Live Webinar Template
                        </option>
<!--                    <option value='75793'>
                            Single Certification Email
                        </option>
                        <option value='73545'>
                            Regionals
                        </option>-->

                </select>
            </div>   

        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
               <div class="form-group">
                   <lable>Email Subject</lable>
                 <input type="text" name="EmailSubject" class="form-control" placeholder="Email Subject">
               </div>
               <div class="form-group">
                   <lable>From Email</lable>
                  <input type="text" name="FromEmail" class="form-control" placeholder="From Email Address"> 
               </div>
                <div class="form-group">
                    <lable>From Name</lable>
                    <input type="text" name="FromName" class="form-control" placeholder="From Name">   
                </div>
                <div class="form-group">
                    <lable>Campaign Name</lable>
                    <input type="text" name="CampaignName" class="form-control" placeholder="Campaign Name (City/Class Codes/Date)">   
                </div>
                <div class="form-group" id="City">
                    <lable>City</lable>
                    <input class="form-control" type="text" name="city", placeholder="City Name/s">
                </div>
            </div>
            <div class="col-sm-4" id="AFs">
               <div class="form-group">
                   <lable>Action Forms</lable>
                 <input type="text" name="ActionForm" class="form-control" placeholder="Action Form">
               </div>
            </div>
            <div class="col-sm-4 no_padding" id="Date">
                <div class="col-sm-6">
                    <label for="">Start Date</label>
                    <input class="form-control" type="date" placeholder="Start Date" name="start_date">
                </div>
                <div class="col-sm-6">
                    <label for="">End Date</label>
                    <input class="form-control" type="date" placeholder="End Date" name="end_date">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <button style="width: 100%" class="btn btn-primary" type="submit">Create Campaign</button>
            </div>
        </div>
    </div>
</form>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        <h3>HTML Email</h3>
        <hr>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://www.pmimd.com/onlinetraining/assets/js/bootstrap.min.js"></script>

<script>
$('.wait').hide();
$('#createcampign').on('submit', function(e){
    $('.wait').show();
    $('#createcampign').hide();
    $('#rawhtml').hide().empty();
    var values = $(this).serialize();
    // get value of action attribute   
    var desination = $(this).prop('action');   		
    // get current location url			
    
    // prevent page from leaving to desination
    e.preventDefault();	
    $.ajax({
          url: desination,
          type: 'post',
          data: values, 
          success: function(data){
              e.preventDefault();
              $('.wait').hide();
              $('#createcampign').show()
              $('#rawhtml').show().html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
    });

});
    
$('select#TemplateID').change(function() {
    
    switch($('select#TemplateID').val()){
        case '73545':
            $('#AFs').hide();
            $('#Date').hide();
            $('#City').hide();
            $('#segments').hide();
            $('#segments').hide();
            break;
        
        //webinars
        case '75809':
            $('#AFs').hide();
            $('#Date').hide();
            $('#City').hide();
            $('#segments').hide();
            $('select#folder_id').val('14da509e82');
            
            break;
            
        default:
            $('#AFs').show();
            $('#Date').show();
            $('#City').show();
            $('#segments').show();
            $('select#folder_id').val('f4233679f0');
    }
});
</script>
</body>
        <div id="rawhtml">
            
        </div>
</html>