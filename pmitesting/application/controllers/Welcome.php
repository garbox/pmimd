<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Welcome extends CI_Controller
{    
        function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("user_model");
        $this->load->model("quiz_model");
        $this->lang->load('basic', $this->config->item('language'));
        if ($this->db->database == '') {
            redirect('install');
        }
    }
    function index(){
        //$this->load->view('header');
        $this->load->view('welcome');
    }   
}