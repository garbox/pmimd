<?php
Class Notification_model extends CI_Model{
    
    function notification_list($limit){
	  $logged_in=$this->session->userdata('logged_in');
		
	  if($this->input->post('search')){
	 $this->db->or_like('savsoft_notification.nid',$this->input->post('search'));
	 $this->db->or_like('savsoft_notification.title',$this->input->post('search'));
	 $this->db->or_like('savsoft_notification.message',$this->input->post('search'));
	 $this->db->or_like('savsoft_notification.notification_date',$this->input->post('search'));
	 $this->db->or_like('savsoft_notification.click_action',$this->input->post('search'));
	 $this->db->or_like('savsoft_notification.notification_to',$this->input->post('search'));
	  }
	  if($logged_in['su'] == '0'){
	  $uid=$logged_in['uid'];
	  $this->db->or_where('savsoft_notification.uid',$uid);
	$this->db->or_where('savsoft_notification.uid','0');
		
	  }
	  $this->db->join('savsoft_users','savsoft_users.uid=savsoft_notification.uid','left');
		$this->db->limit($this->config->item('number_of_rows'),$limit);
		$this->db->order_by('nid','desc');
		$query=$this->db->get('savsoft_notification');
		return $query->result_array();
		
	 
 }
 
    function insert_notification(){
         $uid = $this->input->post('uid');
         $this->db->where('uid', $uid);
         $user_account = $this->db->get('savsoft_users');
         $user_account = $user_account->result_array();
        
        $userdata=array(
         'title'=>$this->input->post('title'),
         'message'=>$this->input->post('message'),
         'click_action'=>$this->input->post('click_action'),
         'user_id'=>$uid,
         'email'=>$user_account[0]['email'],
         'name'=>$user_account[0]['first_name'] . " " . $user_account[0]['last_name'],
         );
        
         $this->db->insert('savsoft_notification',$userdata);
         return $userdata;
 }
    
    function mandrill_api_send($email_info){
       	//Mandrill Send-Template API
        print_r($email_info);
	try {
		require_once $_SERVER['DOCUMENT_ROOT'] .'/pmitesting/mandrill-api-php/src/Mandrill.php';
		$mandrill = new Mandrill('CCJfQw9gdBtLU_gXZ2a9LQ'); 
		$template_name = 'general-email-template';
		 //content area, name = mcedit in main source code
		 //content  = whats going in that container.
		$template_content = array(
			array(
				'name' => 'message',
				'content' => $email_info['message']
			),
            array(
				'name' => 'name',
				'content' => $email_info['name']
			),
            array(
				'name' => 'button_link',
				'content' => $email_info['click_action']
			),
										
		);
		$message = array(
			'html' => '<p>PMI Testing Notification</p>',
			'text' => 'Example text content',
			'subject' => $email_info['title'],
			'from_email' => 'webmaster@pmimd.net',
			'from_name' => 'PMI Online Testing',
			'to' => array(
				array(
					'email' => $email_info['email'], //email address
					'name' => $email_info['first_name']. " " . $email_info['last_name'],  //name of client
					'type' => 'to'
				),				
			),
			'important' => false,
			'track_opens' => true,
			'track_clicks' => true,
			'auto_text' => null,
			'auto_html' => null,
			'inline_css' => true,
			'url_strip_qs' => null,
			'preserve_recipients' => null,
			'view_content_link' => null,
			'bcc_address' => '', // bcc email address
			'tracking_domain' => null,
			'signing_domain' => null,
			'return_path_domain' => null,
			'merge' => true,
			'merge_language' => 'mailchimp',
			'global_merge_vars' => array(
				array(
					'name' => 'merge1',
					'content' => 'merge1 content'
				)
			),
			// Image needed for tracking opens
			'images' => array(
				array(
					'type' => 'image/png',
					'name' => 'IMAGECID',
					'content' => 'ZXhhbXBsZSBmaWxl'
				)
			)
		);
		//async is for bulk sending.
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = '2015-01-01';
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
		
	} 
	catch(Mandrill_Error $e) {
		 //Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id ''
		throw $e;
	}
    }
}
?>
