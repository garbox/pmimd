<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Medical Coding Assesment Test</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.pmimd.com/onlinetraining/assets/css/bootstrap.css">
  <link rel="stylesheet" href="https://www.pmimd.com/onlinetraining/assets/css/style.css">
  <meta property="og:title" content="Test your knowledge now" />
  <meta property="og:image" content="" />
  <meta property="og:description" content=""/>

<style>
    body, html{
        overflow-x: hidden;
    }
    .pmiBlack{
	background-color:#414549;
	height:20px;
    }
    .pmiBlue{
        background-color:#5371ad;
        height:20px;
    }
    .pmiGreen{
        background-color:#0ca24b;
        height:20px;
    }
    .pmiRed{
        background-color:#c01313;
        height:20px;	
    }
    .container-margin{
        margin-top: 30px;
    }   
    h1,h2,h3,h4,h5,h6{
        color:#5371ad;
    }
    a{
        font-size: 15px;
    }
    a.carousel-control:hover{
        background-color: #e6e6e6;
    }
    a.carousel-control{
        width: 70px;
    }
    
</style>
</head>
		        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
          
          ga('create', 'UA-73530823-1', 'auto');
          ga('send', 'pageview');  
        </script>
<body>
<!-- Strip -->
<div class="contianer-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlack"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiBlue"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiGreen"></div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pmiRed"></div>
    </div>
</div>

<!-- Header -->
<div class="container-fluid" style="background-color:#5371ad;">
    <div class="row">
        <div class="container">
            <div class="row-fluid">
                <div class="col-lg-12 center-block" align="center">
                    <img class="img-fluid" src="https://www.pmimd.com/img/Medical Coding Assessment Headerr.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container container-margin">
    <div class="row">
        <div class="col xs-12">
                <div align="center">
            <!--<img src="http://www.pmimd.com/images/pmilogo.gif" alt="PMI Logo">-->
            <h2>Medical Coding Assessment</h2>
        </div>
        </div>
        <hr>
        <div class="col-md-4">
            <div class="col-xs-12" style="padding:10px; border:1px solid #e6e6e6" >
                <h3 align="center">Hear from a Participant</h3>
                <div align="center"><button class="btn btn-primary" data-toggle="modal" data-target="#Participant">Read Me</button></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-xs-12" style="padding:10px; border:1px solid #e6e6e6" >
                <h3 align="center">Hear from a Host</h3>
                <div align="center"><button class="btn btn-primary" data-toggle="modal" data-target="#Host">Read Me</button></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-xs-12" style="padding:10px; border:1px solid #e6e6e6" >
                <h3 align="center">Hear from a Manager</h3>
                <div align="center"><button class="btn btn-primary" data-toggle="modal" data-target="#Manager">Read Me</button></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
       <div class="col-lg-12">
            <h3>What is it?</h3>
            <p>The Medical Coding Assessment is a tool developed by the faculty team at Practice Management Institute (PMI) to measure whether a person is a good candidate to enroll in the Certified Medical Coder (CMC) certification program. The assessment contains 20 questions and provides a grade and rationale for areas where a candidate may need to complete additional training prior to their first CMC class.
            <br><br>
            If you want to earn the CMC credential, this is the place to start! PMI’s Medical Coding Assessment eliminates the guesswork by measuring your knowledge in four different areas of coding competency. 
            </p><br>
            <h3>Why we created it.</h3>
            <p>PMI recommends that CMC certification candidates have at least a year of coding experience prior to enrollment in the course. Recently, more of our instructors have observed participants that have a hard time keeping up with the classroom lecture and materials often lack baseline coding knowledge.  Test scores and classroom observation indicate that more students are struggling with some of the more advanced instruction and homework. By day 2 of the program, their eyes would gloss over and they realized they were in over their heads. They realized it was too late to catch up and they would end up dropping out or doing poorly on the exam.
            <br><br>
            In an effort to set people up for success, PMI’s faculty team went to work designing a comprehensive assessment tool that could measure a candidate’s familiarity with ICD-10-CM, CPT®, E/M, HCPCS coding and modifiers. The assessment included a built-in scoring feature to indicate areas of weakness and guide them to online courses to fortify their knowledge prior to the first CMC class. 
            </p><br>
<hr>
    </div>
    </div>
    </div>
</div>
<div class="container container-margin">
    <div class="row">
        <div class="col-xs-12">
            <a href="https://pmimd.com/pmitesting/index.php/login"><button class="btn btn-primary btn-lg" style="border-radius: 0px; width: 100%">Take the Assessment</button></a>       
        </div>
    </div>
</div>

<div class="container-fluid container-margin" style="background-color:#414549; color:white; padding-top:25px; padding-bottom:25px;">
	<div class="container">
    	<div class="row footer" align="left">
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a href="http://pmimd.com/certify/">Certification</a></p>
                  <a class="footiem" href="https://pmimd.com/certify/certified-medical-coder-certification.asp">Coding</a><br>
                  <a class="footiem"  href="https://pmimd.com/certify/certified-medical-insurance-specialist-certification.asp">Billing</a><br>
                  <a class="footiem"  href="https://pmimd.com/certify/certified-medical-office-manager-certification.asp">Management</a><br>
                  <a class="footiem"  href="https://pmimd.com/certify/certified-medical-compliance-officer-certification.asp">Compliance</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a href="https://www.pmimd.com/programs/default.asp">Curriculum</a></p>
                  <a class="footiem"  href="https://www.pmimd.com/programs/default.asp">Learning Topics</a><br>
                  <a  class="footiem" href="https://www.pmimd.com/programs/locator.asp">Class Locator</a><br>
                  <a class="footiem"  href="https://pmimd.com/DLCON">National Conference</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a>Meet the Team</a></p>
                  <a class="footiem"  href="https://www.pmimd.com/host/default.asp">Host PMI Training</a><br>
                  <a class="footiem"  href="https://www.pmimd.com/testimonials.asp">Success Stories</a><br>
                  <a class="footiem"  href="https://www.pmimd.com/policies.asp">Policies</a><br>
                  <a class="footiem"  href="https://www.pmimd.com/contact.asp">Contact PMI</a><br>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 footerMenu" style="min-height:150px">
                  <p class="MainFont"><a>Resources</a></p>
                  <a class="footiem"  href="https://www.pmimd.com/certify/pmi-certification-renewal.asp">Certification Renewal</a><br>
                  <a class="footiem"  href="https://www.pmimd.com/audio/totalaccess.asp">LMS Subscription</a><br>
                  <a class="footiem"  href="https://www.pmimd.com/certify/ceutracker.asp">CEU Tracker</a><br>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footerMenu" style="padding-top:15px;">
          <a href="http://www.pmimd.com"><img class="center-block " src="https://www.pmimd.com/images/pmilogowhite.png" width="50%" alt=""></a>
          </div>         
      </div>
    </div>
	<div class="container">
    	<div class="row footer" align="center">
        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span style="color:#999999; font-size:12px"><br>
                </span><p><span style="color:#999999; font-size:12px">CPT® is a registered trademark of the American Medical Association. All rights reserved.<br>
                Reprinting, publication or use without the express written consent of Practice Management Institute is prohibited<br>
                ©2017 Practice Management Institute® |  All rights reserved.</span></p>
            </div>
        </div>
    </div>
</div> 
</body>
    <!-- Modal -->
<div class="modal fade" id="Manager" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Libby Purser, CHI, CMC, CMIS, CMOM, CPC, CRC<br>Billing Supervisor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                  <p>Libby Purser is a health information management supervisor for a 55-physician multi-specialty provider network in north Texas. She has worked in medical billing for more than 20 years on the provider and payer sides of the fence. She is a medical coding and billing instructor for PMI and at the University of North Texas. In 2014, she earned the Practice Management Institute Outstanding National Service Award for her role in training and credentialing more than 300 coding and billing professionals in the Dallas area. </p>
                  <p>A coder can make or break practice. If you want money coming in the door, you get a good coder that understands the ins and outs of it and that practice will thrive. The biggest reason for having competent coding staff is on revenue of the practice, but if an auditor finds problems, the impact could be much greater.</p>
                  <p>Coding proficiency and ongoing training is important. Keeping up with current rules is not a one-and-done proposition. Coders need to attend annual training updates to stay on top of the changes that can occur as often as quarterly.  </p>
                  <p>The more training and experience that a coder has, the more knowledge they have. Our organization does not hire coders who are not certified, and more of our coding positons now require auditing as well. A clean claim should be paid in about 15 days.  If a claim is denied, it could take anywhere from 30-120 days to get it paid.  That in itself is incentive to have proper training for staff. Some large organizations expect coders to be able to audit the entire chart that day. When coders are new, they don’t necessarily have the skills yet to do that yet.</p>
                  <p>As a manager of a coding and billing team, I feel very fortunate to have a great team that works well together. I can fill in when needed and I want to learn just as they do. Although my administrative duties take precedent, I still enjoy coding. I could do it all day. Being a manager makes you responsible for so much more.</p>
                  <p>I always offer the advice that if a person really aspires to be more they should look to improve their skills and continue their education. Without that, you are limiting yourself. Stay on top of what is new in the field.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
<!-- Modal -->
<div class="modal fade" id="Host" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ronaele King <br> Program Coordinator Monroe County Medical Society</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                  <p>Tammy Cooper’s path the CMC certification was possible thanks to the Monroe County Medical Society, during one of two CMC classes that they hosted in the fall 2017. PMI shared her inspiring story with Ronaele King, program coordinator at MCMS who shared her perspective on providing the CMC and other continuing education classes for doctor’s offices in Rochester, NY. Ronaele shared her own experience as a client serving the continuing education needs of medical offices in their community.</p>
                  <p>Our medical society used to offer the CMC once a year but now that the demand has been growing, we are planning to increase it to twice annually in addition to the half-and full-day continuing education classes for our membership throughout the year.</p>
                  <p>The room here at the medical society seats 24 but we decided to limit the classes to 12 because for the CMC, the participants needed to have their coding books and the manuals for this class. But this last time we filled the class of 12 and then opened a second class and that filled up, too.</p>
                  <p>There is a huge need for it and we are now going to offer it in the spring and fall for 2018. We have seen more demand in all the classes, especially the CMC class this fall. We got some great feedback. Some of the participants told me that they were using codes that they had never used before. That was in the second or third week of the class. I am very impressed with Pam Joslin, PMI Faculty/Consultant, who taught the classes. Pam explains the concepts well and understands the dynamics of a classroom setting. I think that the in-class experience is a big benefit of attending PMI classes.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
<!-- Modal -->
<div class="modal fade" id="Participant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tammy Cooper, CMC<br>Certified Medical Coder</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                  <p>Tammy is a newly-certified coding professional employed in the Rochester area. She shares her experience climbing the ranks in healthcare administration, and how PMI’s Medical Coding Assessment helped her gain the confidence to become a Certified Medical Coder.</p>
                  <p>I think the medical billing field chose me instead of me choosing it. I started as a per diem evening receptionist after my husband urged me to get out of the house and use my brain instead of spending all day every day with our two toddler daughters as a stay at home mom.  </p>
                  <p>My journey moved quickly form working 10 hours a week to 19 hours per week with added job assignments then to 32 hours a week and within 9 months I was promoted to a full time Accounts Receivable Coordinator for an addictions facility with 2 halfway houses, 5 clinics and supportive living in several counties.  I was responsible for the entire outpatient billing for all of those sites as well as becoming the backup person to the Director of Finance. Working at that position gave a sense of purpose to my life by helping people I felt passionate about.  </p>
                  <p>To this day, I still remember a patient coming to me in tears to thank me. I asked why, and she told me I had helped her learn to be independent again by teaching her how to pay her bills.  </p>
                  <p>I went back to being a stay at home mom for a short period, and then took a billing coordinator job for a physical therapy/mental health facility that brought me on as a consultant to trim the fat as well as transition them through a software conversion.  The department went from six to three, working more efficiently. Staff in other departments congratulated me for “tearing down walls and building bridges” as the billing department was not been permitted to communicate with other departments, only to enter the data they were given. Within a few short weeks, I had literally torn down the cubicle walls in the small billing office to bridge the gap between providers and billers. When the software conversion was completed, I moved on to hospital billing but it was a chaotic corporate position and wasn’t where my heart was. I returned to my billing roots in not-for-profits.</p>
                  <p>In the midst of these career positions, I have completed my associate’s degree in Accounting and my bachelor’s degree in Business. I’m currently working at a not-for-profit organization that helps the migrant population in most of New York State. I am part of, in my opinion, the best team – people who genuinely care about me as a coworker and as a person. One of those supporting people is my supervisor, Jackie.  Jackie has taken the Certified Medical Coding course through PMI in the past.  We had talked about me potentially taking this course as our organization needed another Coder.  I was reluctant because although I’ve been in the business for 20 years, I wasn’t confident that my “old brain” could handle taking the course and take a SIX  HOUR exam!!!  Jackie forwarded an email she had received from PMI with the Medical Coding Assessment attached. She encouraged me to take it because she said she knew I could do well with the course. I took the assessment and did well on it.  I was shocked! She just grinned and said, “I told you could do it!”  With her confidence and encouragement, I went to our manager and requested tuition assistance through our employer to allow me to take the course.  </p>
                  <p>That led to me meeting Pam, our CMC Instructor. What an amazing experience that was!!  She was knowledgeable, bright, fun loving, quirky and light hearted which made the course enjoyable even through all of the stress. The course in itself is quite intimidating as it’s truly only 4 classes, one week off and then test day in total.  The materials covered in the course were intensive and challenging. Fortunately for me, I had a huge support system behind me that provided daily encouragement especially whenever I thought I couldn’t succeed.  That AMAZING team of women I work with asked me daily how it was going and whenever I felt discouraged they were there with encouraging words or a hug whichever I needed at the moment.  </p>
                  <p>Exam day came and went and the next 4 weeks and 3 days were GRUELING!!!  I crazily decided to go back to work after completing the exam.  Jackie asked me how it went and I said I honestly have no idea! I was mentally exhausted from the preparation and taking the exam.  She once again, told me not to worry because she was positive I had passed.  Daily, I called home to have someone check the mail and finally the day that THE mail came … my daughter called and asked if I was coming home to get the mail.  I couldn’t figure out why, she then solemnly said you have your results. I asked if it a tube or an envelope.  (Pam had said a tube meant pass and an envelope meant fail!) Thankfully, it was a tube, she giggled and I about cried because that meant I PASSED!!! I practically ran to Jackie’s office to say I PASSED!! She had happy tears in her eyes with and for me – we were both so excited.  Then I told our team and they too shared my excitement.</p>
                  <p>My experience with PMI has been extraordinary. I’m grateful to Pam for having patience in the classroom and preparing me for exam day.  I’m beyond grateful to the team of amazing women I get to share my work life with and most of all I’m grateful to my VERY insightful supervisor.  Since taking the course, I’m more confident in my ability to find each patient’s story in the provider’s note and that leads making me a better resource for our organization.  I’m looking forward to my next step on this journey.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://www.pmimd.com/onlinetraining/assets/js/bootstrap.js"></script>
<script>
    $("a.carousel-control").mouseover(function(){
        $('span.icon-next').css("color", "#414549");   
        $('span.icon-prev').css("color", "#414549");
        $('.icon-next').css("font-size", "30px");   
        $('.icon-prev').css("font-size", "30px");
                              
    });
    $("a.carousel-control").mouseleave(function(){
        $('span.icon-next').css("color", "#fff");   
        $('span.icon-prev').css("color", "$fff");
        $('.icon-next').css("font-size", "0px");   
        $('.icon-prev').css("font-size", "0px");
                              
    });
</script>
</html>